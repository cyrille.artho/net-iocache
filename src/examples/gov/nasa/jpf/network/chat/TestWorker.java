//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.chat;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;
import java.net.*;

public class TestWorker extends TestCase {
    CoupledLoopBackSocket lbs = new CoupledLoopBackSocket();
    Worker worker;
    ChatServerForTest server;

    public void setUp() {
        server = new ChatServerForTest();
        worker = new Worker(0, lbs, server);
        server.setWorker(worker);
    }

    public void testWorker() {
        String testString = "TeStStRiNg";
        String stringRead = null;

        Thread w = new Thread(worker);
        w.start();

        BufferedReader in = new BufferedReader(new InputStreamReader(lbs.getInputStreamExternal()));
        PrintWriter out = new PrintWriter(lbs.getOutputStreamExternal(), true);
        out.println(testString);
        try {
            if (in.ready()) {
                stringRead = in.readLine();
                assertTrue(stringRead.endsWith(testString));
            }
            out.flush();
            lbs.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        try {
            lbs.close();
            w.join();
        } catch (InterruptedException e) {
        }
    }

    public static void main(String[] args) {
        junit.textui.TestRunner.run(new TestSuite(TestWorker.class));
    }
}

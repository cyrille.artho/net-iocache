//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package simpleLiveness;

import gov.nasa.jpf.vm.Verify;

public class kadai3new {
    public static void main(String[] args) {
	ServerSocket server = new ServerSocket();

	//new ToyHTTPServer(server).start();
	new ThreadPoolServer(server, 3).start();
	//new ThreadPoolServer(server, 1).start();
	//new ToyHTTPClient().start();
	new MultiThreadClient().start();
	// System.out.println("main end");
    }
}

/*
  Simple Queue

  Queues can be closed.
  Reading from a closed queue returns -1.
  Writing to a closed queue does nothing (modified).
*/

class SimpleQueue {
    final int L = 2;

    int[] q = new int[L];
    int p = 0;
    boolean closed = false;  // added
    
    synchronized void close()  // added
    {
	closed = true;
	notify();
    }

    synchronized void put(int x)
    {
	try {
	    while (p >= L && !closed)  // if -> while, !closed added
		wait();
	    if (closed) return;  // added (should throw interrupt)
	    q[p++] = x;
	    if (p == 1)
		notify();
	}
	catch (InterruptedException e) {
	}
    }
    
    synchronized int get()
    {
	try {
	    while (p == 0 && !closed)  // if -> while, !closed added
		wait();
	    if (p == 0 && closed) return -1;  // added
	    int x = q[0];
	    for (int i = 1;  i < p;  i++)
		q[i-1] = q[i];
	    p--;
	    if (p == L-1)
		notify();
	    return x;
	}
	catch (InterruptedException e) {
	    return 0;
	}
    }
}

/*
  Input Stream and Output Stream

  Output streams can be closed.
  Input streams can also be closed (added).
*/

class InputStream {
    SimpleQueue q;

    synchronized int read() {
	if (q == null)
	    return -1;
	return q.get();
    }

    synchronized void close() {  // added
	q.close();
    }

    InputStream(SimpleQueue q) {
	this.q = q;
    }
}

class OutputStream {
    SimpleQueue q;

    synchronized void write(int b) {
	q.put(b);
    }

    synchronized void close() {
	q.close();
    }

    OutputStream(SimpleQueue q) {
	this.q = q;
    }
}

/*
  Mock Socket

  Sockets and server sockets are defined.
  Closing a socket closes its output stream.
  The input stream is also closed (added).
*/

class Socket {
    InputStream is;
    OutputStream os;

    synchronized InputStream getInputStream() { return is; }
    synchronized OutputStream getOutputStream() { return os; }
    
    Socket(SimpleQueue iq, SimpleQueue oq) {
	is = new InputStream(iq);
	os = new OutputStream(oq);
    }

    Socket() {
	Socket s = ServerSocket.theServerSocket.connect();
	this.is = s.is;
	this.os = s.os;
    }

    synchronized void close() {
	is.close();
	os.close();
    }
}

class ServerSocket {

    static ServerSocket theServerSocket;

    ServerSocket() {
	theServerSocket = this;
    }

    Socket server;
    Socket client;

    boolean waitConnect = false;
    boolean closed = false;

    synchronized Socket accept() {
	if (closed) return null;  // added: return null when closed
	try {
	    SimpleQueue iq = new SimpleQueue();
	    SimpleQueue oq = new SimpleQueue();
	    server = new Socket(iq, oq);
	    client = new Socket(oq, iq);
	    waitConnect = true;
	    notify();  // added after model checking by jpf
	    wait();
	    waitConnect = false;
	    if (closed) return null;  // added: return null when closed
	    return server;
	}
	catch (InterruptedException e) {
	    return null;
	}
    }

    synchronized Socket connect() {
	try {
	    while (!waitConnect)
		wait();
	    notify();
	    return client;
	}
	catch (InterruptedException e) {
	    return null;
	}
    }

    synchronized void close() {  // added
	closed = true;
	notify();
    }
}

/*
  Toy HTTP Client
*/

class ToyHTTPClient extends Thread {
    int file = 'a';
    public synchronized void run() {
	Socket s = new Socket();
	InputStream is = s.getInputStream();
	OutputStream os = s.getOutputStream();
	os.write('g'); 	os.write(file);  os.write('0');  os.write('$');
	int n = is.read() - '0';
	int[] buffer = new int[n];
	for (int i = 0;  i < n;  i++) {
	    buffer[i] = is.read();
	}
	FileSystem.verify(file, buffer);
	s.close();
	success();
	ServerSocket.theServerSocket.close();
	// System.out.println("client end");
    }

    private static void success() {
    }
}

/*
  File System
*/

class FileSystem {
    static final int[][] file = {
	//{'D', 'e'},
	{'D', 'e', 'c'},
	{'D', 'e', 'c', 'e', 'm', 'b', 'e', 'r'},
	{ '2', '0', '0', '7'}};
    
    synchronized static int length(int fn) {
	return file[fn-'a'].length;
    }

    synchronized static int get(int fn, int i) {
	return file[fn-'a'][i];
    }

    synchronized static void verify(int fn, int[] buffer) {
	assert file[fn-'a'].length == buffer.length;
	for (int i = 0;  i < file[fn-'a'].length;  i++)
	    assert file[fn-'a'][i] == buffer[i];
    }
}

/*
  Toy HTTP Server
*/

class ToyHTTPServerThread extends Thread {
    ToyHTTPServer t;
    Socket s;

    ToyHTTPServerThread(ToyHTTPServer t, Socket s) {
	this.t = t;
	this.s = s;
    }

    public synchronized void run() {
	t.serveClient(s);
    }
}

class ToyHTTPServer extends Thread {
    ServerSocket server;

    ToyHTTPServer(ServerSocket server) {
	this.server = server;
    }

    public synchronized void run() {
	// System.out.println("server start");
	while (true) {
	    Socket s = server.accept();
	    if (s == null) break;
	    new ToyHTTPServerThread(this, s).start();
	}
	// System.out.println("server end");
    }

    void serveClient(Socket s) {
	// System.out.println("serveClient start");
	InputStream is = s.getInputStream();
	OutputStream os = s.getOutputStream();

	int c;
	while ((c = is.read()) != -1) {
	    switch (c) {
	    case 'g':
		int fn = is.read();
		int from = is.read();
		int to = is.read();
		if (to == '$') to = FileSystem.length(fn) + '0';
		// modified: return the number of bytes to be sent
		os.write(to - from + '0');
		for (int i = from - '0';  i < to - '0';  i++)
		    os.write(FileSystem.get(fn, i));
	    }
	}
	// System.out.println("serveClient end");
    }
}

/*
  Thread Pool Server
*/

class ThreadPoolServer extends ToyHTTPServer {

    ThreadPoolServerThread[] pool;
    boolean[] working;

    ThreadPoolServer(ServerSocket server, int n) {
	super(server);
	pool = new ThreadPoolServerThread[n];
	working = new boolean[n];

	for (int i = 0; i < n; i++) {
	    pool[i] = new ThreadPoolServerThread(this, i);
	    working[i] = false;
	}
    }

    public void run() {  // should not be synchronized

	// System.out.println("ThreadPoolServer start");

	for (int i = 0; i < pool.length; i++) {
	    pool[i].start();
	}

	// -- Accept section --
	Socket s;
	int idle;

	while (true) {
	    s = server.accept();
	    if (s == null)
		break;
	    idle = idleWorker();
	    if (idle < 0)
		break;
	    pool[idle].setSocket(s);
	    synchronized (this) {
		working[idle] = true;
	    }
	    pool[idle].work();
	}
	// -- Accept section --

	for (int i = 0; i < pool.length; i++) {
	    pool[i].terminate();
	}
	
	// System.out.println("ThreadPoolServer end");
    }

    synchronized int idleWorker() {
	try {
	    while (true) {
		for (int i = 0; i < working.length; i++) {
		    if (!working[i])
			return i;
		}
		wait();
	    }
	}
	catch (InterruptedException e) {
	    return -1;
	}
    }

    synchronized void addIdle(int id) {
	working[id] = false;
	notify();
    }
}

class ThreadPoolServerThread extends Thread {
    boolean working = false;
    boolean terminated = false;
    ThreadPoolServer t;
    int id;
    Socket s;

    ThreadPoolServerThread(ThreadPoolServer t, int id) {
	this.t = t;
	this.id = id;
    }

    public synchronized void run() {
	// System.out.println("PoolThread " + id + " start");
	while (true) {
	    if (terminated)
		break;
	    if (!working) {
		try {
		    // System.out.println("PoolThread " + id + " waiting");
		    wait();
		    // System.out.println("PoolThread " + id + " woke up");
		}
		catch (InterruptedException e) {
		    break;
		}
	    } else {
		t.serveClient(s);
		working = false;
		t.addIdle(id);
	    }
	}
	// System.out.println("PoolThread " + id + " end");
    }
    
    synchronized void work() {
	working = true;
	notify();
    }
    
    synchronized void terminate() {
	terminated = true;
	notify();
    }
    
    synchronized void setSocket(Socket socket) {
	s = socket;
    }
}

/*
  Multi Thread Client
*/

class MultiThreadClient extends Thread {

    Object mutex = new Object();
    int nChildren = 0;
    int[] buffer;
    int size;
    int file = 'a';

    java.util.Random rand = new java.util.Random();

    class Child extends Thread {
	int from;
	int to;
	
	Child(int from, int to) {
	    this.from = from;
	    this.to = to;
	    synchronized (mutex) {
		nChildren++;
	    }
	}

	public synchronized void run() {
	    // System.out.println("Child start " + from + " to " + to);

	    Socket s = new Socket();
	    InputStream is = s.getInputStream();
	    OutputStream os = s.getOutputStream();

	    os.write('g');
	    os.write(file);
	    os.write(from + '0');
	    if (to < 0)
		os.write('$');
	    else
		os.write(to + '0');

	    int count = is.read() - '0';

	    // System.out.println("Child count = " + count);

	    if (to < 0) {
		to = from + count;
		size = to;
		buffer = new int[size];
		for (int i = 0;  i < size;  i++)
		    buffer[i] = -1;
	    }

	    int t_s = to;
	    int cur = from;
	    while (cur < t_s) {
		if (buffer[cur] != -1) {
		    // System.out.println("Child aborting");
		    break;
		}
		if (cur >= to) {
		    // System.out.println("Child reading over range");
		    while (cur < t_s && buffer[cur] == -1)
			buffer[cur++] = is.read();
		    break;
		}
		buffer[cur++] = is.read();
		if (to - cur >= 2) { // can create another thread
		    if (Verify.getBoolean()) {
			//if (rand.nextBoolean()) {
			//if (false) {
			int pivot = cur + (to - cur) / 2;
			// System.out.println("Child creating Child");
			Child c = new Child(pivot, to);
			to = pivot; // adjust current thread range
			c.start();
		    }
		}
	    }
	    s.close();

	    synchronized (mutex) {
		nChildren--;
		// System.out.println("nChildren == " + nChildren);
		if (nChildren == 0) {
		    FileSystem.verify(file, buffer);
		    success();
		    ServerSocket.theServerSocket.close();
		}
	    }
	    // System.out.println("Child end");
	}
    }

    public synchronized void run() {
	// saving a thread
	new Child(0, -1).run();
    }

    private static void success() {
    }
}

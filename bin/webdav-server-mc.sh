#!/bin/bash
. ./env.sh

NUM_ACCEPT=2
if [ $# -gt 0 ]
then
	NUM_ACCEPT=$1
fi

PACKAGE="gov.nasa.jpf.network.http"
PROG_ARG="${PACKAGE}.HTTPServer ${NUM_ACCEPT}"
JPF_ARG="${JPF_ARG} +boot.peer.command=./webdav.sh"
SERVER_LOG="$HOME/webdav-server.log"

echo "start JPF running an HTTP server accepting ${NUM_ACCEPT} clients"
java -classpath $CLASSPATH $VM_ARG gov.nasa.jpf.JPF $JPF_ARG $PROG_ARG > $SERVER_LOG

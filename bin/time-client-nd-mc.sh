#!/bin/bash
# Run the ND time client in a model checker against the slow time server.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

CBUILD_DIR=${SCRIPT_DIR}/../c-build

NUM_THREADS=1
if [ "$#" -ge "1" ]
then
	NUM_THREADS=$1
fi

PACKAGE="gov.nasa.jpf.network.alphabet"
PROG_ARG="${PACKAGE}.NDTimeClient ${NUM_THREADS}"
CLIENT_LOG="${HOME}/time-client-nd.log"
JPF_ARG="${JPF_ARG} +log.fine=gov.nasa.jpf.network.listener,gov.nasa.jpf.network.cache.CacheLayer"
NUM_ACCEPTS=`echo "${NUM_THREADS} * 2" | bc`

echo "start the slow time server in the background accepting ${NUM_ACCEPTS} connections."
${CBUILD_DIR}/slow-time-server ${NUM_ACCEPTS} &
PID=$!
echo "start JPF verifying a ND time client"
java -classpath ${CLASSPATH} ${VM_ARG} gov.nasa.jpf.JPF ${JPF_ARG} ${PROG_ARG} > ${CLIENT_LOG}

if [ "$?" -ne "0" ]
then
	echo "Kill server process: ${PID}"
	kill -9 ${PID}
fi

#!/bin/bash
# Verify the alphabet server.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

if [ $# -lt 2 ]
then
	echo "Usage : nio-alphabet.sh {number of threads} {number of messages}"
	exit
fi

PACKAGE="gov.nasa.jpf.network.alphabet"
PROG_ARG="${PACKAGE}.AlphabetServerNio $1"
JPF_ARG="${JPF_ARG} +${OPT_BOOT_CMD}=${SCRIPT_DIR}/alphabet-client-1.sh;$2"
SERVER_LOG="${HOME}/nio-alphabet.logC"

echo "start JPF verifying an alphabet server NIO accepting $1 client(s)"
${RUN_EXAMPLE_CMD} ${JPF_ARG} ${PROG_ARG} > ${SERVER_LOG}

#!/bin/sh
# check errors in log files

OUTS=`find ./log -name '*.log'`

for FILE in $OUTS
do
	if [ "`tail -40 $FILE | grep 'no error'`" != "" ]
	then
		echo "$FILE OK"
	else
		echo "Error in $FILE"
	fi
done


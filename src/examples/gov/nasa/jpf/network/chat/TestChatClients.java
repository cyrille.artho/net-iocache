//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.chat;

/* $Id: TestChatClients.java 170 2006-08-08 07:38:20Z cartho $ */

import java.io.*;
import java.net.*;
import junit.framework.TestCase;

public class TestChatClients extends TestCase {
    String serverAddr = "localhost";
    int serverPort = 4444;

    public TestChatClients(String name) {
        super(name);
    }

    public void testSequential2() {
        try {
            String toprint = "TeStStRiNg";
            String toread1, toread2;
            Socket socket = new Socket();
            InetSocketAddress addr = new InetSocketAddress("localhost", 4444);
            socket.connect(addr);

            InputStreamReader istr =
                new InputStreamReader(socket.getInputStream());
            BufferedReader in = new BufferedReader(istr);
            PrintWriter out =
                new PrintWriter(socket.getOutputStream(), true);
            out.println(toprint);

            out.flush();
            toread1 = in.readLine();

            out.close();
            socket.close();

            try {
                Thread.sleep(10);
            } catch (Exception e) {}

            socket = new Socket();
            socket.connect(addr);

            istr = new InputStreamReader(socket.getInputStream());
            in = new BufferedReader(istr);
            out = new PrintWriter(socket.getOutputStream(), true);

            out.println(toprint);
            out.flush();

            toread2 = in.readLine();

            out.close();
            socket.close();

            try {
                Thread.sleep(10);
            } catch (Exception e) {}

            assertTrue(toread1.endsWith(toprint));
            assertTrue(toread2.endsWith(toprint));
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void testSequential3() {
        try {
            String toprint = "TeStStRiNg";
            String toread1, toread2, toread3;
            Socket socket = new Socket();
            InetSocketAddress addr = new InetSocketAddress("localhost", 4444);
            socket.connect(addr);

            InputStreamReader istr =
                new InputStreamReader(socket.getInputStream());
            BufferedReader in = new BufferedReader(istr);
            PrintWriter out =
                new PrintWriter(socket.getOutputStream(), true);
            out.println(toprint);

            out.flush();
            toread1 = in.readLine();

            out.close();
            socket.close();

            try {
                Thread.sleep(10);
            } catch (Exception e) {}

            socket = new Socket();
            socket.connect(addr);

            istr = new InputStreamReader(socket.getInputStream());
            in = new BufferedReader(istr);
            out = new PrintWriter(socket.getOutputStream(), true);

            out.println(toprint);
            out.flush();

            toread2 = in.readLine();

            out.close();
            socket.close();

            try {
                Thread.sleep(10);
            } catch (Exception e) {}

            socket = new Socket();
            socket.connect(addr);

            istr = new InputStreamReader(socket.getInputStream());
            in = new BufferedReader(istr);
            out = new PrintWriter(socket.getOutputStream(), true);

            out.println(toprint);
            out.flush();
            toread3 = in.readLine();

            out.close();
            socket.close();

            try {
                Thread.sleep(10);
            } catch (Exception e) {}

            assertTrue(toread1.endsWith(toprint));
            assertTrue(toread2.endsWith(toprint));
            assertTrue(toread3.endsWith(toprint));
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    class ConnectChat extends Thread {
        public String toread;
        public String toprint;

        public ConnectChat(String toprint) {
            this.toprint = toprint;
        }

        public void run() {
            try {
                Socket socket = new Socket();
                InetSocketAddress addr = new InetSocketAddress("localhost", 4444);
                socket.connect(addr);

                InputStreamReader istr =
                    new InputStreamReader(socket.getInputStream());
                BufferedReader in = new BufferedReader(istr);
                PrintWriter out =
                    new PrintWriter(socket.getOutputStream(), true);

                out.println(toprint);
                out.flush();

                toread = in.readLine();

                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) { }

                out.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public void testConcurrent3() {
        String toprint = "TeStStRiNg";
        ConnectChat cc1 = new ConnectChat(toprint);
        ConnectChat cc2 = new ConnectChat(toprint);
        ConnectChat cc3 = new ConnectChat(toprint);
        cc1.start();
        cc2.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) { }
        cc3.start();
        try {
            cc1.join();
            cc2.join();
            cc3.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        assertTrue(cc1.toread.endsWith(toprint));
        assertTrue(cc2.toread.endsWith(toprint));
        assertTrue(!cc3.toread.endsWith(toprint));
        //assertTrue((cc3.toread == null) || (!cc3.toread.endsWith(toprint)));
    }

    public void testConcurrent2() {
        String toprint = "TeStStRiNg";
        ConnectChat cc1 = new ConnectChat(toprint);
        ConnectChat cc2 = new ConnectChat(toprint);
        cc1.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) { }
        cc2.start();
        try {
            cc1.join();
            cc2.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        assertTrue((cc1.toread == null) || (cc1.toread.endsWith(toprint)));
        assertTrue((cc2.toread == null) || (cc2.toread.endsWith(toprint)));
    }

    public void testConcurrent1() {
        String toprint = "TeStStRiNg";
        ConnectChat cc1 = new ConnectChat(toprint);
        cc1.start();
        try {
            cc1.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        assertTrue((cc1.toread == null) || (cc1.toread.endsWith(toprint)));
    }
}

package gov.nasa.jpf.network;

import java.util.Collection;
import java.util.Iterator;

public class HelperMethods {

	public static int[] collectionToIntArray(Collection<Integer> x) {
		Iterator<Integer> iterator = x.iterator();
		int[] ret;
		int len = x.size();

		ret = new int[x.size()];
		for (int i = 0; i < len; i++) {
			ret[i] = iterator.next();
		}

		return ret;
	}

	public static byte[] intToByteArray(int x) {
		return intToByteArray(x, null);
	}
	
	public static byte[] intToByteArray(int x, byte[] buff) {
		return intToByteArray(x, buff, 0);
	}
	
	public static byte[] intToByteArray(int x, byte[] buff, int off) {
		byte[] ret = new byte[4];
		
		ret[0] = (byte) (x >> 24);
		ret[1] = (byte) (x >> 16);
		ret[2] = (byte) (x >> 8);
		ret[3] = (byte) x;
		
		if (buff != null) {
			System.arraycopy(ret, 0, buff, off, 4);
		}
		
		return ret;
	}
	
	public static void addIntArray(Collection<Integer> x, int[] a) {
		for (int i : a) {
			x.add(i);
		}
	}
	
	/**
	 * Assume big-endian order of <i>b</i>.
	 * 
	 * @param b
	 * @return
	 */
	public static int byteArrayToInt(byte[] b) {
		return byteArrayToInt(b, 0);
	}
	
	/**
	 * Assume big-endian order of <i>b</i>.
	 * 
	 * @param b
	 * @param offset
	 * @return
	 */
	public static int byteArrayToInt(byte[] b, int offset) {
		int value = 0;
		for (int i = 0; i < 4; i++) {
			int shift = (3 - i) * 8;
			value += (b[i + offset] & 0x000000FF) << shift;
		}
		return value;
	}

	/**
	 * Check if a given character is printable.
	 * @param c A character to be checked.
	 * @return
	 */
	public static boolean isPrintable(int c) {
		int type = Character.getType(c);

		return type == Character.LOWERCASE_LETTER
				|| type == Character.UPPERCASE_LETTER
				|| type == Character.DECIMAL_DIGIT_NUMBER
				|| type == Character.CONNECTOR_PUNCTUATION
				|| type == Character.DASH_PUNCTUATION
				|| type == Character.OTHER_PUNCTUATION;
	}
}

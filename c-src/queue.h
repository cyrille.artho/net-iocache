#ifndef QUEUE_H
#define QUEUE_H

typedef struct queue queue;
struct queue {
    char *data;
    pthread_mutex_t lock;
    pthread_cond_t empty;
    pthread_cond_t full;
    int p, c;	/* indices in queue at which to insert/remove element */
    int size;
};

queue *queue_new (int size);
void queue_put (queue *queue, char *items, int len);
void queue_putItem (queue *queue, char item);
void queue_remove (queue *queue, char *storage, int len);
char queue_removeItem (queue *queue);
int queue_remove_nonblock (queue *q, char *storage, int len);
int queue_peek (queue *queue, char *storage, int len, int timeout);
void queue_free (queue *queue);

#endif

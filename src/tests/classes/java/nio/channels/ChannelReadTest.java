package classes.java.nio.channels;

import gov.nasa.jpf.util.test.TestJPF;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import org.junit.BeforeClass;
import org.junit.Test;

public final class ChannelReadTest extends TestJPF implements Runnable {
	private static final int PORT=9999;
	
	public void run() {
		try {
			ServerSocket ss=new ServerSocket(PORT);
			//accept two connection, one for each of the two test cases below
			for (int i=0;i<2;i++) {
				Socket sock=ss.accept();
				sock.getOutputStream().write(65);
				sock.getOutputStream().write(66);
				sock.close();
			}
			ss.close();
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	//start a server thread on port PORT
	@BeforeClass
	public static void startServer() throws IOException {
		new Thread(new ChannelReadTest()).start();
	}

	@Test
	public void readBeyondEOFblocking() throws IOException {
		if (verifyNoPropertyViolation()) {
			SocketChannel sc = connect();
			assert readByte(sc) == 1;
			assert readByte(sc) == 1;
			assert readByte(sc) == -1; // EOF after two bytes
			assert readByte(sc) == -1;
			sc.close();
		}
	}

	@Test
	public void readBeyondEOFnonBlocking() throws IOException {
		if (verifyNoPropertyViolation()) {
			SocketChannel sc = connect();
			sc.configureBlocking(false);
			int n = 0;
			boolean eof = false;
			
			while (n < 4) {
				int read = readByte(sc);
				if (eof) assert (read == -1);
				// ensure no 0 non-bl. read after EOF
				if (read == -1) eof = true;
				if (read != 0) {
					// ignore non-bl. zero-reads
					if (n++ < 2) {
						// read data twice
						assert (read == 1);
					} else {
						// read EOF twice after that
						assert (read == -1);
					}
				}
			}
			
			sc.close();
		}
	}

	private int readByte(SocketChannel sc) throws IOException {
		ByteBuffer buffer = ByteBuffer.allocate(1);
		return sc.read(buffer);
	}

	private SocketChannel connect() throws IOException {
		final SocketChannel socketChannel = SocketChannel.open();
		socketChannel.connect(new InetSocketAddress("localhost", PORT));
		return socketChannel;
	}
}

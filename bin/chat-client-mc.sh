#!/bin/bash
# Verify the chat client.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

# need two arguments, #clients and #workers. These arguments are usually equal.
if [ $# -lt 2 ]
then
	echo "Usage : chat-client-mc.sh {number of clients} {number of server-side worker threads}"
	exit
else
	NUM_CLIENT=$1
	NUM_WORKER=$2
fi

PACKAGE="gov.nasa.jpf.network.chat"
PROG_ARG="${PACKAGE}.ChatClientRunner ${NUM_CLIENT} H"
JPF_ARG="+${OPTION_PREFIX}.main.termination=false"
CLIENT_LOG="${HOME}/chat-client.log"

echo "starting the chat server accepting ${NUM_CLIENT} client thread(s) ..."
java -classpath $CLASSPATH ${PACKAGE}.ChatServer ${NUM_CLIENT} ${NUM_WORKER} > /dev/null &
sleep 3
echo "starting JPF running ${NUM_CLIENT} chat client(s) ..."
${RUN_EXAMPLE_CMD} ${JPF_ARG} ${PROG_ARG} > ${CLIENT_LOG}


//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.vm;

import java.io.IOException;
import java.net.InetAddress;

import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.NativePeer;
import gov.nasa.jpf.network.cache.*;
import gov.nasa.jpf.network.SystemInfo;
import static gov.nasa.jpf.network.SystemInfo.PROJ_NAME;


public class JPF_java_net_Socket extends NativePeer{

	private static boolean init = false;
	
	/**
	 * Set the static fields in "Socket" depending on a configuration.
	 * 
	 * @param env
	 * @param objRef
	 */
	private static void initialize(MJIEnv env, int objRef) {
		env.setStaticBooleanField("java.net.Socket", "exception_simulation", env.getConfig().getBoolean(PROJ_NAME + ".exception.simulation"));
		env.setStaticBooleanField("java.net.Socket", "main_termination", env.getConfig().getBoolean(PROJ_NAME + ".main.termination"));
		
		init = true;
	}

	@MJI
	public void native_Socket____(MJIEnv env, int objRef) {
		SocketTreeTable t = SocketTreeTable.getInstance();
		int id;

		// First time only: Read the configuration file.
		if (!init) {
			initialize(env, objRef);
		}

		// New socket -> add into the socket-tree table.
		id = env.getIntField(objRef, "socketID");
		if (id > t.maxSocketID()) {
			t.addSocket();
			// Notify the cache layer of the new socket.
			CacheLayer.getInstance().onSocketCreated(id);
		}
	}

	@MJI
	public void native_connect__Ljava_net_InetAddress_2I(MJIEnv env, int objRef, int addrRef, int port)
			throws IOException {
		CacheLayer cl;
		InetAddress addr;
		byte[] ip;
		int address_ref;
		
		// Virtualized mode: The cache must connect to a real IP address.
		// If the address is not specified, use localhost.
		if (CacheLayer.isVirtualMode() && addrRef > 0) {
			address_ref = env.getReferenceField(addrRef, "address");
			ip = env.getByteArrayObject(address_ref);
			addr = InetAddress.getByAddress(ip);
		} else
			addr = InetAddress.getLocalHost();

		int sock_id = env.getIntField(objRef, "socketID");

		cl = CacheLayer.getInstance();
		cl.connect(sock_id, addr, port);
	}

	@MJI
	public boolean isMainAlive(MJIEnv env, int objRef) {		
		return CacheLayer.getInstance().isMainAlive();
	}

	@MJI
	public int native_getInetAddress(MJIEnv env, int objRef) {
		PhysicalConnection c;
		byte[] addr;
		int ipRef = env.newByteArray(4);
		int sock_id = env.getIntField(objRef, "socketID");

		c = CacheLayer.conn_list.get(sock_id);
		addr = c.getInetAddress().getAddress();

		for (int i = 0; i < 4; i++)
			env.setByteArrayElement(ipRef, i, addr[i]);

		return ipRef;
	}

	@MJI
	public int getPort(MJIEnv env, int objRef) {
		PhysicalConnection c;
		int sock_id = env.getIntField(objRef, "socketID");

		c = CacheLayer.conn_list.get(sock_id);
		return c.getPort();
	}

	@MJI
	public int getLocalPort(MJIEnv env, int objRef) {
		PhysicalConnection c;
		int sock_id = env.getIntField(objRef, "socketID");

		c = CacheLayer.conn_list.get(sock_id);
		return c.getLocalPort();
	}

	@MJI
	public boolean isConnected(MJIEnv env, int objRef) {
		PhysicalConnection c;
		int sock_id = env.getIntField(objRef, "socketID");

		c = CacheLayer.conn_list.get(sock_id);
		return c.isConnected();
	}

	@MJI
	public boolean isClosed(MJIEnv env, int objRef) {
		CacheLayer cl = CacheLayer.getInstance();
		int sock_id = env.getIntField(objRef, "socketID");

		return cl.isSocketClosed(sock_id);
	}

	@MJI
	public void close(MJIEnv env, int objRef) throws IOException {
		int sock_id = env.getIntField(objRef, "socketID");

		CacheLayer.getInstance().close(sock_id);
	}

}

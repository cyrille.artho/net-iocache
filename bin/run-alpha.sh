#!/bin/sh
ulimit -t 7200

K=4
while [ $K -lt 4 ]
do
      	K=`expr $K + 1`
	J=0
	while [ $J -lt 1 ]
	do
		J=`expr $J + 1`
		I=0
		while [ $I -lt 1 ]
		do
		        I=`expr $I + 1`
			echo "Client in MC, $K proc./cons. threads, $J messages, run $I..."
		        /usr/bin/time ./alphabet-client-mc.sh $K $J >& alphabet-client-4-353-ns-$K-$J-$I.out
			mv ~/client.log alphabet-client-4-353-ns-$K-$J-$I.log
			sleep 5
		done
	done
done

K=4
while [ $K -lt 5 ]
do
      	K=`expr $K + 1`
	J=2
	while [ $J -lt 3 ]
	do
		J=`expr $J + 1`
		I=0
		while [ $I -lt 1 ]
		do
		        I=`expr $I + 1`
			echo "Server in MC, $K proc./cons. threads, $J messages, run $I..."
		        /usr/bin/time ./alphabet-server-mc.sh $K $J >& alphabet-server-4-353-ns-$K-$J-$I.out
			sleep 6000
			mv ~/server.log alphabet-server-4-353-ns-$K-$J-$I.log
			sleep 5
		done
	done
done
exit

K=4
	J=2
	while [ $J -lt 3 ]
	do
		J=`expr $J + 1`
		I=0
		while [ $I -lt 1 ]
		do
		        I=`expr $I + 1`
			echo "Server in MC, $K proc./cons. threads, $J messages, run $I..."
		        /usr/bin/time ./alphabet-server-mc.sh $K $J >& alphabet-server-4-353-ns-$K-$J-$I.out
			sleep 3600
			mv ~/server.log alphabet-server-4-ns-353-$K-$J-$I.log
			sleep 5
		done
	done

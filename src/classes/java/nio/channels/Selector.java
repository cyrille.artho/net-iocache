package java.nio.channels;

import java.io.IOException;
import java.nio.channels.spi.SelectorProvider;
import java.util.HashSet;
import java.util.Set;

/**
 * An ineffective but basic implementation of the selector
 * @author amarda
 *
 */
public abstract class Selector {
	protected Selector() {
	}

	public static Selector open() throws IOException {
		return SelectorProvider.provider().openSelector();
	}

	public abstract boolean isOpen();

	public abstract Set<SelectionKey> keys();

	public abstract int select() throws IOException;
	public abstract int select(long timeout) throws IOException;

	public abstract Set<SelectionKey> selectedKeys();

	public abstract void close() throws IOException;

	public abstract Selector wakeup();

	protected abstract void cancelKey(SelectionKey key);

	protected abstract SelectionKey registerKey(SelectableChannel channel);
}

package gov.nasa.jpf.network.pws;
/*  FileFinder.java -- function to find files

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.net.*;
import java.io.*;
import java.util.*;

public class FileFinder
{
    private int status = -1;
	private	File theFile = null;

    
    FileFinder(String httpURIPath)
    {
	  	/* 
		 status = -1    --> nothing found, and not allowed to give virtual listing
		 					This may also mean that we encountered an attempt at directory traversal.
         status = 0     --> file not found, but allowed to give dirlisting
         status = 1     --> file found
         
		/* check goes as follows:
		   1. check if defined url is a dir
           1.1 if it is, try /index.html, index.htm and index.php
           1.1.1 if not found, error
           1.2 if it isn't, open the file and dump the contents through :-)*/

		File fileToLookFor = new File("www" + httpURIPath);
		boolean fileFound = false;
        boolean isDirIndexFile = false;
		boolean invalidURL = false;
        this.status = -1;
		this.theFile = null;
		
		/* fix for directory traversal bug, found by Donato Ferrante */
   		for (int i = 0; i < httpURIPath.length() - 1; i++)
		{ 
  			if (httpURIPath.charAt(i) == '.' && httpURIPath.charAt(i + 1) == '.')
			{ 
				invalidURL = true;
  			} 
		}      
		
        if (fileToLookFor.exists() == true && invalidURL == false)
		{
		    if (fileToLookFor.isDirectory() == false)
            {
                this.status = 1; /* we found a file */
                this.theFile = fileToLookFor;
		    }
            else
		    {
        		/* we're looking for the index file of this directory */
				fileFound = false;
					
		        StringTokenizer configuredIndexFiles =  new StringTokenizer (pws.getSetting("directoryindex"));
				/* as long as we have more tokens, and the index hasn't been found yet */
        		while (configuredIndexFiles.hasMoreTokens() == true && this.status != 1)
		        {
				    String indexFileName = configuredIndexFiles.nextToken();
        			this.theFile = new File(fileToLookFor, indexFileName);
		        	if (this.theFile.exists() == true)
				        this.status = 1;
        		}
        		
        		if (this.status != 1) /* we haven't found an index file */
        		{
        			this.status = 0;
        		}
			}
		}
        else
		    this.status = -1;
	}


    public int status()
    {
        return status;
    }
    
    public File theFile()
    {
        return theFile;
    }
}

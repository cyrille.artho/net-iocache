//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.http;

import gov.nasa.jpf.annotation.FilterField;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class HTTPClientDeterministic {

	static class ClientThread extends Thread {
		private InputStream input;
		private Writer output;
		private SocketAddress addr;
		private String haddr="127.0.0.1";
		private Socket sock = new Socket();
		//Socket sock;
		private int msgIdx;
		private byte[] response = new byte[512 * 512];

		ClientThread(int msg) {
			try {
				addr = new InetSocketAddress(InetAddress.getLocalHost(), nativePort);
				haddr=InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				addr = new InetSocketAddress(nativePort);
				haddr="127.0.0.1";
			}
			msgIdx = msg;
		}

		public void run() {
			try {
				System.out.println("[HTTPClient.run] === "+this+" === Connect to " + addr+"... ===");
			    sock.connect(addr);

				input = sock.getInputStream();
				output = new OutputStreamWriter(sock.getOutputStream());

				String message = buildMsg(files.get(msgIdx % files.size()));

				System.out.println("[HTTPClient.run] ==> "+this+" ==> Send message... ==>\n" + message);
				output.write(message);
				output.flush();
				
				//Thread.yield();
				
				int inputLen,i=0;
				//System.out.println("[HTTPClient.run] <-- "+this+" <-- Try to read response... <--");
				while ((inputLen = input.read(response))>-1) {
					System.out.print("[HTTPClient.run] <-- "+this+" <-- Read "+(++i)+". response <--\n"+new String(response, 0, inputLen));
				} 
				System.out.println("[HTTPClient.run] --- "+this+" --- Close connection... ---");				
				sock.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			//System.out.println("[HTTPClient.run] *** "+this+" *** Finished ***");				
		}
		
		private String buildMsg(String file) {
			StringBuffer s = new StringBuffer(String.format("GET /%s HTTP/1.1\r\n", file));
			// Header "Host" is mandatory.
			s.append("Host: "+haddr+":"+logicalPort+"\r\n");
			s.append("Connection: Close\r\n\r\n");
			
			return s.toString();
		}
	}

	//Mark the following as filter field to reduce state space
	@FilterField private static int nativePort = HTTPServer.PORT;
	@FilterField private static int logicalPort = HTTPServer.PORT;
	@FilterField private static int numThreads = 1;
	@FilterField private static int curArg = 0;
	@FilterField static int nonDet = 0;  //0: deterministic http-client; 1: non-deterministic http-client
	
	private static List<String> files=new ArrayList<String>();
	
	/**
	 * converts string argument into integer
	 * @param args runtime arguments
	 * @param idx  <b>args[idx]</b> will be converted to a positive Number 
	 * @return -1 if <b>idx</b> is out of range or args[idx] is not an integer >=0  
	 */
	private static int parsePosNumber(String[] args, int idx) {
		int retVal=-1;     //indicates failure
		try {
			retVal=Integer.parseInt(args[idx]);
		} catch (Exception e) {/*ignore*/}
		return Math.max(retVal, -1);
	}
	
	/**
	 * 
	 * @param args [-port nativePort [logicalPort]] [-threads threadNumber] [File to request]+ 
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		int n;

		//Parse arguments...
		for (int i=0;i<args.length;i++) {
			switch(args[i]) {
			
				//get native and logical port number (if available)
				case "-port":
					if ((n=parsePosNumber(args,i+1))>0) { 
						i++;
						nativePort=n;
						logicalPort=n;  //by default, set logical port equal to native port (may be overridden by the next parameter)
						if ((n=parsePosNumber(args,i+1))>0) { 
							i++;
							logicalPort=n;
						}
					}
					break;
					
				//configure the number of threads (1 is default)
				case "-threads":
					if ((n=parsePosNumber(args,i+1))>0) { 
						i++;
						numThreads=n;
					}
					break;
					
				//if not one of the options above, regard argument as file name to request from the HTTP server
				default:
					files.add(args[i]);
			}
		}
		
//		System.out.println("[HTTPClient.main] Started "+(nd?"non-":"")+"deterministic HTTPClient with "+numThreads+
//				" thread(s), each\n    requesting one of "+numFiles+" different resources from HTTP server at port "+HTTP_PORT+"...");
		
		ClientThread[] t = new ClientThread[numThreads];
		for (int i = 0; i < numThreads; i++) {
			t[i] = new ClientThread(i % files.size());
		}

//		System.out.println("[HTTPClient.main] starting client threads...");
		for (int i = 0; i < numThreads; i++) {
			t[i].start();
		}
//		System.out.println("[HTTPClient.main] says good bye!\n");
	}

}

/*
  syncckpt blocks on a named pipe until some process sends data to the pipe.

  Usage: syncckpt [pipe name]
*/

#define PIPE_NAME "ckpt_sig"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>


typedef struct timeval timeval;


// 5 seconds
const unsigned int WAIT_TIME = 5;

const int FIFO_NAME_MAX_SIZE = 256;


int main(int argc, char **argv) {
	timeval tp;
	char s[32];
	char *dir = getenv("CHKPNT_DIR");
	char *pipe;
	char fifo[FIFO_NAME_MAX_SIZE];
	fd_set set;
	int num, fd;

	if (!dir)
		perror("CHKPNT_DIR is not defined");

	// A pipe name is given.
	if (argc > 1) {
		pipe = argv[1];
	}
	// Use the default name.
	else {
		pipe = PIPE_NAME;
	}

	// Build the pipe name.
	memset(fifo, 0, FIFO_NAME_MAX_SIZE);
	sprintf(fifo, "%s/%s", dir, pipe);

	mknod(fifo, S_IFIFO | 0666, 0);

	printf("waiting until checkpointing completed ...\n");
	fd = open(fifo, O_RDONLY);

	/* Initialize the file descriptor set. */
	FD_ZERO (&set);
	FD_SET (fd, &set);

	// Set timeout
	memset(&tp, 0, sizeof(timeval));
	tp.tv_sec = WAIT_TIME;
	
	num = select(FD_SETSIZE, &set, NULL, NULL, &tp);
	if (num > 0) {
		if (FD_ISSET(fd, &set)) {
			if ((num = read(fd, s, 32)) == -1)
				perror("read");
		}
	}

	close(fd);

	return num ? 0 : 1;
}

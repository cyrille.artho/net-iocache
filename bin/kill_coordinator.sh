#!/bin/bash
# Kill the DMTCP coordinator. Remove the PID file if it exists. Remove immediate
# checkpoints in the script directory.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

${DMTCP_QUIT}

# If "mtcp_restart" is running.
if [ -f ${CHKPNT_DIR}/${MTCP_PID_FILE} ]
then
	rm ${CHKPNT_DIR}/${MTCP_PID_FILE}
fi

# If the original peer is running.
if [ -f ${CHKPNT_DIR}/${PEER_PID_FILE} ]
then
	rm ${CHKPNT_DIR}/${PEER_PID_FILE}
fi

# Remove all immediate checkpoints.
rm ${SCRIPT_DIR}/*.dmtcp

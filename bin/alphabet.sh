#!/bin/bash

source "$(cd "$(dirname "$0")" && pwd)/utils-for-testing.source" || exit

set-specific-help \
"ALPHABET CLIENT and SERVER

This script runs/tests various alphabet client/server setups.

Specific options for choosing the client and server programs are:

    nd  -- run non-deterministic client and server
    nio -- run nio version of (for now just) the server
        -- not stating nd or nio defaults to deterministic server

Specific options for choosing program parameters are:

    threads=x      -- number of threads in SUT (or the server for the run native
	              option)
    messages=x     -- number of messages accepted by the server
                      (defaults for threads and message are 1 or 2, depending 
                      on other options selected)
    version=Simple -- use a simpler version of the alphabet client which uses
                      only one thread per connection (not available in 
                      combination with options nd and nio)
"

set-parameters "$@"

if checkParameter server-sut
then
    DOTJPF="$DOTJPF
jpf-net-iocache.lazy.connect = true
jpf-net-iocache.main.termination=false
jpf-net-iocache.peerResponse.maxDelayMillis = 40
"
else
    DOTJPF="$DOTJPF
jpf-net-iocache.lazy.connect = true
jpf-net-iocache.main.termination=true
jpf-net-iocache.peerResponse.maxDelayMillis = 120
"
fi

PACKAGE="gov.nasa.jpf.network.alphabet"
checkParameter version clientVersion ""

# set up all the nio cases here
if checkParameter nio
then
    optsuffix="-nio"
    checkParameter threads nThreads 2
    checkParameter messages nMessages 2  # default as in bin/testall.sh
    set-server java ${PACKAGE}.AlphabetServerNio "$nThreads"

    if checkParameter server-sut
    then ##### CASE 1 (nio)
	set-client java ${PACKAGE}.AlphabetClient 1 "$nMessages"

    elif checkParameter client-sut
    then ##### CASE 2 (nio)
	reportfail "client case for NIO not implemented"

    elif checkParameter native
    then 
	set-client java ${PACKAGE}.AlphabetClient "$nThreads" "$nMessages"
    fi    
    # the whole next "if/elif" will be skipped
fi

# separate out all 4 non-nio combinations of {client,server}x{nd,non-nd}
if checkParameter nd && ! checkParameter nio
then
    optsuffix="-nd"
    if checkParameter server-sut
    then ##### CASE 1
	checkParameter threads nThreads 2
	checkParameter messages nMessages 2  # default as in bin/testall.sh
	set-server java ${PACKAGE}.NDAlphabetServer "$nThreads"
	set-client java ${PACKAGE}.AlphabetClient 1 "$nMessages" --ip 127.0.0.1 --port 18600
    elif checkParameter client-sut
    then ##### CASE 2
	checkParameter threads nThreads 1
	checkParameter messages nMessages 2  # default as in bin/testall.sh
	NUM_ACCEPTS=`echo "$nThreads * (2 ^ $nMessages)" | bc`

	set-server java ${PACKAGE}.AlphabetServer "$NUM_ACCEPTS"
	set-client java ${PACKAGE}.NDAlphabetClient "$nThreads" "$nMessages"
    elif checkParameter native
	then
	## Classes NDAlphabetClient and NDAlphabetServer use gov.nasa.jpf.vm.Verify
	## so ./jpf-core/build/jpf.jar must be on the classpath.
	checkParameter threads nThreads 1
	checkParameter messages nMessages 2  # default as in bin/testall.sh

	set-server java ${PACKAGE}.AlphabetServer "$nThreads"
	set-client java ${PACKAGE}.NDAlphabetClient "$nThreads" "$nMessages"
    fi
elif ! checkParameter nio
then
    optsuffix=""
    if checkParameter server-sut
    then ##### CASE 3
	checkParameter threads nThreads 2
	checkParameter messages nMessages 2  # default as in bin/testall.sh
	set-client java ${PACKAGE}.AlphabetClient 1 "$nMessages"
	
    elif checkParameter client-sut || checkParameter native
    then ##### CASE 4
	checkParameter threads nThreads 2
	checkParameter messages nMessages 2  # default as in bin/testall.sh
	set-client java ${PACKAGE}.AlphabetClient${clientVersion} "$nThreads" "$nMessages"
    fi
    set-server java ${PACKAGE}.AlphabetServer "$nThreads"
fi

optsuffix="$optsuffix-$nThreads-$nMessages"
TESTNAME="alphabet${optsuffix}"

dotest

package gov.nasa.jpf.network.pws;
/*  ConfTool.java - Grafical configuration tool

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class MimeDialog extends JDialog
	{
	//config-file stuff
    private static String mainConfigFile = "conf/pws.conf";
	private static String mimeConfigFile = "conf/mimetypes.conf";
	private static Map serverSettings = new HashMap();  /* general server settings */
	private static Map mimeTypes = new HashMap();       /* mimetype configuration */
	private static Map actionHandlers = new HashMap();  /* action handlers for certain mimetypes/extensions */

	//window stuff
	GridBagConstraints constraints = new GridBagConstraints();
	private static double hugeWeight = 500.0;  //don't change this in the code

	//stuff i'll need later on
	JTextField fieldMimetype;
	JTextField fieldExtensions;
	JTextField fieldAction;

	JButton buttonOk;
	JButton buttonCancel;
	
	boolean flagButtonOkPressed = false;

	//constructor
	//putting the mimetype & extensions in the constructor is actually pretty stupid, since we
	//could also set the fields in the ConfToolFrame code. Whatever.
	public MimeDialog(String title, String mimetype, String extensions, String action)
		{
		Misc.putSysMessage(0,"We're in the mime dialog now");

		setTitle(title);
		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		
		//draw stuff
		GridBagLayout mimebag1 = new GridBagLayout();
		JPanel mimePanel1 = new JPanel(mimebag1);
		//mimePanel1.setBorder(new TitledBorder(new EtchedBorder(),"Mime Type"));

		//the labels
		setDefaultConstraints();
		constraints.anchor= GridBagConstraints.WEST;
		constraints.insets = new Insets(0,10,0,10);
	    addLabel("Mime type", constraints, mimebag1, mimePanel1);
		constraints.gridy = GridBagConstraints.RELATIVE;
	    addLabel("Extensions", constraints, mimebag1, mimePanel1);
	    addLabel("Action", constraints, mimebag1, mimePanel1);

		//da fields
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridwidth = 3;
		constraints.gridx = 1;
		constraints.insets = new Insets(0,0,5,10);
		constraints.weightx = hugeWeight;
		fieldMimetype = addField(constraints, mimebag1, mimePanel1);
		constraints.gridy = GridBagConstraints.RELATIVE;
		fieldExtensions  = addField(constraints, mimebag1, mimePanel1);
		fieldAction  = addField(constraints, mimebag1, mimePanel1);

		//the "OK", "CANCEL", and "APPLY" button on the bottom of the app
		GridBagLayout mimeBag2 = new GridBagLayout();
		JPanel mimePanel2 = new JPanel(mimeBag2);

		//set the constraints and add the buttons
		setDefaultConstraints();
		constraints.anchor = GridBagConstraints.EAST;
		constraints.fill = GridBagConstraints.VERTICAL;
		constraints.insets = new Insets(5,5,5,5);
		constraints.weightx = hugeWeight;
		buttonOk = addButton("Ok", constraints, mimeBag2, mimePanel2);
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.weightx = 1.0;
		buttonCancel = addButton("Cancel", constraints, mimeBag2, mimePanel2);

		//resize the buttons so they all get the same size
		double width, height;
		width = buttonOk.getPreferredSize().getWidth();
		width = buttonCancel.getPreferredSize().getWidth() > width ? buttonCancel.getPreferredSize().getWidth() : width;
		height = buttonOk.getPreferredSize().getHeight();
		height = buttonCancel.getPreferredSize().getHeight() > width ? buttonCancel.getPreferredSize().getHeight() : height;
		buttonOk.setPreferredSize(new Dimension((int)width,(int)height));
		buttonCancel.setPreferredSize(new Dimension((int)width,(int)height));

	    buttonOk.addActionListener(new ButtonListener());
	    buttonCancel.addActionListener(new ButtonListener());




		//get a handle for the entire window and set the layout
		Container mimeContent = getContentPane();
		GridBagLayout mainMimeBag = new GridBagLayout();
		mimeContent.setLayout(mainMimeBag);

		//stick the entire panel in the frame
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weighty = hugeWeight;
		mainMimeBag.setConstraints(mimePanel1, constraints);
		mimeContent.add(mimePanel1);

		//adding the buttons
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridy = GridBagConstraints.RELATIVE;
		mainMimeBag.setConstraints(mimePanel2, constraints);
		mimeContent.add(mimePanel2);

		//stick content in the fields
		//...
		fieldMimetype.setText(mimetype);
		fieldExtensions.setText(extensions);
		fieldAction.setText(action);

		}

	protected void processWindowEvent(WindowEvent e)
		{
		if (e.getID() == WindowEvent.WINDOW_CLOSING)
			{
			Misc.putSysMessage(0,"Shutting down");
			//dispose();
			//System.exit(0);
			}
		super.processWindowEvent(e);
		}

	JLabel addLabel(String caption, GridBagConstraints constraints, GridBagLayout layout, JPanel panel)
		{
		JLabel label = new JLabel(caption);
		layout.setConstraints(label, constraints);
		panel.add(label);
		return label;
		}

	JButton addButton(String caption, GridBagConstraints constraints, GridBagLayout layout, JPanel panel)
		{
		JButton button = new JButton(caption);
		layout.setConstraints(button, constraints);
		panel.add(button);
		return button;
		}
		
	JTextField addField(GridBagConstraints constraints, GridBagLayout layout, JPanel panel)
		{
		JTextField field = new JTextField();
		layout.setConstraints(field, constraints);
		panel.add(field);
		return field;
		}

	//initialize defaultConstraints
	void setDefaultConstraints()
		{
		constraints.anchor= GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.insets = new Insets(0,0,0,0);
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		}
		
	//listener for the ok, cancel & apply button
	class ButtonListener implements ActionListener
		{
		ButtonListener()
			{
			//do.nothing();
			}
			
		public void actionPerformed(ActionEvent e)
			{
			if(e.getSource() == buttonCancel)
				{
				Misc.putSysMessage(0,"Cancel has been pressed");
				setVisible(false);
				}
			else
				{
				Misc.putSysMessage(0,"Ok has been pressed");
				flagButtonOkPressed = true;
				setVisible(false);
				}
			}
		}
	}
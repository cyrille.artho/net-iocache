package gov.nasa.jpf.network.cache;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.*;

/**
 * This class represents a socket created from a file descriptor.
 * @author watcharin
 *
 */
public class FdSocket extends Socket {

	private FileInputStream in;
	
	private FileOutputStream out;
	
	private int fd;
	
	private boolean connected = true;
	
	public FdSocket(int fd) {
		Class<FileDescriptor> clazz = FileDescriptor.class;
		Constructor<FileDescriptor> c;
		FileDescriptor objFd;
		
		this.fd = fd;
		
		try {
			c = clazz.getDeclaredConstructor(new Class[] { Integer.TYPE });
		} catch (SecurityException e) {
			e.printStackTrace();
			return;
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			return;
		}

		c.setAccessible(true);
		
		try {
			objFd = c.newInstance(new Integer(fd));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return;
		} catch (InstantiationException e) {
			e.printStackTrace();
			return;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return;
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return;
		}

		out = new FileOutputStream(objFd);
		in = new FileInputStream(objFd);
	}
	
	public boolean isConnected() {
		return connected;
	}
	
	public void close() throws IOException {
		connected = false;
		DMTCPProxyCacheLayer.closeSocket(fd);
	}
	
	public boolean isClosed() {
		return !connected;
	}
	
	public void setSoTimeout(int timeout) throws SocketException {
		// Do nothing.
	}
	
	public InetAddress getInetAddress() {
		try {
			return InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public int getPort() {
		// Not support.
		assert false;
		return -1;
	}
	
	public int getLocalPort() {
		// Not support.
		assert false;
		return -1;
	}
	
	public InputStream getInputStream() throws IOException {
		return in;
	}
	
	public OutputStream getOutputStream() throws IOException {
		return out;
	}
	
	public int getFd() {
		return fd;
	}
	
}

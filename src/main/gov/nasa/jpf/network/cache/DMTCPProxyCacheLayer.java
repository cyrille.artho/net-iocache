package gov.nasa.jpf.network.cache;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.network.HelperMethods;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DMTCPProxyCacheLayer extends CheckpointCacheLayer {

	class DMTCPRestart extends Thread {

		private int state;

		private Logger peerLog = JPF.getLogger(PEER_LOG);

		DMTCPRestart(int state) {
			this.state = state;

			// This thread is no longer needed after the main process ends.
			setDaemon(true);
		}

		public void run() {
			ProcessBuilder builder;
			Process p;
			BufferedReader is;
			String line;

			builder = new ProcessBuilder(restoreScript(), checkpointDir,
					String.valueOf(state));
			// Do not remove this statement. It is somehow important.
			builder.redirectErrorStream(true);
			// Prepare before restoring.
			onPreRestore();

			try {
				p = builder.start();
				is = new BufferedReader(new InputStreamReader(
						p.getInputStream()));

				// Log standard output and error of peer. This loop is
				// necessary, even in the case where the log output is not
				// set since this thread must be kept alive until the
				// restart process terminates.
				while ((line = is.readLine()) != null) {
					peerLog.info(String.format("[PEER %s]%s", this, line));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	class PeerInfoProcessingThread extends Thread {
		ServerSocket server;

		public PeerInfoProcessingThread() throws IOException {
			server = new ServerSocket(sutCommPort);
		}

		public void run() {
			while (true) {
				Socket s;
				InputStream in;
				int c;

				try {
					s = server.accept();

					synchronized (this) {
						if (peerLock) {
							s.close();
							notify();
							continue;
						}

						in = s.getInputStream();
						c = in.read();

						if (c == 1) {
							// Process ND flag.
							log.fine("[PeerInfoProcessingThread : run] Receive ND Flag");
							setNDFlag();
						}

						s.close();
						notify();
					}
				} catch (IOException e) {
					log.warning("[PeerInfoProcessingThread : run] " + e);
				}
			}
		}
	}

	private static final CacheLayer INSTANCE = new DMTCPProxyCacheLayer();

	private static final String JNI_LIB = "proxycache";

	private static final String UNIX_SOCK_NAME = "slave-mc";

	/**
	 * Assume the working directory is "bin".
	 */
	private static final String DMTCP_CMD = "../tools/dmtcp/bin/dmtcp_command";

	private static final String OPT_KILL = "-k";

	/**
	 * Option for 'dmtcp_command' to obtain the number of nodes running in DMTCP.
	 */
	private static final String OPT_STATUS = "-s";

	private static final int PACKET_CONNECT_SIZE = 6;

	private static final int PACKET_ACCEPT_SIZE = 2;

	private static final int PACKET_CLOSE_SIZE = 2;

	private static final int PACKET_CREATE_SOCK_SIZE = 1;

	private static final int PACKET_BIND_SIZE = 6;

	private static final byte OP_CONNECT = 1;

	private static final byte OP_ACCEPT = 2;

	private static final byte OP_CLOSE = 3;

	private static final byte OP_CREATE_SOCK = 4;

	private static final byte OP_BIND = 5;

	/**
	 * A mapping from MC FDs to Proxy FDs.
	 */
	private static Map<Integer, Integer> fdMap = new HashMap<Integer, Integer>();

	private static Thread peerInfoProcessing;

	private static Thread activeRestartThread;

	private static int proxyChannel;

	private static int sutCommPort = 8793;

	private static boolean peerLock = false;

	public native static int createUnixDomainChannel(String sockName);

	/**
	 * 
	 * @param fd
	 * @return 0 iff the channel is successfully closed.
	 */
	public native static int closeSocket(int fd);

	public native static int sendViaUnixChannel(int fd, byte[] data, int len);

	/**
	 * Receive data from the specified Unix domain socket.
	 * 
	 * @param fd
	 *            A Unix domain socket
	 * @param buf
	 *            Output parameter.
	 * @param len
	 *            The length of <i>buf</i>.
	 * @return The number of bytes received.
	 */
	public native static int recvViaUnixChannel(int fd, byte[] buf, int len);

	/**
	 * Receive data and a FD from the specified Unix domain socket.
	 * 
	 * @param fd
	 *            A Unix domain socket.
	 * @param buf
	 *            Output parameter.
	 * @param len
	 *            The length of <i>buf</i>.
	 * @param newFd
	 *            The received FD is filled in the first element of this array.
	 * @return The number of bytes received.
	 */
	public native static int recvFd(int fd, byte[] buf, int len, int[] newFd);

	public static CacheLayer getInstance() {
		return INSTANCE;
	}

	public static void setSutCommPort(int port) {
		sutCommPort = port;
	}

	public void onStart() {
		System.loadLibrary(JNI_LIB);

		// Set up a thread for processing the information provided by peers.
		try {
			peerInfoProcessing = new PeerInfoProcessingThread();
		} catch (IOException e) {
			assert false;
		}

		peerInfoProcessing.setDaemon(true);
		// Must start this thread first, because the peer may send a flag before
		// Proxy starts.
		peerInfoProcessing.start();

		// Set up a Unix domain connection between MC and the proxy.
		proxyChannel = createUnixDomainChannel(UNIX_SOCK_NAME);
	}

	public void onFinish() {
		// Remove the Unix domain socket file.
		File f = new File(UNIX_SOCK_NAME);

		onPreRestore();

		f.delete();
	}

	public String killPeerScript() {
		return cacheDir + "/bin/kill_coordinator.sh";
	}

	public void close(int id) throws IOException {
		super.close(id);

		byte[] packet;
		byte[] buf = new byte[8];
		PhysicalConnection conn = conn_list.get(id);
		FdSocket sock = (FdSocket) conn.getSocket();
		int fd = sock.getFd();
		int len;

		// Close the socket if not done.
		if (fdMap.containsKey(fd)) {
			packet = buildPacketClose(fd);
			sendViaUnixChannel(proxyChannel, packet, packet.length);
			len = recvViaUnixChannel(proxyChannel, buf, buf.length);
			assert (len > 0 && buf[2] == 0);

			// Unregister this FD.
			fdMap.remove(fd);
		}
	}
	
	public boolean isServerSocketClosed() {
		// Not really check if the socket is closed.
		return wait_socket == null;
	}

	String restoreScript() {
		return cacheDir + "/bin/restore_checkpoint-proxy.sh";
	}

	String saveStateScript() {
		return cacheDir + "/bin/savestate-dmtcp.sh";
	}

	void addSequenceNumbers() {
		// Do nothing about sequence numbers.
	}

	void saveSequenceNumbers() {
		// Do nothing about sequence numbers.
	}

	void restoreSequenceNumbers(int stateId) {
		// Do nothing about sequence numbers.
	}

	void resetSeqNumbers(int connId) {
		// Do nothing about sequence numbers.
	}

	void createWaitingSocket(int port) throws IOException {
		if (wait_socket == null) {
			// Create a new socket first.
			int[] fds = createSocket();
			byte[] packet;
			byte[] buf = new byte[32];

			// Register a pair of MC FD and Proxy FD.
			fdMap.put(fds[0], fds[1]);

			assert port > 1024 : "Wrong port";
			// Have the proxy bind the socket.
			packet = buildPacketBind(fds[0], port);
			sendViaUnixChannel(proxyChannel, packet, packet.length);
			recvViaUnixChannel(proxyChannel, buf, buf.length);
			assert buf[2] == 0;

			wait_socket = fds[0];
		}
	}

	Object acceptPeer() {
		int[] newFd = new int[1];
		byte[] packet = buildPacketAccept((Integer) wait_socket);
		byte[] buf = new byte[32];
		int proxyFd;

		// Have Proxy accept a connection.
		sendViaUnixChannel(proxyChannel, packet, packet.length);
		recvFd(proxyChannel, buf, buf.length, newFd);
		proxyFd = buf[2];
		assert proxyFd > 0;
		log.fine(String.format(
				"[PROXY_CACHE : acceptPeer] Register FD(%d, %d)", newFd[0],
				proxyFd));
		fdMap.put(newFd[0], proxyFd);

		try {
			server_addr = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			assert false;
		}

		strategy.setIOFlag();

		// Return the received FD.
		return newFd[0];
	}

	PhysicalConnection setNewConnection(Object newSocket, int id)
			throws IOException {
		assert server_addr != null : "[VIRT_CACHE] Peer address is null";
		assert newSocket instanceof Integer : "[CHKPNT_CACHE] FD is not a number";
		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));
		PhysicalConnection new_conn;
		int proxyFd = fdMap.get(newSocket);

		rr_tree.setConnId(id);

		new_conn = new ProxyPhysicalConnection(proxyFd, new FdSocket(
				(Integer) newSocket), rr_tree.getSendPtr(), rr_tree.limit());

		storePhysicalConnection(id, new_conn);

		return new_conn;
	}

	Object createSocketToPeer(InetAddress addr, int port, int id) {
		// Create a new socket first.
		int[] fds = createSocket();
		fdMap.put(fds[0], fds[1]);
		byte[] packet = buildPacketConnect(fds[0], port);
		byte[] buf = new byte[32];

		// Have Proxy connect to a port.
		sendViaUnixChannel(proxyChannel, packet, packet.length);
		recvViaUnixChannel(proxyChannel, buf, buf.length);
		assert buf[2] == 0 : "Fail to connect";

		strategy.setIOFlag();

		return fds[0];
	}

	String checkpointName(int stateId) {
		return String.format("state-%d-*.dmtcp", stateId);
	}

	void setOffRestartingProcess(int restoredState) {
		// Kill the running peers first.
		killDMTCPNodes();

		if (activeRestartThread != null) {
			// Wait until the current restart thread terminates.
			try {
				activeRestartThread.join();
			} catch (InterruptedException e) {
				assert false;
			}
		}

		activeRestartThread = new DMTCPRestart(restoredState);
		activeRestartThread.start();
	}

	void onPostCheckpoint(int stateId) {
		// Setup a Unix domain connection between MC and the proxy.
		proxyChannel = createUnixDomainChannel(UNIX_SOCK_NAME);
		log.info(String
				.format("[PROXYCACHE : onPostCheckpoint(%d)] createUnixDomainChannel OK",
						stateId));

		// Enable Peer Info Processing Thread.
		synchronized (peerInfoProcessing) {
			peerLock = false;
			peerInfoProcessing.notify();
		}
	}

	void onPostRestore(int checkpoint) {
		Map<Integer, Integer> newMap = new HashMap<Integer, Integer>();
		int[] newFd = new int[1];
		byte[] proxyFd = new byte[1];
		byte[] n = new byte[1];
		boolean waitSockChanged = false;

		onPostCheckpoint(checkpoint);

		// Receive the number of FDs to receive.
		recvViaUnixChannel(proxyChannel, n, 1);

		// Receive a FD 'n' times and update 'fdMap'.
		for (int i = 0; i < n[0]; i++) {
			FdSocket sock;
			PhysicalConnection matchingConn;

			// proxyFd is a number recognized by Proxy.
			// newFd is a number recognized by this process (SUT/Cache).
			recvFd(proxyChannel, proxyFd, 1, newFd);

			// If this FD is for listening, change the value of "wait_socket".
			if (wait_socket != null && !waitSockChanged
					&& proxyFd[0] == fdMap.get(wait_socket)) {
				wait_socket = newFd[0];
				waitSockChanged = true;

				newMap.put(newFd[0], (int) proxyFd[0]);
			}
			// Otherwise, this FD is bound to a connection.
			else {
				// Find the connection that holds this Proxy FD.
				matchingConn = findConnection(proxyFd[0], checkpoint);

				// If Proxy has an active connection that the SUT no longer uses, send proxy command "close".
				if (matchingConn == null) {
					byte[] packet = buildPacketClose(newFd[0]);
					byte[] buf = new byte[8];
					int len;
					
					sendViaUnixChannel(proxyChannel, packet, packet.length);
					len = recvViaUnixChannel(proxyChannel, buf, buf.length);
					assert (len > 0 && buf[2] == 0);
				}
				else {
					// Setup a connection by the new FD.
					sock = new FdSocket(newFd[0]);

					matchingConn.changeSocket(sock);
					newMap.put(newFd[0], (int) proxyFd[0]);
				}
			}
		}

		// Assign a new map.
		fdMap = newMap;
	}

	void onLaunchingCheckpointCmd(int stateId) {
		synchronized (peerInfoProcessing) {
			while (peerLock) {
				try {
					peerInfoProcessing.wait();
				} catch (InterruptedException e) {
				}
			}

			peerLock = true;
		}

		// Close the proxy channel.
		closeSocket(proxyChannel);
	}

	boolean checkpointExists(int state) {
		final int STATE = state;
		File dir = new File(checkpointDir);
		FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(CHECKPOINT_PREFIX + STATE);
			}
		};
		String[] accepted = dir.list(filter);
		assert accepted != null;

		return accepted.length > 0;
	}

	String isPeerRunningScript() {
		// If Proxy is the only node left, the peer will be considered
		// terminated.
		return cacheDir + "/bin/is_peer_running.sh";
	}

	@Override
	int numPeerProcesses() {
		// Obtain DMTCP status by command 'dmtcp_command -s'.
		ProcessBuilder builder = new ProcessBuilder(DMTCP_CMD, OPT_STATUS);
		BufferedReader r;
		// Build a pattern for capturing the number of peers from output.
		Pattern pat = Pattern.compile("NUM_PEERS=([0-9]+)");
		String line;
		int num = -1;
		
		try {
			Process p = builder.start();
			r = new BufferedReader(new InputStreamReader(p.getInputStream()));
			
			while ((line = r.readLine()) != null) {
				Matcher m = pat.matcher(line);
		
				// Is it the line we are interested?
				if (m.matches()) {
					// Group 1 is the number of nodes in the output line.
					num = Integer.parseInt(m.group(1));
					break;
				}
			}
			
			p.waitFor();
		} catch (IOException e1) {
			assert false;
		} catch (InterruptedException e) {
			assert false;
		}
		
		return num;
	}
	
	private byte[] buildPacketConnect(int fd, int addr) {
		byte[] packet = new byte[PACKET_CONNECT_SIZE];
		assert fdMap.containsKey(fd) : "Unknown FD";
		int remoteFd = fdMap.get(fd);
		assert remoteFd > 0;

		packet[0] = OP_CONNECT;
		packet[1] = (byte) remoteFd;
		HelperMethods.intToByteArray(addr, packet, 2);

		return packet;
	}

	private byte[] buildPacketAccept(int fd) {
		byte[] packet = new byte[PACKET_ACCEPT_SIZE];
		int remoteFd = fdMap.get(fd);
		assert remoteFd > 0;

		packet[0] = OP_ACCEPT;
		packet[1] = (byte) remoteFd;

		return packet;
	}

	private byte[] buildPacketClose(int fd) {
		byte[] packet = new byte[PACKET_CLOSE_SIZE];
		assert (fdMap.containsKey(fd));
		int remoteFd = fdMap.get(fd);
		assert remoteFd > 0;

		packet[0] = OP_CLOSE;
		packet[1] = (byte) remoteFd;

		return packet;
	}

	private byte[] buildPacketCreateSocket() {
		return new byte[] { OP_CREATE_SOCK };
	}

	private byte[] buildPacketBind(int fd, int addr) {
		byte[] packet = new byte[PACKET_BIND_SIZE];
		int remoteFd = fdMap.get(fd);
		assert remoteFd > 0;

		packet[0] = OP_BIND;
		packet[1] = (byte) remoteFd;
		HelperMethods.intToByteArray(addr, packet, 2);

		return packet;
	}

	/**
	 * Have Proxy create a new socket.
	 * 
	 * @return A two-element array. The first element is a FD for MC. Another
	 *         element is a FD for Proxy.
	 */
	private int[] createSocket() {
		int[] newFd = new int[1];
		byte[] packet = buildPacketCreateSocket();
		byte[] buf = new byte[32];
		int proxyFd;

		// Have the proxy create a new socket.
		sendViaUnixChannel(proxyChannel, packet, packet.length);
		// Receive new FD.
		recvFd(proxyChannel, buf, buf.length, newFd);
		proxyFd = buf[1];

		return new int[] { newFd[0], proxyFd };
	}

	private int toSutFd(int proxyFd) {
		for (int fd : fdMap.keySet()) {
			if (fdMap.get(fd) == proxyFd) {
				return fd;
			}
		}

		return -1;
	}

	private PhysicalConnection findConnection(int fd) {
		return findConnection(fd, -1);
	}

	/**
	 * Find the connection corresponding to the specified Proxy FD.
	 * 
	 * @param fd
	 *            A Proxy FD.
	 * @param checkpointId
	 *            The Id of the checkpoint being restored, or -1 if not during
	 *            restarting.
	 * @return The matching connection.
	 */
	private PhysicalConnection findConnection(int fd, int checkpointId) {
		int idx = 0;

		for (PhysicalConnection p : conn_list) {
			if (p == null)
				continue;
			
			ProxyPhysicalConnection ppc = (ProxyPhysicalConnection) p;

			// Do not care if the connections are closed.
			if (checkpointId < 0) {
				if (ppc.getProxyFd() == fd) {
					return p;
				}
			}
			// Consider only open connections.
			else {
				Milestone m = milestones.get(checkpointId);

				if (!m.isClosed(idx) && ppc.getProxyFd() == fd) {
					return p;
				}

				idx++;
			}
		}

		return null;
	}

	/**
	 * Close all socket FDs held by this process. This method should be called
	 * before restoring a peer from a checkpoint so that all handlers are
	 * released.
	 */
	private void onPreRestore() {
		for (int fd : fdMap.keySet()) {
			ProxyPhysicalConnection ppc;
			int proxyFd = fdMap.get(fd);

			ppc = (ProxyPhysicalConnection) findConnection(proxyFd);
			try {
				if (ppc != null)
					ppc.close();
				// Listening socket.
				else {
					closeSocket(fd);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void killDMTCPNodes() {
		ProcessBuilder builder;
		Process p;

		// Kill all nodes in DMTCP.
		builder = new ProcessBuilder(DMTCP_CMD, OPT_KILL);
		try {
			p = builder.start();
			p.waitFor();
		} catch (IOException e1) {
			assert false;
		} catch (InterruptedException e) {
			assert false;
		}
	}

}

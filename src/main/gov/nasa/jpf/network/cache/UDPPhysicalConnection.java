package gov.nasa.jpf.network.cache;

import gov.nasa.jpf.network.ncp.NCPPacket;

import java.io.*;
import java.net.*;
import java.util.*;

public class UDPPhysicalConnection extends PhysicalConnection {

	/**
	 * The time limit to wait for a response or ACK packet.
	 */
	private static final int SOCKET_TIMEOUT = 4000;

	private static final int WRITE_RETRY = 5;

	private Queue<Byte> inputQueue = new LinkedList<Byte>();

	private DatagramSocket socket;

	private InetAddress peerAddr;

	private byte[] buffer = new byte[4096];

	private int peerPort;
	
	private int sockid;

	UDPPhysicalConnection(int socketId, DatagramSocket sock, InetAddress addr, int port) throws IOException {
		socket = sock;
		peerAddr = addr;
		peerPort = port;
		sockid = socketId;
	}

	public boolean isConnected() {
		return socket.isConnected();
	}

	int available() throws IOException {
		DatagramPacket packet;
		NCPPacket npacket;
		byte[] content;
		long time;
		int len;
		int recvSeq;
		int expectedSeq = CheckpointCacheLayer.cur_expectedSeq.get(sockid);
		boolean fin = false;

		time = System.currentTimeMillis();
		
		// Virtual non-blocking socket.
		socket.setSoTimeout(1);

		while (!fin) {
			packet = new DatagramPacket(buffer, buffer.length);

			try {
				socket.receive(packet);
				
				npacket = new NCPPacket(packet.getData());
				len = npacket.getData_len();
				content = npacket.getContent();
				recvSeq = npacket.getNumber();
				assert recvSeq <= expectedSeq : "[UDPPhysicalConnection] received seq > expected seq";
				
				ack(recvSeq);
				
				// The received packet is not the expected one.
				if (recvSeq < expectedSeq)
					continue;
				
				if (len > 0) {
					for (int i = 0; i < len; i++)
						inputQueue.add(content[i]);
					expectedSeq++;
				} else
					fin = true;
				
				// Check ND flag
				if (npacket.isNDFlagSet()) {
					((CheckpointCacheLayer) CheckpointCacheLayer.getInstance()).setNDFlag();
				}
			} catch (SocketTimeoutException e) {
				fin = true;
			}
		}

		socket.setSoTimeout(0);
		CheckpointCacheLayer.cur_expectedSeq.set(sockid, expectedSeq);

		log.info("[UDP : available] response time " + (System.currentTimeMillis() - time));
		return inputQueue.size();
	}

	int read() throws IOException {
		DatagramPacket packet;
		NCPPacket npacket;
		int len;
		int recvSeq;
		int expectedSeq = CheckpointCacheLayer.cur_expectedSeq.get(sockid);
		boolean reading = true;

		if (inputQueue.isEmpty()) {
			while (reading) {
				packet = new DatagramPacket(buffer, buffer.length);
				socket.receive(packet);

				npacket = new NCPPacket(packet.getData());
				len = npacket.getData_len();
				recvSeq = npacket.getNumber();
				assert recvSeq <= expectedSeq : "[UDPPhysicalConnection.read] received seq > expected seq";

				ack(recvSeq);

				// The received packet is not the expected one.
				if (recvSeq < expectedSeq)
					continue;

				if (len > 0) {
					for (int i = 0; i < len; i++)
						inputQueue.add(buffer[i]);
					reading = false;
				} else
					return -1;
				
				CheckpointCacheLayer.cur_expectedSeq.set(sockid, expectedSeq + 1);
				
				// Check ND flag
				if (npacket.isNDFlagSet()) {
					((CheckpointCacheLayer) CheckpointCacheLayer.getInstance()).setNDFlag();
				}
			}
		}

		return inputQueue.poll();
	}

	void write(byte c) throws IOException {
		write(new byte[] { c }, 0, 1);
	}

	void write(byte[] msg, int off, int len) throws IOException {
		DatagramPacket packet;
		NCPPacket npacket = null;
		byte[] wrappedMsg;
		int retry = 0;
		int sendSeq = CheckpointCacheLayer.cur_sendSeq.get(sockid);
		int recvSeq;
		int expectedSeq;
		boolean fin = false;
		boolean ackReceived = false;

		wrappedMsg = NCPPacket.createDataPacket(sockid, sendSeq, msg, off, len);
		
		socket.setSoTimeout(SOCKET_TIMEOUT);

		while (!fin && retry < WRITE_RETRY) {
			packet = new DatagramPacket(wrappedMsg, 0, wrappedMsg.length, peerAddr, peerPort);
			
			socket.send(packet);

			packet = new DatagramPacket(new byte[64], 64);

			try {
				while (!ackReceived) {
					socket.receive(packet);
					npacket = new NCPPacket(packet.getData());
					if (npacket.isAckPacket()) {
						ackReceived = true;
					} else {
						recvSeq = npacket.getNumber();
						byte[] content = npacket.getContent();
						log.fine("[UDP : write] while waiting for ACK, received content " + new String(content));
						expectedSeq = CheckpointCacheLayer.cur_expectedSeq.get(sockid);

						ack(recvSeq);

						if (recvSeq == expectedSeq) {
							if (len > 0) {
								for (int i = 0; i < len; i++)
									inputQueue.add(buffer[i]);
							}

							CheckpointCacheLayer.cur_expectedSeq.set(sockid, expectedSeq + 1);
						}
					}
				}
				
				assert npacket.getNumber() == sendSeq : "[UDPPhysicalConnection] The sequence number of a received ACK is wrong";
			} catch (SocketTimeoutException e) {
				retry++;
				continue;
			}
			
			fin = true;
		}

		socket.setSoTimeout(0);
		CheckpointCacheLayer.cur_sendSeq.set(sockid, sendSeq + 1);
		
		if (!ackReceived)
			log.warning("[UDP : write] Fail to write packet");
	}

	void close() throws IOException {
		if (socket != null) {
			// send an empty packet
//			DatagramPacket packet = new DatagramPacket(new byte[0], 0, peerAddr, peerPort);
//			socket.send(packet);
			socket.close();
		}
	}

	boolean isClosed() {
		return socket.isClosed();
	}

	private void ack(int seqNum) throws IOException {
		byte[] msg = NCPPacket.createAckPacket(sockid, seqNum);
		
		DatagramPacket packet = new DatagramPacket(msg, msg.length, peerAddr, peerPort);

		socket.send(packet);
	}

}

#!/bin/bash
# Verify the chat server.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

NUM_ACCEPT=2
NUM_WORKERS=2
if [ $# -gt 1 ]
then
	NUM_ACCEPT=$1
	NUM_WORKERS=$2
else
	echo "usage: chat-server-mc-bug.sh {num_accept} {num_workers}"
	exit
fi

PACKAGE="gov.nasa.jpf.network.chat"
PROG_ARG="${PACKAGE}.ChatServerBug ${NUM_ACCEPT} ${NUM_WORKERS}"
JPF_ARG="${JPF_ARG} +${OPTION_PREFIX}.boot.peer.command=${SCRIPT_DIR}/chat-client.sh +${OPT_MAIN_TERMINATION}=false"
SERVER_LOG="$HOME/chat-server-bug.log"

echo "Start model-checking a buggy chat server, with ${NUM_WORKERS} worker(s), waiting for ${NUM_ACCEPT} client(s) ..."
echo ${RUN_EXAMPLE_CMD} ${JPF_ARG} ${PROG_ARG}
${RUN_EXAMPLE_CMD} ${JPF_ARG} ${PROG_ARG}


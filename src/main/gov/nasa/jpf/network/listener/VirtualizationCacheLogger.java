package gov.nasa.jpf.network.listener;

import gov.nasa.jpf.network.cache.CheckpointCacheLayer;

public class VirtualizationCacheLogger extends CacheLogger {
	
	void showStats() {
		super.showStats();
		log.info("Peer BACKTRACKING: " + CheckpointCacheLayer.getPeerBacktrack());
		log.info("Checkpoint RESTORATION: " + CheckpointCacheLayer.getNumRestoration());
		log.info("Saved STATES: " + CheckpointCacheLayer.getNumSavedStates());
	}
	
}

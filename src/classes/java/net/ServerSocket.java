//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package java.net;

import gov.nasa.jpf.annotation.FilterField;

import java.io.IOException;
import java.nio.channels.ServerSocketChannel;

public class ServerSocket {

	int port = -1;

	private boolean bound = false;
	private boolean closed = false;

	private InetAddress bindAddr;
	private ServerSocketChannel ssChannel=null;     //channel that may be associated with the ServerSocket
	
	/**
	 *  counter for the number of calls to accept();
	 *  Annotation FilterField advises jpf to disregard 
	 *  acceptCount for state matching, i.e. changes to
	 *  acceptCount do not cause a transition to a new program state.
	 *  @see #accept() 
	 *  @see "JPF Annotations" on http://babelfish.arc.nasa.gov/trac/jpf/wiki/user/api  
	 */
	@FilterField private static int bindCount=0;
	@FilterField private static int acceptCount=0;

	public ServerSocket() {
		// unbound instance
	}

	/**
	 * create a new server socket and bind it to a port. 
	 * @param port the server socket is bound to wait for incoming connection requests
	 * @throws IOException
	 */
	public ServerSocket(int port) throws IOException {
		bind(new InetSocketAddress(port));
	}

	public ServerSocket(int port, int backlog) throws IOException {
		// Ignore backlog.
		this(port);
	}

	public ServerSocket(int port, int backlog, InetAddress bindAddr)
			throws IOException {
		this(port);
		this.bindAddr = bindAddr;
		bind(new InetSocketAddress(bindAddr, port));
	}
	
	public ServerSocket(ServerSocketChannel ssCh) {
		ssChannel=ssCh;
	}

	/**
	 * creates a <i>native</i> (i.e., cache level) server socket and binds it 
	 * to the specified <b>logicalPort</b>;
	 * Note that the native port used by the CacheLayer may be different 
	 * from <b>logicalPort</b> if 'jpf-net-iocache.dynamicPort=true' 
	 * @param logicalPort 
	 *        port where the server socket will accept connection requests from the remote peer;</br>
	 *        If <b>port</b>==0 a server socket will be created at a free port chosen by the system 
	 *        (see also <b>return</b> value);  
	 * @return >0 : port at which the server socket has been created 
	 *              (equal to parameter <b>logicalPort</b> if <b>logicalPort</b>>0)
	 * @throws IOException if server socket cannot be bound, e.g., BindException if the specified <b>port</b> is already in use.
	 * @see #bind(SocketAddress)
	 * @see 'jpf-net-iocache.dynamicPort' in file jpf.properties
	 */
	private native int nativeBind(int logicalPort) throws IOException;

	/**
	 * binds this {@link ServerSocket} to the specified {@link SocketAddress} and 
	 * creates a corresponding <i>physical</i> peer server socket;
	 * @param address where the server socket will accept connection requests from the remote peer; Note: 
	 * <ul>
	 * <li>Only the port of <b>address</b> will be considered; <b>Host address</b> must be 
	 * <tt>localhost</tt> or a <i>loop back</i> address;</li>
	 * <li>If <b>address</b><tt>==null</tt> or the port of <b>address</b> is equal to 0 
	 * then the server socket will be bound to a free port; 
	 * Use {@link #getLocalPort()} to determine the port the server socket 
	 * has been bound to;</li>
	 * </ul>
	 * @throws IOException
	 * @see #nativeBind(int)
	 */
	public void bind(SocketAddress address)  throws IOException {
		checkOpen();
		checkNotBound();
		if (address==null) {
			this.port = nativeBind(0);
		} else {
			assert address instanceof InetSocketAddress : "Parameter of model method ServerSocket.bind must be InetSocketAddress.";
			InetAddress iaddr=((InetSocketAddress) address).getAddress();
			assert 	iaddr==null || 
					iaddr.equals(InetAddress.getLocalHost()) || 
					iaddr.isLoopbackAddress() :  
				    "Host address of parameter of model method ServerSocket.bind must be localhost or a loopback address";
			this.port = nativeBind(((InetSocketAddress) address).getPort());
		}
		this.bindAddr=InetAddress.getLocalHost();
		setBound();
	}
	
	public boolean isBound() {
		synchronized (this) {
			return bound;
		}
	}

	/**
	 * Notify objects waiting on this. Clients want to wait until ServerSocket is bound.
	 */
	private void setBound() { //FIXME add support for more than one server socket to CacheLayer, see also #close() 
		assert bindCount==0: "Net-iocache does not support binding multiple server sockets, yet! Please use only one server socket."; 
		synchronized (this) {
			bindCount++;
			bound = true;     //Note: server socket cannot be unbound
			this.notifyAll();
		}
	}

	
	/**
	 *   nativeAccept() will trigger the launch of the client application 
	 *   which is supposed to send a connection request to the server under test. 
	 *   This connection request of the launched client will be accepted on 
	 *   the native level and caching structures will be initialized in the 
	 *   cache layer for the newly created socket. 
	 *   nativeAccept is a blocking accept;  
	 *   If 'jpf-net-iocache.maxAccept' is set to
	 *   a number N>=0, nativeAccept may instruct jpf to stop 
	 *   exploring the current state to prune the state space. 
	 *   See also comments in jpf.properties. 
	 *   @return ID of currently accepted socket.
	 *   @see net.ServerSocket.accept(), nio.ServerSocketChannel.accept(),
	 *        peer JPF_java_net_ServerSocket.nativeAccept__I__I
	 */
	private native int nativeAccept(int acceptCount) throws IOException;

	/**
	 * trigger blocking physical accept of a new client connection by net-iocache.
	 * FIXME proper simulation of blocking accept;
	 *       As for now, a thread calling accept() will never go into state BLOCKED.
	 *       Hence, accept() behaves like a non-blocking operation on the model level
	 *       (although the native peer might block for a certain time on the jpf level).  
	 * @return accept Socket to communicate with the remote client peer
	 * @throws IOException
	 * @see {@link #nativeAccept()}
	 */
	public Socket accept() throws IOException {
		checkBound();
		checkOpen();
		Socket s = new Socket();	//CAUTION (side effect): 
		                            //it is important to create the socket s 
        							//before calling nativeAccept, because 
        							//the native counterpart of the socket s
									//will be used by nativeAccept().   
		int nativeSocketID=nativeAccept(acceptCount++);  
		//FIXME if nativeAccept times out, this thread should block and try again, 
		// similar to the case of read() (see CacheLayerInputStream.waitForReadReady()), 
		// rather than throwing an IOException 'Native accept timed out' 
		// (see unit test ServerSocketTest.java)
		
		//if nativeAccept returns -1, the following assertion is not reached 
		//because either an IOException is thrown or the search backtracks 
		//to a previous state
		assert s.socketID==nativeSocketID : "[SocketServer.accept] model socket id '"+
				s.socketID+"' should be equal to physical socket id '"+nativeSocketID+
				"' as returned by native_accept()"; 
		return s;		
	} 

	private void checkBound() throws SocketException {
		if (!isBound())
			throw new SocketException("Socket is not bound yet");
	}
	
	private void checkNotBound() throws SocketException {
		if (isBound()) 
			throw new SocketException("Already bound");
	}
	
	private void checkOpen() throws SocketException {
		if (isClosed())
			throw new SocketException("Socket is closed");
	}
	
	/**
	 * @return true if ServerSocket is closed
	 */
	public boolean isClosed() {
		return closed;
	}

	/**
	 * set this server socket to closed. Note that a closed server socket
	 * cannot be reused (e.g. bound to a new port etc.) 
	 * 
	 * FIXME Add support of closing a server socket 
	 * (and binding another one, see also {@link #bind(SocketAddress)}) 
	 * At current, the corresponding native ServerSocket of the cache layer 
	 * remains open. This is because closing the native server socket may cause 
	 * problems when backtracking to a state where the model level server socket 
	 * is still open. Then the cache layer server socket would have to be
	 * rebound to the previously used port which may cause a 
	 *  java.net.BindException: 'Address already in use', because closed
	 * sockets remain in TIME_WAIT state for a while before their port 
	 * can be reused. While this issue may be solved by using 
	 * {@link #setReuseAddress(true)}, the SUT state is likely to become 
	 * inconsistent with the environment if an already closed
	 * server socket is suddenly open again (lingering connection requests
	 * from remote peers send at a later state may be mistaken as 
	 * connection requests w.r.t. the backtracked earlier state).
	 * Hence when backtracking from a SUT state, where a server socket has 
	 * been closed, to a previous state, where it is open and accept() is 
	 * called, a save strategy requires to restart the remote peers and replay 
	 * the cache content to synchronize the state of the environment with the SUT.
	 *  
	 * @throws IOException
	 */
	public void close() throws IOException {
		if (!isClosed()) {        
			closed=true;
			bindCount = 0;
			if (getChannel()!=null)    //if there is a channel associated with this server socket... 
				getChannel().close();  //... close also the channel 
		}			                   //Note that this is thread-safe, since ssChannel will never become null, again, once it is not null.
	}

	public int getLocalPort() {
		return port;
	}

	public void setReuseAddress(boolean on) {
		// empty stub
		// TODO: Model TIME_WAIT state in TCP/IP
	}

	public InetAddress getInetAddress() {
		return bindAddr;
	}

	public SocketAddress getLocalSocketAddress() {
		if (!isBound()) {
			return null;
		}
		return new InetSocketAddress(getInetAddress(), getLocalPort());
	}
	
	/**
	 * Returns the unique ServerSocketChannel object associated with this socket, if any. 
	 * 
	 * @return the server-socket channel associated with this socket, 
	 * or null if this socket was not created for a channel
	 */
	public ServerSocketChannel getChannel() {
		return ssChannel;
	}
	
	/**
	 *   Dummy method to avoid compile error message in eclipse
	 *   for CacheLayer.java (this method is referred to in 
	 *   <tt>CacheLayer.bind(int)</tt>. 
	 *   The code here should never be executed!
	 */
	public void setSoTimeout(int timeout) throws SocketException {
		throw new UnsupportedOperationException("Net-iocache does not yet support method 'ServerSocket.setSoTimeOut(int)'.");
	}


}

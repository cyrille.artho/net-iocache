package classes.java.net;

import gov.nasa.jpf.util.test.TestJPF;

import java.net.InetSocketAddress;
import java.net.ServerSocket;

import org.junit.Test;

public final class ServerSocketTest extends TestJPF {

	static final int portbase=8888;
	static final String dynPortOn="+jpf-net-iocache.dynamicPort=true";
	static final String dynPortOff="+jpf-net-iocache.dynamicPort=false";
	
	@Test
	public void bindThrowsExceptionIfAlreadyBound() {
		if (verifyNoPropertyViolation()) {
			try {
				ServerSocket ss=new ServerSocket();
				InetSocketAddress addr = new InetSocketAddress("localhost", portbase);
				ss.bind(addr);
				ss.bind(addr);
				fail();
			}
			catch (Exception e) {
				if (!(e.getClass().getName().equals("java.net.SocketException") && 
						e.getMessage().equals("Already bound"))) fail();
			}
		}
	}

	@Test
	public void bindServerSocketToFreePortInStaticPortMode() {
		if (verifyNoPropertyViolation(dynPortOff)) {
			doBindServerSocketToFreePort();
		}
	}

	@Test
	public void bindServerSocketToFreePortInDynamicPortMode() {
		if (verifyNoPropertyViolation(dynPortOn)) {
			doBindServerSocketToFreePort();
		}
	}
	
	@Test
	public void bindServerSocketToFixedLogicalPortInStaticPortMode() {
		int port=portbase+2; //choose port different from the one used in the test case above
		if (verifyNoPropertyViolation(dynPortOff)) {
			doBindServerSocketToFixedPort(port);
		}
	}

	@Test
	public void bindServerSocketToFixedLogicalPortInDynamicPortMode() {
		int port=portbase+2; //choose the same port as in the test case above 
		                     // (in dynamic port mode, this should not be harmful)
		if (verifyNoPropertyViolation(dynPortOn)) {
			doBindServerSocketToFixedPort(port);
		}
	}

	private void doBindServerSocketToFreePort() {
		try {
			ServerSocket ss=new ServerSocket(0);  //this should bind a ServerSocket to a free port
			assert ss.getLocalPort()>0;  
			ss.close();
		}
		catch (Exception e) {
			fail();
		}
	}

	private void doBindServerSocketToFixedPort(int port) {
		try {
			ServerSocket ss=new ServerSocket(port);  
			assert ss.getLocalPort()==port;          
			ss.close();
		}
		catch (Exception e) {
			fail();
		}
	}


}

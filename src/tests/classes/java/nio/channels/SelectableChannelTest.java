package classes.java.nio.channels;

import gov.nasa.jpf.util.test.TestJPF;
//import gov.nasa.jpf.network.echo.Server;

import org.junit.Test;
//import static org.junit.Assert.*;

import java.nio.channels.*;

public class SelectableChannelTest extends TestJPF {
	@Test
	public void configureBlockingTest() throws Exception {
		if (verifyNoPropertyViolation()) {
			SelectableChannel [] channels = null;
			channels = new SelectableChannel [] {
					SocketChannel.open(),
					ServerSocketChannel.open()};
			for (int i = 0; i < channels.length; i++) {
				SelectableChannel channel = channels[i];
				channel.close();
				try {
					channel.configureBlocking(true);
					assertTrue(false);
				} catch (ClosedChannelException e) {
					// Correct result
				}
			}
		}
	}
}

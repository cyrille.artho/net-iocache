//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.*;

public class MockCacheLayer extends CacheLayer {

	boolean delayed = false;
	
	public MockCacheLayer() {
		this(false);
	}
	
	public MockCacheLayer(boolean delayed) {
		this.delayed = delayed;
	}
	
	protected Socket createSocket(InetAddress addr, int port) {
		return new MockSocket(addr, port, delayed);
	}

	protected static void reset() {
		CacheLayer.reset();
		
		// clear list of connections
		MockCacheLayer.conn_pool.clear();
		MockCacheLayer.conn_list.clear();
		MockCacheLayer.num_accept = new ArrayList<Integer>();
		
		MockCacheLayer.cur_accept = 0;
	}
	
	static void setLogger(Logger logger) {		
		log = logger;
	}
	
	static void changeTest(String testName) {
		Handler[] handlers = log.getHandlers();
		
		assert handlers.length <= 1;
		if (handlers.length != 0) {
			log.removeHandler(handlers[0]);
		}
		
		try {
			log.addHandler(new FileHandler(testName + ".xml"));
		} catch (SecurityException e) {
			e.printStackTrace();
			assert false;
		} catch (IOException e) {
			e.printStackTrace();
			assert false;
		}
	}
	
}

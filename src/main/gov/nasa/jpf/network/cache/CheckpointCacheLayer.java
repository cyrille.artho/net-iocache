package gov.nasa.jpf.network.cache;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.network.cache.event.Event;
import gov.nasa.jpf.network.cache.event.RecvByteEvent;
import gov.nasa.jpf.network.cache.event.SendByteEvent;
import gov.nasa.jpf.network.cache.strategy.CheckpointingStrategy;
import gov.nasa.jpf.network.cache.strategy.IOOnlyStrategy;
import gov.nasa.jpf.network.ncp.NCPPacket;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Logger;

public abstract class CheckpointCacheLayer extends CacheLayer {

	class MTCPRestart extends Thread {
		
		private int state;
		
		private Logger peerLog = JPF.getLogger(PEER_LOG);
		
		MTCPRestart(int state) {
			this.state = state;
		}
		
		public void run() {
			ProcessBuilder builder;
			Process p;
			BufferedReader is;
			String line;
			
			builder = new ProcessBuilder(restoreScript(), checkpointDir + "/state-"
					+ state + ".mtcp");
			// Do not remove this statement. It is somehow important.
			builder.redirectErrorStream(true);
			
			try {
				p = builder.start();
				is = new BufferedReader(new InputStreamReader(p
						.getInputStream()));

				// Log standard output and error of peer. This loop is
				// necessary, even in the case where the log output is not set
				// since this thread must be kept alive until the MTCP process
				// terminates.
				while ((line = is.readLine()) != null) {
					String msg = "[PEER " + this + "]" + line;
					
					if (line.startsWith("mtcp"))
						peerLog.fine(msg);
					else
						peerLog.info(msg);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	static final String PEER_LOG = "gov.nasa.jpf.network.peer";
	
	static final String CHECKPOINT_PREFIX = "state-";
	
	private static final int RESTORE_DELAY = 150;
	
	/**
	 * If nothing arrives after <i>SOCKET_TIMEOUT</i>, the socket is considered to be closed.
	 */
	private static final int SOCKET_TIMEOUT = 20000;

	static String checkpointDir;

	protected CheckpointingStrategy strategy = new IOOnlyStrategy(this);
	
	Object wait_socket;
	
	/**
	 * <i>parent[i]</i> is the ID of the parent of state <i>i</i>.
	 */
	private static List<Integer> parents = new ArrayList<Integer>();

	/**
	 * <i>peerPointers[i]</i> is the position of the last request the peer got
	 * from connection <i>i</i>.
	 */
	private static List<Integer> peerPointers = new ArrayList<Integer>();

	/**
	 * The table of all checkpoints. (State ID) &lt;-&gt; (Checkpoint)
	 */
	private static Hashtable<Integer, Checkpoint> checkpointPool = new Hashtable<Integer, Checkpoint>();

	private static int numSavedStates = 0;
	
	/**
	 * The number of times checkpoints are restored.
	 */
	private static int numRestoration = 0;
	
	/**
	 * The number of times the peer is backtracked.
	 */
	private static int peerBacktrack = 0;
	
	private static int curState = -1;
	
	/**
	 * sendSeqs[i] is for state i.
	 */
	private List<List<Integer>> sendSeqs = new ArrayList<List<Integer>>();

	/**
	 * expectedSeqs[i] is for state i.
	 */
	private List<List<Integer>> expectedSeqs = new ArrayList<List<Integer>>();

	/**
	 * cur_sendSeq[i] is for logical connection i.
	 */
	static List<Integer> cur_sendSeq = new ArrayList<Integer>();

	/**
	 * cur_expectedSeq[i] is for logical connection i.
	 */
	static List<Integer> cur_expectedSeq = new ArrayList<Integer>();
	
	/**
	 * The list of checkpointed state IDs.
	 */
	private static List<Integer> checkpointIds = new LinkedList<Integer>();

	/**
	 * Mapping from each checkpoint identifier to the number of checkpoint files.
	 */
	private static Map<Integer, Integer> numPeerCheckpoints = new HashMap<Integer, Integer>();

	/**
	 * This flag will be set if the cache needs to make a checkpoint at the next state change.
	 */
	private static boolean saveFlag = false;
	
	/**
	 * This flag will be set if the cache receives a ND notification. 
	 */
	private static boolean ndFlag = false;
	
	private List<Integer> peerPorts = new ArrayList<Integer>();

	/**
	 * If true, the cache will save peer state when condition are met. Otherwise, the cache
	 * will not try to save states.
	 */
	private boolean saveEnabled = true;
	
	protected CheckpointCacheLayer() {
	}
	
	public static void setCheckpointDir(String checkpointDirArg) {
		checkpointDir = checkpointDirArg;
	}
	
	public static int getNumSavedStates() {
		return numSavedStates;
	}
	
	public static int getNumRestoration() {
		return numRestoration;
	}
	
	public static int getPeerBacktrack() {
		return peerBacktrack;
	}
	
	public static boolean isSaveFlagSet() {
		return saveFlag;
	}
	
	public void changeState(int id) {
		int max_state_id = milestones.size() - 1;

		super.changeState(id);
		
		if (id < 0)
			return;
		// not visited before
		else if (id > max_state_id) {
			processNewState(id);
		}
		else {
			processVisitedState(id);
		}

		// This statement must be after processNew/VisitedState, because those
		// methods may reference the old value.
		curState = id;
	}

	public void write(int id, byte c) throws IOException {
		Event event_c = new SendByteEvent(c);
		SendRecvCache rr_tree;
		int treeId = stt.getTreeID(id);
		assert treeId >= 0 : "Negative tree ID";

		// obtain the corresponding tree and set the active connection
		rr_tree = commData.get(treeId);
		rr_tree.setConnId(id);
		
		// replayed message
		if (rr_tree.isNextSendEvent(event_c)) {
			rr_tree.moveSendPtr(event_c);
		}
		// Physically write
		else {
			// The current connection has sent nothing or the message can be added to some branch.
			if (rr_tree.canAdd(event_c) || rr_tree.getSendPtr() == 0) {
				rebindConnection(id);
				
				try {
					conn_list.get(id).write(c);
				} catch (IOException e) {
					// We do not handle exceptions during writing.
					assert false : "[CheckpointCacheLayer : write(IB)] Fail to write";
				}
				
				rr_tree.addSendEvent(event_c);
				conn_list.get(id).setOutStreamPos(rr_tree.getSendPtr());
				pollPeer(id);
				strategy.setIOFlag();
			}
			// Found a new message trace. Must restart the connection or restore a checkpoint
			else {
				log.fine("[CHKPNT_CACHE : write] Found a new message trace. " + event_c);
				rr_tree.addSendEvent(event_c);
				resyncPeer(rr_tree, id);
			}
			
			updatePeerPointer(id, rr_tree.getSendPtr());
		}
	}
	
	public void write(int id, byte[] msg, int off, int len) throws IOException {
		Event[] events = new Event[len];
		
		for (int i = 0;i < len;i++) {
			events[i] = new SendByteEvent(msg[off + i]);
		}
		
		SendRecvCache rr_tree;
		int treeId = stt.getTreeID(id);
		assert treeId >= 0 : "Negative tree ID";

		// obtain the corresponding tree and set the active connection
		rr_tree = commData.get(treeId);
		rr_tree.setConnId(id);
		
		int lastCommon;
		// Move through common bytes as much as possible.
		for (lastCommon = 0;lastCommon < len;lastCommon++) {
			// replayed message
			if (rr_tree.isNextSendEvent(events[lastCommon])) {
				rr_tree.moveSendPtr(events[lastCommon]);
			}
			else
				break;
		}

		// Something left to physically write
		if (lastCommon < len) {
			// If part of the message is common, shift the arrays first.
			if (lastCommon != 0) {
				int newLen = len - lastCommon;
				Event[] newEvents = new Event[newLen];
				byte[] newMsg = new byte[newLen];
				System.arraycopy(events, lastCommon, newEvents, 0, newLen);
				System.arraycopy(msg, lastCommon + off, newMsg, 0, newLen);

				events = newEvents;
				msg = newMsg;
				len = newLen;
				off = 0;
			}
			
			// The current connection has sent nothing or the message can be
			// added to some branch.
			if (rr_tree.canAdd(events[0]) || rr_tree.getSendPtr() == 0) {
				rebindConnection(id);

				try {
					conn_list.get(id).write(msg, off, len);
				} catch (IOException e) {
					// We do not allow exceptions during writing.
					assert false : "[CheckpointCacheLayer : write(IB[]II)] Fail to write";
				}

				for (Event e : events)
					rr_tree.addSendEvent(e);
				conn_list.get(id).setOutStreamPos(rr_tree.getSendPtr());
				pollPeer(id);
				strategy.setIOFlag();
			}
			// Found a new message trace. Must synchronize the SUT and peer.
			else {
				for (Event e : events)
					rr_tree.addSendEvent(e);

				resyncPeer(rr_tree, id);
			}

			// Must set the active connection again since some operations prior may change it.
			rr_tree.setConnId(id);
			assert rr_tree.getConnId() == id : "Wrong active connection";
			updatePeerPointer(id, rr_tree.getSendPtr());
		}
	}
	
	public int accept(int port) throws IOException {
		int num_connection = conn_list.size();
		int idle;

		// Create a waiting socket instance if not exist.
		createWaitingSocket(port);

		assert cur_accept <= num_connection : "there are accept calls more than the number of connections";
		// new connection
		if (conn_list.get(cur_accept) == null) {
			PhysicalConnection conn;
			
			strategy.setIOFlag();

			stt.setTreeID(cur_accept, commData.size() - 1);
			conn = createPhysicalConnection(cur_accept);
			
			onConnectionEstablished(conn);
			pollPeer(cur_accept);
		} else {
			idle = stt.unusedTree();
			stt.setTreeID(cur_accept, idle);
			getReady(idle);
		}

		return cur_accept++;
	}
	
	public String killPeerScript() {
		return cacheDir + "/bin/kill_mtcp.sh";
	}
	
	public void onStart() {
	}
	
	/**
	 * This method is called by a listener when the search is finished.
	 */
	public void onFinish() {
	}
	
	public void setSaveFlag() {
		saveFlag = true;
	}
	
	public void setStrategy(CheckpointingStrategy s) {
		strategy = s;
	}
	
	protected void processNewState(int id) {
		if (strategy.shouldSave(id)) {
			boolean success = savePeerState(id);
			
			if (success) {
				// Save the new checkpoint in the pool.
				checkpointPool.put(id, new Checkpoint(id, ndFlag, peerPointers));				
			}
		}

		resetSaveFlags();
		// save sending and expected sequence numbers
		saveSequenceNumbers();
		
		assert parents.size() == id : "Inconsistent parent-child relation";
		// Update the parent-child relation.
		log.fine(String.format("[CHKPNT : changeState] %d is the parent of %d", curState, id));
		parents.add(curState);
	}
	
	protected void processVisitedState(int id) {
	}
	
	void setNDFlag() {
		ndFlag = true;
		setSaveFlag();
	}
	
	void getReady(int id) {
		super.getReady(id);
		
		if (isServerSUT) {
			// Move the peer pointer to the accept node.
			updatePeerPointer(id, 1);
		}
	}
	
	PhysicalConnection createPhysicalConnection(int id) throws IOException {
		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));
		PhysicalConnection conn;
		Object newSocket = null;

		rr_tree.setConnId(id);
		restart++;

		// initiate a new socket
		// The cache layer can initiate a connection by itself.
		if (!isServerSUT) {
			newSocket = createSocketToPeer(server_addr, server_port, id);
		}
		// The cache waits for a connection.
		else {
			newSocket = acceptPeer();
		}

		conn = setNewConnection(newSocket, id);
		resetSeqNumbers(id);
		replay(id);
		
		// "getReady" also adds a peer pointer if necessary.
		if (isServerSUT)
			getReady(id);
		else
			peerPointers.add(0);
		
		return conn;
	}
	
	/**
	 * This method is only used in case of server-side SUT.
	 * 
	 * @return
	 */
	Object acceptPeer() {
		DatagramSocket socket = null;
		int port = 0;
		DatagramPacket packet = new DatagramPacket(new byte[64], 64);
		NCPPacket ncpPacket;
		byte[] data;

		try {
			boolean findingSocket = true;
			
			((DatagramSocket) wait_socket).receive(packet);

			// extract peer's address
			server_addr = packet.getAddress();
			port = packet.getPort();

			// convert to NCPPacket format
			ncpPacket = new NCPPacket(packet.getData());
			assert ncpPacket.isConnectPacket() : "[VIRTCACHE: WaitDatagramConnectionThread] not a CONNECT packet";

			// Find an available port.
			while (findingSocket) {
				try {
					int randPort = (int) (Math.random() * 64000 + 1024);
					socket = new DatagramSocket(randPort);
					findingSocket = false;
				} catch (BindException e) {
				}
			}
			
			// use the current number of connections as a file descriptor
			data = NCPPacket.createAcceptPacket(totalLogicalConnections());
			packet = new DatagramPacket(data, data.length, server_addr, port);

			// send ACK
			socket.send(packet);
		} catch (IOException e) {
			assert false : "[WaitDatagramConnectionThread : run()] IOException";
		}

		assert port > 0 : "No port";
		peerPorts.add(port);
		strategy.setIOFlag();

		return socket;
	}
	
	PhysicalConnection setNewConnection(Object newSocket, int id) throws IOException {
		assert server_addr != null : "[VIRT_CACHE] Peer address is null";
		assert newSocket instanceof DatagramSocket : "[CHKPNT_CACHE] New socket is not DatagramSocket";
		PhysicalConnection new_conn = new UDPPhysicalConnection(id, (DatagramSocket) newSocket, server_addr, peerPorts.get(id));

		storePhysicalConnection(id, new_conn);
		
		return new_conn;
	}
	
	/**
	 * Replay I/O operations from the beginning to the current positions of request/response pointers.
	 * 
	 * @param id The index of the PhysicalConnection instance.
	 * @throws IOException
	 */
	void replay(int id) throws IOException {
		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));
		PhysicalConnection conn = conn_list.get(id);
		List<Event> event_path;

		rr_tree.setConnId(id);
		event_path = rr_tree.getEventPath();

		for (Event e : event_path) {
			replayIO(e, conn);
		}
	}
	
	/**
	 * Replay I/O operations from the first request node after a given position
	 * to another given position.
	 * 
	 * @param id
	 *            The index of the PhysicalConnection instance.
	 * @param checkpointId
	 * 				The id of the checkpoint being loaded.          
	 * @param from
	 *            The start position. The node at this point is not replayed.
	 * @param to
	 *            The last position to be replayed.
	 * @throws IOException
	 */
	void replay(int id, int checkpointId, int from, int to) throws IOException {
		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));
		PhysicalConnection conn = conn_list.get(id);
		List<Event> event_path = rr_tree.getEventPath(from, to);
		boolean startReplay = false;
		
		// we do not need to replay the first request message
		event_path.remove(0);
		
		// replay I/O operations
		for (Event e : event_path) {
			// skip until finding the next request message.
			if (!startReplay && e instanceof SendByteEvent) {
				startReplay = true;
			}
			
			if (startReplay) {
				replayIO(e, conn);
			}
		}
	}
	
	/**
	 * Replay I/O operations from the saved request pointer to the current request pointer on every saved connection.
	 * @param checkpoint The ID of the checkpoint that was restored.
	 * @return The number of connections in <i>checkpoint</i>.
	 */
	int replayFromCheckpoint(int checkpoint) throws IOException {
		SendRecvCache rr_tree = commData.get(0);
		Milestone stone = milestones.get(checkpoint);
		int numSavedConns = stone.numConnections();
		int curReqPtr;
		int chkReqPtr;
		int numPeers = 0;
		int expected = numPeerCheckpoints.get(checkpoint);
		boolean ready = false;

		// Ensure all peer processes are successfully restored. Try 3 times at most.
		for (int i = 0; i < 3 && !ready; i++) {
			numPeers = numPeerProcesses();
			
			if (numPeers != expected) {
				try {
					Thread.sleep(RESTORE_DELAY);
				} catch (InterruptedException e) {
					assert false;
				}
			}
			else
				ready = true;
		}
		assert ready : String.format("[CheckpointCacheLayer : replayFromCheckpoint] Fail to load every peer node (%d/%d)", numPeers, expected);
		
		// must do this on every connection in the tree
		for (int i = 0;i < numSavedConns;i++) {
			// If the SUT is a server, change the tree every round.
			if (isServerSUT) {
				rr_tree = commData.get(i);
			}
			
			rr_tree.setConnId(i);
			
			curReqPtr = rr_tree.getSendPtr();
			chkReqPtr = stone.getReqPtr(i);
			
			// Nothing to replay
			if (chkReqPtr == curReqPtr) {
				continue;
			}
			assert chkReqPtr < curReqPtr : "[CHKPNTCACHE : replayFromCheckpoint] Request pointer from checkpoint is prior to current request pointer";
			
			replay(i, checkpoint, chkReqPtr, curReqPtr);
		}
		
		return numSavedConns;
	}

	/**
	 * If the SUT is not in sync with the peer, restore a new peer in sync from
	 * a checkpoint.
	 * 
	 * @param A logical connection id.
	 */
	void rebindConnection(int id) {
		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));
		int req;
		int peerPtr;

		rr_tree.setConnId(id);

		assert !peerPointers.isEmpty();

		req = rr_tree.getSendPtr();
		peerPtr = peerPointers.get(id);

		// The SUT is not in sync with the peer.
		if (req != peerPtr) {
			try {
				// Create a new peer in sync with the SUT.
				handleNewTrace(id, rr_tree.numLogicalConnection());
			} catch (IOException e) {
				assert false : String.format(
						"[CHKPNT_CACHE : rebindConnection(%d)] %s", id, e);
			}
		}
	}
	
	String restoreScript() {
		return cacheDir + "/bin/restore_checkpoint.sh";
	}
	
	abstract String isPeerRunningScript();
	
	abstract String saveStateScript();

	/**
	 * Add a new element to the current lists of sending/expected sequence
	 * numbers.
	 */
	void addSequenceNumbers() {
		cur_sendSeq.add(0);
		cur_expectedSeq.add(0);
	}
	
	/**
	 * Save sending and expected sequence numbers.
	 */
	void saveSequenceNumbers() {
		sendSeqs.add(new ArrayList<Integer>(cur_sendSeq));
		expectedSeqs.add(new ArrayList<Integer>(cur_expectedSeq));
	}
	
	/**
	 * Restore next sequence number and expected sequence number.
	 * @param stateId The Id of the restored state.
	 */
	void restoreSequenceNumbers(int stateId) {
		cur_sendSeq = new ArrayList<Integer>(sendSeqs.get(stateId));
		cur_expectedSeq = new ArrayList<Integer>(expectedSeqs.get(stateId));
	}
	
	abstract void createWaitingSocket(int port) throws IOException;
	
	Object createSocketToPeer(InetAddress addr, int port, int id) {
		DatagramSocket udpSock = null;
		DatagramPacket packet = null;
		byte[] msg = NCPPacket.createConnectPacket(totalLogicalConnections() - 1);
		final int MAX_RETRY = 3;
		int retry = 0;
		boolean freePortFound = false;
		boolean fin = false;

		// create a new datagram socket bound to a free port.
		while (!freePortFound) {
			try {
				udpSock = new DatagramSocket((int) (Math.random() * 64000) + 1024);
				freePortFound = true;
			} catch (SocketException e) {
			}
		}

		assert udpSock != null : "[VIRTCACHE : createDatagramSocket] Socket is null";

		try {
			udpSock.setSoTimeout(SOCKET_TIMEOUT);

			while (!fin && retry++ < MAX_RETRY) {
				// Connect
				packet = new DatagramPacket(msg, msg.length, addr, port);

				udpSock.send(packet);

				log.info("send CONNECT to " + addr + " port " + port);

				// accept ack
				packet = new DatagramPacket(new byte[64], 64);
				try {
					udpSock.receive(packet);
				} catch (IOException e) {
					if (e instanceof SocketTimeoutException) {
						continue;
					} else
						assert false : e;
				}

				log.info("recv CONNECT_ACK");
				fin = true;
				udpSock.setSoTimeout(0);
			}
			
			assert fin;
		} catch (SocketException e) {
			assert false : e;
		} catch (IOException e) {
			assert false : e;
		}

		strategy.setIOFlag();
		// update remote port
		port = packet.getPort();
		
		if (id < peerPorts.size()) {
			peerPorts.set(id, port);
		}
		else
			peerPorts.add(port);

		return udpSock;
	}
	
	void resetSeqNumbers(int connId) {
		int size = cur_sendSeq.size();
		assert size == cur_expectedSeq.size() : "[CHKPNTCACHE : resetSeqNumbers] Inconsistent size of arrays";
		
		if (size <= connId) {
			assert size == connId : "[CHKPNTCACHE : resetSeqNumbers] Size of array is too small";
			cur_sendSeq.add(0);
			cur_expectedSeq.add(0);
		}
		else {
			cur_sendSeq.set(connId, 0);
			cur_expectedSeq.set(connId, 0);
		}
	}
	
	/**
	 * Store <i>conn</i> as the physical connection corresponding to logical
	 * connection <i>id</i>.
	 * 
	 * @param id Logical connection Id.
	 * @param conn A physical connection.
	 */
	void storePhysicalConnection(int id, PhysicalConnection conn) {
		// newly created logical connection
		if (id >= conn_list.size()) {
			conn_list.add(conn);
		}
		// existed logical connection
		else {
			conn_list.set(id, conn);
		}
	}
	
	String checkpointName(int stateId) {
		return String.format("state-%d.mtcp", stateId);
	}
	
	void setOffRestartingProcess(int restoredState) {
		new MTCPRestart(restoredState).start();
	}
	
	/**
	 * Return the name of the script that blocks until a peer is successfully
	 * restored and sends a signal to the cache layer.
	 * 
	 * @return
	 */
	String syncScript() {
		return cacheDir + "/c-build/syncckpt";
	}
	
	/**
	 * Called after checkpoint a peer.
	 */
	void onPostCheckpoint(int stateId) {
	}
	
	/**
	 * Called after restore a checkpoint.
	 * @param stateId
	 */
	void onPostRestore(int stateId) {
	}
	
	/**
	 * This method is called before giving a checkpointing command to DMTCP.
	 * @param stateId
	 */
	void onLaunchingCheckpointCmd(int stateId) {
	}
	
	int totalLogicalConnections() {
		int n = 0;
		
		for (SendRecvCache t : commData) {
			n += t.numLogicalConnection();
		}
		
		return n;
	}
	
	boolean checkpointExists(int state) {
		File chkpnt = new File(checkpointDir + "/" + checkpointName(state));
		
		return chkpnt.exists();
	}
	
	/**
	 * Non-blockingly read a datagram packet from a specified channel. 
	 * @param channel
	 * @param packet
	 * @return The length of received data or -1 if no data is received.
	 */
	int recvDatagram(DatagramChannel channel, byte[] packet) {
	    ByteBuffer buffer = ByteBuffer.allocate(32);
	    SocketAddress remote = null;
	    int len = 0;
	    
	    try {
	    	remote = channel.receive(buffer); 
		} catch (IOException e) {
			assert false;
		}

		if (remote == null)
			return -1;
		
		buffer.flip();
		len = buffer.remaining();
		buffer.get(packet, 0, len);
    	
		return len;
	}

	/**
	 * Find the number of peer processes currently running.
	 * 
	 * @return
	 */
	abstract int numPeerProcesses();

	/**
	 * Replay a given I/O operation on a given physical connection.
	 * @param e
	 * @param conn
	 * @throws IOException
	 */
	private static void replayIO(Event e, PhysicalConnection conn) throws IOException {
		if (e instanceof SendByteEvent) {
			SendByteEvent byteE = (SendByteEvent) e;
			conn.write(byteE.b);
		} else if (e instanceof RecvByteEvent) {
			conn.read();
		}
	}
	
	private boolean shouldRestore() {
		return true;
	}
	
	/**
	 * Save peer's virtual machine state.
	 */
	private boolean savePeerState(int id) {
		long begin = System.currentTimeMillis();
		ProcessBuilder builder = new ProcessBuilder(isPeerRunningScript());
		Process p;
		int rv;
		
		checkpointIds.add(id);

		// The save function is disabled.
		if (!saveEnabled)
			return false;
		
		try {
			// Check if the peer is running.
			p = builder.start();
			
			// Save peer state if it is running.
			if (p.waitFor() == 0) {
				builder = new ProcessBuilder(saveStateScript(),
						String.valueOf(id), checkpointDir);
				onLaunchingCheckpointCmd(id);
				log.info(String.format("[CHKPNTCACHE : saveState(%d)]", id));
				p = builder.start();
				rv = p.waitFor();
				// The script returns 254 or 255 on error.
				assert (rv < 254) : "Error in saving a state.";
				
				// Peer is saved normally.
				if (rv < 254) {
					onPostCheckpoint(id);
					numSavedStates++;
					log.info("[CHKPNTCACHE : saveState] OK: " + (System.currentTimeMillis() - begin) + " ms");
					// Store the number of checkpoint files.
					numPeerCheckpoints.put(id, rv);
					
					return true;
				}
				else {
					log.info("[CHKPNTCACHE : saveState] peer may have terminated.");
//					assert false;
				}
			}
			else {
				log.info("[CHKPNTCACHE : saveState] Peer terminates. No need to send signal.");
				// Peer terminates. Disable the save function.
				saveEnabled = false;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	/**
	 * Restore a peer from a given checkpoint and replay I/O operations from
	 * that checkpoint to the current state. New connections are created if
	 * necessary.
	 * 
	 * @param conn A logical connection identifier.
	 * @param numConns The number of logical connections in the active tree.
	 * @param checkpoint The identifier of the checkpoint to be restored.
	 * @throws IOException
	 */
	private void restore(int conn, int numConns, int checkpoint) throws IOException {
		int numSavedConns;
		
		restorePeerState(checkpoint);
		fillPeerPointers(checkpoint);

		numSavedConns = replayFromCheckpoint(checkpoint);
		
		// The active connection can be polled now.
		if (conn < numSavedConns) {
			pollPeer(conn);
		}
		
		// Partial restart: Some connections may not be in the
		// checkpoint. Must restart them.
		for (int i = numSavedConns; i < numConns; i++) {
			createPhysicalConnection(i);
			// Must poll to make sure that there is no response pending.
			// If it is the active connection, poll and add.
			// Otherwise, only poll.
			pollPeer(i, i != conn);
		}

		// A new peer is restored. Re-enable the save function.
		saveEnabled = true;
	}
	
	/**
	 * Update field <i>peerPointers</i> after a peer has been restored.
	 * @param stateId The id of the checkpoint that the peer is restored from.
	 */
	private void fillPeerPointers(int stateId) {
		peerPointers.clear();
		
		if (stateId >= 0) {
			peerPointers.addAll(checkpointPool.get(stateId).getPeerPointers());
		}
		
		// Otherwise, leave the peerPointers array empty.
	}
	
	/**
	 * Restore peer's virtual machine state.
	 */
	private void restorePeerState(int id) {
		final int MAX_RETRIES = 3;
		long elapsed;
		int restoredState = id;
		int sync = 1;
		int retries = 0;
		assert checkpointExists(id) : "Checkpoint does not exist.";
		log.info("[CHKPNT_CACHE : restorePeerState] "
				+ checkpointName(restoredState));

		elapsed = System.currentTimeMillis();

		while (sync != 0 && retries++ < MAX_RETRIES) {
			log.info(String.format("[CHKPNT_CACHE : restorePeerState(%d)] setOffRestartingProcess", id));
			setOffRestartingProcess(restoredState);
			sync = syncckpt();
		}
		assert sync == 0 : "[CHKPNT_CACHE : restorePeerState] Restoring fails";

		elapsed = System.currentTimeMillis() - elapsed;
		log.info(String.format(
				"[CHKPNT_CACHE : restorePeerState] Elapsed time: %d ms",
				elapsed));

		onPostRestore(id);
		restoreSequenceNumbers(id);
	}

	private int syncckpt() {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Callable<Integer> task = new Callable<Integer>() {
			public Integer call() {
				ProcessBuilder builder = new ProcessBuilder(syncScript());
				Process p;
				int rv = -1;

				try {
					p = builder.start();
					rv = p.waitFor();
				} catch (IOException e) {
					assert false : "[VIRTCACHE : syncckpt]";
				} catch (InterruptedException e) {
					assert false : "[VIRTCACHE : syncckpt]";
				}

				return rv;
			}
		};

		Future<Integer> f = executor.submit(task);
		int rv = -1;

		try {
			rv = f.get(3, TimeUnit.SECONDS);
			// Must shut down the executor service, otherwise it will not allow the Java process to terminate.
			executor.shutdown();
		} catch (TimeoutException e) {
			assert false : "Timeout";
		} catch (InterruptedException e) {
			assert false;
		} catch (ExecutionException e) {
			assert false;
		}
		
		return rv;
	}
	
	/**
	 * 
	 * @param id Logical connection ID
	 * @return The "closest" checkpoint ID if there exists a valid checkpoint; otherwise, -1.
	 */
	private int closestCheckpoint(int id) {
		SendRecvCache tree;
		List<List<Integer>> paths;
		int treeId;
		int numConns;
		int numSavedConns;
		
		if (isServerSUT) {
			int counter = 0;
			paths = new ArrayList<List<Integer>>();
			
			// Each logical connection is in a different tree.
			for (SendRecvCache t : commData) {
				int req;
				
				t.setConnId(counter);
				req = t.getSendPtr();
				
				paths.add(t.getSendPath(0, req));
				counter++;
			}
			
			numConns = commData.size();
		} else {
			// Each logical connection is in the same tree.
			treeId = stt.getTreeID(id);
			tree = commData.get(treeId);
			numConns = tree.numLogicalConnection();
			tree.setConnId(id);
			paths = tree.getSendPaths();
		}

		// Check validity of checkpoints bottom-up.
		int cp = curState;
		while (cp != -1) {			
			if (checkpointPool.containsKey(cp)) {
				Checkpoint checkpoint = checkpointPool.get(cp);
				numSavedConns = checkpoint.numConnections();
				
				if (numSavedConns <= numConns) {
					boolean isOk = true;

					// Validity check: For all connections, the peer pointer
					// must be in the path from the root node to the
					// the current request pointer.
					for (int j = 0; j < numSavedConns; j++) {
						List<Integer> p = paths.get(j);
						int req = checkpoint.getPtr(j);

						if (req != 0 && !p.contains(req)) {
							isOk = false;
							break;
						}
					}
					
					if (isOk) {
						return cp;
					}
				}
			}
			
			cp = parents.get(cp);
		}
		
		return -1;
	}

	/**
	 * If the strategy suggests restoration, find the closest checkpoint and
	 * restore it. Otherwise, take the "replay" approach.
	 * 
	 * @param conn
	 *            The active connection identifier.
	 * @param numConns
	 *            The number of logical connections in the active tree.
	 * @throws IOException
	 */
	private void handleNewTrace(int conn, int numConns) throws IOException {
		if (shouldRestore()) {
			int chkpntId = closestCheckpoint(conn);
			assert chkpntId >= 0;

			// Restore the peer from a checkpoint.
			restore(conn, numConns, chkpntId);
			numRestoration++;
		}
		else {
			createPhysicalConnection(conn);
			pollPeer(conn);
		}
		
		incPeerBacktrack();
	}
	
	/**
	 * Reset all flags involving in the decision on saving or not saving a
	 * state.
	 */
	private static void resetSaveFlags() {
		saveFlag = ndFlag = false;
	}

	private static void incPeerBacktrack() {
		peerBacktrack++;
	}
	
	private void updatePeerPointer(int connId, int reqPtr) {
		int size = peerPointers.size();
		
		if (connId < size) {
			peerPointers.set(connId, reqPtr);
		}
		else {
			assert connId == size : "Inconsistent peer pointers";
			peerPointers.add(reqPtr);
		}
	}

	/**
	 * Restore a checkpoint or create a new connection.
	 * 
	 * @param rr_tree
	 * @param id
	 *            A logical connection id.
	 * @throws IOException
	 */
	private void resyncPeer(SendRecvCache rr_tree, int id) throws IOException {
		int numConns = rr_tree.numLogicalConnection();

		// Restore or restart.
		log.fine(String.format("[CHKPNT : resyncPeer] handleNewTrace(%d,%d)", id, numConns));
		handleNewTrace(id, numConns);
	}

}

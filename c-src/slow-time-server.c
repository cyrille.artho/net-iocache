/*
  Meta time server. At the beginning, it simulates contacting several time servers. It accepts requests from clients instead of automatically answer.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <netinet/in.h>


#define MYPORT 	"47000"  // the port users will be connecting to
#define BACKLOG 	10     // how many pending connections queue will hold


typedef struct sockaddr_storage sockaddr_storage;
typedef struct addrinfo addrinfo;
typedef struct timeval Timeval;

static int64_t timeval_to_ms(const Timeval *tv);
static int64_t cur_time_ms();
void *worker_run(void *);
void contact_time_servers();


int num_clients;


int main(int argc, char **argv) {
	sockaddr_storage their_addr;
	socklen_t addr_size;

	addrinfo hints;
	addrinfo *servinfo;
	addrinfo *p;
	pthread_t threads[8];

	int sockfd, new_fd;
	int yes = 1;
	int rv;
	int i;

	if (argc < 2) {
		perror("Usage: slow-time-server (#clients)");
		return 1;
	}

	num_clients = (int) strtol(argv[1], NULL, 0);

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    getaddrinfo(NULL, MYPORT, &hints, &servinfo);

    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
	  if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
		perror("server: socket");
		continue;
	  }

	  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		perror("setsockopt");
		exit(1);
	  }

	  if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
		close(sockfd);
		perror("server: bind");
		continue;
	  }

	  break;
    }

    if (p == NULL) {
        fprintf(stderr, "server: failed to bind\n");
        return 2;
    }

    freeaddrinfo(servinfo); // all done with this structure

    listen(sockfd, BACKLOG);

    // now accept an incoming connection:
    addr_size = sizeof their_addr;

	for (i = 0;i < num_clients;i++) {
		printf("Contact time servers ... @ %lld\n", cur_time_ms());
		contact_time_servers();

		printf("waiting ... @ %lld\n", cur_time_ms());
		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);

		// ready to communicate on socket descriptor new_fd!
		printf("accepted socket %d\n", new_fd);

		rv = pthread_create(&threads[i], NULL, worker_run, (void *) new_fd);
		if (rv) {
			printf("ERROR; return code from pthread_create() is %d\n", rv);
			exit(-1);
		}
	}

	close(sockfd);

	for (i = 0;i < num_clients;i++) {
		pthread_join(threads[i], NULL);
	}
	
	printf("slow-time-server ends\n");
	return 0;
}

void *worker_run(void *id) {
	const int LEN = 16;

	char c[LEN];
	char *answer;
	int new_fd = (int) id;
	int rv;

	memset(c, 0, LEN);

	rv = recv(new_fd, c, 11, MSG_WAITALL);
	if (rv > 0) {
		printf("worker %d: receive %s\n", (int) id, c);
		answer = (c[6] == 'd') ? "23/08/10" : "10/08/23";

		send(new_fd, answer, strlen(answer), 0);
		printf("worker %d: send %s\n", (int) id, answer);
	}
	else {
		perror("Nothing received");
	}

	close(new_fd);
	pthread_exit(NULL);
}

void visit(int *Value, int N, int k) {
	static int level = -1;
	int i;

	level = level + 1;
	Value[k] = level;

	if (level == N) {
		// Print
	}
	else
		for (i = 0; i < N; i++)
			if (Value[i] == 0)
				visit(Value, N, i);

	level = level - 1;
	Value[k] = 0;
}

void contact_time_servers() {
	const int N = 10;

	int value[N];
	int i;
	int round;

	for (round = 0; round < 6; round++) {
		for (i = 0; i < N; i++) {
			value[i] = 0;
		}

		visit(value, N, 0);
	}
}

inline static int64_t timeval_to_ms(const Timeval *tv) {
	return tv->tv_sec * 1000LL + tv->tv_usec / 1000;
}

static int64_t cur_time_ms() {
	Timeval tv;

	gettimeofday(&tv, NULL);
	return timeval_to_ms(&tv);
}

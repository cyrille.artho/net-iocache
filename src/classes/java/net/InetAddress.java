//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package java.net;

/**
 * Model class InetAddress is entirely implemented by the
 * native peer to avoid inconsistency between model level
 * and native level internet address representations since
 * those may be highly platform-dependent
 *
 */
public class InetAddress {
	
	/**
	 * private constructor; use static methods below to create
	 * an InetAddress object.
	 */
	private InetAddress() {
		assert false: "private InetAddress constructor must not be called.";
	}
	
	public static native InetAddress[] getAllByName(String host) throws UnknownHostException;
	public static native InetAddress getByName(String host) throws UnknownHostException; 
	public static native InetAddress getByAddress(byte[] addr) throws UnknownHostException;
	public static native InetAddress getLocalHost() throws UnknownHostException;
	
	public native String getCanonicalHostName();
	public native String getHostName();
	public native String getHostAddress();
	public native byte[] getAddress();
	public native boolean isLoopbackAddress();
	public native String toString();
}

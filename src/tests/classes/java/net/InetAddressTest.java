package classes.java.net;

import gov.nasa.jpf.util.test.TestJPF;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.Arrays;

import org.junit.Test;

public final class InetAddressTest extends TestJPF {

	/**
	 * check if an InetAddress has at least 4 bytes and 
	 * is consistent with the result of InetAddress.getByAddress() 
	 * @param iaddr InetAddress to check
	 * @return
	 * @throws UnknownHostException
	 */
	private boolean checkAddress(InetAddress iaddr) throws UnknownHostException {
		byte[] addr1 =iaddr.getAddress();
		byte[] addr2 = InetAddress.getByAddress(addr1).getAddress();
		return 	addr1[0]==addr2[0] &&
				addr1[1]==addr2[1] &&
				addr1[2]==addr2[2] &&
				addr1[3]==addr2[3]; 
	}
	
	@Test
	public void inetAddressStaticMethods() {
		if (verifyNoPropertyViolation()) {
			try {
				assert checkAddress(InetAddress.getLocalHost());
				assert checkAddress(InetAddress.getByName("localhost"));
				InetAddress[] addresses=InetAddress.getAllByName("localhost");
				assert addresses.length>0;
				for (InetAddress iaddr:addresses)
					assert checkAddress(iaddr);
			}
			catch (Exception e) {
				fail();
			}
		}
	}

	@Test
	public void inetAddressExceptions() {
		if (verifyNoPropertyViolation()) {
			@SuppressWarnings("unused")
			InetAddress iaddr;
			try {
				iaddr=InetAddress.getByName("localhostt");
				fail(); //hopefully unknown host
			} catch (UnknownHostException e) {
				//test passed
			}
			
			try {
				@SuppressWarnings("unused")
				InetAddress[] addresses=InetAddress.getAllByName("localhostt");
				fail();  //hopefully unknown host
			} catch (UnknownHostException e) {
				//test passed
			}
			
			try {
				iaddr=InetAddress.getByAddress(new byte[]{127,0,0,1,1});
				fail();    //IP address has two many bytes
			} catch (UnknownHostException e) {
				//test passed
			}
		}
	}

	@Test
	public void inetAddressNonStaticMethods() {
		if (verifyNoPropertyViolation()) {
			try {
				InetAddress lh=InetAddress.getLocalHost();
				assert lh.getCanonicalHostName().length()>0; 
				assert lh.getHostName().length()>0; 
				assert lh.getHostAddress().length()>0; 
				assert lh.toString().endsWith(lh.getHostAddress());
				assert lh.equals(InetAddress.getLocalHost());
				assert InetAddress.getByName("localhost").isLoopbackAddress();
				assert InetAddress.getByAddress(new byte[]{127,0,0,1}).isLoopbackAddress();
			} catch (Exception e) {
				fail();
			}
		}
	}

}

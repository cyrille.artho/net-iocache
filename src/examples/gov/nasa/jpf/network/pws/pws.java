package gov.nasa.jpf.network.pws;
/* pws.java - Pegasi Web Server main class

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.io.*;
import java.net.*;
import java.util.*;

public class pws
{

	/* Constants for the server - DO NOT CHANGE!  (unless needed) */
	private static String crlf = "\r\n";
	private static int defaultReadBufferSize = 1048576;

	private static String serverName = "Pegasi Web Server";
	private static String serverVersion = "0.2.3";
	private static String copyRight = "Copyright (C) 2000-2004 Jan De Luyck & Kris Van Hulle";

    private static String mainConfigFile = "conf/pws.conf";
	private static String mimeConfigFile = "conf/mimetypes.conf";
	
	private static String encoding = "UTF-8";

	public static BufferedWriter errorLog = null;
    public static BufferedWriter accessLog = null;

	private static Map serverSettings = new HashMap();  /* general server settings */
	private static Map mimeTypes = new HashMap();       /* mimetype configuration */
	private static Map actionHandlers = new HashMap();  /* action handlers for certain mimetypes/extensions */

	/* 0 -> no output, 1 -> error output, 2 -> all output */
	private static int verbosity = 2;
	private static boolean useLogFiles = false;

    private static int amountOfThreads = 0;
    
    private static boolean haltOnConfigErrors = true;
    
	public static String getEncoding()
	{
		return encoding;
	}
	
    public static String crlf()
    {
        return crlf;
    }
    
    public static boolean useLogFiles()
    {
        return useLogFiles;
    }
    
    public static void setUseLogFiles(boolean value)
    {
        useLogFiles = value;
    } 
    
    public static String getActionHandler(String extension)
    {
        return (String) actionHandlers.get(extension.toLowerCase());
    }
    
    public static String getMimeType(String extension)
    {
        String mimeType = (String) mimeTypes.get(extension.toLowerCase());
        if (mimeType == null)
            return getSetting("defaulttype");
        else
            return mimeType;
    }

	public static String getSetting(String keyWord)
	{
		return (String) serverSettings.get(keyWord.toLowerCase());
	}

	public static String serverName()
	{
		return serverName;
	}

	public static String serverVersion()
	{
		return serverVersion;
	}

	public static int defaultReadBufferSize()
	{
		return defaultReadBufferSize;
	}
	
	private static boolean processCommandLine(String[] commandLine)
    {
        /* allowed command line options:
            -v -> verbosity increased by 1
			-n -> no log
            -h -> help
        */
		
        Map validOptions = new HashMap();
        validOptions.put("-v", "0");
        validOptions.put("--verbose", "0");
        validOptions.put("-h", "1");
        validOptions.put("--help", "1");
        validOptions.put("-n", "2");
        validOptions.put("--nolog", "2");
        validOptions.put("-i", "3");
        validOptions.put("--ignore", "3");
        
        boolean noLog = false;
        int verbosityLevel = 0;
        boolean outputHelp = false;
        String illegalOptions = "";
        boolean returnValue = false;
        
        setUseLogFiles(true);
        
        for (int i=0; i < commandLine.length; i++)
		{
		    commandLine[i] = commandLine[i].toLowerCase().trim();
            
            if (validOptions.get(commandLine[i]) != null)
            {
    		    switch(Integer.parseInt(validOptions.get(commandLine[i]).toString()))
	    	    {
		            case 0:
		            {
		                /* increase verbosity level by 1*/
		                verbosityLevel++;
    		            returnValue = false;
	    	            break;
		            }
		            case 1:
		            {
    		            /* output help */
	    	            outputHelp = true;
		                returnValue = true;
		                break;
    		        }
	    	        case 2:
		            {
		                /* disable loggin tot he log files */
		                setUseLogFiles(false);
		                break;
    		        }
    		        case 3:
    		        {
    		            /* don't halt when a server config error has been detected */
    		            haltOnConfigErrors = false;
    		            break;
    		        }
    		    }
    		}
   		    else
   		    {
	            outputHelp = true;
	            illegalOptions += commandLine[i] + " ";
	            returnValue = true;
	        }
	    }
		
		if (verbosityLevel > 2) verbosityLevel = 2;
		
		if (illegalOptions.length() > 0)
		    System.out.println("Illegal options: " + illegalOptions);
		
		if (outputHelp == true)
		{
            System.out.println("");
            System.out.println(serverName + " version " + serverVersion + ",\n" + copyRight);
            System.out.println(serverName + " comes with ABSOLUTELY NO WARRANTY. \nFor more details, see the GNU");
            System.out.println("license distributed with this server.");
            System.out.println("This is free software, and you are welcome to redistribute it.");
            System.out.println("");
            System.out.println("usage: pws [-v|--verbose] [-h|--help] [-n|--nolog] [-i|--ignore]");
            System.out.println("");
		    System.out.println("-v and --verbose: increase verbosity level by 1;");
		    System.out.println("-h and --help   : show this help page;");
		    System.out.println("-n and --nolog  : disables the use of the logfiles on harddisk;");
		    System.out.println("-i and --ignore : ignores any misconfigurations that might be present.");
		    System.out.println("                  (USE AT OWN RISK!)");
            System.out.println("");
		}
		
		verbosity = verbosityLevel;
		return returnValue;
    }
	
	
	pws()
	{
		Server aThread;

		/* parse configfile (main config file) */
    	ConfProcessor.parseConfigurationFile(mainConfigFile,serverSettings, actionHandlers, mimeTypes);
		
		/* check parsed configuration information */
		
		int checkValue = ConfProcessor.checkConfigurationData(serverSettings);
		
		if (this.haltOnConfigErrors == false || checkValue == 0)
		{
    	    /* parse mimetype configuration file */
		    ConfProcessor.parseMimeTypes(mimeConfigFile, mimeTypes);

			/* declare socketserver (port listener) and socketreturn */
			ServerSocket theSocketServer = null;
			Socket socketReturn = null;

			/* attempt to open port */
			try
			{
				theSocketServer= new ServerSocket(Integer.parseInt(getSetting("port")));
			}
			catch (IOException e)
			{
		    	}

		    	/* if we get this far, it seems that the port was available, so we start
        	  	to listen upon it :-)
	     		NOTE: this function holds further processing until a connection is established
		      	or something else happends (system crash/nerveous breakdown/world blown up/etc...). 
		  
		    	The server is NOT started (aka no accept is done) if the port could not be opened.
			    */
		  
			int count = 2;
		    	while (theSocketServer != null && (--count >= 0))
       			{
          			try
		       		{
           				socketReturn = theSocketServer.accept();
		       		}
       				catch (IOException e)
		       		{
		       		}

		       		/* ok, a client connection was detected. */

		       		/*instantiate a new thread for this client connection...
       				  the new threads are 'daemon' style, so they die with the main thread :-) */
		       		if (amountOfThreads < Integer.parseInt(getSetting("maxclients")))
		       		{
    		       		aThread = new Server(socketReturn, socketReturn.getInetAddress());
		           		aThread.start();
		           	}
		       	}
		}
   	}

   	public static void main(String argv[])
   	{
   	    System.out.println(serverName + " v" + serverVersion + " Starting...\n");
   	    
        /* if this returns true, stop server, otherwise go on doing whatever it wanted to do in 
           this lifetime */
        if (processCommandLine(argv) == false)
        {
		    /* start the server */
       	    pws mainServer = new pws();
       	}
   	}
}

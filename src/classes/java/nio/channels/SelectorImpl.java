package java.nio.channels;

import java.io.IOException;
import java.nio.channels.spi.AbstractSelector;
import java.util.HashSet;
import java.util.Set;

/**
 * List of lock objets.
 * <ul>
 *	<li>this</li>
 *	<li>cancelledKeys</li>
 * </ul>
 *
 * this is used to wait for some keys become available.
 * cancelledKeys is used to protect itself from modified by multiple threads.
 */
public class SelectorImpl extends AbstractSelector {
	protected Set <SelectionKey> registeredKeys;
	protected Set <SelectionKey> cancelledKeys;
	protected Set <SelectionKey> logicalReadyKeys;
	private boolean init = false;
//	static Logger log = JPF.getLogger("gov.nasa.jpf.network.cache.CacheLayer");

	public SelectorImpl() {
		registeredKeys = new HashSet<SelectionKey>();
		cancelledKeys = new HashSet<SelectionKey>();
		logicalReadyKeys = new HashSet<SelectionKey>();
	}

	/**
	 * Using this method will report a false positive error, use select with timeout instead.
	 */
	public int select() {
		selectPreProcess();

		int nLogicalReadyKeys = logicalReadyKeys.size();

		if (nLogicalReadyKeys == 0) {
			synchronized (this) {
				try {
					this.wait();
				}
				catch (InterruptedException e) { }
			}
		}
		return nLogicalReadyKeys;
	}

	public int select(long timeout) {
		selectPreProcess();

		int nLogicalReadyKeys = logicalReadyKeys.size();

		if (nLogicalReadyKeys == 0) {
			Thread.yield();
		}
		return nLogicalReadyKeys;
	}

	public Selector wakeup() {
		synchronized (this) {
			this.notify();
		}

		return this;
	}

	public Set<SelectionKey> keys(){
		if (isOpen() == false) {
			throw new ClosedSelectorException();
		}

		return registeredKeys;
	}

	public Set<SelectionKey> selectedKeys() {
		if (isOpen() == false) {
			throw new ClosedSelectorException();
		}

		return logicalReadyKeys;
	}

	protected void cancelKey(SelectionKey key){
		if (registeredKeys.contains(key)) {
			synchronized (cancelledKeys) {
				cancelledKeys.add(key);
			}
		} else {
			System.err.println("Trying to cancel an unknown key");
		}
	}

	protected void closeImpl() throws IOException {
		for (SelectionKey key : registeredKeys) {
			key.channel().close();
		}
		registeredKeys.clear();
		logicalReadyKeys.clear();
		cancelledKeys.clear();
	}

	protected SelectionKey registerKey(SelectableChannel channel){
		SelectionKey key = new SelectionKey();
		key.setChannel(channel);
		key.setSelector(this);
		registeredKeys.add(key);
		return key;
	}

	private synchronized void removeCancelledKeys(){
		for (SelectionKey key : cancelledKeys) {
//			log.info("A key has been removed");
			logicalReadyKeys.remove(key);
			registeredKeys.remove(key);
		}
		cancelledKeys.clear();
	}

	private void checkReadiness() {
		for (SelectionKey key : registeredKeys) {
			key.readyOps(0); // we reset the key ready ops
			key = checkAcceptable(key);
			key = checkReadable(key);
			key = checkWritable(key);
			if (key.readyOps() != 0) {
				logicalReadyKeys.add(key);
			}
		}
	}

	private SelectionKey checkAcceptable(SelectionKey key) {
		if ((key.interestOps() & SelectionKey.OP_ACCEPT) != 0) {
			ServerSocketChannel channel = (ServerSocketChannel) key.channel();
			if (channel.isReadyToAccept()) {
				key.readyOps(key.readyOps()|SelectionKey.OP_ACCEPT);
			}
		}
		return key;
	}

	private SelectionKey checkReadable(SelectionKey key) {
		if ((key.interestOps() & SelectionKey.OP_READ) != 0) {
			SocketChannel channel = (SocketChannel) key.channel();
			if (channel.isReadyToRead()) {
				key.readyOps(key.readyOps()|SelectionKey.OP_READ);
			}
		}
		return key;
	}

	private SelectionKey checkWritable(SelectionKey key) {
		if ((key.interestOps() & SelectionKey.OP_WRITE) != 0) {
			//TODO must check the send buffer
			SocketChannel channel = (SocketChannel) key.channel();
			if (channel.isReadyToWrite()) {
				key.readyOps(key.readyOps()|SelectionKey.OP_WRITE);
			}
		}
		return key;
	}

	private void selectPreProcess() {
		removeCancelledKeys();
		checkReadiness();
		removeCancelledKeys();
	}
}

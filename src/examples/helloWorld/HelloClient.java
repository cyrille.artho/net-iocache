package helloWorld;

import java.io.*;
import java.net.*;

public class HelloClient {

	/**
	 * Probably the simplest 'non-trivial' client-server application.
	 */
	public static void main(String[] args) throws IOException {
		InetAddress addr = InetAddress.getLocalHost();
		int port = (args.length>0)?Integer.parseInt(args[0]):HelloServer.DEFAULT_PORT;

		Socket sock = new Socket(addr, port);
		System.out.println("[HelloClient.main] created connection to "+sock+".");

		OutputStream out=sock.getOutputStream();
		InputStream  in =sock.getInputStream();

		final String request = "Hello";
		out.write(request.getBytes());
		System.out.println("[HelloClient.main] sent message '"+request+"'.");

		byte[] buf = new byte[10];
		int len=in.read(buf);
		String response=new String(buf,0,len);
		System.out.println("[HelloClient.main] received message '"+response+"'.");
		
		sock.close();
		System.out.println("[HelloClient: main] closed connection. Good bye!");
	}
}

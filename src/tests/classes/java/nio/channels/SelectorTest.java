package classes.java.nio.channels;

import gov.nasa.jpf.util.test.TestJPF;
import gov.nasa.jpf.network.echo.Server;
import org.junit.Test;
//import static org.junit.Assert.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.channels.spi.SelectorProvider;
//import java.nio.charset.*;
import java.util.*;

public final class SelectorTest extends TestJPF {
	@Test
	public void nonblockingConnectAndFinishConnectTest() throws Exception {
	// test relies on connect executing without a peer being present
	// (lazy.connect) and also requires main.termination to be set
		if (verifyNoPropertyViolation("+jpf-net-iocache.lazy.connect=true","+jpf-net-iocache.main.termination=true")) {
			Selector connectSelector = SelectorProvider.provider().openSelector();
			InetSocketAddress isa = new InetSocketAddress("localhost", Server.PORT);
			SocketChannel sc = SocketChannel.open();
			sc.configureBlocking(false);
			boolean result = sc.connect(isa);
			while (!result) {
				SelectionKey connectKey = sc.register(connectSelector, SelectionKey.OP_CONNECT);
				int keysAdded = connectSelector.select(5);
				if (keysAdded > 0) {
					Set readyKeys = connectSelector.selectedKeys();
					Iterator i = readyKeys.iterator();
					while (i.hasNext()) {
						SelectionKey sk = (SelectionKey)i.next();
						i.remove();
						SocketChannel nextReady = (SocketChannel)sk.channel();
						result = nextReady.finishConnect();
						if (result)
							sk.cancel();
					}
				}
				else {
					return;
				}
			}

			byte[] bs = new byte[] { (byte)0xca, (byte)0xfe, (byte)0xba, (byte)0xbe };
			ByteBuffer bb = ByteBuffer.wrap(bs);
			sc.configureBlocking(true);
			sc.write(bb);
			bb.rewind();

			ByteBuffer bb2 = ByteBuffer.allocateDirect(100);
			int n = sc.read(bb2);
			bb2.flip();

			assertEquals(bb, bb2);

			sc.close();
		}
	}

	@Test(expected=AssertionError.class)
	public void selectShouldReturnAllPossibilities0() throws Exception {
		if (verifyNoPropertyViolation()) {
			Selector selector = Selector.open();
			SocketChannel socketChannel1 = connectAndRegister(selector, false);
			SocketChannel socketChannel2 = connectAndRegister(selector, false);
			int n = selector.select(5);
			assertEquals(n, 0);
		}
	}

	@Test(expected=AssertionError.class)
	public void selectShouldReturnAllPossibilities1() throws Exception {
		if (verifyNoPropertyViolation()) {
			Selector selector = Selector.open();
			SocketChannel socketChannel1 = connectAndRegister(selector, false);
			SocketChannel socketChannel2 = connectAndRegister(selector, false);
			int n = selector.select(5);
			assertEquals(n, 1);
		}
	}

	@Test(expected=AssertionError.class)
	public void selectShouldReturnAllPossibilities2() throws Exception {
		if (verifyNoPropertyViolation()) {
			Selector selector = Selector.open();
			SocketChannel socketChannel1 = connectAndRegister(selector, false);
			SocketChannel socketChannel2 = connectAndRegister(selector, false);
			int n = selector.select(5);
			if (n == 1) {
				for (SelectionKey key : selector.selectedKeys()) {
					SocketChannel sc = (SocketChannel) key.channel();
					assertEquals(sc, socketChannel1);
				}
			}
		}
	}

	@Test(expected=AssertionError.class)
	public void selectShouldReturnAllPossibilities3() throws Exception {
		if (verifyNoPropertyViolation()) {
			Selector selector = Selector.open();
			SocketChannel socketChannel1 = connectAndRegister(selector, false);
			SocketChannel socketChannel2 = connectAndRegister(selector, false);
			int n = selector.select(5);
			assertEquals(n, 2);
		}
	}

	private SocketChannel connectAndRegister(Selector selector, final boolean blocking) throws IOException {
		final SocketChannel socketChannel = SocketChannel.open();
		socketChannel.configureBlocking(blocking);
		socketChannel.connect(new InetSocketAddress("localhost", Server.PORT));
		socketChannel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
		return socketChannel;
	}
}

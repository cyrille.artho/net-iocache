//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.http;

import java.net.*;
import java.io.*;
import java.util.*;

// Small, simple HTTP server
public class HTTPServer {

    static final int PORT = 8080;

	String NAME;

	String VERSION;

	int serverPort;

	// No command line parameters are required
	public static void main(String args[]) {
		HTTPServer server = new HTTPServer("[HTTPServer]", "1.0", PORT);
		server.run(args);
	}

	// Create an HTTPServer for a particular TCP port
	public HTTPServer(String name, String version, int port) {
		this.NAME = name;
		this.VERSION = version;
		this.serverPort = port;
	}

	// Display name and version number
	public void displayVersionInfo() {
		System.out.println(NAME + " version " + VERSION+" started...");
	}

	// Run until interrupted
	public void run(String[] args) {
		//displayVersionInfo();
		try {
			// Get a server socket
			ServerSocket server = getServerSocket();
			int localPort = server.getLocalPort();
			int numAccept = Integer.parseInt(args[0]);
			// Let us know that you're listening
			System.out.println(NAME + " is listening on port " + localPort + ".");
//			if (numAccept!=0) 
//				System.out.println(NAME + " will terminate after having accepted and served "+numAccept+" connection(s).");
			for (int i = 0; i < numAccept || numAccept == 0; i++) {
				// Accept a connection
				Socket client = server.accept();
				// Handle the connection with a separate thread
				System.out.println(NAME+ " accepted "+(i+1)+". connection. Start HTTPServerThread...");
				(new HTTPServerThread(client)).start();
			}
		} catch (Exception ex) {
			System.out.println(NAME + " unable to listen on " + serverPort + ".");
			ex.printStackTrace();
			System.exit(1);
		}
		//System.out.println(NAME+" says good bye!");
	}

	// Get a server socket on the hard-wired server port
	ServerSocket getServerSocket() throws Exception {
		return new ServerSocket(serverPort);
	}
}

// Handle a single server connection
class HTTPServerThread extends Thread {
	Socket client;
	static private int ID=0;
	protected int id;
	
	// Keep track of the client socket
	public HTTPServerThread(Socket client) {
		this.client = client;
		this.id=ID++;
	}

	// Thread entry point
	public void run() {
		HTTPInputStream inStream;
		BufferedOutputStream outStream;
		try {
			// Create a stream to send data to the client
			outStream = new BufferedOutputStream(client.getOutputStream());
			inStream = new HTTPInputStream(client.getInputStream());
			// Get the client's request
			//System.out.println("[HTTPServerThread.run, id="+id+"] read http request...");
			HTTPRequest request = inStream.getRequest();
			// Display info about it
			System.out.println("[HTTPServerThread.run, id="+id+"] received http request\n"+request);
			// Sorry, we only handle gets
			if (request.isGetRequest())
				processGetRequest(request, outStream);
			System.out.println("[HTTPServerThread.run, id="+id+"] request completed. Closing connection...");
		} catch (IOException ex) {
			System.out.println("[HTTPServerThread.run, id="+id+"] IOException occurred when processing request:\n"+ex);
		}
		try {
			client.close();
		} catch (IOException ex) {
			System.out.println("[HTTPServerThread.run, id="+id+"] IOException occurred when closing socket:\n"+ex);
		}
		//System.out.println("[HTTPServerThread.run, id="+id+"] terminated.");
	}

	// Process an HTTP GET
	void processGetRequest(HTTPRequest request, BufferedOutputStream outStream) throws IOException {
		/*
		 * If you want to use this in a secure environment then you should place
		 * some restrictions on the requested file name
		 */
		String fileName = request.getFileName();
		
		// Remove the prefix, if any.
		if (fileName.startsWith("/"))
			fileName = fileName.substring(1);
		
		File file = new File(fileName);
		
		// Give them the requested file
		if (file.exists())
			sendFile(outStream, file);
		else
			System.out.println("[HTTPServerThread.processGetRequest, id="+id+"]  requested file " + file.getCanonicalPath() + " does not exist.");
	}

	static byte[] getBytes(String str) {
		int len = str.length();
		byte[] buff = new byte[len];

		for (int i = 0; i < len; i++) {
			buff[i] = (byte) str.charAt(i);
		}

		return buff;
	}

	// A simple HTTP 1.0 response
	void sendFile(BufferedOutputStream out, File file) {
		try {
			DataInputStream in = new DataInputStream(new FileInputStream(file));
			int len = (int) file.length();
			byte buffer[] = new byte[len];
			in.readFully(buffer);
			in.close();
			out.write(getBytes("HTTP/1.0 200 OK\r\n"));
			out.write(getBytes("Content-Length: " + buffer.length + "\r\n"));
			out.write(getBytes("Content-Type: text/html\r\n\r\n"));
			out.write(buffer);
			out.flush();
			out.close();
			System.out.println("\n[HTTPServerThread.sendFile, id="+id+"] file sent: " + file.getCanonicalPath()+
					           "\n[HTTPServerThread.sendFile, id="+id+"]   "+len+" bytes.\n");
		} catch (Exception ex) {
			try {
				System.err.println("[HTTPServerThread.sendFile, id="+id+"]  error when sending file "+ file.getCanonicalPath() +":\n"+ex);
				out.write(getBytes("HTTP/1.0 400 " + "No can do" + "\r\n"));
				out.write(getBytes("Content-Type: text/html\r\n\r\n"));
			} catch (IOException ioe) {}
		}
	}
}

// Convenience class for reading client requests
class HTTPInputStream extends FilterInputStream {
	
	private BufferedReader reader;
	
	public HTTPInputStream(InputStream in) {
		super(in);
		reader = new BufferedReader(new InputStreamReader(in));
	}

	// Get the whole request
	public HTTPRequest getRequest() throws IOException {
		HTTPRequest request = new HTTPRequest();
		String line;
		do {
			line = reader.readLine();
			line = (line == null) ? "" : line;
			//System.out.println("[HTTPInputStream.getRequest] read line of "+line.length()+" characters.");
			
			if (line.length() > 0)
				request.addLine(line);
			else
				break;
		} while (true);
		return request;
	}
}

class HTTPRequest {
	Vector<String> lines = new Vector<String>();

	public HTTPRequest() {
	}

	public void addLine(String line) {
		lines.addElement(line);
	}

	// Is this a GET or isn't it?
	boolean isGetRequest() {
		if (lines.size() > 0) {
			String firstLine = (String) lines.elementAt(0);
			if (firstLine.length() > 0)
				if (firstLine.substring(0, 3).equalsIgnoreCase("GET"))
					return true;
		}
		return false;
	}

	boolean isHeadRequest() {
		if (lines.size() > 0) {
			String firstLine = (String) lines.elementAt(0);
			if (firstLine.length() > 0)
				if (firstLine.substring(0, 4).equalsIgnoreCase("HEAD"))
					return true;
		}
		return false;
	}

	// What do they want to get?
	String getFileName() {
		if (lines.size() > 0) {
			String firstLine = (String) lines.elementAt(0);
			String fileName = firstLine.substring(firstLine.indexOf(" ") + 1);
			int n = fileName.indexOf(" ");
			if (n != -1)
				fileName = fileName.substring(0, n);

			if (fileName.equals(""))
				fileName = "index.htm";
			if (fileName.charAt(fileName.length() - 1) == '/')
				fileName += "index.htm";
			return fileName;
		} else
			return "";
	}

	public String toString() {
		if (lines.isEmpty())
			return "";
		StringBuffer sb=new StringBuffer();
		for (String s:lines) {
			sb.append(s);
			sb.append("\n");
		}
		return sb.toString();
	}
	
	boolean hasRange() {
		return lines.size() >= 2; // assume that the third line defines
		// "Range".
	}

	int[] getRange() {
		assert hasRange() : "[MultipartHTTPRequest : getRange(int[])] No range is defined.";
		String[] start_end;
		String str_range;
		String rangeLine = lines.elementAt(2);
		int[] range = new int[2];
		int equal_index = rangeLine.indexOf('=');

		str_range = rangeLine.substring(equal_index + 1);
		start_end = str_range.split("-");
		range[0] = Integer.parseInt(start_end[0]);
		range[1] = Integer.parseInt(start_end[1]);
		return range;
	}

}

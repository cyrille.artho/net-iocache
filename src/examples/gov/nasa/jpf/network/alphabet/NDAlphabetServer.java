//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.alphabet;

import gov.nasa.jpf.vm.Verify;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class NDAlphabetServer {

	static class WorkerThread extends Thread {

		Socket sock;

		WorkerThread(Socket s) {
			sock = s;
		}

		public void run() {
			InputStream in=null;
			OutputStream out=null;
			int req, resp;
			boolean finished = false;

			try {
				in = sock.getInputStream();
				out = sock.getOutputStream();

				while (!finished) {
//					System.out.println("[NDAlphabetServer.WorkerThread : run() threadID=" + getId() + "] trying to read...");
					req = in.read();

					if (req == -1) {
						finished = true;
						System.out.println("[NDAlphabetServer.WorkerThread: run() threadID=" + getId() + "] received end of stream.");
					} else {
						System.out.println("[NDAlphabetServer.WorkerThread : run() threadID=" + getId() + "] read '" + (char) req+"'.");
						int cap = Verify.getIntFromList(0,'a'-'A');
						resp = req - '0' + 'a' - cap;

//						System.out.println("[NDAlphabetServer.WorkerThread : run() threadID=" + getId() + "] trying to write '" + (char) resp+"'...");
						out.write(resp);
						System.out.println("[NDAlphabetServer.WorkerThread : run() threadID=" + getId() + "] wrote '" + (char) resp+"'.");
					}
				}
			} catch (IOException e) {
				System.err.println("[NDAlphabetServer.WorkerThread : run()] " + e);
			}

			try {
				assert (in != null && out != null && sock != null);
				System.out.println("[NDAlphabetServer.WorkerThread: run() threadID=" + getId() + "] closing connection...");
				in.close();
				out.close();
				sock.close();
			}
			catch(IOException e) {
				System.err.println("[NDAlphabetServer.WorkerThread : run()] " + e);
			}
		}
	}

	public static final int SERVER_PORT = 18600;

	/**
	 * 
	 * @param args (#connections) [port] 
	 * @throws IOException
	 */
	public static void main(String[] args) throws Exception {
		Socket[] sock;
		WorkerThread[] workers;
		int maxConnection=args.length>0?Integer.parseInt(args[0]):0;
		ServerSocket ssock = new ServerSocket(args.length > 1 ? Integer.parseInt(args[1]) : SERVER_PORT);
		System.out.println("[NDAlphabetServer: main] server socket opened at port "+ssock.getLocalPort());

		if (maxConnection > 0) {
			sock = new Socket[maxConnection];
			workers= new WorkerThread[maxConnection];
			
//			System.out.println("[NDAlphabetServer: main] is serving "+maxConnection+
//					" connection"+ (maxConnection>1?"s...":"..."));
			for (int i = 0; i < maxConnection; i++) {
				sock[i] = ssock.accept();
				System.out.println("[NDAlphabetServer: main] accepted "
									+(i+1)+". connection. Creating new worker thread...");

				workers[i]=new WorkerThread(sock[i]);
				workers[i].start();
//				System.out.println("[NDAlphabetServer.WorkerThread: run() threadID=" 
//									+ workers[i].getId() + "] started.");
				
			}
		} else {
//			System.out.println("[NDAlphabetServer: main] is serving an unlimited number of connections...");
			while (true) {
				Socket s = ssock.accept();
				System.out.println("[NDAlphabetServer: main] accepted a connection. Creating new worker thread...");

				new WorkerThread(s).start();
			}
		}
		
//		System.out.println("[NDAlphabetServer: main(String[])] waiting for worker threads to terminate...");
		for (WorkerThread w:workers)
			w.join();
		
		System.out.println("[NDAlphabetServer: main(String[])] closing server socket...");
	    ssock.close();
//		System.out.println("[NDAlphabetServer: main(String[])] says good bye!");
	}
}

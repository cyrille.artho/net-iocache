#!/bin/sh
. ./env.sh

PACKAGE="gov.nasa.jpf.network.jget"

SEQ_LEN=2
if [ $# -gt 0 ]
then
	SEQ_LEN=$1
fi

echo "start webserver serving ${SEQ_LEN}-length file ..."
java -classpath ${CLASSPATH} ${PACKAGE}.WebServer 8080 ${SEQ_LEN}


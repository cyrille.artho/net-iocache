package gov.nasa.jpf.network.pws;
/*  ConfProcessor.java - functions that have to do something with configuration file processing

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.io.*;
import java.util.*;

public abstract class ConfProcessor
{
	/* parseConfigurationFile (String, Map)
	   ------------------------------------
	   Parses the configuration file specified with the String parameter -> full path to file.
	   all parsing is done in meanner of '<keyword> [space] <parameter1> ... <parameterN>
	*/
	
	public static String readLine(FileReader f) throws IOException {
		StringBuffer sb = new StringBuffer();
		int c = f.read();
		
		while(c != -1 && c != '\n') {
			sb.append((char) c);
			c = f.read();
		}
		
		return sb.toString();
	}
	
	public static void parseConfigurationFile (String file, Map settingsMap, Map actionHandlers, Map mimeTypes)
	{ 
		Map specialKeyWords = new HashMap();
		specialKeyWords.put("addtype","1");
		specialKeyWords.put("action","2");

		FileReader config = null;
		try
		{
			if (new File(file).exists() == true)
			{
				config = new FileReader(file);

				String aLine, keyWord, value;

				while (config.ready())	/* as long as the stream gives the ready signal, read from it */
				{
					aLine = readLine(config);
					if (aLine != null && aLine.length() > 0)
					{
						/* replace tabs with spaces
						   replace '"' with spaces    -> more apache httpd compatibility, and I don't
						   use those damn characters anyway! 
						*/
						aLine = aLine.replace('\t',' ').replace('\"',' ').trim();
						if (aLine.length() > 0 && aLine.charAt(0) != '#')
						{
							int positionOfSpace = aLine.indexOf(' ');

							// if we can't find a space, this is not a 'valid' entry, so we log and ignore it
							if (positionOfSpace == -1)
								Misc.putSysMessage(8, "ConfProcessor: error: unknown entry " + aLine + " found in " + file + ". Ignoring.");
							else
							{
								keyWord = aLine.substring(0,positionOfSpace).trim().toLowerCase();
								value = aLine.substring(positionOfSpace + 1).trim();
								
								/* here the keyword is checked upon for special values, such as:
								   (list not complete, don't think it ever will be)
								   addtype action scriptalias
							   
								   Special handling is required for these keywords.
								*/
								if (specialKeyWords.get(keyWord) != null)
								{
									switch (Integer.parseInt(specialKeyWords.get(keyWord).toString()))
									{
										case 1:
										{
										    /* ADDTYPE HANDLING
										       Adds a mimetype to the mimetype repository */
        		        		            positionOfSpace = value.indexOf(' ');

	        	    	    	            if (positionOfSpace != -1)
    	        	    	    	        {
        	        	    	    	        String Mime = value.substring(0, positionOfSpace).trim();
            	        	    	    	    String extvalue = value.substring(positionOfSpace + 1).trim();
	
    	                    	    	    	String aValue;
	
	            	                	    	StringTokenizer extTokened = new StringTokenizer(extvalue);
			                            	    while (extTokened.hasMoreTokens() == true)
    			                            	{
            			                        	aValue = extTokened.nextToken();
            		    	            	        /* check wether the first character of aValue = '.' */
            	    	    	            	    if (aValue.charAt(0) == '.')
            	        	    	            	    aValue = aValue.substring(1);
            	                	            
    	            	            	        	mimeTypes.put(aValue, Mime);  /* put ext-mime mapping for this extension */
	    	                	        	    }
		    		                        }
											break;
										}
										
										case 2:
										{
									        /* ACTION HANDLING
									           Allows for a 'preprocessor' to be used. File is to be passed
									           through this preprocessor first before outputting it to the client. */
									        aLine = new String(value);
									        positionOfSpace = aLine.indexOf(' ');
							    	    
								        	if (positionOfSpace != -1)
									        {
									            String Mime = aLine.substring(0, positionOfSpace).trim();
								            
									            String preProcessor = aLine.substring(positionOfSpace + 1).trim();
								            
								        	    actionHandlers.put(Mime, preProcessor);
									        }
											break;
										}
									}
								}
								else
								{
							        /* default action for all other things */
							        settingsMap.put(keyWord,value);
								}
							}
						}
					}
				}
			}
		}
		catch (FileNotFoundException e) /* is caught with the '.exists()' in the beginning */
		{}                              /* javac otherwise nags */
		catch (IOException e)
		{}
		finally
		{
			try
			{
				if (config != null) /* file has been opened, thus it exists, thus it must be closed again */
					config.close();
			}
			catch (IOException e)
			{}
		}
	}

    public static void parseMimeTypes (String file, Map mimeTypes)
    {	
		FileReader mimefile = null;
        try
        {
            if (new File(file).exists() == true)
            {
                mimefile = new FileReader(file);
                
				String aLine, Mime, value;

                while (mimefile.ready())
                {
                    aLine = readLine(mimefile);
					if (aLine != null && aLine.length() > 0)
					{
						aLine = aLine.trim().replace('\t',' ');	/* get rid of spaces around the string, 
																	replace all tabs with spaces */
																	
                    	if (aLine.length() > 0 && aLine.charAt(0) != '#')
	                    {	
    	                    int positionOfSpace = aLine.indexOf(' ');

        	                if (positionOfSpace != -1)
            	            {
                	            Mime = aLine.substring(0, positionOfSpace).trim();
                    	        value = aLine.substring(positionOfSpace + 1).trim();

                        	    String aValue;

                            	StringTokenizer valueTokened = new StringTokenizer(value);
	                            while (valueTokened.hasMoreTokens() == true)
    	                        {
        	                        aValue = valueTokened.nextToken();
            	                    mimeTypes.put(aValue, Mime);  /* put ext-mime mapping for this extension */
                	            }
	                        }
                    	}
                	}
				}
            }
        }
		catch (FileNotFoundException e) /* is caught with the '.exists()' in the beginning */
		{}
		catch (IOException e)
		{}
		finally
		{
			try
			{
				if (mimefile != null) /* file has been opened, thus it exists, thus it must be closed again */
					mimefile.close();
			}
			catch (IOException e)
			{}
		}

    }
	
	public static int checkConfigurationData(Map configData)
	/* This function is intended to check whatever configuration data that needs checking.

	   Return values: 0 -> all ok; 1 -> major error, stop
	*/
	{
	    /* DIE statement check */
	    /* if we can find this, we tell the server to HALT */
	    if (configData.containsKey("die") == true)
	    {
	        Misc.putSysMessage(8, "'Die' found -- " + (String)configData.get("die"));
	        return 1;
	    }

		/* SERVERROOT CHECK */
		/* check if serverroot is set, if not, error out 
                   if it is set, check if the dir exists */
		if (configData.containsKey("serverroot") == false)
		{
			Misc.putSysMessage(8,"The ServerRoot directive is missing from the configuration file! -- Cannot continue.");
			return 1;
		}
		else
		{
		    File serverRoot = new File(configData.get("serverroot").toString());
		    if (serverRoot.exists() != true)
		    {
		        Misc.putSysMessage(8,"ServerRoot does not exist!");
		        return 1;
		    }
		}
	    
	    /* PORT CHECK */
	    if (configData.containsKey("port") == false)
	    {
	        Misc.putSysMessage(8, "The Port directive is missing from the configuration file! -- Cannot continue.");
	        return 1;
	    }

		/* SERVERNAME CHECK */
		if (configData.containsKey("servername") == false)
		{
		    Misc.putSysMessage(8, "The ServerName directive is missing from the config file! -- Cannot continue.");
		    return 1;
		}

	    /* DOCUMENTROOT CHECK --> set to '' to avoid null errors */
	    if (configData.containsKey("documentroot") == false)
	        configData.put("documentroot","");
	    
        /* TIMEOUT CHECK */
        if (configData.containsKey("timeout") == false)
            configData.put("timeout","10");
	    
	    /* MAXCLIENTS CHECK */
	    if (configData.containsKey("maxclients") == false)
	        configData.put("maxclients", "150");
	    
        /* DIRECTORYINDEX CHECK */
	    if (configData.containsKey("directoryindex") == false)
	        configData.put("directoryindex", "index.html index.htm");
	    
	    /* SERVERADMIN CHECK */
	    if (configData.containsKey("serveradmin") == false)
	        configData.put("serveradmin","you@your.address");
		
		/* DEFAULTTYPE CHECK */
		if (configData.containsKey("defaulttype") == false)
		    configData.put("defaulttype", "text/plain");
		
		/* SERVERSIGNATURE CHECK */
	    if (configData.containsKey("serversignature") == false)
	        configData.put("serversignature", "off");
	
        /* READBUFFERSIZE CHECK */
		/* readbuffersize must be between 1 byte and 2 megabyte */
		if (configData.containsKey("readbuffersize") == true)
		{
			if ((Integer.parseInt(configData.get("readbuffersize").toString()) <= 0) ||	(Integer.parseInt(configData.get("readbuffersize").toString()) >= 2097152))
			{
				configData.put("readbuffersize", Integer.toString(pws.defaultReadBufferSize()));
				Misc.putSysMessage(8,"checkConfigurationData: restored readBufferSize to default value (" + pws.defaultReadBufferSize() + ")");
			}
		}
		else
			configData.put("readbuffersize", Integer.toString(pws.defaultReadBufferSize()));

        if (pws.useLogFiles() == true)
        {

    		/* ERRORLOG CHECK */
	    	if (configData.containsKey("errorlog") != true)
		    	configData.put("errorlog", configData.get("serverroot").toString() + "/logs/error.log");
		
    		/* ACCESSLOG CHECK */
	    	if (configData.containsKey("accesslog") != true)
		    	configData.put("accesslog", configData.get("serverroot").toString() + "/logs/access.log");

    		/* set & open error and access log files */
	    	File accessLog = new File(configData.get("accesslog").toString());
		    File errorLog = new File(configData.get("errorlog").toString());
		
    		/* create the file if it doesn't exist yet */
	    	try
		    {
    			errorLog.createNewFile();
	    		accessLog.createNewFile();
		
		    	if (errorLog.isFile() && accessLog.isFile())	/* the files existed, or where created */
			    {
    				pws.errorLog = new BufferedWriter(new FileWriter(errorLog.getPath(), true)); /* true is for append */
	    			pws.accessLog = new BufferedWriter(new FileWriter(accessLog.getPath(), true));
		    	}
    		}
	    	catch (IOException e)
    		{ 
	    	    Misc.putSysMessage(8,"Error while creating/opening log files. Logging disabled.");
    		    pws.setUseLogFiles(false);
	    	}
		}
		return 0;
	}
	
    //alternative way of parsing the mimetypes for the conftool, because we need access to them in another way
    //this apparently throws out all mimetypes without a matching extension - which is fine for me
    public static void parseMimeTypes2 (String file, Map mimeTypes)
    {	
		BufferedReader mimefile = null;
        try
        {
            if (new File(file).exists() == true)
            {
                mimefile = new BufferedReader(new FileReader(file));
                
				String aLine, Mime, value;

                while (mimefile.ready())
                {
                    aLine = mimefile.readLine();
					if (aLine != null && aLine.length() > 0)
					{
						aLine = aLine.trim().replace('\t',' ');	/* get rid of spaces around the string, 
																	replace all tabs with spaces */
																	
                    	if (aLine.length() > 0 && aLine.charAt(0) != '#')
	                    {	
    	                    int positionOfSpace = aLine.indexOf(' ');

        	                //right... we could prolly change this so that we can have mimetypes
        	                //without extensions - that would be kinda neat or something
        	                if (positionOfSpace != -1)
            	            {
                	            Mime = aLine.substring(0, positionOfSpace).trim();
                    	        value = aLine.substring(positionOfSpace + 1).trim();

								mimeTypes.put(Mime, value);
								
                        	    /*String aValue;

                            	StringTokenizer valueTokened = new StringTokenizer(value);
	                            while (valueTokened.hasMoreTokens() == true)
    	                        {
        	                        aValue = valueTokened.nextToken();
            	                    mimeTypes.put(aValue, Mime);  /* put ext-mime mapping for this extension 
                	            }*/
	                        }
                    	}
                	}
				}
            }
        }
		catch (FileNotFoundException e) /* is caught with the '.exists()' in the beginning */
		{}
		catch (IOException e)
		{}
		finally
		{
			try
			{
				if (mimefile != null) /* file has been opened, thus it exists, thus it must be closed again */
					mimefile.close();
			}
			catch (IOException e)
			{}
		}

    }

	//also needed for the conftool, also because of the mimetypes
	public static void parseConfigurationFile2 (String file, Map settingsMap, Map actionHandlers, Map mimeTypes)
	{ 
		BufferedReader config = null;
		try
		{
			if (new File(file).exists() == true)
			{
				config = new BufferedReader(new FileReader(file));

				String aLine, keyWord, value;

				while (config.ready())	/* as long as the stream gives the ready signal, read from it */
				{
					aLine = config.readLine();
					if (aLine != null && aLine.length() > 0)
					{
						/* replace tabs with spaces
						   replace '"' with spaces    -> more apache httpd compatibility, and I don't
						   use those damn characters anyway! 
						*/
						aLine = aLine.replace('\t',' ').replace('\"',' ').trim();
						if (aLine.length() > 0 && aLine.charAt(0) != '#')
						{
							int positionOfSpace = aLine.indexOf(' ');
							keyWord = aLine.substring(0,positionOfSpace).trim().toLowerCase();
							value = aLine.substring(positionOfSpace + 1).trim();
							
							/* here the keyword is checked upon for special values, such as:
							   (list not complete, don't think it ever will be)
							   addtype action
							   
							   Special handling is required for these keywords.
							*/

							if (keyWord.equals("addtype"))
							{
							    /* ADDTYPE HANDLING
							       Adds a mimetype to the mimetype repository */
        	                    positionOfSpace = value.indexOf(' ');

            	                if (positionOfSpace != -1)
                	            {
                    	            String Mime = value.substring(0, positionOfSpace).trim();
                        	        String extvalue = value.substring(positionOfSpace + 1).trim();
                        	        
                        	        mimeTypes.put(Mime,extvalue);
									/*
                            	    String aValue;

                                	StringTokenizer extTokened = new StringTokenizer(extvalue);
	                                while (extTokened.hasMoreTokens() == true)
    	                            {
            	                        aValue = extTokened.nextToken();
            	                        /* check wether the first character of aValue = '.' **
            	                        if (aValue.charAt(0) == '.')
            	                            aValue = aValue.substring(1);
            	                            
            	                        Misc.putSysMessage(6, "Added ext: " + aValue +" for mime: " + Mime);
                	                    mimeTypes.put(aValue, Mime);  /* put ext-mime mapping for this extension **
                    	            }
                    	            */
	                            }
							}
							else
							{
							    if (keyWord.equals("action"))
							    {
							        /* ACTION HANDLING
							           Allows for a 'preprocessor' to be used. File is to be passed
							           through this preprocessor first before outputting it to the client. */
							        aLine = new String(value);
							        positionOfSpace = aLine.indexOf(' ');
							        
							        if (positionOfSpace != -1)
							        {
							            String Mime = aLine.substring(0, positionOfSpace).trim();
							            
							            String preProcessor = aLine.substring(positionOfSpace + 1).trim();
							            
							            Misc.putSysMessage(6, "Added actionHandler for: " + Mime + " to: " + preProcessor);
							            actionHandlers.put(Mime, preProcessor);
							        }
							    }
							    else
							    {
							        /* default action for all other things */
							        settingsMap.put(keyWord,value);
							    }
							}
						}
					}
				}
			}
		}
		catch (FileNotFoundException e) /* is caught with the '.exists()' in the beginning */
		{}
		catch (IOException e)
		{}
		finally
		{
			try
			{
				if (config != null) /* file has been opened, thus it exists, thus it must be closed again */
					config.close();
			}
			catch (IOException e)
			{}
		}
	}

	public static void dumpConfigurationFile (String file, Map settingsMap, Map actionHandlers, Map mimeTypes)
	{ 
		BufferedWriter config = null;
		try
		{
			if (new File(file).exists() == true)
			{
			//rename it to a .bak file
			new File((String)(file + ".bak")).delete();
			new File(file).renameTo(new File((String)(file + ".bak")));
			config = new BufferedWriter(new FileWriter(file));
			new File(file).createNewFile();

	    	Iterator keyIter = settingsMap.keySet().iterator();
	    	String currentKey;
	    	while (keyIter.hasNext())
	    		{
	    		currentKey = (String)keyIter.next();
				//Misc.putSysMessage(0,currentKey + ": " + (String)serverSettings.get(currentKey));
				config.write(currentKey + " " + (String)settingsMap.get(currentKey));
				config.newLine();
				}

			}
		}
		//catch (FileNotFoundException e) /* is caught with the '.exists()' in the beginning */
		//{}
		catch (IOException e)
		{}
		finally
		{
			try
			{
				if (config != null) /* file has been opened, thus it exists, thus it must be closed again */
					config.close();
			}
			catch (IOException e)
			{}
		}
	}
}

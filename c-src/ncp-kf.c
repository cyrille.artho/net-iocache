#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <netinet/in.h>

#include "ncp-kf.h"


SocketTable sock_table[NUM_SOCKETS];
pthread_t 	proxy 				= 0;
int 		socket_table_size	= 0;


// [override]
int socket(int domain, int type, int protocol) {
	int p;
	int idx;
	int syssock = 0;

	if (!proxy) {
		ncp_init();
	}

	// new UDP socket
	printf("[socket] domain %d, type %d, protocol %d\n", domain, type, protocol);

	if ((domain == PF_INET || domain == PF_INET6) && type == SOCK_STREAM) {
		printf("[socket] Stream requested. Use datagram instead\n");
		p = gnu_socket(domain, SOCK_DGRAM, 0);
	}
	else {
		p = gnu_socket(domain, type, protocol);
		syssock = 1;
	}

	// allocate a row in Socket Table
	idx = socket_fds_alloc(p);
	sock_table[idx].is_sys_sock = syssock;

	printf("[socket] returns %d\n", p);
	return p;
}

// [override]
int listen(int sockfd, int backlog) {
	int	idx	= socket_fd_index(sockfd);
	printf("[listen] socket %d\n", sockfd);

	if (sock_table[idx].is_sys_sock) {
		printf("[listen] System socket detected. Call standard 'listen'\n");
		return gnu_listen(sockfd, backlog);
	}
	else
		return 0;
}

// [override]
int accept(int sockfd, Sockaddr *addr, socklen_t *addrlen) {
	static int (*real_socket)(int, int, int) = NULL;
	int realfd = ncp_real_fd(sockfd);

	char 	buf[MAXBUFLEN];
	char 	accept[ACCEPT_PACKET_SIZE];
	int 	byte_recv;
	int 	child_sock;

	Sockaddr_in 	*addr_v4;
	Sockaddr_in6 	*addr_v6;

	uint 	local_port 	= usocket_socket_port(realfd);
	int		idx 		= socket_fd_index(sockfd);

	if (sock_table[idx].is_sys_sock) {
		return gnu_accept(sockfd, addr, addrlen);
	}

	real_socket = real_socket ? real_socket : dlsym(RTLD_NEXT, "socket");

	// ignore calling accept on port zero
	if (!local_port) {
		return 0;
	}

	printf("[accept] wait for CONNECT @ port %d\n", local_port);

	byte_recv = read_ctrl_sink(sockfd, buf);

	// Fill 'addr' according to its family
	if (*addrlen >= sizeof(Sockaddr_in6)) {
		addr_v6 = (Sockaddr_in6 *) addr;
		*addr_v6 = *((Sockaddr_in6 *) &sock_table[socket_fd_index(sockfd)].addr);
	}
	else if (*addrlen >= sizeof(Sockaddr_in)) {
		addr_v4 = (Sockaddr_in *) addr;
		*addr_v4 = *((Sockaddr_in *) &sock_table[socket_fd_index(sockfd)].addr);
	}
	else
		perror("[accept] Unknown family address");

	// Fill 'addrlen'
	*addrlen = sock_table[socket_fd_index(sockfd)].addrlen;

	// Check if it is a correct "connect" packet.
	if (is_connect_packet(buf, byte_recv))
		printf("[accept] Connect packet received\n");
	else {
		perror("[accept] not a CONNECT packet");
		return -1;
	}

	// create a new socket to handle an incoming connection
	if (addr->sa_family == AF_INET)
		child_sock = real_socket(PF_INET, SOCK_DGRAM, 0);
	else if (addr->sa_family == AF_INET6) {
		child_sock = real_socket(PF_INET6, SOCK_DGRAM, 0);
	} else {
		perror("[accept] Unknown family");
		return -1;
	}

	idx = socket_fds_alloc(child_sock);
	sock_table[idx].addr = *(Sockaddr_storage *) addr;
	sock_table[idx].addrlen = *addrlen;

	// Create and send an accept packet
	accept_packet(child_sock, accept);
	sendto(child_sock, accept, ACCEPT_PACKET_SIZE, 0, addr, *addrlen);
	// save local address of new socket
	printf("save local address as port %u\n", usocket_socket_port(child_sock));
	ncp_save_local_addr(child_sock);

	return child_sock;
}

// [override]
int connect(int sockfd, const struct sockaddr *serv_addr, socklen_t addrlen) {
	int realfd = ncp_real_fd(sockfd);

	char buf[MAXBUFLEN];
	int byte_recv;
	int idx = socket_fd_index(sockfd);

	if (sock_table[idx].is_sys_sock) {
		return gnu_connect(sockfd, serv_addr, addrlen);
	}

	// ignore connection to port zero
	if (serv_addr->sa_family == AF_INET6) {
		struct sockaddr_in6 *v6 = (struct sockaddr_in6 *) serv_addr;

		if (!ntohs(v6->sin6_port))
			return 0;
	} else if (serv_addr->sa_family == AF_INET) {
		struct sockaddr_in *tmp = (struct sockaddr_in *) serv_addr;

		if (!ntohs(tmp->sin_port))
			return 0;
	}

	connect_packet(sockfd, buf);
	// send a "connect" packet
	sendto(realfd, buf, CONNECT_PACKET_SIZE, 0, serv_addr, addrlen);
	printf("[connect] send a CONNECT packet\n");
	ncp_save_local_addr(sockfd);

	// receive an Accept packet
	while (is_ctrl_sink_empty(sockfd));
	byte_recv = read_ctrl_sink(sockfd, buf);

	// check if it is an Accept packet
	if (!is_accept_packet(buf, byte_recv)) {
		perror("[connect] Accept packet not received");
		return -1;
	}

	return 0;
}

// [override]
ssize_t send(int s, const void *buf, size_t len, int flags) {
	char recv_buf[CTRL_PACKET_SIZE];
	char packet[MAXBUFLEN];
	int rv;
	int i;

	int done 		= 0;
	int idx 		= socket_fd_index(s);
	int packet_size	= data_packet(s, buf, len, packet);
	int realfd 		= ncp_real_fd(s);

	if (sock_table[idx].is_sys_sock) {
		return gnu_send(s, buf, len, flags);
	}

	for (i = 0; i < RETRY && !done; i++) {
		// send a data packet
		rv = sendto(realfd, packet, packet_size, flags, (struct sockaddr *) &sock_table[idx].addr,
				sock_table[idx].addrlen);

		sleep(1);
		if (!is_ctrl_sink_empty(s)) {
			read_ctrl_sink(s, recv_buf);
			printf("[send] ACK %d arrives\n", packet_seq_number(recv_buf));
			done = 1;
		}
	}

	if (i == RETRY) {
		perror("[send] exceed the number of retries");
		return -1;
	}

	return rv;
}

// [override]
ssize_t recv(int s, void *buf, size_t len, int flags) {
	char packet[MAXBUFLEN];
	int	rv;
	int idx	= socket_fd_index(s);
	int retry = 0;
	int max_retries	= 20;
	printf("[recv] socket %d\n", s);

	if (sock_table[idx].is_sys_sock) {
		printf("[recv] System socket detected. Call standard function.\n");
		return gnu_recv(s, buf, len, flags);
	}

	while (retry < max_retries) {
		if (!is_data_sink_empty(s)) {
			read_data_sink(s, packet);

			rv = packet_data(packet, buf);

			return rv;
		}
		else {
			retry++;
			sleep(1);
		}
	}

	// count as the socket is closed
	printf("[recv] Socket %d is closed\n", s);
	return 0;
}

// [override]
int bind (int sockfd, const Sockaddr *myaddr, socklen_t addrlen) {
	int idx = socket_fd_index(sockfd);
	int realfd = ncp_real_fd(sockfd);

	memcpy(&sock_table[idx].local_addr, myaddr, addrlen);
	sock_table[idx].local_addrlen = addrlen;

	return gnu_bind(realfd, myaddr, addrlen);
}

// [override]
int getpeername(int socket, struct sockaddr *__restrict__ name, socklen_t *__restrict__ namelen) {
	int idx = socket_fd_index(socket);
	size_t addrlen = sizeof(sock_table[idx].addr);

	if (sock_table[idx].is_sys_sock || 0 == sock_table[idx].addrlen) {
		return gnu_getpeername(socket, name, namelen);
	}

	if (*namelen < addrlen)
		memcpy(name, &sock_table[idx].addr, (size_t) *namelen);
	else
		memcpy(name, &sock_table[idx].addr, addrlen);

	*namelen = addrlen;

	return 0;
}

// [override]
ssize_t write (int fd, const void *buf, size_t nbytes) {
	int idx = socket_fd_index(fd);

	if (-1 == idx || sock_table[idx].is_sys_sock) {
		return gnu_write(fd, buf, nbytes);
	}
	else {
		return send(fd, buf, nbytes, 0);
	}
}

// [override]
ssize_t read (int fd, void *buf, size_t nbytes) {
	int idx = socket_fd_index(fd);

	if (-1 == idx || sock_table[idx].is_sys_sock) {
		return gnu_read(fd, buf, nbytes);
	}
	else {
		return recv(fd, buf, nbytes, 0);
	}
}

// [GNU C lib]
int gnu_socket(int domain, int type, int protocol) {
	static int (*real_socket)(int, int, int) = NULL;

	real_socket = real_socket ? real_socket : dlsym(RTLD_NEXT, "socket");

	return real_socket(domain, type, protocol);
}

// [GNU C lib]
int gnu_connect(int sockfd, const struct sockaddr *serv_addr, socklen_t addrlen) {
	static int (*real_connect)(int, const struct sockaddr *, socklen_t) = NULL;

	real_connect = real_connect ? real_connect : dlsym(RTLD_NEXT, "connect");

	return real_connect(sockfd, serv_addr, addrlen);
}

int gnu_listen(int sockfd, int backlog) {
	static int (*real_listen)(int, int) = NULL;

	real_listen = real_listen ? real_listen : dlsym(RTLD_NEXT, "listen");

	return real_listen(sockfd, backlog);
}

// [GNU C lib]
int gnu_accept(int sockfd, Sockaddr *addr, socklen_t *addrlen) {
	static int (*real_accept)(int, Sockaddr *, socklen_t *) = NULL;

	real_accept = real_accept ? real_accept : dlsym(RTLD_NEXT, "accept");

	return real_accept(sockfd, addr, addrlen);
}

// [GNU C lib]
int gnu_bind(int sockfd, const Sockaddr *myaddr, socklen_t addrlen) {
	static int (*real_bind)(int, const Sockaddr *, socklen_t) = NULL;

	real_bind = real_bind ? real_bind : dlsym(RTLD_NEXT, "bind");

	return real_bind(sockfd, myaddr, addrlen);
}

// [GNU C lib]
ssize_t gnu_send(int s, const void *buf, size_t len, int flags) {
	static ssize_t (*real_send)(int, const void *, size_t, int) = NULL;

	real_send = real_send ? real_send : dlsym(RTLD_NEXT, "send");

	return real_send(s, buf, len, flags);
}

// [GNU C lib]
ssize_t gnu_recv(int s, void *buf, size_t len, int flags) {
	static ssize_t (*real_recv)(int, void *, size_t, int) = NULL;

	real_recv = real_recv ? real_recv : dlsym(RTLD_NEXT, "recv");

	return real_recv(s, buf, len, flags);
}

// [GNU C lib]
int gnu_getpeername(int socket, struct sockaddr *__restrict__ name, socklen_t *__restrict__ namelen) {
	static int (*real_getpeername)(int, struct sockaddr *, socklen_t *) = NULL;

	real_getpeername = real_getpeername ? real_getpeername : dlsym(RTLD_NEXT, "getpeername");

	return real_getpeername(socket, name, namelen);
}

// [GNU C lib]
ssize_t gnu_write (int fd, const void *buf, size_t nbytes) {
	static int (*real_write)(int, const void *, size_t) = NULL;

	real_write = real_write ? real_write : dlsym(RTLD_NEXT, "write");

	return real_write(fd, buf, nbytes);
}

// [GNU C lib]
ssize_t gnu_read (int fd, void *buf, size_t nbytes) {
	static int (*real_read)(int, void *, size_t) = NULL;

	real_read = real_read ? real_read : dlsym(RTLD_NEXT, "read");

	return real_read(fd, buf, nbytes);
}

int connect_packet(int sock_fd, char *buffer) {
	memset(buffer, 0, CONNECT_PACKET_SIZE);

	buffer[0] = sock_fd;
	buffer[5] = 0x01;

	return CONNECT_PACKET_SIZE;
}

int is_connect_packet(const char *packet, int size) {
	return size == CONNECT_PACKET_SIZE && packet[5] == 0x01;
}

int accept_packet(int sock_fd, char *buffer) {
	memset(buffer, 0, ACCEPT_PACKET_SIZE);

	buffer[0] = sock_fd;
	buffer[5] = 0x02;

	return ACCEPT_PACKET_SIZE;
}

int is_accept_packet(const char *packet, int size) {
	return size == ACCEPT_PACKET_SIZE && packet[5] == 0x02;
}

int data_packet(int sockfd, const void *data, int data_len, char *packet) {
	memset(packet, 0, 10 + data_len);

	packet[0] = sockfd;
	*((int*) (packet + 1)) = get_packet_number(sockfd);
	*((int*) (packet + 6)) = data_len;

	inc_packet_number(sockfd);
	memcpy(packet + 10, data, data_len);

	return data_len + 10;
}

int ack_packet(int sockfd, int seq, char *packet) {
	memset(packet, 0, CTRL_PACKET_SIZE);

	packet[0] 					= sockfd;
	*((int*) (packet + 1)) 	= seq;
	packet[5]					= 0x04;

	return CTRL_PACKET_SIZE;
}

int is_ack_packet(const char *packet, int size) {
	return size == CTRL_PACKET_SIZE && packet[5] == 0x04;
}

int is_data_packet(const char *packet) {
	int len = packet_data_len(packet);

	return len > 0;
}

int packet_seq_number(const char *packet) {
	return *((int*) (packet + 1));
}

int packet_data_len(const char *packet) {
	return *((int*) (packet + 6));
}

// Fill the data part of a given packet to storage and return the data length
int packet_data(const char *packet, char *storage) {
	int len = packet_data_len(packet);

	memcpy(storage, packet + 10, len);

	return len;
}

uint usocket_socket_port(int sockfd) {
	struct sockaddr_storage addr;
	struct sockaddr_in6 *addr_v6;
	struct sockaddr_in *addr_v4;
	socklen_t addrlen = sizeof addr;

	memset(&addr, 0, sizeof addr);
	getsockname(sockfd, (struct sockaddr *) &addr, &addrlen);

	if (addr.ss_family == AF_INET) {
		addr_v4 = (struct sockaddr_in *) &addr;
		return ntohs(addr_v4->sin_port);
	} else if (addr.ss_family == AF_INET6) {
		addr_v6 = (struct sockaddr_in6 *) &addr;
		return ntohs(addr_v6->sin6_port);
	}

	return 0;
}

// Allocate a block each in the socket descriptor array and the packet number array
// Return the index of this socket in the Socket Table.
int socket_fds_alloc(int socket_fd) {
	int idx = socket_fd_index(socket_fd);

	// New row in the table
	if (idx < 0) {
		idx = socket_table_size++;
	}
	// Row reused
	else {
		queue_free(sock_table[idx].data_sink);
		queue_free(sock_table[idx].ctrl_sink);
	}

	sock_table[idx].data_sink = queue_new(DATA_SINK_SIZE);
	sock_table[idx].ctrl_sink = queue_new(DATA_SINK_SIZE);

	sock_table[idx].virt_fd = socket_fd;
	sock_table[idx].real_fd = socket_fd;
	sock_table[idx].send_seq = 0;
	sock_table[idx].expected_seq = 0;
	sock_table[idx].addrlen = 0;
	sock_table[idx].local_addrlen = 0;
	sock_table[idx].is_sys_sock = 0;

	memset(&sock_table[idx].addr, 0, sizeof(Sockaddr_storage));
	memset(&sock_table[idx].local_addr, 0, sizeof(Sockaddr_storage));

	return idx;
}

// Return the index of the socket descriptor array that contains "socket_fd"
int socket_fd_index(int socket_fd) {
	int i = 0;

	for (i = 0; i < socket_table_size; i++) {
		if (sock_table[i].virt_fd == socket_fd)
			return i;
	}

	return -1;
}

int get_packet_number(int socket_fd) {
	return sock_table[socket_fd_index(socket_fd)].send_seq;
}

// Increase the packet number for sending by one.
void inc_packet_number(int socket_fd) {
	sock_table[socket_fd_index(socket_fd)].send_seq++;
}

// Return the expected sequence number of a given socket
int get_expected_seq(int socket_fd) {
	return sock_table[socket_fd_index(socket_fd)].expected_seq;
}

// Increase the expected sequence number by one.
void inc_expected_seq(int socket_fd) {
	sock_table[socket_fd_index(socket_fd)].expected_seq++;
}

// Return the port number of a given address
int get_port(const Sockaddr_storage *addr) {
	Sockaddr_in6 *tmp = (Sockaddr_in6 *) addr;
	return ntohs(tmp->sin6_port);
}

// Copy a given packet to the data sink of a given socket.
void write_data_sink(int socket_fd, char *packet, int size) {
	SocketTable row		= sock_table[socket_fd_index(socket_fd)];
	queue 		*sink 	= row.data_sink;

	queue_put(sink, packet, size);
}

// Read a packet from the data sink of a given socket
// Return the size of the returned packet in byte
int read_data_sink(int socket_fd, char *packet) {
	SocketTable row = sock_table[socket_fd_index(socket_fd)];
	queue *sink = row.data_sink;
	int data_len;
	int *p_data_len;
	char buff[MAXBUFLEN];

	queue_peek(sink, buff, CTRL_PACKET_SIZE, -1);

	p_data_len = (int *) (buff + 6);
	data_len = *p_data_len;

	queue_remove(sink, packet, CTRL_PACKET_SIZE + data_len);

	return CTRL_PACKET_SIZE + data_len;
}

int is_data_sink_empty(int socket_fd) {
	SocketTable row = sock_table[socket_fd_index(socket_fd)];
	queue *sink = row.data_sink;

	return sink->p == sink->c;
}

// Copy a given packet to the control sink of a given socket.
void write_ctrl_sink(int socket_fd, char *packet) {
	SocketTable row = sock_table[socket_fd_index(socket_fd)];
	queue *sink = row.ctrl_sink;

	queue_put(sink, packet, CTRL_PACKET_SIZE);
}

// Read a packet from the control sink of a given socket
// Return the size of the returned packet in byte
int read_ctrl_sink(int socket_fd, char *packet) {
	SocketTable 	row		= sock_table[socket_fd_index(socket_fd)];
	queue			*sink	= row.ctrl_sink;

	queue_remove(sink, packet, CTRL_PACKET_SIZE);

	return CTRL_PACKET_SIZE;
}

int is_ctrl_sink_empty(int socket_fd) {
	SocketTable row = sock_table[socket_fd_index(socket_fd)];
	queue *sink = row.ctrl_sink;

	return sink->p == sink->c;
}

void ncp_init() {
	int rv = pthread_create(&proxy, NULL, proxy_run, (void *) 1);

	if (rv) {
		perror("ERROR; pthread_create()");
		exit(-1);
	}
}

// Main method for the proxy
void *proxy_run(void *id) {
	Pollfd 	poll_subjects[NUM_SOCKETS];
	// poll_virtfd[i] is the virtual socket descriptor of poll_subjects[i].fd
	int		poll_virtfd[NUM_SOCKETS];
	char 	packet[MAXBUFLEN];
	char 	ack[CTRL_PACKET_SIZE];

	ssize_t 	recv_packet_size;
	int 		rv;
	int			i;
	int			virtfd;
	int 		realfd;
	int			seq_num;
	int 		expected;
	int			idx;
	int	 		num_subjects;

	while (1) {
		num_subjects = 0;

		// Read the list of registered sockets
		for (i = 0;i < socket_table_size;i++) {
			if (sock_table[i].virt_fd >= 0 && !sock_table[i].is_sys_sock) {
				poll_virtfd[num_subjects] = sock_table[i].virt_fd;
				poll_subjects[num_subjects].fd = sock_table[i].real_fd;
				poll_subjects[num_subjects].events = POLLIN;
				num_subjects++;
			}
		}

		rv = poll(poll_subjects, num_subjects, SHORT_TIMEOUT);

		if (rv > 0) {
			for (i = 0;i < num_subjects;i++) {
				if (poll_subjects[i].revents & POLLIN) {
					virtfd = poll_virtfd[i];
					realfd = poll_subjects[i].fd;
					// Find the socket table index associated with socket "s"
					idx = socket_fd_index(virtfd);

					expected = get_expected_seq(virtfd);
					sock_table[idx].addrlen = sizeof(Sockaddr_storage);
					recv_packet_size = recvfrom(realfd, packet, MAXBUFLEN, 0,
							(Sockaddr *) &sock_table[idx].addr, &sock_table[idx].addrlen);
					seq_num = packet_seq_number(packet);

					// data packet
					if (is_data_packet(packet)) {
						if (seq_num == expected) {
							ack_packet(virtfd, expected, ack);
							sendto(realfd, ack, CTRL_PACKET_SIZE, 0, (struct sockaddr *) &sock_table[idx].addr,
									sock_table[idx].addrlen);

							inc_expected_seq(virtfd);
							write_data_sink(virtfd, packet, recv_packet_size);
//							printf("[proxy] forward a packet to the data sink\n");
						}
						// Old packet comes. The opponent might not have got an ACK. Send the ACK again.
						else if (seq_num < expected) {
							ack_packet(virtfd, seq_num, ack);
							sendto(realfd, ack, CTRL_PACKET_SIZE, 0, (struct sockaddr *) &sock_table[idx].addr,
									sock_table[idx].addrlen);
							printf("[proxy] Old packet comes. Send ACK %d\n", seq_num);
						}
					}
					// control packet
					else {
						write_ctrl_sink(virtfd, packet);
//						printf("[proxy] forward packet %d to the control sink of socket %d\n", seq_num, s);
					}
				}
			}
		}
	}
}

// If the given socket has a local address, rebind it; otherwise, do nothing.
void ncp_rebindsock(int sockfd) {
	int newsock;
	int rv;

	int idx = socket_fd_index(sockfd);

	// if the socket was bound before, recover it.
	if (sock_table[idx].local_addrlen > 0) {
		printf("[rebind] virtual id %d\n", sock_table[idx].virt_fd);
		newsock = gnu_socket(PF_INET, SOCK_DGRAM, 0);
		rv = gnu_bind(newsock, (Sockaddr *) &sock_table[idx].local_addr,
				sock_table[idx].local_addrlen);

		if (!rv) {
			sock_table[idx].real_fd = newsock;
		}
		else
			perror("[rebind] Fail to rebind");
	}
}

// Save the local address associated to a given virtual socket.
void ncp_save_local_addr(int sock) {
	Sockaddr_storage *p_local_addr;
	socklen_t *p_addr_len;
	uint addr_sz = sizeof(Sockaddr_storage);
	int idx = socket_fd_index(sock);

	p_local_addr = &sock_table[idx].local_addr;
	p_addr_len = &sock_table[idx].local_addrlen;
	*p_addr_len = (socklen_t) addr_sz;

	memset(p_local_addr, 0, addr_sz);
	getsockname(sock, (Sockaddr *) p_local_addr, p_addr_len);
}

// Return the real socket descriptor from a given virtual socket descriptor.
int ncp_real_fd(int virt_fd) {
	int idx = socket_fd_index(virt_fd);
	return sock_table[idx].real_fd;
}

// Called after MTCP has restarted the process. Rebind every socket if possible.
void ncp_rebind_all_sockets() {
	int i;

	for (i = 0; i < socket_table_size; i++) {
		ncp_rebindsock(sock_table[i].virt_fd);
	}
}

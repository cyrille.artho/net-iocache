package classes.java.nio.channels;

import gov.nasa.jpf.util.test.TestJPF;

import java.net.InetSocketAddress;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.NotYetBoundException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import org.junit.Test;


public class ServerSocketChannelTest extends TestJPF {
	
	static final int portbase=1110;
	static final String clientCmd="+jpf-net-iocache.boot.peer.command=sh bin/server-socket-channel-test-client.sh ";
	
	@Test
	public void bindAfterClose() throws Exception {
		if (verifyNoPropertyViolation()) {
			int ok = 0;
			try {
			        ServerSocketChannel ch =
					ServerSocketChannel.open();
		       		ch.close();
				ok++;
		        	ch.bind(null);
			} catch (ClosedChannelException e) {
				ok++;
			}
			assert (ok == 2);
		}
	}

	@Test
	public void isOpenShouldReturnTrueAfterOpen() throws Exception {
		if (verifyNoPropertyViolation()) {
			ServerSocketChannel ssc = ServerSocketChannel.open();
			assertTrue(ssc.isOpen());
		}
	}

	@Test
	public void isOpenShouldReturnFalseAfterClose() throws Exception {
		if (verifyNoPropertyViolation()) {
			ServerSocketChannel ssc = ServerSocketChannel.open();
			ssc.close();
			assertFalse(ssc.isOpen());
		}
	}

	@Test
	public void isBlockingShouldReturnTrueByDefault() throws Exception {
		if (verifyNoPropertyViolation()) {
			ServerSocketChannel ssc = ServerSocketChannel.open();
			assertTrue(ssc.isBlocking());
		}
	}

	@Test
	public void configureBlockingTest() throws Exception {
		if (verifyNoPropertyViolation()) {
			ServerSocketChannel ssc = ServerSocketChannel.open();
			ssc.configureBlocking(false);
			assertFalse(ssc.isBlocking());
		}
	}

	@Test
	public void acceptShouldThrowNotYetConnectedExceptionIfNotBound() throws Exception {
		if (verifyNoPropertyViolation()) {
			ServerSocketChannel ssc = ServerSocketChannel.open();
			ssc.configureBlocking(false);
			try {
				ssc.accept();
				fail();
			}
			catch (NotYetBoundException e) {
			}
		}
	}

	@Test
	public void acceptThrowsClosedChannelExceptionIfCalledAfterClose() throws Exception {
		if (verifyNoPropertyViolation()) {
			ServerSocketChannel ssc = ServerSocketChannel.open();
			ssc.configureBlocking(false);
			ssc.close();
			try {
				ssc.accept();
				fail();
			}
			catch (ClosedChannelException e) {
			}
		}
	}
	
	/*
	 * The socket channel returned by ServerSocketChannel.accept(), if any, will be 
	 * in blocking mode regardless of the blocking mode of this channel. 
	 */
	@Test
	public void acceptReturnsChannelInBlockingMode() throws Exception {
		int port=portbase+1;  //choose a different port of each test case since a port may remain in state "in use" for a while even if the server has been shut down
		if (verifyNoPropertyViolation(clientCmd+port)) {
			ServerSocketChannel ssc = ServerSocketChannel.open();
			ssc.socket().bind(new InetSocketAddress("localhost", port));
			SocketChannel sc = ssc.accept();
			assertTrue(sc.isBlocking());
			sc.close();
			ssc.configureBlocking(false);
			sc=ssc.accept();
			assertTrue(sc==null || sc.isBlocking());
			if (sc!=null) sc.close();
			ssc.close();
		}
	} 
	
	
	/*
	 * ServerSocketChannel#accept() should never return null in blocking mode if 
	 * a client sends a connect request to the server
	 */
	@Test 
	public void blockingAcceptNeverReturnsNullIfConnection() throws Exception {
		int port=portbase+2;  //choose a different port of each test case since a port may remain in state "in use" for a while even if the server has been shut down
		if (verifyNoPropertyViolation(clientCmd+port)) {
			ServerSocketChannel ssc = ServerSocketChannel.open();
			ssc.socket().bind(new InetSocketAddress("localhost", port));
			SocketChannel sc = ssc.accept();
			assertNotNull(sc);
			sc.close();
			ssc.close();
		}
	} 

	/*
	 * ServerSocketChannel#accept() returns SocketChannel or null in non-blocking mode 
	 * In the model class, both cases should occurs while JPF explores the state space
	 * Hence it should neither be the case that accept() always returns null nor that
	 * it always returns not null. This is why we expect  both of the two following 
	 * tests to fail!
	 */
	@Test(expected=AssertionError.class)
	public void nonblockingAcceptReturnsAlwaysNull() throws Exception {
		int port=portbase+3;  //choose a different port of each test case since a port may remain in state "in use" for a while even if the server has been shut down
		if (verifyNoPropertyViolation(clientCmd+port)) {
			ServerSocketChannel ssc = ServerSocketChannel.open();
			ssc.socket().bind(new InetSocketAddress("localhost", port));
			ssc.configureBlocking(false);
			SocketChannel sc = ssc.accept(); 
			assertNull(sc);  
			ssc.close();
		}
	}
	@Test(expected=AssertionError.class)
	public void nonblockingAcceptReturnsAlwaysNotNull() throws Exception {
		int port=portbase+4;  //choose a different port of each test case since a port may remain in state "in use" for a while even if the server has been shut down
		if (verifyNoPropertyViolation(clientCmd+port)) {
			ServerSocketChannel ssc = ServerSocketChannel.open();
			ssc.socket().bind(new InetSocketAddress("localhost", port));
			ssc.configureBlocking(false);
			SocketChannel sc = ssc.accept();
			assertNotNull(sc);
			sc.close(); 
			ssc.close();
		}
	}
}

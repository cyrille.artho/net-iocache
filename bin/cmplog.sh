#!/bin/bash
# Show the log difference between two log files with a given name in directories "log" and "out".
#
# Exit value
# 0: No difference
# 1: Log files are different.
# 2: The template file does not exist.

. `dirname "$0"`/env.sh

# Filter out sensitive lines.
# usage: filter (file)
filter() {
	grep -Ev '[0-9]{1,2}/[0-9]{1,2}/[0-9]{1,2}|([0-9]+ [0-9]+:[0-9][0-9]:[0-9][0-9]) ' < $1 | \
		grep -v 'RIACS/NASA' | \
		grep -vi 'elapsed time:' | \
		grep -v '^max memory:' | \
		grep -v '^instructions:' | \
		grep -v '^loaded code:' | \
		grep -v '^heap:' | \
		grep -v '^states:' | \
		grep -v '^search:' | \
		grep -v '^choice generators:' | \
		grep -v 'receive : [0-9]' | \
		grep -v 'response : [a-zA-Z]' | \
		grep -v 'saveState' | \
		grep -v 'createUnixDomainChannel' | \
		grep -v 'Saved STATES' | \
		grep -v '^\[INFO\] \[PEER ..*\]' | \
		grep -v '^\[INFO\] ----------------------------------- \[' | \
		grep -v "^\[INFO\] starting PEER ..* '.*/..*'" | \
		grep -v 'Model checking /' | \
		grep -v 'file sent: ..*[/\\]bin[/\\]www[/\\]..*' | \
		grep -v 'localport=' | \
		sed  -e 's/File sent:.*bin/File sent: ....bin/' \
		-e 's/ (rev [0-9]*)//'
}

if [ "$#" -lt 1 ]
then
	echo "usage: $0 (log file name)"
	exit 1
fi

LOG_DIR="${SCRIPT_DIR}/log"
FILE="${LOG_DIR}/$1"
OUT="${SCRIPT_DIR}/out/$1"

if [ -f $OUT ]
then
	BASE=`basename ${OUT} .log`
	OUT_FILTERED="${LOG_DIR}/${BASE}.out.filtered"
	LOG_FILTERED="${LOG_DIR}/${BASE}.log.filtered"
	filter ${OUT} > ${OUT_FILTERED}
	filter ${FILE} > ${LOG_FILTERED}

	if [ "`cmp ${OUT_FILTERED} ${LOG_FILTERED}`" != "" ]
	then
		echo "To compare the outputs, type:"
		# diff <old> <new>
		echo "diff -uw ${OUT_FILTERED} ${LOG_FILTERED}"
		exit 1
	else
		# Remove temporary files.
		rm ${OUT_FILTERED} ${LOG_FILTERED}
		exit 0
	fi
fi

exit 2

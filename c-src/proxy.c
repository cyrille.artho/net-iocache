#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <netdb.h>
#include <assert.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <dmtcpaware.h>

#include "proxy.h"
#include "queue.h"


// A queue of FDs in the backlog that have not been accepted by the SUT.
queue *backlog_fds;
// The list of FDs shared with a SUT.
int proxy_fds[MAX_FDS];
int num_fds = 0;
// The communication FD that connects to MC.
int comm_fd;
// This server socket FD is checked for pending connections before checkpointing.
int listening_fd = 0;
// This flag prevents the main function from polling during checkpointing.
int save_req = 0;


void ms_sleep(int ms);
int prepare_unix_sock();

void pre_ckpt();
void post_ckpt();
void post_restart();

int process_packet(const char *packet);
void process_connect(int fd, int port);
void process_accept(int fd);
void process_close(int fd);
void process_create_sock();
void process_bind(int fd, int port);

void notify_cache();
// Add the given FD to the list of FDs.
void fd_list_add(int fd);
// Remove the element matching the given FD that first appears in the list of FDs.
void fd_list_remove(int fd);

int main() {
	fd_set read_fds;
	int rv = dmtcpIsEnabled();

	// Setup callback functions.
	if (rv) {
		dmtcpInstallHooks(pre_ckpt, post_ckpt, post_restart);
		backlog_fds = queue_new(MAX_FDS * sizeof(int));
	}

	// Open a Unix domain channel.
	comm_fd = prepare_unix_sock();

	while (1) {
		timeval timeout;
		int packet_size;
 		char packet[RECV_PACKET_MAX_SIZE];

		// Wait until checkpointing is finished.
		while (save_req) {
			fprintf(stderr, "[%s] Wait until checkpointing is finished.\n", __func__);
		}
		
		// Set the FD to be observed.
		FD_ZERO(&read_fds);
		FD_SET(comm_fd, &read_fds);
		// Set timeout.
		timeout.tv_sec = 0L;
 		timeout.tv_usec = 0L;
		// Wait for an incoming packet.
		rv = select(comm_fd + 1, &read_fds, NULL, NULL, &timeout);

		if (rv > 0) {
			packet_size = recv(comm_fd, packet, sizeof(packet), 0);
			if (packet_size > 0)
				process_packet(packet);
		}

		ms_sleep((int) (SELECT_TIMEOUT_USEC / 1000));
	}

	_real_close(comm_fd);
	return 0;
}

int sendfd(int s, int fd, char *buf, int len) {
	struct iovec iov;
	struct msghdr msg;
	struct cmsghdr *cmsg;
	int n;
	char cms[CMSG_SPACE(sizeof(int))];
	
	iov.iov_base = buf;
	iov.iov_len = (size_t) len;

	memset(&msg, 0, sizeof msg);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = (caddr_t)cms;
	msg.msg_controllen = CMSG_LEN(sizeof(int));

	cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_len = CMSG_LEN(sizeof(int));
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type = SCM_RIGHTS;
	memmove(CMSG_DATA(cmsg), &fd, sizeof(int));

	if((n = sendmsg(s, &msg, 0)) != (int) iov.iov_len)
		return -1;
        return 0;
}

int recvfd(int s) {
        int n;
        int fd;
        char buf[1];
        struct iovec iov;
        struct msghdr msg;
        struct cmsghdr *cmsg;
        char cms[CMSG_SPACE(sizeof(int))];

        iov.iov_base = buf;
        iov.iov_len = 1;

        memset(&msg, 0, sizeof msg);
        msg.msg_name = 0;
        msg.msg_namelen = 0;
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;

        msg.msg_control = (caddr_t)cms;
        msg.msg_controllen = sizeof cms;

        if((n=recvmsg(s, &msg, 0)) < 0)			
			return -1;
		

		if(n == 0){		
			return -1;
        }
        cmsg = CMSG_FIRSTHDR(&msg);
        memmove(&fd, CMSG_DATA(cmsg), sizeof(int));
        return fd;
}

void ms_sleep(int ms) {
	timespec interval;
	interval.tv_sec = ms / 1000;
	interval.tv_nsec = (ms % 1000) * 1000000l;

	nanosleep(&interval, NULL);
}

int prepare_unix_sock() {
	sockaddr_un addr;
	const int MAX_RETRIES = 80;
	const int INTERVAL = 40;
	int sockfd;
	int len;
	int i;
	const char *sock_addr = MC_SOCK_NAME;

	if ((sockfd = _real_socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		perror("server: socket");
		exit(1);
	}
	
	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, sock_addr);
	
	len = sizeof(addr.sun_family) + strlen(addr.sun_path);

	ms_sleep(INTERVAL);
	// Connect to the cache.
	for (i = 0; i < MAX_RETRIES; i++) {
		if (_real_connect(sockfd, (sockaddr *) &addr, len) == 0) {
			break;
		}
		ms_sleep(INTERVAL);
	}
	assert(i < MAX_RETRIES);

	return sockfd;
}

void pre_ckpt() {
	int active = 1;
	
	_real_close(comm_fd);
	// Set Save Flag.
	save_req = 1;

	// Clear the backlog if Proxy runs as a server.
	while (listening_fd > 0 && active > 0) {
		timeval timeout;
		fd_set read_fds;

		// Set the FD to be observed.
		FD_ZERO(&read_fds);
		FD_SET(listening_fd, &read_fds);
		// Set timeout.
		timeout.tv_sec = 0L;
		timeout.tv_usec = SELECT_TIMEOUT_USEC;
		// Wait for an incoming packet.
		active = select(listening_fd + 1, &read_fds, NULL, NULL, &timeout);

		if (active > 0) {
			int fd = accept(listening_fd, NULL, NULL);
			// Store this connection in a queue.
			queue_put(backlog_fds, (char *) &fd, sizeof(int));
		}
	}
}

void post_ckpt() {
	// Notify the I/O cache, which is blocking on "syncckpt".
	fprintf(stderr, "[%d (real)][%s] Notify Cache @ %lld\n", _real_getpid(), __func__, cur_time_ms());
	notify_cache();
	// Reconnect the communication channel.
	comm_fd = prepare_unix_sock();
	// Clear Save Flag so that the main function can continue running.
	save_req = 0;	
}

void post_restart() {
	int i;
	char c_num_fds = (char) num_fds;
		
	post_ckpt();

	// Send the number of FDs to be sent.
	send(comm_fd, &c_num_fds, 1, 0);
	
	// Share the FDs in the list.
	for (i = 0; i < num_fds; i++) {
		char buf = (char) proxy_fds[i];
		// Send the Proxy FD number as a content and attach the FD.
		sendfd(comm_fd, proxy_fds[i], &buf, 1);
	}
}

int process_packet(const char *packet) {
	int fd;
	int port;
	int op = (int) packet[0];

	switch (op) {
	case OP_CONNECT:
		fd = packet[1];
		port = ntohl(*(int *) (packet + 2));
		process_connect(fd, port);
		break;
	case OP_ACCEPT:
		fd = packet[1];
		process_accept(fd);
		break;
	case OP_CLOSE:
		fd = packet[1];
		process_close(fd);		
		break;
	case OP_CREATE_SOCK:
		process_create_sock();
		break;
	case OP_BIND:
		fd = packet[1];
		port = ntohl(*(int *) (packet + 2));
		process_bind(fd, port);
		break;
	default:
		assert(0);
	}
	
	return 0;
}

void process_connect(int fd, int port) {
	addrinfo hints;
	addrinfo *servinfo;
	addrinfo *p;
	const int ACK_SIZE = 3;
	int rv;
	char str_port[4];
	char packet[ACK_SIZE];

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	sprintf(str_port, "%d", port);
	getaddrinfo(NULL, str_port, &hints, &servinfo);

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((rv = connect(fd, p->ai_addr, p->ai_addrlen)) == -1) {
			continue;
		}
		
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "server: failed to bind\n");
		assert(0);
	}

	freeaddrinfo(servinfo); // all done with this structure

	packet[0] = OP_CONNECT;
	packet[1] = (char) fd;
	packet[2] = (char) rv;

	send(comm_fd, packet, sizeof(packet), 0);

}

void process_accept(int fd) {
	const int ACK_SIZE = 3;
	int rv;
	char packet[ACK_SIZE];

	// The backlog has a FD.
	if (backlog_fds->p > backlog_fds->c) {
		queue_remove(backlog_fds, (char *) &rv, sizeof(rv));
	} else {
		rv = accept(fd, NULL, NULL);
	}

	packet[0] = OP_ACCEPT;
	packet[1] = (char) fd;
	// New FD
	packet[2] = (char) rv;
	
	// Send ACK + new FD
	sendfd(comm_fd, rv, packet, sizeof(packet));
	// Add new FD to the list.
	fd_list_add(rv);
}

void process_close(int fd) {
	const int ACK_SIZE = 3;
	int rv;
	char packet[ACK_SIZE];

	// Must shut down before closing to make sure the socket will not be stuck in a waiting state.
	shutdown(fd, SHUT_RDWR);
	rv = close(fd);

	packet[0] = OP_CLOSE;
	packet[1] = (char) fd;
	packet[2] = (char) rv;

	// Remove this FD from the list.
	fd_list_remove(fd);
	// Send ACK.
	send(comm_fd, packet, sizeof(packet), 0);	
}

void process_create_sock() {
	const int ACK_SIZE = 2;
	int fd = socket(AF_INET, SOCK_STREAM, 0);
	char packet[ACK_SIZE];

	packet[0] = OP_CREATE_SOCK;
	packet[1] = (char) fd;
	
	// Send ACK + FD
	sendfd(comm_fd, fd, packet, sizeof(packet));
	// Add new FD to the list.
	fd_list_add(fd);
}

void process_bind(int fd, int port) {
	addrinfo hints;
	addrinfo *servinfo;
	addrinfo *p;
	const int ACK_SIZE = 3;
	int rv;
	char str_port[4];
	char packet[ACK_SIZE];

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	sprintf(str_port, "%d", port);
	getaddrinfo(NULL, str_port, &hints, &servinfo);

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if (bind(fd, p->ai_addr, p->ai_addrlen) == -1) {
			perror("server: bind");
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "server: failed to bind\n");
		assert(0);
	}

	freeaddrinfo(servinfo); // all done with this structure
	rv = listen(fd, BACKLOG);

	packet[0] = OP_BIND;
	packet[1] = (char) fd;
	packet[2] = (char) rv;
	// Remember this listening FD.
	listening_fd = fd;

	send(comm_fd, packet, sizeof(packet), 0);
}

void notify_cache() {
	char *dir = getenv(CHKPNT_DIR);
	char fifo[FIFO_NAME_MAX_SIZE];
	char s[2];
	int fd;
	int num;
	
	if (!dir)
		perror("CHKPNT_DIR is not defined");

	// Build the pipe name.
	memset(fifo, 0, FIFO_NAME_MAX_SIZE);
	sprintf(fifo, "%s/%s", dir, PIPE_NAME);

	// notify the cache in both cases
	mknod(fifo, S_IFIFO | 0666, 0);
	memset(s, 0, sizeof s);
	
	fd = open(fifo, O_WRONLY);
	s[0] = 1;
		
	if ((num = write(fd, s, strlen(s))) == -1)
		perror("write");

	_real_close(fd);
}

void fd_list_add(int fd) {
	assert(num_fds < MAX_FDS);
	proxy_fds[num_fds++] = fd;
}

void fd_list_remove(int fd) {
	int i = 0;

	for (i = 0; i < num_fds; i++) {
		if (proxy_fds[i] == fd) {
			// Move the remaining elements forward.
			memmove(proxy_fds + i, proxy_fds + i + 1, sizeof(int) * (num_fds - i - 1));
			num_fds--;
			return;
		}
	}

	assert(0);
}

//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import static org.junit.Assert.*;
import gov.nasa.jpf.network.cache.event.SendByteEvent;

import java.io.IOException;
import java.net.InetAddress;
import java.util.logging.*;

import org.junit.*;
import org.junit.runner.*;
import org.junit.runner.notification.*;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

@RunWith(CacheLayerTest.CacheLayerRunner.class)
public class CacheLayerTest {

	public static class CacheLayerRunner extends BlockJUnit4ClassRunner {

		public CacheLayerRunner(Class<?> klass) throws InitializationError {
			super(klass);
		}

		@Override
		public void run(RunNotifier rn) {
			rn.addListener(new RunListener() {
				public void testStarted(Description description) {
					changeTest(CLASS_NAME + "." + description.getMethodName());
				}
			});
			
			super.run(rn);
		}

	}

	private static final String CLASS_NAME = "CacheLayerTest";

	public static CacheLayer cacheLayer;

	public static byte[] numeric = { '0', '1', '2', '3' };

	// The variable which specifies the delay behavior of a newly created
	// MockCacheLayer instance.
	static boolean delayed = false;

	private static Logger log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	@BeforeClass
	public static void init() throws Exception {
		// Set related classes to testing mode
		MockCacheLayer.setLogger(log);
		SendRecvCache.setLogger(log);
	}

	private static void resetCache() throws Exception {
		MockCacheLayer.reset();
		SendRecvCache.reset();

		cacheLayer = new MockCacheLayer(delayed);
	}

	private static void connect() throws Exception {
		MockCacheLayer.stt.addSocket();
		cacheLayer.connect(0, InetAddress.getLocalHost(), 5555);
		cacheLayer.changeState(0);
	}

	@Before
	public void setUp() throws Exception {
		resetCache();

	}

	@Test
	public void read() throws Exception {
		byte[] buffer = new byte[1];
		int num_byte;

		connect();
		// alternate write/read
		writeToCache(0, (byte) '0');
		cacheLayer.changeState(1);
		num_byte = cacheLayer.read(0, buffer);
		assertEquals(1, num_byte);
		assertEquals((byte) 'a', buffer[0]);

		writeToCache(0, (byte) '1');
		cacheLayer.changeState(2);
		cacheLayer.read(0, buffer);
		assertEquals((byte) 'b', buffer[0]);

		writeToCache(0, (byte) '2');
		cacheLayer.changeState(3);
		cacheLayer.read(0, buffer);
		assertEquals((byte) 'c', buffer[0]);

		writeToCache(0, (byte) '3');
		cacheLayer.changeState(4);
		cacheLayer.read(0, buffer);
		assertEquals((byte) 'd', buffer[0]);

		writeToCache(0, (byte) '4');
		cacheLayer.changeState(5);
		cacheLayer.read(0, buffer);
		assertEquals(0, buffer[0]);
		// alternate write/read

		resetCache();
		connect();
		// write all -> read all
		writeToCache(0, (byte) '0');
		cacheLayer.changeState(1);
		writeToCache(0, (byte) '1');
		cacheLayer.changeState(2);
		writeToCache(0, (byte) '2');
		cacheLayer.changeState(3);
		writeToCache(0, (byte) '3');
		cacheLayer.changeState(4);
		cacheLayer.read(0, buffer);
		assertEquals((byte) 'a', buffer[0]);
		cacheLayer.changeState(5);
		cacheLayer.read(0, buffer);
		assertEquals((byte) 'b', buffer[0]);
		cacheLayer.changeState(6);
		cacheLayer.read(0, buffer);
		assertEquals((byte) 'c', buffer[0]);
		cacheLayer.changeState(7);
		cacheLayer.read(0, buffer);
		assertEquals((byte) 'd', buffer[0]);
		// write all -> read all

		resetCache();
		connect();
		// double write/double read
		writeToCache(0, (byte) '0');
		cacheLayer.changeState(1);
		writeToCache(0, (byte) '1');
		cacheLayer.changeState(2);
		cacheLayer.read(0, buffer);
		assertEquals((byte) 'a', buffer[0]);
		cacheLayer.changeState(3);
		cacheLayer.read(0, buffer);
		assertEquals((byte) 'b', buffer[0]);
		// double write/double read

		// -- Server is delayed in first read --
		delayed = true;
		resetCache();
		delayed = false;
		connect();
		writeToCache(0, (byte) '0');
		num_byte = cacheLayer.read(0, buffer);
		assertEquals(-1, num_byte);
		// -- Server is delayed in first read --

		// -- Server has got a response --
		cacheLayer.pollPeer(0);
		cacheLayer.read(0, buffer);
		assertEquals((byte) 'a', buffer[0]);
		// -- Server has got a response --
	}

	@Test
	public void write() throws Exception {
		SendRecvCache tree = (SendRecvCache) MockCacheLayer.commData.get(0);
		
		connect();

		// write new data
		writeToCache(0, numeric[0]);
		assertEquals(new SendByteEvent(numeric[0]), tree.nodes.get(1).event);
		cacheLayer.changeState(1);

		writeToCache(0, numeric[1]);
		assertEquals(new SendByteEvent(numeric[1]), tree.nodes.get(3).event);
		cacheLayer.changeState(2);
		// write new data

		// write duplicate data
		cacheLayer.changeState(1);
		writeToCache(0, numeric[1]);
		assertEquals(new SendByteEvent(numeric[1]), tree.nodes.get(3).event);
		// write duplicate data
	}

	@Test
	public void changeState() throws Exception {
		// change state before creating any connection
		cacheLayer.changeState(0);
		cacheLayer.changeState(1);
		cacheLayer.changeState(2);
		cacheLayer.changeState(3);
		cacheLayer.connect(0, InetAddress.getLocalHost(), 5555);
		cacheLayer.changeState(4);
		resetCache();
	}

	private static void changeTest(String testName) {
		Handler[] handlers = log.getHandlers();
		FileHandler file = null;

		try {
			file = new FileHandler(testName + ".xml");
		} catch (SecurityException e) {
			e.printStackTrace();
			assert false;
		} catch (IOException e) {
			e.printStackTrace();
			assert false;
		}

		assert handlers.length <= 1;
		if (handlers.length != 0) {
			log.removeHandler(handlers[0]);
			handlers[0].close();
		}

		assert file != null;
		log.addHandler(file);
		MockCacheLayer.setLogger(log);
		SendRecvCache.setLogger(log);
	}
	
	private static void writeToCache(int id, byte c) throws IOException {
		byte[] msg = new byte[] { c };
		
		cacheLayer.write(id, msg, 0, 1);
	}

}

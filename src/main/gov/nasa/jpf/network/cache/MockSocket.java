//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class MockSocket extends Socket {

	boolean delayed = false;
	
	public class MockInputStream extends InputStream {
		char[] data = new char[] { 'a', 'b', 'c', 'd', 0 };

		int p = 0;
		
		public int read() throws IOException {
			assert (!delayed) : "read called when no data available";
			return data[p++];
		}

		public int available() {
			if (delayed) {
				delayed = false; 
				return 0;
			}
			
			return stream_limit - p;
		}
	}

	public class MockOutputStream extends OutputStream {
		public void write(int b) throws IOException {
			stream_limit++;
		}
	}

	int stream_limit = 0;

	public MockSocket(InetAddress addr, int port) {
		this(addr, port, false);
	}

	public MockSocket(InetAddress addr, int port, boolean delayed) {
		this.delayed = delayed;
	}
	
	public InputStream getInputStream() {
		return new MockInputStream();
	}

	public OutputStream getOutputStream() {
		return new MockOutputStream();
	}

}

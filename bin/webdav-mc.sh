#!/bin/bash

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

PACKAGE="gov.nasa.jpf.network.webdav"
PROG_ARG="${PACKAGE}.WebDAVTest PROPFIND http://localhost/myWebDAV/ test.xml"
CLIENT_LOG="${HOME}/webdav.log"

echo "start JPF verifying a WebDAV client ..."
${RUN_EXAMPLE_CMD} ${JPF_ARG} ${PROG_ARG} > ${CLIENT_LOG}

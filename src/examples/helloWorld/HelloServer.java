package helloWorld;

import java.io.*;
import java.net.*;

public class HelloServer {

	public static final int DEFAULT_PORT = 10000;

	private static void wait(int millis) {
		try{Thread.sleep(millis);}catch(InterruptedException e){};
	}
	
	/**
	 * Probably the simplest 'non-trivial' client-server application.
	 */
	public static void main(String[] args) throws IOException {
		int port=(args.length>0)?Integer.parseInt(args[0]):DEFAULT_PORT;
		
		ServerSocket ssock = new ServerSocket(port);
		System.out.println("[HelloServer.main] waiting for connection requests at port "+port);

		Socket sock = ssock.accept();
		System.out.println("[HelloServer.main] accepted connection.");
		
		InputStream  in =sock.getInputStream();
		OutputStream out=sock.getOutputStream();

		byte[] buf=new byte[10];
		int len;
		do {
			len=in.read(buf);
			if (len>0) {
				String request=new String(buf,0,len);
				System.out.println("[HelloServer.main] received message '"+request+"'.");
				
				final String response="world!";
				out.write(response.getBytes()); 
				System.out.println("[HelloServer.main] sent message '"+response+"'.");			
			}
		} while (len!=-1);
	    wait(50);    //wait a bit to synchronize output with the client in the log file
		System.out.println("[HelloServer.main] received eof.");						
		
		sock.close();
	    ssock.close();
		System.out.println("[HelloServer.main] closed server port and says good bye!");
	}
}

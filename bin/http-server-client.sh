#!/bin/sh
SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

PACKAGE="gov.nasa.jpf.network.http"
SERVER_LOG="$HOME/http-server.log"


echo "start an HTTPServer in the background"
java -classpath $CLASSPATH ${PACKAGE}.HTTPServer 2 > $SERVER_LOG &
sleep 1
echo "start an HTTP client"
java -classpath $CLASSPATH ${PACKAGE}.HTTPClient "${SCRIPT_DIR}/http-server-client.sh" 2


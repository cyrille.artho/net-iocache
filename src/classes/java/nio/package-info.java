/**
 * This package contains Byte* classes and exceptions.
 *
 * All of them are implemented straightforwardly but
 * only few methods are tested.
 */
package java.nio;

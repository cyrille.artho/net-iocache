#!/bin/bash
# Run the ND alphabet server SUT with the deterministic alphabet client running on DMTCP.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

if [ $# -lt 1 ]
then
	echo "Usage : $0 [-m] [-i] [-n] [-a] [<#threads>-<#messages>]+"
	echo "-m Move the log file from home to the log directory."
	echo "-i Run with the io strategy."
	echo "-n Run with the nd strategy."
	echo "-a Run with the always strategy."
	exit
fi

chk_vdso_exit

# Set the location of checkpoints
export CHKPNT_DIR="${PROJ_TOP}/tmp-alphabet-server"
# Set the SUT port number. This value must be exported so that DMTCP can read.
export SUT_PORT="8791"

JPF_ARG="+${OPT_CHECKPOINT_DIR}=${CHKPNT_DIR} +${OPT_SUT_PORT}=${SUT_PORT}"
PACKAGE="gov.nasa.jpf.network.alphabet"
OUT_FILENAME=`output_file $0`
LOG="${HOME}/${OUT_FILENAME}"
JPF_FILE="${EXAMPLE_DIR}/gov/nasa/jpf/network/alphabet/NDAlphabetServer-dmtcp.jpf"
LOG_DIR="log"
PORT=18586

# Check options given.
MOVE_LOG=0
IO_STRATEGY=0
ND_STRATEGY=0
ALWAYS_STRATEGY=0
while [[ $1 == -* ]]
do
	if [ $1 = "-m" ]
	then
		MOVE_LOG=1
	elif [ $1 = "-i" ]
	then
		IO_STRATEGY=1
	elif [ $1 = "-n" ]
	then
		ND_STRATEGY=1
	elif [ $1 = "-a" ]
	then
		ALWAYS_STRATEGY=1
	fi

	shift
done

# $1: #threads
# $2: #messages
# $3: Subfix of the log file name.
move_log() {
	# Add parameters to the log file name.
	PARAM=`echo ${OUT_FILENAME} | sed -e "s/\.log/-$1-$2-$3\.log/"`
	mv ${LOG} ${LOG_DIR}/${PARAM}
}

# $1: #threads
# $2: #messages
# $3: Strategy (0 = io, 1 = nd, 2 = always)
run() {
	# Recreate the checkpoint directory.
	rm -rf ${CHKPNT_DIR}
	mkdir ${CHKPNT_DIR}
	# Remove any checkpoint file left on this directory.
	rm -f ${SCRIPT_DIR}/*.dmtcp

	cd ${SCRIPT_DIR}
	# Must run the coordinator with option "background", otherwise the peer cannot restart.
	${DMTCP_COORDINATOR} --ckptdir ${CHKPNT_DIR} --background

	# Check the third argument for the strategy to be used.
	if [ $3 -eq "0" ]
	then
		JPF_FILE="${EXAMPLE_DIR}/gov/nasa/jpf/network/alphabet/NDAlphabetServer-dmtcp.jpf"
		SUBFIX="io"
	elif [ $3 -eq "1" ]
	then
		JPF_FILE="${EXAMPLE_DIR}/gov/nasa/jpf/network/alphabet/NDAlphabetServer-dmtcp-nd.jpf"
		SUBFIX="nd"
	elif [ $3 -eq "2" ]
	then
		JPF_FILE="${EXAMPLE_DIR}/gov/nasa/jpf/network/alphabet/NDAlphabetServer-dmtcp-always.jpf"
		SUBFIX="always"
	fi

	# Start JPF running the server side. It will wait until Proxy sets up a Unix domain channel.
	${RUN_JPF_JNILIB} ${JPF_FILE} ${JPF_ARG} $1 ${PORT} > ${LOG} &
	JPF_PID=$!
	sleep 2

	# Start Proxy.
	echo "Start Proxy"
	start_proxy
	echo "Proxy PID: $!"
	sleep 1

	# Start the client side.
	echo "Start Secure Client"
	${DMTCP_CHECKPOINT} ${CBUILD_DIR}/alphabet-client $1 $2 &
	# Save the peer PID.
	PEER_PID=$!
	echo "Client PID: ${PEER_PID}"
	echo "${PEER_PID}" > ${CHKPNT_DIR}/${PEER_PID_FILE}

	# Wait the JPF process.
	wait ${JPF_PID}

	if [ "${MOVE_LOG}" -eq "1" ]
	then
		move_log $1 $2 ${SUBFIX}
	fi
}

# Run until no case remains
while [ $# -gt 0 ]
do
	THREADS=`echo $1 | cut -d '-' -f1`
	MSG=`echo $1 | cut -d '-' -f2`

	if [ "${IO_STRATEGY}" -eq "1" ]
	then
		run ${THREADS} ${MSG} 0
	fi

	if [ "${ND_STRATEGY}" -eq "1" ]
	then
		run ${THREADS} ${MSG} 1
	fi

	if [ "${ALWAYS_STRATEGY}" -eq "1" ]
	then
		run ${THREADS} ${MSG} 2
	fi

	shift
done

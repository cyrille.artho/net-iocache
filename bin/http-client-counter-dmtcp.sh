#!/bin/bash
# Verify the HTTP client SUT against thttpd with Proxy Cache.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

NUM_CLIENTS=2
if [ $# -gt 0 ]
then
	NUM_CLIENTS=$1
fi

chk_vdso

# Set the location of checkpoints
export CHKPNT_DIR="${PROJ_TOP}/tmp-http-client"
export SUT_PORT="8791"

JPF_ARG=`echo "${JPF_ARG} +${OPT_VIRTUAL_MODE}=true +${OPT_CHECKPOINT_DIR}=${CHKPNT_DIR} +${OPT_LAZY}=false +${OPT_DMTCP_ENABLED}=true +${OPT_SUT_PORT}=${SUT_PORT}" | sed -e 's/CacheNotifier/VirtualizationCacheNotifier/' -e 's/CacheLogger/VirtualizationCacheLogger/'`
PACKAGE="gov.nasa.jpf.network.http"
PORT="8080"
PROG_ARG="${PACKAGE}.HTTPClient /cgi-bin/counter ${NUM_CLIENTS}"
CLIENT_LOG="${HOME}/http-client-dmtcp-counter.log"

# Recreate the checkpoint directory.
rm -rf ${CHKPNT_DIR}
mkdir ${CHKPNT_DIR}
# Remove any checkpoint file left on this directory.
rm -f ${SCRIPT_DIR}/*.dmtcp
# Remove old count file
rm -f cgi-bin/count
touch cgi-bin/count

# Must run the coordinator with option "background", otherwise the peer cannot restart.
${DMTCP_COORDINATOR} --ckptdir ${CHKPNT_DIR} --background
# Wait a little to make sure that the coordinator is on.
sleep 1

# Start JPF. It will wait until Proxy sets up a Unix domain channel.
${RUN_JPF_JNILIB} ${JPF_ARG} ${PROG_ARG} &> ${CLIENT_LOG} &
JPF_PID=$!
sleep 2

# Start the server running on DMTCP.
# thttpd uses the MTCP default signal number for some purpose. We must change it otherwise DMTCP will not be able to stop threads.
${DMTCP_CHECKPOINT} env MTCP_SIGCKPT=${MTCP_SIGNAL_NUMBER} thttpd -c '/cgi-bin/*' -p ${PORT} -l ${HOME}/tmp.log -d ${SCRIPT_DIR} -D &
# Save the peer PID.
echo $! > ${CHKPNT_DIR}/${PEER_PID_FILE}
# Start Proxy. Must run in background to avoid the problem about "stdout".
${DMTCP_CHECKPOINT} ${CBUILD_DIR}/proxy &

# Wait until JPF terminates.
wait ${JPF_PID}
rm -f cgi-bin/count

//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import java.io.IOException;

import gov.nasa.jpf.vm.*;
import gov.nasa.jpf.vm.choice.ThreadChoiceFromSet;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.NativePeer;

public class JPF_gov_nasa_jpf_network_cache_CacheLayerOutputStream extends NativePeer{

	private static CacheLayer cl = CacheLayer.getInstance();

	// XXX:return number of bytes
	@MJI
	public int native_write___3BII__I(MJIEnv env, int robj, int b_ref,
			int off, int len) {
		int id = env.getIntField(robj, "id");
		byte[] msg = env.getByteArrayObject(b_ref);

		try {
			cl.write(id, msg, off, len);
		} catch (IOException e) {
			env.throwException(e.getClass().getName(), e.getMessage());
		}

		// Only for CheckpointCacheLayer, we must break the transition after
		// "write" to prevent multiple writes in one
		// transition.
		// if (cl instanceof CheckpointCacheLayer) {
			breakTransition(env);
		// }

		return 0;
	}

	@MJI
	public boolean isNextReadBlocked____Z(MJIEnv env, int robj) {
		int id = env.getIntField(robj, "id");
		return CacheLayer.getInstance().isNextReadBlocked(id);
	}

	/**
	 * Break the current transition by setting a new choice generator with only
	 * one choice, which is the current thread.
	 *
	 * @param env
	 */
	private static void breakTransition(MJIEnv env) {
		VM vm = env.getVM();
		SystemState ss = vm.getSystemState();
		Transition t = ss.getTrail();
		ThreadInfo info = t.getThreadInfo();
		ChoiceGenerator<?> cg = new ThreadChoiceFromSet("transition breaker", new ThreadInfo[]{info}, false);
		ss.setNextChoiceGenerator(cg);
	}

}

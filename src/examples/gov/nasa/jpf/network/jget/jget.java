//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.jget;

/* $Id: jget.java 297 2006-09-20 08:29:15Z cartho $ */

//revisar peticion de Datos, Nombre de Archivo
//agregar catch para cada tipo de excepcion
//Agregar indentificador de Type para ftp
import gov.nasa.jpf.vm.Verify;

import java.net.*;
import java.io.*;
import java.util.*;

class jget implements Runnable {
	static Thread th;
	static URL url;
	static Linlyn lin;
	static int threadMax = 5, N = 0, length = -1, contador = 0, tolerancia = 5;
	static int[][] log;
	static Vector DTs = new Vector(20, 5);
	static boolean chunked = false, writing = true, terminadoOk = false, terminadoInconcluso = false;
	static RandomAccessFile raf, raf_log;
	static FileOutputStream fos;
	static File f, f_log;
	static BufferedReader bf;
	static String path = "./", file, estado, location, help, MIME = "Unspecified", user, pass;
	static String[] url_s;
	static final String version = "0.4.4 beta";

	static public void main(String[] args) {
		try {
			help = "jget "
					+ version
					+ "\n"
					+ "\n"
					+ "Try:\tjget URL\n"
					+ " or\tjget [-options] URL [output path]\n"
					+ "\n"
					+ "Options:\n"
					+ "\t-c <value>\tSet the total number of connections to the Server.\n"
					+ "\t-r\t\tTry to resume the download. Only works if the *.jget file exists.\n"
					+ "\t-t <value>\tSet the tolerance for recursive download when using \"*\" in the URL.\n"
					+ "\t-l\t\tUse this flag to download all the listed files in a *.m3u file. Is necessary set this flag at the begin of the command.\n"
					+ "\t-h\t\tPrint this help\n" + "\n" + "Bugs:\n" + "\tSome times, after often resuming, the output file\n"
					+ "\tor the one of references corrupts. In this case, try\n" + "\tagain to download the file, but without resuming.\n"
					+ "\n" + "For any doubt, commentary or bug, just send an e-mail to sparedes@ing.uchile.cl\n" + "\n";

			parsear_args(args);
			new jget();
			th.start();
		} catch (Exception e) {
			pr("\n");
			System.exit(0);
		}
	}

	static public void pr(Object O) {
		if (O == null)
			System.out.println("null");
		else
			System.out.print(O.toString());

	}

	public jget() {
		th = new Thread(this);
	}

	public void run() {
		// Metodos del Main

		if (!loadData()) {
			pr("\n\nFinished with problems");
			System.exit(-1);
		}

		pr("\tLength of File= ");
		pr("" + length + "\n");
		pr("\tStarting Download with " + threadMax + " connections\n");

		try {
			for (int i = 0; i < threadMax; i++) {
				pr("starting DownloadThread " + i + "\n");
				DownloaderThread DT = new DownloaderThread(url, log[i][0], log[i][1], i, user, pass);

				DTs.add(DT);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		int total = 0, b_inicio = 0, g = 0, gaux = 0;

		try {
			for (int i = 0; i < threadMax; i++)
				b_inicio += log[i][2];
			total = b_inicio;

			for (int i = 0; i < DTs.size(); i++) {
				DownloaderThread DT = (DownloaderThread) DTs.elementAt(i);
				DT.start();
			}
			
			while (writing) {

				synchronized(th) {
					for (int i = 0; i < DTs.size(); i++) {
						DownloaderThread DT = (DownloaderThread) DTs.elementAt(i);
						DT.sleep = true;
					}
				}

				for (int i = 0; i < DTs.size(); i++) {
					DownloaderThread DT = (DownloaderThread) DTs.elementAt(i);

					int l = DT.cola.size();
					for (int j = 0; j < l; j++) {
						Paquete pq = (Paquete) DT.cola.removeFirst();

						raf.seek(pq.from);
						raf.write(pq.data, 0, pq.length);
						System.out.printf("[Jget : run] Write:\n%s\nto byte %d\n", new String(pq.data, 0, pq.length), pq.from);

						log[i][2] += pq.length;
						total += pq.length;
					}

					raf_log.seek((12 * i) + 8);

					if (length != -1) {
						gaux = (int) ((50 / (double) length) * total);
					} else {
						if (gaux != total)
							pr("-[" + total + "]");
						gaux = total;
					}
				}

				boolean aux = true;
				synchronized(th) {
					for (int i = 0; i < DTs.size(); i++) {
						DownloaderThread DT = (DownloaderThread) DTs
								.elementAt(i);
						DT.sleep = false;
						aux = (DT.finished && aux);
					}
					th.notifyAll();
				}

				if (length != -1) {
					if (total >= length) {
						terminadoOk = true;
						break;
					} else if (aux) {
						terminadoInconcluso = true;
						break;
					}
				} else if (aux)
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			pr("Finished with Problems\n");
			System.exit(-1);
		} finally {
			try {
				if (raf_log != null)
					raf_log.close();
				if (raf != null)
					raf.close();
				if (fos != null)
					fos.close();
				if (terminadoOk) {
					if (f_log != null) {
						pr("\tDeleting *.jget, success= ");
						if (f_log.delete())
							pr("OK\n");
						else
							pr("Not Deleted\n");
					}
					pr("\tReplacing output file, success= ");
				} else
					pr("\n\t)");

				pr("\n\n\tLength= " + total + " of " + length + "\n");
				if (terminadoInconcluso)
					pr("\n\t[Download Incomplete]\n");
				else
					pr("\n\t[Download Complete]\n");
			} catch (Exception e) {
				pr("There's a problem\n\t" + estado);
				pr("\n\t" + e.toString());
			}
		}

		System.exit(0);
	}

	static public boolean loadData() {
		try {
			connectUrl();

			if (file.equals("/") || file.equals(""))
				file = url.getHost() + ".html";
			else {
				if (file.endsWith("/")) {
					file = file.substring(0, file.length() - 1);
					file = file.substring(file.lastIndexOf("/"));
					file = file + ".html";
				} else
					file = file.substring(file.lastIndexOf("/"));
			}

			f = new File(path + URLDecoder.decode(file) + ".jgetInc");
			f.createNewFile();
			System.err.printf("[loadData] Check write permission\n");

			raf = new RandomAccessFile(new File(path + URLDecoder.decode(file) + ".jgetInc"), "rw");

			N = (length / threadMax) - 1;
			f_log = new File(path + URLDecoder.decode(file) + ".jget");

			raf_log = new RandomAccessFile(new File(f_log.getPath()), "rw");
			log = new int[threadMax][3];

			int d = 0, a = 0;
			for (int t = 0; t < threadMax - 1; t++) {
				d = a;
				if (d != 0)
					d += 1;
				a += N;
				log[t][0] = d;
				log[t][1] = a;
			}
			d = a;
			if (d != 0)
				d += 1;
			log[threadMax - 1][0] = d;
			log[threadMax - 1][1] = length;

			escribirRaf_log(raf_log, log);
		} catch (IOException e) {
			pr("An IO problem has occurred\n");
			pr("\t\t" + e.toString());/* e.printStackTrace(); */
			return false;
		} catch (Exception e) {
			pr("A problem has occurred\n");
			// pr("\t\t"+e.toString());/*e.printStackTrace();*/
			e.printStackTrace();
			return false;
		}
		return true;
	}

	static public void connectUrl() throws Exception {
		int port;
		pr("\n\tConnecting to " + url.toString());

		port = url.getPort();
		file = url.getFile();
		if (file.equals(""))
			file = "/";

		if (port == -1)
			port = 80;

		Socket s = null;
		PrintWriter pw = null;
		InputStream is = null;
		try {
			s = new Socket(url.getHost(), port);
			pw = new PrintWriter(s.getOutputStream(), true);
			is = s.getInputStream();
		} catch (Exception e) {
			pr("Not connected\n");
			estado = "A problem has occurred with the connection, " + e.toString();
			return;
		}

		pr(" OK\n");
		pw.println("GET " + file + " HTTP/1.0\r\n");
		pr("\tWaiting response from server...\n");

		while (true) {
			String line = readLine(is);
			if (line == null || line.equals(""))
				break;
			parse(line);
		}
		is.close();
		pw.close();
		s.close();
	}

	static public void parse(String line) throws Exception {
		if (line.startsWith("HTTP/1")) {
			estado = "OK";
			location = null;
			pr(line + "\n");
		} else if (line.startsWith("Content-Length:")) {
			line = line.substring(line.indexOf(" "));
			length = Integer.parseInt(line.trim());
		}
	}

	static public void parsear_args(String[] args) {
		int i = 0;
		while (i < args.length) {
			if (args[i].startsWith("-")) {
				threadMax = Integer.parseInt(args[++i]);
				++i;
				continue;
			}

			if (url == null) {
				url_s = new String[1];
				url_s[0] = args[i];

				try {
					url = new URL(url_s[0]);
				} catch (Exception e) {
					pr("Malformed URL\n");
					System.exit(-1);
				}

				++i;
				continue;
			} else {
				path = args[i];
				if (!path.endsWith("/"))
					path += "/";
				++i;
			}
		}
	}

	static public void escribirRaf_log(RandomAccessFile raf_log, int[][] log) {
	}

	static public String readLine(InputStream is) throws Exception {
		String s = "";
		char ch;

		Verify.beginAtomic();
		while (true) {
			ch = (char) is.read();
			if (ch == '\r')
				continue;
			else if (ch == '\n')
				break;
			else
				s += ch;
		}
		Verify.endAtomic();
		
		return s;
	}
}

class DownloaderThread implements Runnable {
	Thread th;
	int id, d, f;
	URL url;
	boolean sleep = false, finished = false;
	LinkedList cola;
	String user, pass;
	static final String version = "0.4.4 beta";
	Linlyn lin;

	public DownloaderThread(URL url, int d, int f, int id, String user, String pass) {
		this.url = url;
		this.d = d;
		this.f = f;
		this.id = id;
		this.user = user;
		this.pass = pass;
		cola = new LinkedList();
		th = new Thread(this);
		// System.out.println("thread iniciado("+id+")");
	}

	public void start() {
		th.start();
	}
	
	public void run() {
		Socket s = null;
		PrintWriter pw = null;
		InputStream is = null;

		try {
			byte[] bufer = new byte[1024 * 100];
			int port = url.getPort();

			if (port == -1)
				port = 80;
			String file = url.getFile();
			if (file.equals(""))
				file = "/";

			s = new Socket(url.getHost(), port);
			pw = new PrintWriter(s.getOutputStream(), true);
			is = s.getInputStream();

			String msg = "GET " + file + " HTTP/1.0\r\nRange: bytes=" + d + "-" + f + "\r\n";
			System.out.println("[DownloaderThread : run] send: " + msg);
			pw.println(msg);

			while (true) {
				String line = readLine(is);
				if (line == null || line.equals(""))
					break;
			}

			int i = -1, read = 0;
			while (true) {
				try {
					if (f >= 0 && bufer.length + d >= f)
						read = f - d + 1;
					else
						read = bufer.length;
					i = is.read(bufer, 0, read);
				} catch (Exception e) {
					if (f >= 0) {
						System.out.print("error in read ");
						System.out.println(e.toString());
					}
					i = -1;
				}

				if (i == -1)
					break;
				
				synchronized (jget.th) {
					while (sleep) {
						jget.th.wait();
					}

					cola.add(new Paquete(bufer, i, (long) d));
				}
				
				d += i;
				if (f >= 0 && d >= f)
					break;
			}

		} catch (Exception e) {
			System.err.println("\nException in thread(" + id + ")");/* System.exit(-1); */
		} finally {
			try {
				if (pw != null)
					pw.close();
				if (is != null)
					is.close();
				if (s != null)
					s.close();
			} catch (Exception not) {
			}
			finished = true;
			th = null;
		}
	}

	public String readLine(InputStream is) throws Exception {
		String s = "";
		char ch;

		Verify.beginAtomic();
		while (true) {
			ch = (char) is.read();
			if (ch == '\r')
				continue;
			else if (ch == '\n')
				break;
			else
				s += ch;
		}
		Verify.endAtomic();
		
		return s;
	}
}

#!/bin/bash
# Verify the rupy server.

. `dirname "$0"`/env.sh

if [ $# -lt 1 ]
then
	echo
	echo "Usage : $0 [-n|m] [#threads]+"
	echo "  -n No model checking - use normal jvm and print output to standard out."
	echo "  -m Move the log file of model checking output from home to the log directory."
    echo
	exit
fi

PACKAGE="se.rupy.http"
OUT_FILENAME=`output_file $0`
LOG="${HOME}/${OUT_FILENAME}"

#(un)comment the subsequent 2 lines to set the rupy version to run
#SUT_DIR="${EXAMPLE_DIR}/se/rupy/http"  #this is an experiment for the ASE 2013 paper, based on rupy 0.4.4
SUT_DIR="../rupy/src/se/rupy/http"      #this is the current rupy code

SUT_FILE="Daemon"
SUT_ARGS="-port 8080 -verbose -pass secret -timeout 3 -threads 1"
#The leading "_" in SUT_ARGS is a dummy parameter ignored by rupy. 
#  It is necessary because, due to a bug in jpf rev 960, 
#  SUT_ARGS starting with "-" such as "-port" would get lost."
PEER="${SCRIPT_DIR}/rupy-client.sh"
LOG_DIR="${SCRIPT_DIR}/log"

# Check if any options are set.
MOVELOG=""
NATIVE=""

while [[ $1 == -* ]]
do
	if [ $1 = "-m" ]; then
		MOVELOG="enabled"
	elif [ $1 = "-n" ]; then
		NATIVE="enabled"
	fi
	shift
done

if [ $NATIVE ]; then
	RUN_EXAMPLE_CMD="java -classpath ${CLASSPATH}"
	SUT="${PACKAGE}.${SUT_FILE}"
else 
	SUT="${SUT_DIR}/${SUT_FILE}.jpf"
	SUT_ARGS="+${OPT_BOOT_CMD}=${PEER};%nativePort%;%logicalPort% +target.args=${SUT_ARGS// /,}" 
fi

move_log() {
	# Add parameters to the log file name.
	PARAM=`echo ${OUT_FILENAME} | sed -e "s/\.log/\.log/"`
	mv ${LOG} ${LOG_DIR}/${PARAM}
}

run_testcase() {

	if [ $NATIVE ]; then
		echo "Run rupy server in the background:"
		echo "${RUN_EXAMPLE_CMD} ${SUT} ${SUT_ARGS}  &"
		${RUN_EXAMPLE_CMD} ${SUT} ${SUT_ARGS} &
		SERVER=$!
		sleep 1
		echo "Run rupy client ${PEER}"
		${PEER} 
		kill ${SERVER}
	else
		echo "Start JPF model-checking a rupy server:"
		echo "${RUN_EXAMPLE_CMD} ${SUT} ${SUT_ARGS} > ${LOG}"
		${RUN_EXAMPLE_CMD} ${SUT} ${SUT_ARGS} | tee ${LOG}
	fi
	
	if [ $MOVELOG ]; then
		move_log
	fi
}

run_testcase

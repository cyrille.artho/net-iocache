#!/bin/bash
# Run the non-deterministic alphabet client against the secure server.

SCRIPT_DIR=`dirname $0`
. "${SCRIPT_DIR}/env.sh"

if [ $# -lt 1 ]
then
	echo "Usage : $0 {number of threads}"
	exit
fi

JPF_ARG="${JPF_ARG} +log.fine=gov.nasa.jpf.network.listener +log.info=gov.nasa.jpf.network.cache.CacheLayer"
PACKAGE="gov.nasa.jpf.network.alphabet"
PROG_ARG="${PACKAGE}.NDAlphabetClient $1"
CLIENT_LOG="${HOME}/secure-client-nd.log"
CBUILD="${SCRIPT_DIR}/../c-build"
NUM_SERVER_ACCEPT=`expr $1 \* 2`

echo "start a secure server in the background"
${CBUILD}/secure-server ${NUM_SERVER_ACCEPT} 2 > /dev/null &
sleep 1
echo "start JPF verifying an alphabet client with $1 thread(s)"
java -classpath $CLASSPATH $VM_ARG gov.nasa.jpf.JPF $JPF_ARG $PROG_ARG > $CLIENT_LOG

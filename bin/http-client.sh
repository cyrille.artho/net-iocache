#!/bin/bash
#running http-client with $1 thread(s); called by http-server test cases (http-server-mc.sh) 

. `dirname "$0"`/env.sh

PACKAGE="gov.nasa.jpf.network.http"
declare -a REQ_FILES=("bin/www/abc.txt" "bin/www/alphabet-client.log" "bin/www/changes.txt" "bin/www/http-client.log" "bin/www/http-server.log")
NumThreads=1

if [ ! $# == 0 ]; then
	NumThreads=$1
fi

java -classpath ${CLASSPATH} ${PACKAGE}.HTTPClient ${REQ_FILES[@]} $NumThreads

#PID=$!

#trap "kill -9 ${PID}; exit" SIGTERM
#wait ${PID}

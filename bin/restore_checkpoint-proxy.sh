#!/bin/bash
# Restore a peer from a given script.

SCRIPT_DIR=`dirname "$0"`
SCRIPT_DIR=`cd ${SCRIPT_DIR}; pwd`
. "${SCRIPT_DIR}/env.sh"

ERR_LOG="${CHKPNT_DIR}/err.log"

if [ $# -lt 1 ]
then
	exit 1
fi

CHECKPOINT_DIR="$1"
STATE_ID="$2"
# All dmtcp files associated to this state.
DMTCP_FILES="${CHECKPOINT_DIR}/state-${STATE_ID}-*.dmtcp"

if [ "$DMTCP_CHECKPOINT_DIR" != "" ]  # set by the new refactored script: bin/utils-for-testing.source
then
    # $CHECKPOINT_DIR gets set from DMTCPProxyCacheLayer.java:35: builder = new ProcessBuilder(restoreScript(), checkpointDir...
    #
    # The value of checkpointDir comes from the --ckptdir ${CHKPNT_DIR} when starting JPF, 
    # so really $CHECKPOINTDIR == $CHKPNT_DIR
    # 
    # When mtcp saves checkpoints, it makes a new file name inside
    # "export $DMTCP_CHECKPOINT_DIR" (or ./, if unset) (see
    # uniquepid.cpp).  HOWEVER, if the processes to be saved were
    # restored from a checkpoint, that checkpoint file name path is
    # re-used (see finishrestore() in mtcp.c).  This complicates
    # doing multiple checkpoints, because a new one will overwrite the
    # old one used during restore.
    # 
    # The old code solved this problem by saving all checkpoints with
    # the same (default) name inside of ./bin/.  After saving and
    # before restoring, the checkpoint was copied to/from $CHKPNT_DIR.
    # 
    # The next few lines do the same things with different directories
    # so that all temporary files are in ./test/ and none are in
    # ./bin/.  Checkpoints are saved/restored to/from "$TESTRUN_DIR".
    # After saving, before restoring, these are copied to/from
    # $CHKPNT_DIR.

    # Remove old temporary checkpoints.
    rm -f "$TESTRUN_DIR"/*.dmtcp

    # copy to staging area
    cd "$CHECKPOINT_DIR"
    prefix=state-${STATE_ID}
    for f in $prefix-*.dmtcp
    do
	cp -al "$f" "$TESTRUN_DIR/${f#prefix}"
	# Now the next time a checkpoint is saved, these same file
	# names in "$TESTRUN_DIR" will be used.  "cp -al" created hard
	# links, so saving may overwrite the existing checkpoint.  To
	# guard against this, savestate-dmtcp.sh has been changed to
	# remove the *dmtcp files before saving.
    done

    cd "$TESTRUN_DIR"
    ${DMTCP_RESTART} --host "localhost" --join ./*.dmtcp
    exit # SKIP THE OLD CODE BELOW
fi

# Remove old temporary checkpoints.
rm -f ${SCRIPT_DIR}/*.dmtcp
# Copy the new set of checkpoints to the temp area.
for F in ${DMTCP_FILES}
do
	DEST=`basename ${F} | sed -r 's|state-[[:digit:]]+-||'`
	cp ${F} ${SCRIPT_DIR}/${DEST}
done

# Run the restart script.
${DMTCP_RESTART} --host "localhost" --join ${SCRIPT_DIR}/ckpt_*.dmtcp

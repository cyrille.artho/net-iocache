//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package java.io;

import gov.nasa.jpf.network.cache.CacheLayerInputStream;

public class BufferedReader extends Reader {

	//abstract wrapper interface for internal reader which is either a 
	//CacheLayerInputStream if connected to iocache, 
	//or a Reader.
	interface InternalReader {
		public boolean ready() throws IOException;
		public void close() throws IOException;
		public int read() throws IOException;
		public int read(char[] cbuf, int off, int len) throws IOException;
	}
	
	class InReader implements InternalReader {
		private Reader reader;
		public InReader(Reader in) {
			reader = in;
		}
		public boolean ready() throws IOException {
	    	return reader.ready();			
		}
		public void close() throws IOException {
			reader.close();
		}
		public int read() throws IOException {
			return reader.read();
		}
		public int read(char[] cbuf, int off, int len) throws IOException {
			return reader.read(cbuf, off, len);
		}
	}

	class InCacheLayerInputStream implements InternalReader {
		private CacheLayerInputStream clis;
		public InCacheLayerInputStream(CacheLayerInputStream in) {
			clis = in;
		}
		public boolean ready() throws IOException {
	    	return clis.isChannelReadable();			
		}
		public void close() throws IOException {
			clis.close();
		}
		public int read() throws IOException {
			return clis.read();
		}
		public int read(char[] cbuf, int off, int len) throws IOException {
			byte[] buf=new byte[cbuf.length];
			int ret=clis.read(buf, off, len);
			if (ret>0)
				System.arraycopy((new String(buf)).toCharArray(), off, cbuf, off, ret);
			return ret;
		}
	}
	
	
	private InternalReader inreader;
	
	public BufferedReader(Reader in) {
		try {
			//use reflection to find out, if the InputStream in, 
			//  which is a private field of class InputStreamReader, 
			//  is an instance of CacheLayerInputStream. If so, we are running under jpf-net-iocache...
			//  Dirty but works so far...
			Object obj=in.getClass().getDeclaredField("in").get(in);
			if (obj instanceof CacheLayerInputStream)
				inreader = new InCacheLayerInputStream((CacheLayerInputStream) obj);
			else 
				inreader = new InReader(in);
		} catch (Exception e) {
			inreader = new InReader(in);
		}
	}

	//public native int read(char[] cbuf, int off, int len) throws IOException;
	public int read(char[] cbuf, int off, int len) throws IOException {
		return inreader.read(cbuf, off, len);
	}

	public boolean ready() throws IOException {
    	return inreader.ready();
	}

	public void close() throws IOException {
		inreader.close();
	}
	
	public String readLine() throws IOException {
		StringBuffer buffer = new StringBuffer();
		
		for(int c = inreader.read(); c != -1 && c != '\n'; c = inreader.read()) {
			buffer.append((char) c);
		}
		//Get rid of '\r' at the end of the string 
		int lastIdx=buffer.length()-1;
	        if ((lastIdx != -1) && (buffer.charAt(lastIdx)=='\r'))
			buffer.delete(lastIdx, lastIdx+1);
		
		return buffer.toString();
	}
}

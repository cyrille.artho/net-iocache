#!/bin/bash
# start the alphabet client with one thread.

# set JPF_HOME and class paths
. `dirname "$0"`/env.sh

PACKAGE="gov.nasa.jpf.network.alphabet"
# Default: one message
NUM_MSG=1
IP="$2"
PORT="$3"

if [ $# -gt 0 ]
then
	# set the number of messages from the first argument.
	NUM_MSG=$1
fi

CMD="java -classpath $CLASSPATH ${PACKAGE}.AlphabetClient 1 ${NUM_MSG}"

if [ $# -gt 2 ]
then
	OPT="--ip ${IP} --port ${PORT}"
else
	if [ $# -gt 1 ]
	then
		OPT="--ip ${IP}"
	else
		OPT=""
	fi
fi

echo "start an alphabet client with ${NUM_MSG} message(s)"
${CMD} ${OPT} &

PID=$!

trap "kill -9 ${PID}; exit" SIGTERM
wait ${PID}

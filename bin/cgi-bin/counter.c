#include <stdio.h>
#include <sys/file.h>

#define COUNT_FILE "count"

int get_count(FILE *file) {
    int count;
    int ch;
    count = 0;
    while ((ch = fgetc(file)) != EOF) {
	if ((ch >= '0') && (ch <= '9')) {
	    count = 10 * count + (ch - '0');
	} else {
	    break;
	}
    }
    return count;
}

void write_header() {
    printf ("Content-type: text/html\n\n");
}

void write_count(FILE *file, int count) {
    fseek (file, 0, SEEK_SET);
    fprintf (file, "%d\n", count);
}

int main() {
    int count;
    int filedes;
    FILE * file;
    file = fopen (COUNT_FILE, "r+");
    if (!file) {
	file = fopen (COUNT_FILE, "w+");
    }
    if (!file) {
	perror ("fopen");
    }
    filedes = fileno (file);
    flock (filedes, LOCK_EX);
    count = get_count(file);
    write_count (file, ++count);
    flock (filedes, LOCK_UN);
    write_header();
    printf ("%d\n", count);
}

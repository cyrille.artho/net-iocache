//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.alphabet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;

public class AlphabetClientSimple extends Thread {

	static private int port=AlphabetServer.SERVER_PORT;
	static private int messages=1;
	static int idCount=0;
	private int id=idCount++;
	
	// Usage: AlphabetClientSimple (#threads) (#messages) [options]
	//
	// Options:
	// --port <port number>
	public static void main(String[] args) throws Exception {
		int threads=1;

		try {
			threads = Integer.parseInt(args[0]);
			messages = Integer.parseInt(args[1]);
			// Parse optional arguments
			for (int i = 2; i < args.length; i++) 
				if (args[i].equals("--port")) 
					port = Integer.parseInt(args[++i]);
		} catch (NumberFormatException e) {
			System.out.println("WARNING: exception '"+e+"' was thrown while parsing the program arguments "+Arrays.asList(args)+". Using default values.");
		}
		
		for (int i = 0; i < threads; i++) 
			(new AlphabetClientSimple()).start();

	}

	@Override
	public void run() {
		String thread=getName();
		long offset=id*messages;
		try {
			System.out.println("[AlphabetClientSimple.run] "+thread+" connects to the server on port="+port+"...");
			Socket sock=new Socket("localhost", port);
			OutputStream out=sock.getOutputStream();
			InputStream in=sock.getInputStream();
			for (int i=0; i<messages; i++) {
				int msg=(int) ('0'+(i+offset)%26);
				System.out.println("[AlphabetClientSimple.run] "+thread+" sends '"+(char)msg+"'...");
				out.write(msg);
				char rsp=(char) in.read();
				assert rsp==msg-'0'+'a' : "[AlphabetClientSimple.run] expected '"+(char)(msg-'0'+'a')+"' but received '"+rsp+"'";
				System.out.println("[AlphabetClientSimple.run] "+thread+" received '"+rsp+"'.");
			}
			System.out.println("[AlphabetClientSimple.run] "+thread+" closes connection...");
			sock.close();
		} catch (IOException e) {
			System.out.println("[AlphabetClientSimple.run] "+thread+" terminated with "+e);
			e.printStackTrace();
			assert false;
		} 
	}
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <search.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <dmtcpaware.h>
#include <sockettable.h>

#include "dmtcpsetup.h"

	
close_order *order;
Entry check_list[32];
pid_t main_process;
uint accepting_port;
char shared_file_fullname[256];


int open_udp_socket(uint port);
// Clear the check list and fill it with the given list of FDs.
void build_check_list(int *fd_list, size_t fd_size);

// Find and check an unchecked entry whose port is matched with the given port.
// Fill field 'recovery_pid' of the matching entry with the given PID.
// Fill field 'lazy_pids' of the matching entry with the given 'lazy' array.
// Return the FD of that entry or -1 if not found.
int check_entry(uint port, uint pid, uint *lazy, uint lazy_len);

// Return the replay data array of the entry whose port is matched with the one given.
// Argument "matched" is filled with the index of the matched entry.
char *replay_data_array(uint port, int *idx);

// Find the entry whose FD is matched with the given FD. "data" will point to the replay data array of that entry.
// Return the length of data in that array.
ssize_t replay_data_by_fd(int fd, char **data);

// Fill the given storage with the checked FDs in the check list. Return the number of those FDs.
int checked_entries(int *fds);
// Fill the given storage with the unchecked FDs and their remote ports in the check list. Return the number of those FDs.
int unchecked_entries(int *fds, uint16_t *ports);
// Fill the "remote_port" field of each unchecked entry. Return the number of unchecked entries.
int fill_unchecked_entries_with_ports();
// Find the entry in the checklist with the given FD. Return NULL if no matching entry is found.
Entry *find_entry_by_fd(int fd);

// Connect to the given port. Map the socket obtained from this connection to the given FD.
int connect_to_port(int fd, uint16_t port);
// Return the remote port number of the given socket.
uint16_t socket_get_port(int fd);
// Return the local port number of the given socket.
uint16_t socket_get_local_port(int fd);
// Read the recovery file and fill the specified storage with the segment, excluding the header part, associated to this process.
// Return the size of the segment in bytes or -1 if there is no matching segment. Assume the storage has enough space for the segment.
int find_recovery_info(char *storage);

void ms_sleep(int ms);
// Send the given FD from this process to the "dest" process.
int send_fd(int fd, uint dest);
// Receive a FD from the "src" process and return this FD.
int recv_fd(uint src);

void premain() {
	char *dir = getenv(CHKPNT_DIR);
	int rv = dmtcpIsEnabled();

	// Setup callback functions.
	if (rv) {
		dmtcpInstallHooks(pre_ckpt, post_ckpt, post_restart);
		order = close_order_new();
	}

	main_process = getpid();

	// Setup a shared file.
	memset(shared_file_fullname, 0, sizeof(shared_file_fullname));
	sprintf(shared_file_fullname, "%s/%s", dir, SHARED_FILENAME);
}

void pre_ckpt() {
//	fprintf(stderr, "[%d][%s] Begin\n", getpid(), __func__);
	int active[32];
	char buff[1024];
	char *cur_buff = buff;
	size_t count = sizeof(active) / sizeof(int);
	uint establishedConns;
	uint pending_conn;
	uint local_port;
	int i = 0;
	int number = 0;
	int recovery_info_size;

	// Read a relevant entry from the shared file.
	recovery_info_size = find_recovery_info(buff);
	// If there is no recovery info, do nothing.
	if (recovery_info_size < 0) {
//		fprintf(stderr, "[%d][%s] End\n", getpid(), __func__);
		return;
	}
	
	// Read the accepting port.
	accepting_port = ntohl(*(uint *) (cur_buff));
	cur_buff += sizeof(accepting_port);
	// Read the number of complete connections.
	establishedConns = (uint) *cur_buff++;
	// Read the number of pending connections.
	pending_conn = (uint) *cur_buff++;

	// Obtain the list of active sockets.
	active_sock(active, &count);
	build_check_list(active, count);
	// Prepare a close order.
	close_order_clear(order);

	// Read the local port numbers of the complete connections.
	for (i = 0; i < establishedConns; i++) {
		uint pids[MAX_OWNERS];
		uint pids_len = 0;
		uint i;
		int cur_fd;
		
		local_port = ntohl(*(uint *) cur_buff);
		cur_buff += sizeof(local_port);

		// Read the number of owners of this connection.
		pids_len = (uint) *cur_buff++;
		// Read the owners' PIDs of this connection.
		for (i = 0; i < pids_len; i++) {
			pids[i] = ntohl(*(uint *) cur_buff);
			cur_buff += sizeof(uint);
		}

		// Check the matching entry and fill the PID of the first process.
		cur_fd = check_entry(local_port, pids[0], pids + 1, pids_len - 1);
		assert(cur_fd > 0);

		close_order_add(order, cur_fd, number++);
	}

	// Read the pending connection ports and their buffered data
	for (i = 0; i < pending_conn; i++) {
		char *replay;
		int idx = 0;

		// Read port.
		local_port = ntohl(*(uint *) cur_buff);
		cur_buff += sizeof(local_port);
		// Find the entry that matches the local port.
		replay = replay_data_array(local_port, &idx);
		// Obtain the length of the replay data.
		check_list[idx].data_len = ntohl(*(uint *) cur_buff);
		cur_buff += sizeof(check_list[idx].data_len);
		// Obtain replay data.
		memcpy(replay, cur_buff, check_list[idx].data_len);
		cur_buff += check_list[idx].data_len;
	}

	// Remember the original port number for the pending sockets.
	assert(fill_unchecked_entries_with_ports() == pending_conn);
	
	// Close all sockets bound to connections.
	for (i = 0;i < count;i++) {
		int rv = 0;
		int idx = socket_info_index(active[i]);
		
		// Protect this socket. Do not let it be marked inactive.
		sock_table[idx].is_protected = 1;
		rv = close(active[i]);

		if (rv) {
			printf("Fail in close(): %d\n", errno);
		}
		assert(!rv);
	}

//	fprintf(stderr, "[%d][%s] End\n", getpid(), __func__);
}

void post_ckpt() {
//	fprintf(stderr, "[%d][%s] Begin\n", getpid(), __func__);
	int active[32];
	uint16_t ports[32];
	size_t count = sizeof(active) / sizeof(int);
	int i = 0;
	socket_type role;

	// If this process is the main process.
	if (getpid() == main_process) {
		// Notify the I/O cache, which is blocking on "syncckpt".
		notify_cache();
	}
	
	// Handle complete connections.
	count = checked_entries(active);
	
	// Rebind sockets.
	for (i = 0;i < count;i++) {
		int fd = close_order_get(order, i);
		int idx = socket_info_index(fd);
		// No need to protect anymore.
		sock_table[idx].is_protected = 0;
		
		role = sock_table[idx].type;
		// Check if we should create connections by accepting or connecting.
		assert(role == ACCEPTING || role == CONNECTING);
		if (role == ACCEPTING) {
			Entry *e = find_entry_by_fd(fd);
			pid_t pid = getpid();			
			assert(e != NULL);

			if (e->recovery_pid == pid)
				setup_conn_by_accepting(fd, e->lazy_pids, e->lazy_pids_len);
			else
				setup_conn_by_waiting(fd, e->recovery_pid);
		}
		else {
			timespec interval;
			// 100 ms
			interval.tv_sec = 0;
			interval.tv_nsec = 100000000l;
			// Wait a little bit, hoping the SUT side is waiting on "accept".
			nanosleep(&interval, NULL);
		
			setup_conn_by_connecting(fd);
		}
	}

	// Handle the remaining sockets.
	count = unchecked_entries(active, ports);
	for (i = 0; i < count; i++) {
		char *data;
		ssize_t len;
		int rv;
		
		// Connect to the original port.
		connect_to_port(active[i], ports[i]);
		// Register the PID again for pending connections.
		registerPid();

		// Obtain the replay data.
		len = replay_data_by_fd(active[i], &data);
		// Use the wrapper version of "send". A header must be detached.
		rv = send(active[i], data, len, 0);
	}

//	fprintf(stderr, "[%d][%s] End\n", getpid(), __func__);
}

void post_restart() {
	post_ckpt();
}

void active_sock(int *active, size_t *size) {
	int i = 0;
	int array_len = sizeof(sock_table) / sizeof(SocketInfo);
	size_t counter = 0;

	for (i = 0;i < array_len;i++) {
		// End of table
		if (!sock_table[i].fd)
			break;

		if (sock_table[i].fd > 0 && sock_table[i].is_active) {
			active[counter] = sock_table[i].fd;
			counter++;
		}
	}

	*size = counter;
}

int socket_info_index(int fd) {
	int i = 0;
	int len = sizeof(sock_table) / sizeof(SocketInfo);

	for (i = 0;i < len;i++) {
		if (sock_table[i].fd == fd) {
			return i;
		}
	}

	return -1;
}

void setup_conn_by_accepting(int fd, const uint *lazy_pids, uint lazy_pids_len) {
//	fprintf(stderr, "[%d][%s] Begin\n", getpid(), __func__);
	addrinfo hints;
	addrinfo *servinfo;
	addrinfo *p;
	sockaddr_storage their_addr;
	char str_port[32];
	socklen_t addr_size = sizeof(their_addr);
	/* int idx = socket_info_index(active[0]); */
	int sockfd = 0;
	int i = 1;
	int new_fd = 0;
		
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	memset(str_port, 0, sizeof(str_port));
	sprintf(str_port, "%u", LISTENING_PORT);

	getaddrinfo(NULL, str_port, &hints, &servinfo);

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = _real_socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		// If the listening socket FD clashes, change it to a reserved one.
		if (sockfd == fd) {
			dup2(sockfd, RESERVED_LISTENING_SOCKFD);
			_real_close(sockfd);
			sockfd = RESERVED_LISTENING_SOCKFD;
		}

		if (_real_setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(int)) == -1) {
			perror("setsockopt");
			break;
		}

		if (_real_bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			_real_close(sockfd);
			perror("server: bind");
			continue;
		}

		break;
	}

	// all done with this structure
	freeaddrinfo(servinfo);
	// Listen at a certain port.
	_real_listen(sockfd, BACKLOG);
	printf("[%s] Accepting @ port %s\n", __func__, str_port);

	// Accept a connection.
	new_fd = _real_accept(sockfd, (struct sockaddr *) &their_addr, &addr_size);

	// Compare the new FD and the original FD.
	if (fd != new_fd) {
		int rv = dup2(new_fd, fd);
		assert(rv == fd);
		
		_real_close(new_fd);
	}

	_real_close(sockfd);
	// Update the remote port.
	set_socket_info_remote_port(fd, addr_port((sockaddr *) &their_addr));

	// Distribute this FD to all lazy processes.
	for (i = 0; i < lazy_pids_len; i++) {
		send_fd(fd, lazy_pids[i]);
	}
}

void setup_conn_by_connecting(int fd) {
	connect_to_port(fd, LISTENING_PORT);
	// Update the remote port.
	set_socket_info_remote_port(fd, LISTENING_PORT);
}

void setup_conn_by_waiting(int fd, uint recovery_pid) {
	fprintf(stderr, "[%d][%s(%d, %u)] \n", getpid(), __func__, fd, recovery_pid);
	uint16_t remote_port;
	int new_fd = recv_fd(recovery_pid);
	assert(new_fd == fd);

	// Update the remote port.
	remote_port = socket_get_port(new_fd);
	set_socket_info_remote_port(new_fd, remote_port);
}

void notify_cache() {
	char *dir = getenv(CHKPNT_DIR);
	char fifo[FIFO_NAME_MAX_SIZE];
	char s[2];
	int fd;
	int num;
	
	if (!dir)
		perror("CHKPNT_DIR is not defined");

	// Build the pipe name.
	memset(fifo, 0, FIFO_NAME_MAX_SIZE);
	sprintf(fifo, "%s/%s", dir, PIPE_NAME);

	// notify the cache in both cases
	mknod(fifo, S_IFIFO | 0666, 0);
	memset(s, 0, sizeof s);
	
	fd = _real_open2(fifo, O_WRONLY);
	s[0] = 1;
		
	if ((num = write(fd, s, strlen(s))) == -1)
		perror("write");

	_real_close(fd);
}

//-- close_order operations --
close_order *close_order_new() {
	close_order *order = (close_order *) malloc(sizeof(close_order));

	memset(order, 0, sizeof(close_order));

	return order;
}

void close_order_free(close_order *order) {
	free(order);
}

void close_order_add(close_order *order, int fd, int number) {
	order->fds[number] = fd;
	order->size++;
}

int close_order_get(const close_order *order, int number) {
	return order->fds[number];
}

int close_order_size(const close_order *order) {
	return order->size;
}

// Clear all contents in the close order.
int close_order_clear(close_order *order) {
	int size = order->size;
	
	memset(order, 0, sizeof(close_order));

	return size;
}

int close_number(int fd) {
	char buf[32];
	int len = 0;
	int ret = 0;

	memset(buf, 0, sizeof(buf));
	
	len = read(fd, buf, sizeof(buf));
	assert(len > 8);
	ret = atoi(buf + 8);
	
	return ret;
}

// Open the UDP socket bound to a specified port.
int open_udp_socket(uint port) {
	addrinfo *servinfo;
	addrinfo *p;
	addrinfo hints;
	char s_port[8];
	int sockfd;
	int rv;

	memset(&hints, 0, sizeof hints);
	memset(&s_port, 0, sizeof s_port);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;
	// Convert the port number to string.
	sprintf(s_port, "%u", port);

	if ((rv = getaddrinfo(NULL, s_port, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return -1;
	}
	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = _real_socket(p->ai_family, p->ai_socktype,
							 p->ai_protocol)) == -1) {
			perror("listener: socket");
			continue;
		}
		if (_real_bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			_real_close(sockfd);
			perror("listener: bind");
			continue;
		}
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "listener: failed to bind socket\n");
		return 2;
	}
	
	freeaddrinfo(servinfo);

	return sockfd;
}

void build_check_list(int *fd_list, size_t fd_size) {
	int i;
	
	memset(check_list, 0, sizeof(check_list));

	for (i = 0;i < fd_size; i++) {
		check_list[i].fd = fd_list[i];
	}
}

int check_entry(uint port, uint pid, uint *lazy, uint lazy_len) {
	sockaddr addr;
	socklen_t addrlen = sizeof(sockaddr);
	int i = 0;
	int len = sizeof(check_list) / sizeof(Entry);

	for (i = 0; i < len; i++) {
		uint16_t p;

		if (!check_list[i].fd)
			return -1;
		
		getsockname(check_list[i].fd, &addr, &addrlen);
		p = ntohs(((struct sockaddr_in *) &addr)->sin_port);

		// If the port matches and this entry is not checked.
		if (p == port && !check_list[i].checked) {
			check_list[i].checked = 1;
			check_list[i].recovery_pid = pid;
			memcpy(check_list[i].lazy_pids, lazy, lazy_len * sizeof(uint));
			check_list[i].lazy_pids_len = lazy_len;

			return check_list[i].fd;
		}
	}

	return -1;
}

char *replay_data_array(uint port, int *idx) {
	sockaddr addr;
	socklen_t addrlen = sizeof(sockaddr);
	int i = 0;

	for (i = 0; i < sizeof(check_list) / sizeof(Entry); i++) {
		uint16_t p;
		
		getsockname(check_list[i].fd, &addr, &addrlen);
		p = ntohs(((struct sockaddr_in *) &addr)->sin_port);

		if (p == port) {
			*idx = i;
			return check_list[i].replay_data;
		}
	}

	return 0;
}

ssize_t replay_data_by_fd(int fd, char **data) {
	Entry *e = find_entry_by_fd(fd);
	if (e == NULL)
		return -1;
	
	*data = e->replay_data;
	return e->data_len;	
}

int checked_entries(int *fds) {
	int i;
	int n = sizeof(check_list) / sizeof(Entry);
	int count = 0;

	for (i = 0; i < n; i++) {
		// No more entry.
		if (check_list[i].fd <= 0)
			break;
		
		if (check_list[i].checked) {
			fds[count++] = check_list[i].fd;
		}		
	}

	return count;
}

int unchecked_entries(int *fds, uint16_t *ports) {
	int i;
	int n = sizeof(check_list) / sizeof(Entry);
	int count = 0;

	for (i = 0; i < n; i++) {
		// No more entry.
		if (check_list[i].fd <= 0)
			break;
		
		if (!check_list[i].checked) {
			fds[count] = check_list[i].fd;
			ports[count] = check_list[i].remote_port;
			count++;
		}		
	}

	return count;
}

int fill_unchecked_entries_with_ports() {
	int i;
	int n = sizeof(check_list) / sizeof(Entry);
	int count = 0;

	for (i = 0; i < n; i++) {
		// No more entry.
		if (check_list[i].fd <= 0)
			break;
		
		if (!check_list[i].checked) {
			check_list[i].remote_port = socket_get_port(check_list[i].fd);
			count++;
		}		
	}

	return count;
}

Entry *find_entry_by_fd(int fd) {
	int i;
	int n = sizeof(check_list) / sizeof(Entry);

	for (i = 0; i < n; i++) {
		// No more entry.
		if (check_list[i].fd <= 0)
			break;

		if (check_list[i].fd == fd) {
			// Matching entry found.
			return check_list + i;
		}		
	}

	return NULL;	
}

int connect_to_port(int fd, uint16_t port) {
	addrinfo hints;
	addrinfo *servinfo;
	addrinfo *p;
	char str_port[32];

	int sockfd;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	memset(str_port, 0, sizeof(str_port));
	sprintf(str_port, "%u", port);

	getaddrinfo("localhost", str_port, &hints, &servinfo);

	// loop through all the results and connect to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = _real_socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			continue;
		}

		if (_real_connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			_real_close(sockfd);
			continue;
		}

		break;
	}

	if (!p)
		printf("[%s] Fail time: %lld\n", __func__, cur_time_ms());
	assert(p);

	// all done with this structure
	freeaddrinfo(servinfo);

	// Map the new socket to the original FD.
	if (fd != sockfd) {
		int rv = dup2(sockfd, fd);
		assert(rv == fd);
		
		_real_close(sockfd);
	}

	return 0;
}

uint16_t socket_get_port(int fd) {
	sockaddr peer_addr;
	socklen_t addr_len = sizeof(sockaddr);
		
	getpeername(fd, &peer_addr, &addr_len);
		
	return ntohs(((struct sockaddr_in *) &peer_addr)->sin_port);
}

uint16_t socket_get_local_port(int fd) {
	sockaddr peer_addr;
	socklen_t addr_len = sizeof(sockaddr);
		
	getsockname(fd, &peer_addr, &addr_len);
		
	return ntohs(((struct sockaddr_in *) &peer_addr)->sin_port);
}

int find_recovery_info(char *storage) {
	// Waiting time when no shared file is found.
	const int DELAY = 15;
	
	FILE *fp = NULL;
	pid_t self = getpid();
	uint pid = 0;
	uint entry_size = 0;
	int ret = 0;

	// Wait until the file is created.
	while (!fp) {
		fp = _real_fopen(shared_file_fullname, "r");

		if (!fp)
			ms_sleep(DELAY);
	}

	// Search of a matching entry.
	while (pid != self) {
		size_t len;
		
		// Skip to the next entry.
		fseek(fp, entry_size, SEEK_CUR);
		
		len = fread(&pid, sizeof(pid), 1, fp);
		// End-of-file
		if (len == 0) {
			break;
		}

		fread(&entry_size, sizeof(entry_size), 1, fp);
		pid = ntohl(pid);
		entry_size = ntohl(entry_size);
	}

	// If a matching entry is found.
	if (pid == self) {
		fread(storage, sizeof(char), entry_size, fp);
		ret = entry_size;
	}
	else {
		ret = -1;
	}

	_real_fclose(fp);

	return ret;
}

void ms_sleep(int ms) {
	timespec interval;
	interval.tv_sec = ms / 1000;
	interval.tv_nsec = (ms % 1000) * 1000000l;

	nanosleep(&interval, NULL);
}

int send_fd(int fd, uint dest) {
	struct iovec iov;
	struct msghdr msg;
	struct cmsghdr *cmsg;
	sockaddr_un addr;
	int sockfd;
	int n;
	int len;
	char sock_addr[UNIX_SOCKADDR_MAX_LEN];
	char buf[1];
	char cms[CMSG_SPACE(sizeof(int))];

	if ((sockfd = _real_socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		perror("server: socket");
		exit(1);
	}
	
	memset(sock_addr, 0, sizeof(sock_addr));
	sprintf(sock_addr, "%d-%u", getpid(), dest);

	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, sock_addr);

	// Create a channel between this process and "dest".
	len = sizeof(addr.sun_family) + strlen(addr.sun_path);
	if (_real_connect(sockfd, (sockaddr *) &addr, len) < 0) {
		perror("client: connect");
		exit(1);
	}

	// Send FD
	buf[0] = 0;
	iov.iov_base = buf;
	iov.iov_len = 1;

	memset(&msg, 0, sizeof msg);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = (caddr_t)cms;
	msg.msg_controllen = CMSG_LEN(sizeof(int));

	cmsg = CMSG_FIRSTHDR(&msg);
	cmsg->cmsg_len = CMSG_LEN(sizeof(int));
	cmsg->cmsg_level = SOL_SOCKET;
	cmsg->cmsg_type = SCM_RIGHTS;
	memmove(CMSG_DATA(cmsg), &fd, sizeof(int));

	if((n = sendmsg(sockfd, &msg, 0)) != iov.iov_len)
		return -1;

	_real_close(sockfd);

	return 0;
}

int recv_fd(uint src) {
	struct iovec iov;
	struct msghdr msg;
	struct cmsghdr *cmsg;
	sockaddr_un addr;
	int sockfd;
	int fd;
	int n;
	int len;
	char sock_addr[UNIX_SOCKADDR_MAX_LEN];
	char buf[1];
	char cms[CMSG_SPACE(sizeof(int))];

	if ((sockfd = _real_socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		perror("server: socket");
		exit(1);
	}
	
	memset(sock_addr, 0, sizeof(sock_addr));
	sprintf(sock_addr, "%u-%d", src, getpid());

	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, sock_addr);

	// Create a channel between this process and "dest".
	unlink(sock_addr);
	len = sizeof(addr.sun_family) + strlen(addr.sun_path);
	if (_real_bind(sockfd, (sockaddr *) &addr, len) < 0) {
		perror("client: connect");
		exit(1);
	}
	
	iov.iov_base = buf;
	iov.iov_len = 1;

	memset(&msg, 0, sizeof msg);
	msg.msg_name = 0;
	msg.msg_namelen = 0;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	msg.msg_control = (caddr_t)cms;
	msg.msg_controllen = sizeof cms;

	if ((n = recvmsg(sockfd, &msg, 0)) < 0)
		return -1;
	
	if (n == 0) {
		perror("unexpected EOF");
		return -1;
	}
	
	cmsg = CMSG_FIRSTHDR(&msg);
	memmove(&fd, CMSG_DATA(cmsg), sizeof(int));
	
	return fd;
}

package gov.nasa.jpf.network.cache;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a checkpoint of a peer process created by a
 * checkpointing environment.
 * 
 * @author watcharin
 * 
 */
public class Checkpoint {

	/**
	 * <i>peerPointers[i]</i> is the position of the peer pointer for connection
	 * <i>i</i>.
	 */
	private List<Integer> peerPointers;
	
	/**
	 * State number.
	 */
	private int id;

	/**
	 * This field is true if this checkpoint is created due to a
	 * non-deterministic instruction on the peer side.
	 */
	private boolean nd;
	
	Checkpoint(int id, boolean nd, List<Integer> peerPtrs) {
		this.id = id;
		this.nd = nd;
		peerPointers = new ArrayList<Integer>(peerPtrs);
	}

	public String toString() {
		return String.format("(ID: %d, ND: %b", id, nd);
	}
	
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @return True if this checkpoint is created due to a non-deterministic
	 *         instruction on the peer side; false, otherwise.
	 */
	public boolean isNd() {
		return nd;
	}
	
	public List<Integer> getPeerPointers() {
		return peerPointers;
	}
	
	/**
	 * 
	 * @return The number of connections saved in this checkpoint.
	 */
	public int numConnections() {
		return peerPointers.size();
	}

	/**
	 * 
	 * @param connId
	 * @return The position of the peer checkpoint of the given connection.
	 */
	public int getPtr(int connId) {
		return peerPointers.get(connId);
	}
	
}

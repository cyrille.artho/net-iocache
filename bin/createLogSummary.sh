if [[ "$1" == "" ]]; then
	cd log
	mkdir summary
	for f in *.txt; do
		(head -13; tail -21) < "${f}" > "summary/${f}"
	done
else
	cat ${1}-*.txt | grep -E "^(gov|elapsed|max)" > ${1}Summary.txt
fi
/*
  This program simulates behavior of a secure server. It responses to the first greeting message from a client non-deterministically.
  All responses after that represent encrypted messages.

  Extra memory is allocated for experiment.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/prctl.h>
#include <netinet/in.h>


#define MYPORT 	"18586"  // the port users will be connecting to
#define BACKLOG 	10     // how many pending connections queue will hold
#define NUM_CHILDREN 11


typedef struct sockaddr_storage sockaddr_storage;
typedef struct addrinfo addrinfo;


void *worker_run(void *);
// Create child processes equal to a specified number.
void createchildren(int children);
// Allocate and free extra memory.
void fillmem(int mb);
void freemem();


const int num_branches = 2;
int num_clients;
int num_msg;
int num_children = NUM_CHILDREN;
char *extra_mem = NULL;


int main(int argc, char **argv) {
    sockaddr_storage their_addr;
    socklen_t addr_size;

    addrinfo hints;
    addrinfo *servinfo;
    addrinfo *p;
	pthread_t threads[8];

    int sockfd, new_fd;
    int yes = 1;
	int rv;
	int i;

	if (argc < 3) {
	  perror("Usage: secure-server (#clients) (#messages) [#extra children]");
	  return 1;
	}

	num_clients = (int) strtol(argv[1], NULL, 0);
	num_msg = (int) strtol(argv[2], NULL, 0);

	if (argc > 3)
		num_children = (int) strtol(argv[3], NULL, 0);

	createchildren(num_children);
	fillmem(2);

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    getaddrinfo(NULL, MYPORT, &hints, &servinfo);

    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
	  if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
		perror("server: socket");
		continue;
	  }

	  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		perror("setsockopt");
		exit(1);
	  }

	  if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
		close(sockfd);
		perror("server: bind");
		continue;
	  }

	  break;
    }

    if (p == NULL) {
        fprintf(stderr, "server: failed to bind\n");
        return 2;
    }

    freeaddrinfo(servinfo); // all done with this structure

    listen(sockfd, BACKLOG);

    // now accept an incoming connection:
    addr_size = sizeof their_addr;

	for (i = 0;i < num_clients;i++) {
		printf("waiting ...\n");
		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);

		// ready to communicate on socket descriptor new_fd!
		printf("accepted socket %d\n", new_fd);

		rv = pthread_create(&threads[i], NULL, worker_run, (void *) new_fd);
		if (rv) {
			printf("ERROR; return code from pthread_create() is %d\n", rv);
			exit(-1);
		}
	}

	close(sockfd);

	for (i = 0;i < num_clients;i++) {
		pthread_join(threads[i], NULL);
	}

	freemem();
	printf("secure-server ends\n");
	return 0;
}

void *worker_run(void *id) {
	char c[2];
	int new_fd = (int) id;
	int i;
	int rv;
	int remainder = 0;

	memset(c, 0, sizeof c);

	// First response
	rv = recv(new_fd, c, 1, 0);
	// Generate a random number sequence after getting the first message.
	srand(time(0));
	if (rv > 0) {
		printf("worker %d: receive %s\n", (int) id, c);
		remainder = rand() % num_branches;
		c[0] = 'A' + (remainder * 2);
		send(new_fd, c, 1, 0);
		printf("worker %d: send %s\n", (int) id, c);
	}
	else {
		perror("Nothing received");
	}

	// From the second response
	for (i = 1;i < num_msg;i++) {
		rv = recv(new_fd, c, 1, 0);

		if (rv > 0) {
			printf("worker %d: receive %s\n", (int) id, c);
			c[0] = 'B' + (remainder * 2);
			send(new_fd, c, 1, 0);
			printf("worker %d: send %s\n", (int) id, c);
		}
		else {
			perror("Nothing received");
			break;
		}
	}

	close(new_fd);
	pthread_exit(NULL);
}

void createchildren(int children) {
	int i;
	
	for (i = 0; i < children; i++) {
		int cpid = fork();

		if (cpid == 0) {
			prctl(PR_SET_PDEATHSIG, SIGHUP);
			fillmem(2);
			pause();
			freemem();
			_exit(0);
		}
	}
}

void fillmem(int mb) {
	int mem_size = mb * 1024 * 1024;
	int i = 0;

	extra_mem = malloc(mem_size);

	srand(time(0));
	printf("MEM: %d\n", mem_size);

	for (i = 0; i < mem_size; i++) {
		extra_mem[i] = (char) rand();
	}
}

void freemem() {
	free(extra_mem);
}

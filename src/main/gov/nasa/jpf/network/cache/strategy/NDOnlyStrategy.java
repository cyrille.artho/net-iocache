package gov.nasa.jpf.network.cache.strategy;

import gov.nasa.jpf.network.cache.CheckpointCacheLayer;

public class NDOnlyStrategy implements CheckpointingStrategy {
	
	@Override
	public boolean shouldSave(int stateId) {
		return stateId == 0 || CheckpointCacheLayer.isSaveFlagSet();
	}
	
	@Override
	public void setIOFlag() {
	}
	
}

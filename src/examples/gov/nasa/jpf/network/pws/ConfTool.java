package gov.nasa.jpf.network.pws;
/*  ConfTool.java - Grafical configuration tool

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.io.*;
import java.awt.*;

public class ConfTool
	{
		
	//window stuff
	static ConfToolFrame window;
	
	public static void main(String[] args)
		{
		Misc.putSysMessage(0,"Main app launched, launching main frame");

		window = new ConfToolFrame("Configuration Tool for PWS");
		Toolkit theKit = window.getToolkit();
		Dimension wndSize = theKit.getScreenSize();
		window.setBounds(wndSize.width/4, wndSize.height/8, wndSize.width/2, wndSize.height/4*3 );
		window.setVisible(true);
		
		}
	}
Changelog for Pegasi Web Server (note: ConfTool is NOT discussed here)

Explanation of symbols used:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  = changed something
  + added something
  - removed something
  * change in version number

Changes leading to version 0.2.3
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
= Security bugfixes Donato Ferrante
  http://seclists.org/lists/bugtraq/2004/Mar/0104.html
= Updated email addressess in README
= Updated Thanks file
= Updated copyright dates
= Updated URLEncoding and URLDecoding to use new functions instead of 
  deprecated ones

Changes leading to version 0.2.2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
= made a little change in the 'logging' system, for compatibility with 
  Win32 systems
+ attempted to create a CGI interface. I've got a bit of work left :-(
+ added & implemented multiple commandline options:
   -n or --nologs : disable logging
   -h or --help : shows help for the commandline options
   -v or --verbose : allows you to increase the verbosity by 1 level, max to 2.
                     standard verbosity is 0, no output at all, except the title.
+ implemented maxclients directive.
= updated conftool source (made by uXs)
= changed the config-parsing routine, it contained a bug where you could crash
  it by just entering a keyword without a value in the conf file.
= changed a few output levels, level 1 (was error -> is warning),
  level 8 (is error). Level 8 is always shown on the stdout!
= changed the header date to actually give the current date instead of 
  'somewhere in the near future'
= changed date classes from GregorianCalender to SimpleDateFormat.
* version 0.2.2-pre1 released *
+ server now accepts semi-valid requests, such as:
    GET /			(httpversion missing, defaults to 1.0)
	GET HTTP/1.1	(path missing, defaults to /)
	GET				(combination of the 2 above)
= moved all output-related functions to a new class, OutputHeader
= moved some functions from Connection.java to Misc.java  
+ implemented URLEncoding when giving a directory listing
= changed read/output implementation, is now 200* as fast using std functions
+ created new class FileFinder, which, remarkably, finds files :-)
+ added header Content-Length to all output of server
+ added !DOCTYPE tags to server-generated html messages
= moved all statements to flush the streams inside the OutputHandler
+ added the compatibility with older (< HTTP/1.0) browsers by not sending any 
  headers out.
= corrected sending of headers, they where sent after the body -- must have 
  gotten broken somewhere <g>
+ implemented the HEAD method
= changed the Date output to use US dates, not locale-specific dates
+ added Last-Modified header to all output
* version 0.2.2-pre2 released *
= changed the default HTTP version with 'semi-valid' requests to 0.9
+ added 'die' keyword to force people to edit the config file first before
  running the server - and to avoid 'it don't work!' messages
= updated the ConfTool sourcecode (uXs)
+ added the -i | --ignore commandline option to ignore any server
  misconfigurations that might be present (use at own risk!)
+ implemented the TRACE method
+ added 'connection: keep-alive' method for multiple requests over the same
  connection.
+ implemented keyword 'timeout' for timeouts while waiting for client input
+ implemented keyword 'servername'
= moved all inputrelated things to the new InputHandler
* version 0.2.2 released *


Changes leading to version 0.2.1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- implemented read buffering system to increase output performance. 
  The standard buffer size is 2048bytes, but you can set another value
  using the configuration file keyword readBufferSize: <amount>
  If it is set <= 0, or > 1048576 (1 mb), the buffersize is reset to the
  standard value.
- server now returns an error when a protocol different from HTTP is used
- empty lines are now allowed in configuration and mime files
= configuration file is now more 'apache' style, to allow a quick reconfigure
  using the apache conf files :-)
+ server now uses dirIndexFiles keyword in configuration file to look for index 
  files when a directory is requested.
+ server now gracefully exits when the port is already in use instead of
  crashing.
+ server now urldecodes the URI's before processing them
+ server now can handle variables in URI string
+ server now gracefully exits when the ServerRoot directive is not set in conf
  file
+ server can now use log files instead of stdout/stderr; they can be set using 
  errorLog and accessLog in conf file
+ started work on full HTTP/1.1 GET compliance (see TODO file for more details 
  on what to do still)
= modified config file parser to ignore " characters (apache config compatibility)
+ server now returns a 'default' mime type when it cannot find the correct one in 
  the mimetype definition file.
= corrected spelling of word 'extension'. It was written with a final 't' instead
  of an 's'. And it was bugging me that I misspelled it. Damn. :oPP
+ added functionality to add mimetypes through the config file, using 'addType' 
  keyword
+ added 'preprocessor' functionality (basic, for now), using 'action' keyword in 
  config file. This has been tested with PHP, and seems to work.
+ added the functionality for the 'serversignature' directive. You can choose
  between 'off', 'on', and 'email'.
* version 0.2.1 released *


Changes leading to version 0.2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
+ binary file support!
+ use of config file for main server settings
+ implemented biggest part of GET method
+ directory listing when index file not found
+ multiple index files (index. htm/html/php)
+ implemented use of mime-types to return content-type header
+ implemented use of multiple headers
+ implemented multithreading
* version 0.2 released *


Changes leading to version 0.1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
+ initial release
+ text file support
+ basic server design
* version 0.1 released *

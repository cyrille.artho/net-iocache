/*
  This program simulates behavior of a secure client. It simply sends 'A', 'B', and 'C' in order.
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

#include <arpa/inet.h>

#define MYPORT "18600"  // the port users will be connecting to
#define BACKLOG 10     // how many pending connections queue will hold
#define MAXDATASIZE 64 // max number of bytes we can get at once

typedef struct sockaddr_storage 	sockaddr_storage;
typedef struct sockaddr 			sockaddr;
typedef struct sockaddr_in			sockaddr_in;
typedef struct addrinfo 			addrinfo;


int num_clients;
int num_msg;


// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa);
void *client_run(void *id);
void *producer_run(void *id);
void *consumer_run(void *id);


int main(int argc, char **argv) {
	pthread_t threads[64];
	int i;
	int rv;
	
	if (argc < 3) {
		perror("Usage: alphabet-client (#clients) (#messages)");
		exit(1);
	}

	num_clients = (int) strtol(argv[1], NULL, 0);
	num_msg = (int) strtol(argv[2], NULL, 0);

	for (i = 0;i < num_clients;i++) {
		rv = pthread_create(&threads[i], NULL, client_run, (void *) i);

		if (rv) {
			perror("Error in creating a thread");
			pthread_exit((void *) 1);
		}
	}

	for (i = 0;i < num_clients;i++) {
		pthread_join(threads[i], NULL);
	}

	return 0;
}

void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void *client_run(void *id) {
	addrinfo hints;
	addrinfo *servinfo;
	addrinfo *p;

	int sockfd;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	getaddrinfo("localhost", MYPORT, &hints, &servinfo);

	// loop through all the results and connect to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		pthread_exit((void *) 2);
	}

	freeaddrinfo(servinfo); // all done with this structure

	// Generate a random number sequence.
	srand(time(0));
	int remainder = rand() % 2;

	int i;
	int thread_id = (int) id;
	for (i = 0;i < num_msg;i++) {
		char buf[MAXDATASIZE];
		char req = (char) ('0' + remainder + 2 * (num_msg * thread_id + i));

		printf("producer %d: send '%c'\n", sockfd, req);
		// send input
		if (send(sockfd, &req, 1, 0) == -1) {
			perror("send");
			pthread_exit((void *) 1);
		}

		int numbytes;
		// wait for an answer
		if ((numbytes = recv(sockfd, buf, 1, 0)) == -1) {
			perror("recv");
			pthread_exit((void *) 1);
		}

		buf[numbytes] = '\0';

		printf("consumer %d: received '%s'\n", sockfd, buf);

		// Extra ND operation.
		srand(time(0));
	}

	close(sockfd);
	pthread_exit(NULL);
}

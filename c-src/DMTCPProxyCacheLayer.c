#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <gov_nasa_jpf_network_cache_DMTCPProxyCacheLayer.h>


#define BACKLOG 1


typedef struct sockaddr sockaddr;
typedef struct sockaddr_un sockaddr_un;


int recvfd(int s, void *storage, int len, int *new_fd);

JNIEXPORT jint JNICALL Java_gov_nasa_jpf_network_cache_DMTCPProxyCacheLayer_createUnixDomainChannel
(JNIEnv *env, jclass cls, jstring sock_name) {
	sockaddr_un addr;
	socklen_t len;
	jboolean is_copy = 0;
	int sockfd;
	int accepted_fd;
	const char *sock_addr = (*env)->GetStringUTFChars(env, sock_name, &is_copy);
	
	if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		perror("server: socket");
		exit(1);
	}
	
	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, sock_addr);

	// Create a channel between this process and "dest".
	unlink(sock_addr);
	len = sizeof(addr.sun_family) + strlen(addr.sun_path);
	if (bind(sockfd, (sockaddr *) &addr, len) < 0) {
		perror("client: connect");
		exit(1);
	}

	listen(sockfd, BACKLOG);
	accepted_fd = accept(sockfd, NULL, NULL);

	close(sockfd);

	// Release the string argument.
	(*env)->ReleaseStringUTFChars(env, sock_name, sock_addr);

	return (jint) accepted_fd;
}

JNIEXPORT jint JNICALL Java_gov_nasa_jpf_network_cache_DMTCPProxyCacheLayer_closeSocket
(JNIEnv *env, jclass cls, jint fd) {
	int rv = close(fd);
	
	return (jint) rv;
}

JNIEXPORT jint JNICALL Java_gov_nasa_jpf_network_cache_DMTCPProxyCacheLayer_sendViaUnixChannel
(JNIEnv *env, jclass cls, jint fd, jbyteArray data, jint len) {
	jbyte *buf = (jbyte *) malloc(len * sizeof(jbyte));
	jint rv;

	// Store data in a local array.
	(*env)->GetByteArrayRegion(env, data, (jsize) 0, (jsize) len, buf);
	
	rv = (jint) send((int) fd, buf, (size_t) len, 0);

	free(buf);
	
	return rv;
}

JNIEXPORT jint JNICALL Java_gov_nasa_jpf_network_cache_DMTCPProxyCacheLayer_recvViaUnixChannel
(JNIEnv *env, jclass cls, jint fd, jbyteArray buf, jint len) {
	jbyte *local = (jbyte *) malloc(len * sizeof(jbyte));
	jint rv;

	// Receive data and store it in local storage.
	rv = recv((int) fd, local, (size_t) len, 0);

	if (rv > 0) {
		// Move the received data to the argument.
		(*env)->SetByteArrayRegion(env, buf, 0, rv, local);
	}

	free(local);
	
	return rv;
}

JNIEXPORT jint JNICALL Java_gov_nasa_jpf_network_cache_DMTCPProxyCacheLayer_recvFd
(JNIEnv *env, jclass cls, jint fd, jbyteArray buf, jint len, jintArray new_fd) {
	jbyte *local = (jbyte *) malloc(len * sizeof(jbyte));
	jint rv;
	int recv_fd;

	// Receive data and store it in local storage.
	rv = recvfd((int) fd, local, (size_t) len, &recv_fd);

	if (rv > 0) {
		// Move the received data to the argument.
		(*env)->SetByteArrayRegion(env, buf, 0, rv, local);
		// Output the new FD.
		(*env)->SetIntArrayRegion(env, new_fd, 0, 1, &recv_fd);
	}

	free(local);
	
	return rv;	
}

int recvfd(int s, void *storage, int len, int *new_fd) {
	int n;
	struct iovec iov;
	struct msghdr msg;
	struct cmsghdr *cmsg;
	char cms[CMSG_SPACE(sizeof(int))];

	iov.iov_base = storage;
	iov.iov_len = len;
	
	memset(&msg, 0, sizeof msg);
	msg.msg_name = 0;
	msg.msg_namelen = 0;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = (caddr_t)cms;
	msg.msg_controllen = sizeof cms;

	if((n = recvmsg(s, &msg, 0)) < 0)			
		return -1;
	
	if(n == 0){		
		return -1;
	}

	cmsg = CMSG_FIRSTHDR(&msg);
	// Output the new FD.
	memmove(new_fd, CMSG_DATA(cmsg), sizeof(int));
	
	return n;
}

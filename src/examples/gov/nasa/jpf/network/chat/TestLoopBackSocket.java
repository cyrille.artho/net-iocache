//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.chat;

/* $Id: TestLoopBackSocket.java 179 2006-08-09 05:50:06Z cartho $ */

import java.io.*;
import junit.framework.TestCase;

public class TestLoopBackSocket extends TestCase {
    LoopBackSocket lbSocket;

    public void setUp() {
        lbSocket = new LoopBackSocket();
    }

    public void testLoopBack() {
        try {
            PrintWriter writer = new PrintWriter(lbSocket.getOutputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(lbSocket.getInputStream()));
            String example = "----SAMPLE STRING----";
            writer.write(example);
            writer.flush();
            writer.close();
            String read = reader.readLine();
            assertEquals(example, read);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

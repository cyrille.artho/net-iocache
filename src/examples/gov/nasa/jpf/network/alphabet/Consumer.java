//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.alphabet;

import java.io.IOException;
import java.io.InputStream;

public class Consumer extends Thread {

	private InputStream is;

	private byte[] messages;

	public Consumer(InputStream stream, byte[] messages) {
		is = stream;
		this.messages = messages;
	}

	public void run() {
		int b;

		try {
			for (int i = 0; i < messages.length; i++) {
//				System.out.println("[Consumer: run() threadID=" 
//									+ getId() + "] trying to read "+(i+1)+". response...");
				b = is.read();
				assert b==messages[i]-'0'+'a' : "[Consumer: run() threadID=" 
						+ getId() + "] expected '"+(char)(messages[i]-'0'+'a')+"' but received '"+(b==-1?"EOF":(char)b)+"'"; 
				System.out.println("[Consumer: run() threadID=" 
									+ getId() + "] read "+(i+1)+". response: '" + (char) b+"'");
			}

			System.out.println("[Consumer: run() threadID=" 
					+ getId() + "] closes connection...");
			is.close();
		} catch (IOException e) {
			System.out.println("\n[Consumer: run() threadID=" 
								+ getId() + "] IOException occurred!\n"+e+"\n");
			assert false;
		}
//		System.out.println("[Consumer: run() threadID=" 
//									+ getId() + "] terminates.");
	}
}

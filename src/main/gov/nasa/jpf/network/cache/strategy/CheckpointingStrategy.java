package gov.nasa.jpf.network.cache.strategy;

public interface CheckpointingStrategy {

	public boolean shouldSave(int stateId);
	
	public void setIOFlag();
	
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <netinet/in.h>
#include <mtcp.h>

#include "mtcpsetup.h"
#include "ncp.h"


// From mtcp.c
extern int dmtcp_info_pid_virtualization_enabled;

const int HOUR = 3600;
const char *VIRTCACHE_FIFO = "VIRTCACHE_FIFO";


// This function blocks until it gets a signal to write a checkpoint from the cache.
void ckpt_trigger(int sec) {
    char s[32];
	char *fifo = getenv(VIRTCACHE_FIFO);
    fd_set set;
    int num, fd;

	if (!fifo)
	  perror("VIRTCACHE_FIFO is not defined");

    mknod(fifo, S_IFIFO | 0666, 0);

    printf("waiting for checkpoint command ...\n");
    fd = open(fifo, O_RDONLY);

    /* Initialize the file descriptor set. */
    FD_ZERO (&set);
    FD_SET (fd, &set);

    select(FD_SETSIZE, &set, NULL, NULL, NULL);

    if (FD_ISSET(fd, &set)) {
        if ((num = read(fd, s, 32)) == -1)
    		perror("read");
    }

	close(fd);
}

void mtcpsetup_premain() {
    FILE *file;
    char filename[8];
    int pid = getpid();
	int c;

    sprintf(filename, "%d", pid);

    if ((file = fopen(filename, "r+"))) {
		c = fgetc(file);

		// Second call
		if (c == EOF) {
			printf("[premain] second call\n");
			fputc('1', file);
			fclose(file);
			return;
		}
		// Third call
		else {
			printf("[premain] third call\n");
			printf("[premain] setup MTCP\n");
			fclose(file);
		}
    }
	// First call
    else {
		printf("[premain] first call, touch file %s\n", filename);
		file = fopen(filename, "w");
		fclose(file);
		return;
    }
    
    printf("Setup MTCP\n");

	// Must initialize this external variable, otherwise an error occurs when restarting
    dmtcp_info_pid_virtualization_enabled = 0;

    pid_t tid = (pid_t) syscall (SYS_gettid);

    printf("[init] tid %d\n", tid);
    printf("[init] pid %d\n", getpid());

	// Replace function sleep with our custom function.
    mtcp_set_callbacks(ckpt_trigger, NULL, mtcpsetup_post_ckpt, NULL, NULL, NULL);
    mtcp_init("testmtcp.mtcp", HOUR, 1);
    mtcp_ok();
}

void mtcpsetup_post_ckpt(int is_restarting) {
	char *fifo = getenv(VIRTCACHE_FIFO);
	char s[2];
	int fd;
	int num;
	
	// after restoring
	if (is_restarting)
		ncp_rebind_all_sockets(is_restarting);
	// after checkpointing
	else {
		mknod(fifo, S_IFIFO | 0666, 0);

		fd = open(fifo, O_WRONLY);
		
		memset(s, 0, sizeof s);
		s[0] = 1;
		
		printf("Tell MTCP to checkpoint\n");
		if ((num = write(fd, s, strlen(s))) == -1)
			perror("write");

		close(fd);
	}
}

#!/bin/bash
# Move or rename a checkpoint file to "state-<id>.sh". The exit value is the number of checkpoint files created.
# Usage: savestate-dmtcp.sh <state ID> <checkpoint directory>

SCRIPT_DIR=`dirname "$0"`
SCRIPT_DIR=`cd ${SCRIPT_DIR}; pwd`
. ${SCRIPT_DIR}/env.sh

ERR_LOG="${CHKPNT_DIR}/err.log"
ERR_CODE=255
SAVE_ERR_CODE=254

if [ $# -lt 2 ]
then
	echo "[$0] Usage: savestate-dmtcp.sh <state ID> <checkpoint directory>" >> ${ERR_LOG}	
	exit ${ERR_CODE}
fi

CHKPNT_STORAGE="$2"
STATE_FILE="${CHKPNT_STORAGE}/state-$1.sh"

# Tell DMTCP to save peer state.
rm -f "$TESTRUN_DIR"/*.dmtcp  # see notes in restore_checkpoint-proxy.sh
${DMTCP_SAVE} 2>> ${ERR_LOG}
# Error checking
if [ "$?" -ne "0" ]
then
	echo "Fail to save peer state" >> ${ERR_LOG}
	exit ${SAVE_ERR_CODE}
fi

# Block until DMTCP saves peer state.
${CBUILD_DIR}/syncckpt 2>> ${ERR_LOG}
# Timeout. Assume that the peer was terminated.
if [ "$?" -ne "0" ]
then
	echo "[$0] Timeout. Assume that the peer was terminated." >> ${ERR_LOG}
	exit ${ERR_CODE}
fi

# Does a checkpoint (*.dmtcp file) exist?
if [ "$DMTCP_CHECKPOINT_DIR" = "" ]  # set by the new refactored script: bin/utils-for-testing.source
then
    SRC=`ls ${SCRIPT_DIR}/*.dmtcp`
else
    SRC=$DMTCP_CHECKPOINT_DIR/*.dmtcp
fi
if [ "$?" -eq "0" ]
then
	COUNT=0

	# Rename every checkpoint file.
	for F in ${SRC}
	do
		NAME=`basename ${F}`
		DEST_DMTCP="${CHKPNT_STORAGE}/state-$1-${NAME}"

		# Rename and move dmtcp files.
		# We must move, NOT copy. Some peer process may terminate in the future.
		# If the old checkpoint file of that process remained in the directory, it would be incorrectly regarded as the new checkpoint file.
		mv ${F} ${DEST_DMTCP} 2>> ${ERR_LOG}

		COUNT=`expr ${COUNT} + 1`
	done

	exit ${COUNT}
else
	echo "[$0] No checkpoint to move or rename." >> ${ERR_LOG}
	exit ${ERR_CODE}
fi

// This program fills allocated memory with random values. It is used for GDB benchmark.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main() {
	const int mem_size = 256 * 1024;
	int *mem = (int *) malloc(sizeof(int) * mem_size);
	int i = 0;
	
	srand(0);
	printf("MEM: %d\n", mem_size);
	
	for (i = 0; i < mem_size; i++) {
		mem[i] = rand();
	}
	
	free(mem);

	return 0;
}

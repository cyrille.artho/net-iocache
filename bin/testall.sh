#!/bin/bash
# Perform some key tests. 

. `dirname "$0"`/env.sh

LOG_DIR="${SCRIPT_DIR}/log"
FMT="%-50s... "
FAILED=0
DIFF=0
SKIPPED=0
# The revision of jpf-core used to produce the template log files.
TEMPLATE_JPF_REV="960"

printok() {
	echo -e "[   \033[32mOK\033[0m   ]"
}

printfailed() {
	echo -e "[ \033[31mFAILED\033[0m ]"
	FAILED=`expr ${FAILED} + 1`
} 

printdiff() {
	echo -e "[  \033[33mDIFF\033[0m  ]"
	DIFF=`expr ${DIFF} + 1`
} 

printna() {
	echo -e "[ \033[33m N/A  \033[0m ]"
	SKIPPED=`expr ${SKIPPED} + 1`
}

# Print a result according to a given value.
# 0: OK
# 1: DIFF
# 2: FAILED
print_result() {
	case "$1" in 
		0) printok
			;;
		1) printdiff
			;;
		*) printfailed
			;;
	esac
}

# Run a shell script with given arguments and print the result.
# usage: run_case (case name) [argument]*
run_case() {
	if [ "$#" -lt "1" ]
	then
		echo "usage: run_case (case name) [argument]*"
	fi

	CASE_NAME="$1"
	LOG=`output_file $1.sh`
	shift

	printf "${FMT}" "Running ${CASE_NAME}"
	
	case ${CASE_NAME} in 
	helloWorld*)
		${SCRIPT_DIR}/${CASE_NAME}.sh $@ > ${LOG_DIR}/${LOG}
		RV=$?
		;;
	*)
		# Cannot redirect output to a file here; otherwise it will be subject to checkpointing.
		${SCRIPT_DIR}/${CASE_NAME}.sh $@ > /dev/null
		RV=$?
		mv ${HOME}/${LOG} ${LOG_DIR}/
esac

	# Compare log files in "log" and "out".
	COMPARE_MSG=`${SCRIPT_DIR}/cmplog.sh ${LOG}`
	CMP_RV=$?

	# Print result based on the return value of the script and log comparison.
	if [ "${RV}" -eq "0" ]
	then
		if [ "${CMP_RV}" -eq 1 ]
		then
			print_result 1
		else
			print_result 0
		fi
	else
		print_result 2
	fi

	# Print a message from the comparison script, if any.
	if [ -n "${COMPARE_MSG}" ]
	then
		echo "${COMPARE_MSG}"
	fi
}

test_normal() {
	echo "Start testing the normal cache approach."

	run_case helloWorldClient-mc
	run_case helloWorldServer-mc

#	run_case alphabet-client-mc 2-2
#	run_case alphabet-client-nd-mc 1-2
#	run_case alphabet-server-mc 2-2
#	run_case alphabet-server-nd-mc 2-2

#	printf "${FMT}" "Checking for thttpd"
#	# This test requires thttpd.
#	if [ "`which thttpd`" != "" ]
#	then
#		printok
#		run_case http-client-mc 2
#		run_case http-client-nd-mc 2
#	else
#		printna
#		echo "Skipping the http-client test. thttpd is not installed."
#	fi
#
#	run_case http-server-mc 2
#	run_case http-server-nd-mc 2
}

test_chkpnt() {
	echo "Start testing the checkpointing approach."

	run_case secure-client-nd-dmtcp -i 2-2
	run_case secure-server-nd-dmtcp -i 2-2
}

# Print current jpf-core revision.
jpf_rev() {
	printf "${FMT}" "Checking current jpf-core revision"
	hg --cwd ${JPF_HOME} par | grep 'changeset' | sed 's/.*[[:space:]]\([0-9]\{1,\}\):.*/\1/'
}

printf "${FMT}" "Revision of jpf-core used to produce template log"
echo "${TEMPLATE_JPF_REV}"
jpf_rev
test_normal

# Before performing DMTCP-based tests, we run several checks.
# Check OS. Only Linux is supported.
printf "${FMT}" "Checking if Linux is available"
if [ `uname` != "Linux" ]
then
	printna
	echo "Your OS is not supported. Currently DMTCP supports only Linux."
	DMTCP=0
else
	printok
	DMTCP=1
fi

if [ ${DMTCP} -eq 1 ]
then
	# Check if DMTCP exists.
	# We actually check the existence of directory "bin" within DMTCP.
	printf "${FMT}" "Checking for DMTCP"
	if [ ! -e "${DMTCP_DIR}/bin" ]
	then
		printna
		echo "DMTCP is not built yet. Please build it first by running 'ant make'."
		DMTCP=0
	fi
fi

if [ ${DMTCP} -eq 1 ]
then
	# Check VDSO
	chk_vdso_silent
	if [ $? -ne 0 ]
	then
		DMTCP=0
		printna
		echo "Must set /proc/sys/vm/vdso_enabled to 0."
	else
		printok
	fi
fi

if [ ${DMTCP} -eq 1 ]
then
	test_chkpnt
fi

echo "=== Test summary ==="
if [ ${SKIPPED} -ne 0 ]
then
	echo "${SKIPPED} or more tests were skipped due to missing components,"
	echo "or because of DMTCP not being supported on this system."
fi

if [ ${FAILED} -eq 0 -a ${DIFF} -eq 0 ]
then
	echo "All tests passed."
else
	echo "${FAILED} tests failed, ${DIFF} tests have different output."

	if [ "${FAILED}" -eq "0" ]
	then
		exit 0
	else
		exit 1
	fi
fi

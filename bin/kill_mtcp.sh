#!/bin/bash
# Kill process 'mtcp_restart' or the peer process.
# If some process is killed, return 0; otherwise, return 1.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

ERR_LOG="${CHKPNT_DIR}/err.log"

restart_coordinator() {
	dmtcp_command -q
	echo "Close the coordinator." >> ${ERR_LOG}
	dmtcp_coordinator --ckptdir ${HOME}/projects/jpf/jpf-net-iocache/tmp-secure-server --background
	echo "Restart the coordinator." >> ${ERR_LOG}
	sleep 1
}

# If "mtcp_restart" is running.
if [ -f ${CHKPNT_DIR}/${MTCP_PID_FILE} ]
then
	MTCP_PID=`cat ${CHKPNT_DIR}/${MTCP_PID_FILE}`

	kill -9 ${MTCP_PID}
	RV=$?

	rm ${CHKPNT_DIR}/${MTCP_PID_FILE}

	exit ${RV}
fi

# If the original peer is running.
if [ -f ${CHKPNT_DIR}/${PEER_PID_FILE} ]
then
	PEER_PID=`cat ${CHKPNT_DIR}/${PEER_PID_FILE}`

	kill -9 ${PEER_PID}
	RV=$?

	rm ${CHKPNT_DIR}/${PEER_PID_FILE}

	exit ${RV}
fi

exit 1

package gov.nasa.jpf.network.pws;
/*  ConfTool.java - Grafical configuration tool

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class ConfToolFrame extends JFrame
	{
	//config-file stuff
    private static String mainConfigFile = "conf/pws.conf";
	private static String mimeConfigFile = "conf/mimetypes.conf";
	private static Map serverSettings = new HashMap();  /* general server settings */
	private static Map mimeTypes = new HashMap();       /* mimetype configuration */
	private static Map actionHandlers = new HashMap();  /* action handlers for certain mimetypes/extensions */

	//window stuff
	private static double hugeWeight = 500.0;  //don't change this in the code
	GridBagConstraints constraints = new GridBagConstraints();

	//stuff i'll probably need later on
	JButton buttonOk;
	JButton buttonCancel;
	JButton buttonApply;

	JButton buttonBrowse1;	
	JButton buttonBrowse2;	
	JButton buttonBrowse3;	
	JButton buttonBrowse4;	

	JButton buttonInsert;	
	JButton buttonDelete;	

	JTextField fieldBrowse1;
	JTextField fieldBrowse2;
	JTextField fieldBrowse3;
	JTextField fieldBrowse4;

	JTextField fieldAdmin;
	JTextField fieldServer;
	JTextField fieldType;
	JTextField fieldPort;
	JTextField fieldClients;
	JTextField fieldBuffer;
	JTextField fieldTimeout;

	JTextField fieldIndexList;
	JList indexList;
	Vector someList;

	JLabel labelMimeType;
	JLabel labelMimeExtensions;
	JLabel labelMimeAction;
	JList mimeList;
	Vector someList2;
	
	JButton buttonInsert2;
	JButton buttonEdit2;
	JButton buttonDelete2;
	
	JCheckBox checkBoxListing;
	JCheckBox checkBoxServerSig;

	//constructor
	public ConfToolFrame(String title)
		{
		Misc.putSysMessage(0,"We're now in the main frame");

		//get the config data and dump it on the console
		/*
			we'll have to reprogram the parser later because we'll have to write the config files too,
			INCLUDING the comments - blah
		*/
		Misc.putSysMessage(0,"Parsing main configuration file...");
    	ConfProcessor.parseConfigurationFile2(mainConfigFile,serverSettings, actionHandlers, mimeTypes);
		Misc.putSysMessage(0,"Parsing mime types...");
		ConfProcessor.parseMimeTypes2(mimeConfigFile, mimeTypes);
		
		/* //not needed any more atm
    	Iterator keyIter = serverSettings.keySet().iterator();
    	String currentKey;
    	while (keyIter.hasNext())
    		{
    		currentKey = (String)keyIter.next();
			Misc.putSysMessage(0,currentKey + ": " + (String)serverSettings.get(currentKey));
			}
		*/
		/* //not needed any more atm
    	Iterator keyIter = mimeTypes.keySet().iterator();
    	String currentKey;
    	while (keyIter.hasNext())
    		{
    		currentKey = (String)keyIter.next();
			Misc.putSysMessage(0,currentKey + ": " + (String)mimeTypes.get(currentKey));
			}
		*/


		
		//set some window stuff
		Misc.putSysMessage(0,"Processing window layout");

		setTitle(title);
		enableEvents(AWTEvent.WINDOW_EVENT_MASK);

		//part 1 of tab 1 - settings typed in by the luser
		
		GridBagLayout bag1 = new GridBagLayout();
		JPanel panel1 = new JPanel(bag1);
		panel1.setBorder(new TitledBorder(new EtchedBorder(),"Some Settings"));

		//1-1 labels
		setDefaultConstraints();
		constraints.anchor= GridBagConstraints.WEST;
		constraints.insets = new Insets(0,10,0,10);
	    addLabel("Server Admin", constraints, bag1, panel1);
		constraints.gridy = GridBagConstraints.RELATIVE;
	    addLabel("Servername", constraints, bag1, panel1);
	    addLabel("Default Type", constraints, bag1, panel1);
	    addLabel("HTTP Port", constraints, bag1, panel1);
	    addLabel("Max Clients", constraints, bag1, panel1);
		constraints.gridx = 2;
		constraints.gridy = 2;
	    addLabel("Readbuffersize", constraints, bag1, panel1);
		constraints.gridy = GridBagConstraints.RELATIVE;
	    addLabel("Timeout", constraints, bag1, panel1);

		//1-1 fields
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridwidth = 3;
		constraints.gridx = 1;
		constraints.insets = new Insets(0,0,5,10);
		constraints.weightx = hugeWeight;
		fieldAdmin = addField(constraints, bag1, panel1);
		constraints.gridy = GridBagConstraints.RELATIVE;
		fieldServer  = addField(constraints, bag1, panel1);
		constraints.gridwidth = 1;
		fieldType = addField(constraints, bag1, panel1);
		fieldPort = addField(constraints, bag1, panel1);
		fieldClients = addField(constraints, bag1, panel1);
		constraints.gridx = 3;
		fieldBuffer = addField(constraints, bag1, panel1);
		fieldTimeout = addField(constraints, bag1, panel1);

		//1-1 checkboxes
		setDefaultConstraints();
		constraints.anchor= GridBagConstraints.WEST;
		constraints.gridwidth = 2;
		constraints.gridy = 5;
		constraints.insets = new Insets(0,10,0,10);
		checkBoxListing = addCheckBox("ReturnDirListing", constraints, bag1, panel1);
		constraints.gridx = GridBagConstraints.RELATIVE;
		checkBoxServerSig = addCheckBox("Serversignature", constraints, bag1, panel1);


		//part 2 of tab 1 - settings that will involve choosing a directory or file
		GridBagLayout bag2 = new GridBagLayout();
		JPanel panel2 = new JPanel(bag2);
		panel2.setBorder(new TitledBorder(new EtchedBorder(),"Select directories or files"));
		
		//1-2 labels
		setDefaultConstraints();
		constraints.anchor= GridBagConstraints.WEST;
		constraints.gridx = 0;
		constraints.insets = new Insets(0,10,0,10);
	    addLabel("Server Root", constraints, bag2, panel2);
		constraints.gridy = GridBagConstraints.RELATIVE;
	    addLabel("Document Root", constraints, bag2, panel2);
	    addLabel("Error Log", constraints, bag2, panel2);
	    addLabel("Access Log", constraints, bag2, panel2);

		//1-2 fields
		setDefaultConstraints();
		constraints.anchor= GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.weightx = hugeWeight;
		constraints.gridx = 1;
		fieldBrowse1 = addField(constraints, bag2, panel2);
		constraints.gridy = GridBagConstraints.RELATIVE;
		fieldBrowse2 = addField(constraints, bag2, panel2);
		fieldBrowse3 = addField(constraints, bag2, panel2);
		fieldBrowse4 = addField(constraints, bag2, panel2);
		
		//1-2 buttons
		setDefaultConstraints();
		constraints.insets = new Insets(0,10,0,10);
		constraints.gridx = 2;
	    buttonBrowse1 = addButton("Browse...", constraints, bag2, panel2);
		constraints.gridy = GridBagConstraints.RELATIVE;
   		buttonBrowse2 = addButton("Browse...", constraints, bag2, panel2);
	    buttonBrowse3 = addButton("Browse...", constraints, bag2, panel2);
	    buttonBrowse4 = addButton("Browse...", constraints, bag2, panel2);
	    buttonBrowse1.addActionListener(new ButtonListener(fieldBrowse1));
	    buttonBrowse2.addActionListener(new ButtonListener(fieldBrowse2));
	    buttonBrowse3.addActionListener(new ButtonListener(fieldBrowse3));
	    buttonBrowse4.addActionListener(new ButtonListener(fieldBrowse4));

		//part 3 of tab 1 - recognized index files
		GridBagLayout bag3 = new GridBagLayout();
		JPanel panel3 = new JPanel(bag3);
		panel3.setBorder(new TitledBorder(new EtchedBorder(),"Recognized index-files"));
		
		//1-3 field
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.insets = new Insets(0,10,0,0);
	    fieldIndexList = addField(constraints, bag3, panel3);

		//1-3 list
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridy = GridBagConstraints.RELATIVE;
		constraints.insets = new Insets(0,10,0,0);
		constraints.weightx = hugeWeight;
		constraints.weighty = hugeWeight;
		//make a list and stuff if
		someList = new Vector();
		StringTokenizer configuredIndexFiles =  new StringTokenizer ((String)serverSettings.get("directoryindex"));
		while (configuredIndexFiles.hasMoreTokens())
			someList.add(configuredIndexFiles.nextToken());
		JList indexList = new JList(someList);
		//make it scrollable by sticking it in a scrollpane
		JScrollPane scrollPane = new JScrollPane(indexList);
		//apply the constraints and draw the wretched thing
		bag3.setConstraints(scrollPane, constraints);
		panel3.add(scrollPane);
		
		//1-3 buttons
		setDefaultConstraints();
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 1;
		constraints.insets = new Insets(0,10,5,5);
		buttonInsert = addButton("Insert", constraints, bag3, panel3);
		constraints.gridy = GridBagConstraints.RELATIVE;
		buttonDelete = addButton("Delete", constraints, bag3, panel3);
	    buttonInsert.addActionListener(new ListButtonListener(fieldIndexList, indexList, someList));
	    buttonDelete.addActionListener(new ListButtonListener(fieldIndexList, indexList, someList));

		
		//the "OK", "CANCEL", and "APPLY" button on the bottom of the app
		GridBagLayout bag4 = new GridBagLayout();
		JPanel panel4 = new JPanel(bag4);

		//set the constraints and add the buttons
		setDefaultConstraints();
		constraints.anchor = GridBagConstraints.EAST;
		constraints.fill = GridBagConstraints.VERTICAL;
		constraints.insets = new Insets(5,5,5,5);
		constraints.weightx = hugeWeight;
		buttonOk = addButton("Ok", constraints, bag4, panel4);
		constraints.gridx = GridBagConstraints.RELATIVE;
		constraints.weightx = 1.0;
		buttonCancel = addButton("Cancel", constraints, bag4, panel4);
		buttonApply = addButton("Apply", constraints, bag4, panel4);

		//resize the buttons so they all get the same size
		double width, height;
		width = buttonOk.getPreferredSize().getWidth();
		width = buttonCancel.getPreferredSize().getWidth() > width ? buttonCancel.getPreferredSize().getWidth() : width;
		width = buttonApply.getPreferredSize().getWidth() > width ? buttonApply.getPreferredSize().getWidth() : width;
		height = buttonOk.getPreferredSize().getHeight();
		height = buttonCancel.getPreferredSize().getHeight() > width ? buttonCancel.getPreferredSize().getHeight() : height;
		height = buttonApply.getPreferredSize().getHeight() > width ? buttonApply.getPreferredSize().getHeight() : height;
		buttonOk.setPreferredSize(new Dimension((int)width,(int)height));
		buttonCancel.setPreferredSize(new Dimension((int)width,(int)height));
		buttonApply.setPreferredSize(new Dimension((int)width,(int)height));

	    buttonCancel.addActionListener(new CancelButtonListener());
	    buttonOk.addActionListener(new CancelButtonListener());
	    buttonApply.addActionListener(new CancelButtonListener());
		
		//the first tabpage of the tabbedpane
		JPanel tab1 = new JPanel();
		GridBagLayout mainBag1 = new GridBagLayout();
		tab1.setLayout(mainBag1);
		
		//start adding the panels allready
		//adds the panels to the first tab
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		mainBag1.setConstraints(panel1, constraints);
		tab1.add(panel1);
		constraints.gridy = GridBagConstraints.RELATIVE;
		mainBag1.setConstraints(panel2, constraints);
		tab1.add(panel2);
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weighty = hugeWeight;
		mainBag1.setConstraints(panel3, constraints);
		tab1.add(panel3);


		//part 1 of tab 2 - mime-types
		GridBagLayout bag5 = new GridBagLayout();
		JPanel panel5 = new JPanel(bag5);
		panel5.setBorder(new TitledBorder(new EtchedBorder(),"Mime-types"));
		
		//2-1 fields
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.insets = new Insets(0,10,0,0);
	    labelMimeType = addLabel("[mimetype]",constraints, bag5, panel5);
		constraints.gridy = GridBagConstraints.RELATIVE;
	    labelMimeExtensions = addLabel("[extensions]",constraints, bag5, panel5);
	    labelMimeAction = addLabel("[action]",constraints, bag5, panel5);
	    
		//2-1 list
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridy = GridBagConstraints.RELATIVE;
		constraints.insets = new Insets(0,10,0,0);
		constraints.weightx = hugeWeight;
		constraints.weighty = hugeWeight;
		//make a list and stuff if
		someList2 = new Vector();
		//add stuff to the list here
		/*
			right...
			this makes sure the list is sorted when we see it, which is kinda
			nice since there are a LOT of mime-types
			now we only have to make sure that when we add new types, or change old
			ones, we KEEP IT SORTED
			not necessary for deleting - can you tell why ? :-)
			--
			it'd be handy to do this for the index-list too, but that's probably overkill
			it'll have to be done for the actions tho, but that's prolly just copy/paste anyway
			--
			/me gonna play GP2 for a bit now
			--
			okay... since it's apparantly possible to sort vectors, let's dump all this complicated
			shit with sorted maps and keeping it sorted and stuff and just sort it every time we add
			something
		*/
		/*TreeMap sortedMimeTypes = new TreeMap();
		sortedMimeTypes.putAll(mimeTypes);*/
    	//Iterator mimeIter = mimeTypes.keySet().iterator();
    	Iterator mimeIter = mimeTypes.keySet().iterator();
    	
    	String currentmimeKey;
    	while (mimeIter.hasNext())
    		{
    		currentmimeKey = (String)mimeIter.next();
			someList2.add(currentmimeKey);
			//Misc.putSysMessage(0,currentKey + ": " + (String)serverSettings.get(currentKey));
			}
		//ugh... wouldn't you just want to kill somebody sometimes ?
		//or all the time ? bah
		Collections.sort(someList2);
		JList mimeList = new JList(someList2);
		mimeList.addMouseListener(new MimeTypeListListener(mimeList, someList2));
		//make it scrollable by sticking it in a scrollpane
		JScrollPane scrollPane2 = new JScrollPane(mimeList);
		//apply the constraints and draw the wretched thing
		bag5.setConstraints(scrollPane2, constraints);
		panel5.add(scrollPane2);
		
		//2-1 buttons
		setDefaultConstraints();
		constraints.anchor = GridBagConstraints.NORTH;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 1;
		constraints.insets = new Insets(0,10,5,5);
		buttonInsert2 = addButton("New", constraints, bag5, panel5);
		constraints.gridy = GridBagConstraints.RELATIVE;
		buttonEdit2 = addButton("Edit", constraints, bag5, panel5);
		buttonDelete2 = addButton("Delete", constraints, bag5, panel5);
	    buttonInsert2.addActionListener(new MimeButtonListener(mimeList, someList2));
	    buttonEdit2.addActionListener(new MimeButtonListener(mimeList, someList2));
	    buttonDelete2.addActionListener(new MimeButtonListener(mimeList, someList2));
		
		//let's create another tab-thingie to make the code even more complicated
		//also set a gridbaglayout, because that's the only layout that doesn't suck
		JPanel tab2 = new JPanel();
		GridBagLayout mainBag2 = new GridBagLayout();
		tab2.setLayout(mainBag2);

		//adds the panels to the second tab
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		mainBag2.setConstraints(panel5, constraints);
		tab2.add(panel5);
		/*constraints.gridy = GridBagConstraints.RELATIVE;
		mainBag2.setConstraints(panel5, constraints);
		tab2.add(panel5);
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weighty = hugeWeight;
		mainBag1.setConstraints(panel3, constraints);
		tab1.add(panel3);
		*/


		//part 1 of tab 3 - actions
		//we let the actions part be as similar as possible as the extension
		//differences:
		//- no "new" button needed as we have no actions for non-existing mimetypes
		//- you can't edit the mimetype field in the dialog box
		//- some cosmetic stuff
		//- come to think of it, let's throw the delete button out too
		//- or better, let's just throw out the entire actions part, and just stick an extra field
		//	in the mimetypes list
		//  hooray
		//  another job well done :-)


		//create the tabbedPane
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		//stick the frames in the tabbedPane
		tabbedPane.add("Misc Settings",tab1);	
		tabbedPane.add("Mime Types",tab2);	

		//get a handle for the entire window and set the layout
		Container content = getContentPane();
		GridBagLayout mainBag = new GridBagLayout();
		content.setLayout(mainBag);

		//stick the entire tabbedPane in the window
		//set some constraints first, because what else would we do ?
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weighty = hugeWeight;
		mainBag.setConstraints(tabbedPane, constraints);
		content.add(tabbedPane);

		//adding the buttons would be nice too
		setDefaultConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridy = GridBagConstraints.RELATIVE;
		mainBag.setConstraints(panel4, constraints);
		content.add(panel4);
		
		//put the right stuff (not the movie) in the fields - the list allready has the right
		//content
		//could prolly be put also in this function but i'm too lazy
		initFields();
		
		Misc.putSysMessage(0,"Done processing layout");
		}

	//repetitive stuff goes in functions
	JLabel addLabel(String caption, GridBagConstraints constraints, GridBagLayout layout, JPanel panel)
		{
		JLabel label = new JLabel(caption);
		layout.setConstraints(label, constraints);
		panel.add(label);
		return label;
		}

	JButton addButton(String caption, GridBagConstraints constraints, GridBagLayout layout, JPanel panel)
		{
		JButton button = new JButton(caption);
		layout.setConstraints(button, constraints);
		panel.add(button);
		return button;
		}

	JTextField addField(GridBagConstraints constraints, GridBagLayout layout, JPanel panel)
		{
		JTextField field = new JTextField();
		layout.setConstraints(field, constraints);
		panel.add(field);
		return field;
		}

	JCheckBox addCheckBox(String caption, GridBagConstraints constraints, GridBagLayout layout, JPanel panel)
		{
		JCheckBox checkBox = new JCheckBox(caption);
		layout.setConstraints(checkBox, constraints);
		panel.add(checkBox);
		return checkBox;
		}

		
	//initialize defaultConstraints
	//used approx 3624563 times
	void setDefaultConstraints()
		{
		constraints.anchor= GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridwidth = 1;
		constraints.gridheight = 1;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.insets = new Insets(0,0,0,0);
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		}
		
	//code for killing the app
	//this should change
	protected void processWindowEvent(WindowEvent e)
		{
		if (e.getID() == WindowEvent.WINDOW_CLOSING)
			{
			Misc.putSysMessage(0,"Shutting down");
			dispose();
			System.exit(0);
			}
		super.processWindowEvent(e);
		}
	
	//stuff the fields with data
	void initFields()
		{
		//misc settings
		fieldAdmin.setText((String)serverSettings.get("serveradmin"));
		fieldServer.setText((String)serverSettings.get("servername"));
		fieldType.setText((String)serverSettings.get("defaulttype"));
		fieldPort.setText((String)serverSettings.get("port"));
		fieldClients.setText((String)serverSettings.get("maxclients"));
		fieldBuffer.setText((String)serverSettings.get("readbuffersize"));
		fieldTimeout.setText((String)serverSettings.get("timeout"));
		if(serverSettings.get("returndirlisting").toString().toLowerCase().equals("true"))
			checkBoxListing.setSelected(true);
		if(serverSettings.get("serversignature").toString().toLowerCase().equals("on"))
			checkBoxServerSig.setSelected(true);
		//dirs & files & stuff
		fieldBrowse1.setText((String)serverSettings.get("serverroot"));
		fieldBrowse2.setText((String)serverSettings.get("documentroot"));
		fieldBrowse3.setText((String)serverSettings.get("errorlog"));
		fieldBrowse4.setText((String)serverSettings.get("accesslog"));
		}
	
	
	//listener for the browse buttons	
	class ButtonListener implements ActionListener
		{
		JTextField field;
		
		ButtonListener(JTextField help)
			{
			field = help;
			}
			
		public void actionPerformed(ActionEvent e)
			{
			//Misc.putSysMessage(0,"A browse button has been clicked");
		    JFileChooser chooser = new JFileChooser();
		    chooser.setCurrentDirectory(new File(field.getText()));
		    if(e.getSource() == buttonBrowse3 || e.getSource() == buttonBrowse4)
	    		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			else
    			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    		int returnVal = chooser.showOpenDialog(null);
    		if(returnVal == JFileChooser.APPROVE_OPTION)
    			{
    			//Misc.putSysMessage(0,"You selected \"" + chooser.getSelectedFile().toString() + "\"");
	    		field.setText(chooser.getSelectedFile().toString());
	    		}
    		//else
    			//Misc.putSysMessage(0,"You didn't select nuthin'");
    			
			}
		}

	class ListButtonListener implements ActionListener
		{
		JTextField field;
		JList list;
		Vector vector;
		
		ListButtonListener(JTextField helpField, JList helpList, Vector helpVector)
			{
			field = helpField;
			list = helpList;
			vector = helpVector;
			}
			
		public void actionPerformed(ActionEvent e)
			{
			//Misc.putSysMessage(0,"Insert/delete button has been clicked");
			if(e.getSource() == buttonInsert)
				{
				if(vector.contains(field.getText()))
					{
					JOptionPane.showMessageDialog(null,"Ge zijt zelf ne vetzak.","Debug Message",JOptionPane.INFORMATION_MESSAGE);
					}
				else
					{					
					if(!field.getText().toLowerCase().equals(""))
						{
						vector.add(field.getText());
						list.setListData(vector);
						field.setText("");
						}
					}
				}
			else
				{
				if(list.getSelectedIndex()>=0)
					{
					field.setText((String)vector.elementAt(list.getSelectedIndex()));
					vector.remove(list.getSelectedIndex());
					list.setListData(vector);
					}
				}
			}
		}

	class MimeButtonListener implements ActionListener
		{
		JTextField field;
		JList list;
		Vector vector;
		
		MimeButtonListener(JList helpList, Vector helpVector)
			{
			list = helpList;
			vector = helpVector;
			}
			
		public void actionPerformed(ActionEvent e)
			{
			Misc.putSysMessage(0,"A mime button has been clicked");
			if(e.getSource() == buttonInsert2)
				{
				Misc.putSysMessage(0,"Insert...");
				MimeDialog mimeDialog;
				mimeDialog = new MimeDialog("Enter new mimetype","","","");
				Toolkit theKit2 = mimeDialog.getToolkit();
				Dimension wndSize2 = theKit2.getScreenSize();
				mimeDialog.setBounds(wndSize2.width/4, wndSize2.height/3, wndSize2.width/2, wndSize2.height/4 );
				mimeDialog.setModal(true);
				mimeDialog.setVisible(true);
				if(mimeDialog.flagButtonOkPressed == true)
					{
					Misc.putSysMessage(0,"add " + mimeDialog.fieldMimetype.getText() + " " + mimeDialog.fieldExtensions.getText() + " to mimetypes");
					if(vector.contains(mimeDialog.fieldMimetype.getText()))
						{
						JOptionPane.showMessageDialog(null,"Ge zijt nog altijd zelf ne vetzak.","Debug Message",JOptionPane.INFORMATION_MESSAGE);
						}
					else
						{
						if(mimeDialog.fieldExtensions.getText().trim().equals("") || mimeDialog.fieldMimetype.getText().trim().equals(""))
							{
							JOptionPane.showMessageDialog(null,"Mimetype or extension missing...","Warning",JOptionPane.INFORMATION_MESSAGE);
							}
						else
							{
							//add da mimetype
							vector.add(mimeDialog.fieldMimetype.getText());
							Collections.sort(vector);
							list.setListData(vector);
							mimeTypes.put(mimeDialog.fieldMimetype.getText(),mimeDialog.fieldExtensions.getText());
							if(!mimeDialog.fieldAction.getText().trim().equals(""))
								{
								actionHandlers.put(mimeDialog.fieldMimetype.getText(),mimeDialog.fieldAction.getText().trim());
								}
							//showtime
							list.setSelectedValue(mimeDialog.fieldMimetype.getText(),true);
							labelMimeType.setText((String)vector.elementAt(list.getSelectedIndex()));
							labelMimeExtensions.setText((String)mimeTypes.get((String)vector.elementAt(list.getSelectedIndex())));
							labelMimeAction.setText((String)actionHandlers.get((String)vector.elementAt(list.getSelectedIndex())));
							}
						}
					}
				else
					{
					Misc.putSysMessage(0,"do nothing");
					}
				mimeDialog.dispose();
				}
			else
				{
				if(list.getSelectedIndex()>=0)
					{
					if(e.getSource() == buttonEdit2)
						{
						Misc.putSysMessage(0,"Edit: " + (String)vector.elementAt(list.getSelectedIndex()));
						MimeDialog mimeDialog;
						mimeDialog = new MimeDialog("Edit mimetype",(String)vector.elementAt(list.getSelectedIndex()),(String)mimeTypes.get((String)vector.elementAt(list.getSelectedIndex())),(String)actionHandlers.get((String)vector.elementAt(list.getSelectedIndex())));
						Toolkit theKit2 = mimeDialog.getToolkit();
						Dimension wndSize2 = theKit2.getScreenSize();
						mimeDialog.setBounds(wndSize2.width/4, wndSize2.height/3, wndSize2.width/2, wndSize2.height/4 );
						mimeDialog.setModal(true);
						mimeDialog.setVisible(true);
						if(mimeDialog.flagButtonOkPressed == true)
							{
							Misc.putSysMessage(0,"edit " + mimeDialog.fieldMimetype.getText() + " " + mimeDialog.fieldExtensions.getText() + " to mimetypes");
							if(mimeDialog.fieldMimetype.getText().equals((String)vector.elementAt(list.getSelectedIndex())))
								{
								Misc.putSysMessage(0,"Mmkay... changing");
								//go ahead and save it
								mimeTypes.put(mimeDialog.fieldMimetype.getText(),mimeDialog.fieldExtensions.getText());
								if(!mimeDialog.fieldAction.getText().trim().equals(""))
									{
									actionHandlers.put(mimeDialog.fieldMimetype.getText(),mimeDialog.fieldAction.getText().trim());
									}
								else
									{
									if(actionHandlers.containsKey(mimeDialog.fieldMimetype.getText()))
										{
										actionHandlers.remove(mimeDialog.fieldMimetype.getText());
										}
									}
								//update da labels
								labelMimeType.setText((String)vector.elementAt(list.getSelectedIndex()));
								labelMimeExtensions.setText((String)mimeTypes.get((String)vector.elementAt(list.getSelectedIndex())));
								labelMimeAction.setText((String)actionHandlers.get((String)vector.elementAt(list.getSelectedIndex())));
								}
							else
								{
								//check if there is an mimetype and an extension, and if not, refuse to update
								if(!mimeDialog.fieldExtensions.getText().trim().equals("") && !mimeDialog.fieldMimetype.getText().trim().equals(""))
									{
									//check if the mimetype allready exists, and if so, notify luser and abort
									if(vector.contains(mimeDialog.fieldMimetype.getText()))
										{
										JOptionPane.showMessageDialog(null,"Ge zijt weeral zelf ne vetzak.","Debug Message",JOptionPane.INFORMATION_MESSAGE);
										}
									else
										{
										//remove old mimetype
										mimeTypes.remove((String)vector.elementAt(list.getSelectedIndex()));
										actionHandlers.remove((String)vector.elementAt(list.getSelectedIndex()));
										vector.remove((String)vector.elementAt(list.getSelectedIndex()));
										//insert da new one
										vector.add(mimeDialog.fieldMimetype.getText());
										Collections.sort(vector);
										list.setListData(vector);
										mimeTypes.put(mimeDialog.fieldMimetype.getText(),mimeDialog.fieldExtensions.getText());
										if(!mimeDialog.fieldAction.getText().trim().equals(""))
											{
											actionHandlers.put(mimeDialog.fieldMimetype.getText(),mimeDialog.fieldAction.getText().trim());
											}
										//update da labels
										list.setSelectedValue(mimeDialog.fieldMimetype.getText(),true);
										labelMimeType.setText((String)vector.elementAt(list.getSelectedIndex()));
										labelMimeExtensions.setText((String)mimeTypes.get((String)vector.elementAt(list.getSelectedIndex())));
										labelMimeAction.setText((String)actionHandlers.get((String)vector.elementAt(list.getSelectedIndex())));
										}
									}
								}
							}
						else
							{
							Misc.putSysMessage(0,"do nothing");
							}
						}
					else
						{
						int temp;
						temp = list.getSelectedIndex();
						Misc.putSysMessage(0,"Delete: " + (String)vector.elementAt(list.getSelectedIndex()));
						mimeTypes.remove((String)vector.elementAt(list.getSelectedIndex()));
						actionHandlers.remove((String)vector.elementAt(list.getSelectedIndex()));
						vector.remove((String)vector.elementAt(list.getSelectedIndex()));
						labelMimeType.setText("[mimetype]");
						labelMimeExtensions.setText("[extensions]");
						labelMimeAction.setText("[action]");
						list.setListData(vector);
						Misc.putSysMessage(0,"" + temp + " " + vector.size());
						if(temp+1>vector.size())
							temp--;
						list.setSelectedIndex(temp);
						}
					}
				}
			}
		}

	//listener for the mime-type-list
	//i don't understand how this works
	//but it seems to work, so i don't care
	//anyway
	//this updates the fields, so you can see the extensions and actionhandler for the selected mimetype
	class MimeTypeListListener extends MouseAdapter
		{
		JList list;
		Vector vector;
		//JLabel labelMimeType;
		//JLabel labelMimeExtensions;
		

		MimeTypeListListener(JList helpList, Vector helpVector)
			{
			list = helpList;
			vector = helpVector;
			}
		public void mouseClicked(MouseEvent e)
			{
			/*int index = list.locationToIndex(e.getPoint());
			Misc.putSysMessage(0,"" + index);*/
			if(list.getSelectedIndex()>=0)
				{
				labelMimeType.setText((String)vector.elementAt(list.getSelectedIndex()));
				labelMimeExtensions.setText((String)mimeTypes.get((String)vector.elementAt(list.getSelectedIndex())));
				labelMimeAction.setText((String)actionHandlers.get((String)vector.elementAt(list.getSelectedIndex())));
				/*Misc.putSysMessage(0,"Or: " + (String)vector.elementAt(list.getSelectedIndex()));
				Misc.putSysMessage(0,labelMimeType.getText());*/
				}
			}
		}
			


	//listener for the ok, cancel & apply button
	//the name of this class is wrong
	class CancelButtonListener implements ActionListener
		{
		CancelButtonListener()
			{
			//do.nothing();
			}
			
		public void actionPerformed(ActionEvent e)
			{
			//Misc.putSysMessage(0,"A button has been clicked");
			if(e.getSource() == buttonCancel)
				{
				Misc.putSysMessage(0,"Shutting down");
				dispose();
				System.exit(0);
				}
			else
				{
				if(e.getSource() == buttonOk)
					{
					/*
						we need to update the serversettings first...
					*/
	    			ConfProcessor.dumpConfigurationFile(mainConfigFile,serverSettings, actionHandlers, mimeTypes);				
					Misc.putSysMessage(0,"Shutting down");
					dispose();
					System.exit(0);
					}
				else
					{
					JOptionPane.showMessageDialog(null,"Not implemented yet !","Danger, Will Robinson !",JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}
		}
	}
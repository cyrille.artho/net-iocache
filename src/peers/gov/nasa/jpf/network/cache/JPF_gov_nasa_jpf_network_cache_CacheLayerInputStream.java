//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import java.io.IOException;

import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.NativePeer;

public class JPF_gov_nasa_jpf_network_cache_CacheLayerInputStream extends NativePeer{

	@MJI
	public int native_read____I(MJIEnv env, int robj) throws IOException {
		byte[] buff = new byte[1];
		int id = env.getIntField(robj, "id");
		int num = CacheLayer.getInstance().read(id, buff);
		
		return num > 0 ? buff[0] : -1;
	}

	@MJI
	public int native_read___3BII__I(MJIEnv env, int robj, int b_ref, int off, int len)
			throws IOException {
		byte[] buff = new byte[len];
		int id = env.getIntField(robj, "id");
		int ret = CacheLayer.getInstance().read(id, buff);

		for (int i = 0; i < ret; i++)
			env.setByteArrayElement(b_ref, off + i, buff[i]);
		
		return ret;
	}

	@MJI
	public boolean isNextReadBlocked____Z(MJIEnv env, int robj) {
		int id = env.getIntField(robj, "id");
		return CacheLayer.getInstance().isNextReadBlocked(id);
	}

	@MJI
	public int native_available____I(MJIEnv env, int robj) {
		int id = env.getIntField(robj, "id");
		if (CacheLayer.getInstance().isNextReadBlocked(id)) {
			return 0;
		} else {
			return 1;
		}
	}
	
	/*
	 * waitForReadReady__I__Z
	 *   peer method of gov.nasa.jpf.network.cache.CacheLayerInputStream.waitForReadReady(int timeOutMillis)
	 *   Waiting until at least one byte is received from the network peer or timeOutMillis is reached
	 *   @param timeOutMillis: timeOut limit in milliseconds: if zero then wait until message is received 
	 *                         from the peer or an IOException has occurred 
	 *   @return true  if a message has received (and can be read from the cache subsequently) 
	 *           false if no message has been received within timeOutMillis or an IOException has occurred
	 */
//	public static boolean waitForReadReady__I__Z(MJIEnv env, int robj, int timeOutMillis) {
//		CacheLayer cl=CacheLayer.getInstance();
//		int id = env.getIntField(robj, "id");
//		Long startTime=System.currentTimeMillis();
//		while (cl.isNextReadBlocked(id) && (timeOutMillis==0 || System.currentTimeMillis()-startTime < timeOutMillis)) {
//			try {
//				cl.pollPeer(id);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		return !cl.isNextReadBlocked(id);
//	}
}

#!/bin/bash
# Do not run this file directly. This script is supposed to be called by Apache Ant.

SCRIPT_DIR=`dirname "$0"`
CBUILD_DIR="${SCRIPT_DIR}/c-build"
JVM_DIR="/usr/lib/jvm"
JNI_H="include/jni.h"

if [ -z "${JAVA_HOME}" ]
then
	echo "*** JAVA_HOME is not set."

	# Guess by "locate".
	JAVA_HOME="`locate jni.h | head -1 | sed -e 's,/[^/]*/[^/]*$,,'`"

	if [ -z "${JAVA_HOME}" ]
	then
		# Look at possible locations.
		for F in `ls -d ${JVM_DIR}/java-*`
		do
			ls ${F}/${JNI_H}
			if [ "$?" -eq 0 ]
			then
				JAVA_HOME="${F}"
				break
			fi
		done

		if [ -z "${JAVA_HOME}" ]
		then
			# Final method. Find by "which".
			JAVA_HOME="`which java | sed -e 's,[^/]*/[^/]*$,,'`"

			ls ${JAVA_HOME}/${JNI_H}
			if [ "$?" -ne 0 ]
			then
				echo "*** Java is not detected on this system."
				exit 1
			fi
		fi
	fi
fi
echo "*** Guessed Java home: ${JAVA_HOME}"

# Call Makefile in directory 'c-build'. Do not use the default 'CFLAGS', which contains option 'O2'.
# This option causes buffer overflow during verification.
make JAVA_HOME=${JAVA_HOME} CFLAGS= -C ${CBUILD_DIR}

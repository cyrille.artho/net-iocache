#!/bin/sh

# assume that current directory is "test"
OUTS=`find . -name 'test-*.sh'`

for SCRIPT in $OUTS
do
    if [ "$SCRIPT" != "./test-chat-client.sh" ]
    then
	${SCRIPT}
    fi
done


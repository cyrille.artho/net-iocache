#!/bin/bash

source "$(cd "$(dirname "$0")" && pwd)/utils-for-testing.source" || exit

PACKAGE="se.rupy.http"
ClientPACKAGE="gov.nasa.jpf.network.http"
REQ_FILES=("www/abc.txt" "www/lorem-ipsum-1.txt" "www/lorem-ipsum-2.txt" "lorem-ipsum-3.txt" "lorem-ipsum-4.txt")
set-specific-help \
"rupy http application server

This script runs/tests the rupy application server.
http://code.google.com/p/rupy/

Specific options for choosing the client and server programs are:

    [rupy-dir=path]  -- path to the main directory of the rupy server to test
                        relative to the net-iocache top directory
                        default: src/examples/se/rupy/http (rupy 0.4.4)
"

set-parameters "$@"
checkParameter rupy-dir rupyDir "src/examples/se/rupy/http"
jpfFile="${rupyDir}/Daemon.jpf";
set-server java ${PACKAGE}.Daemon "-port 8080 -verbose -pass secret -timeout 3 -threads 1"

if checkParameter server-sut
then
	SUT="server-sut"
	if [ -f "$jpfFile" ]; then
		DOTJPF=`cat "$jpfFile"`
	fi;
	#set default jpf parameters for rupy (may be overridden in $jpfFile)
    DOTJPF="
jpf-net-iocache.lazy.connect = false
jpf-net-iocache.main.termination=true
jpf-net-iocache.peerResponse.maxDelayMillis = 60
$DOTJPF
"
	set-client java ${ClientPACKAGE}.HTTPClient "-port \$1 \$2" "${REQ_FILES[@]}"
elif checkParameter native
then
	SUT="native"
	set-client java ${ClientPACKAGE}.HTTPClient -port 8000 "${REQ_FILES[@]}"
elif checkParameter client-sut
then
   	reportfail "client-sut case not implemented, yet. Please use option server-sut or native" 
fi

TESTNAME="rupy"

# the servers run relative to the test dir, so must copy the files inside there
make-test-output-directories "$TESTNAME-$SUT"
mkdir "$TESTRUN_DIR/app"
mkdir "$TESTRUN_DIR/app/content"
cp -r "$SCRIPT_DIR/www" "$TESTRUN_DIR/app/content" || reportfail "could not copy www server files to test dir"

dotest

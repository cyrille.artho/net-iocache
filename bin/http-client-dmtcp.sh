#!/bin/bash
# Verify the HTTP client SUT against thttpd in the checkpointing mode.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

if [ $# -lt 1 ]
then
	echo "Usage : $0 [-m] [-i] [-n] [-a] [-d] <#threads>+"
	echo "-m Move the log file from home to the log directory."
	echo "-i Run with the io strategy."
	echo "-n Run with the nd strategy."
	echo "-a Run with the always strategy."
	echo "-d Use non-deterministic client (SUT)."
	exit
fi

chk_vdso_exit

# Set the location of checkpoints
export CHKPNT_DIR="${PROJ_TOP}/tmp-http-client"
export SUT_PORT="8791"

JPF_ARG="+${OPT_CHECKPOINT_DIR}=${CHKPNT_DIR} +${OPT_SUT_PORT}=${SUT_PORT}"
PACKAGE="gov.nasa.jpf.network.http"
OUT_FILENAME=`output_file $0`
LOG="${HOME}/${OUT_FILENAME}"
LOG_DIR="log"
PORT="8080"

declare -a REQ_FILES=("www/abc.txt" "www/lorem-ipsum-1.txt" "www/lorem-ipsum-2.txt" "lorem-ipsum-3.txt" "lorem-ipsum-4.txt")

# Check options given by user.
MOVE_LOG=0
ND=0
IO_STRATEGY=0
ND_STRATEGY=0
ALWAYS_STRATEGY=0
while [[ $1 == -* ]]
do
	if [ $1 = "-m" ]
	then
		MOVE_LOG=1
	elif [ $1 = "-d" ]
	then
		ND=1
	elif [ $1 = "-i" ]
	then
		IO_STRATEGY=1
	elif [ $1 = "-n" ]
	then
		ND_STRATEGY=1
	elif [ $1 = "-a" ]
	then
		ALWAYS_STRATEGY=1
	fi

	shift
done

# $1: #threads
# $2: Subfix of the log file name.
move_log() {
	# Add parameters to the log file name.
	PARAM=`echo ${OUT_FILENAME} | sed -e "s/\.log/-$1-$2\.log/"`
	mv ${LOG} ${LOG_DIR}/${PARAM}
}

# $1: #threads
# $2: Strategy (0 = io, 1 = nd, 2 = always)
run() {
	# Recreate the checkpoint directory.
	rm -rf ${CHKPNT_DIR}
	mkdir ${CHKPNT_DIR}
	# Remove any checkpoint file left on this directory.
	rm -f ${SCRIPT_DIR}/*.dmtcp

	# Must run the coordinator with option "background", otherwise the peer cannot restart.
	${DMTCP_COORDINATOR} --ckptdir ${CHKPNT_DIR} --background
	# Wait a little to make sure that the coordinator is on.
	sleep 1

	# If the client is non-deterministic, specify request files equal to the number of threads.
	if [ "${ND}" -eq "1" ]
	then
		PROG_ARG="-n ${REQ_FILES[@]:0:$1}"
	else
		PROG_ARG=${REQ_FILES[0]}
	fi

	# Check the third argument for the strategy to be used.
	if [ $2 -eq "0" ]
	then
		JPF_FILE="${EXAMPLE_DIR}/gov/nasa/jpf/network/http/HTTPClient-dmtcp.jpf"
		SUBFIX="io"
	elif [ $2 -eq "1" ]
	then
		JPF_FILE="${EXAMPLE_DIR}/gov/nasa/jpf/network/http/HTTPClient-dmtcp-nd.jpf"
		SUBFIX="nd"
	elif [ $2 -eq "2" ]
	then
		JPF_FILE="${EXAMPLE_DIR}/gov/nasa/jpf/network/http/HTTPClient-dmtcp-always.jpf"
		SUBFIX="always"
	fi

	# Start JPF. It will wait until Proxy sets up a Unix domain channel.
	${RUN_JPF_JNILIB} ${JPF_FILE} ${JPF_ARG} ${PROG_ARG} $1 &> ${LOG} &
	JPF_PID=$!
	sleep 2

	# Start the server running on DMTCP.
	# thttpd uses the MTCP default signal number for some purpose. We must change it otherwise DMTCP will not be able to stop threads.
	${DMTCP_CHECKPOINT} env MTCP_SIGCKPT=${MTCP_SIGNAL_NUMBER} thttpd -p ${PORT} -l ${HOME}/tmp.log -d ${SCRIPT_DIR} -D &
	# Save the peer PID.
	echo $! > ${CHKPNT_DIR}/${PEER_PID_FILE}

	start_proxy

	# Wait until JPF terminates.
	wait ${JPF_PID}

	if [ "${MOVE_LOG}" -eq "1" ]
	then
		move_log $1 ${SUBFIX}
	fi
}

# Run until no case remains
while [ $# -gt 0 ]
do
	if [ "${IO_STRATEGY}" -eq "1" ]
	then
		run $1 0
	fi

	if [ "${ND_STRATEGY}" -eq "1" ]
	then
		run $1 1
	fi

	if [ "${ALWAYS_STRATEGY}" -eq "1" ]
	then
		run $1 2
	fi

	shift
done

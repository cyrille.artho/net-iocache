#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char **argv) {
	if (argc < 3) {
		printf("Usage: %s (#processes) (URL)\n", argv[0]);
		exit(1);
	}

	int num_proc = atol(argv[1]);
	int i;
	char *url = argv[2];

	for (i = 0; i < num_proc; i++) {
		// Create a curl process
		int pid = fork();

		// Child process
		if (!pid) {
			execl("/usr/bin/curl", "curl", url, NULL);
		}
	}

	return 0;
}

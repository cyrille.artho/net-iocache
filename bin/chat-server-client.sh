#!/bin/bash

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

PACKAGE="gov.nasa.jpf.network.chat"
NUM_THREADS=1

if [ $# -gt 0 ]
then
	NUM_THREADS=$1
fi

SERVER_LOG="${HOME}/server.log"

java -classpath ${CLASSPATH} ${PACKAGE}.ChatServer ${NUM_THREADS} ${NUM_THREADS} > $SERVER_LOG &
sleep 2
java -classpath ${CLASSPATH} ${PACKAGE}.ChatClientRunner ${NUM_THREADS} H

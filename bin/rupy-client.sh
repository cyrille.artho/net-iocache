#!/bin/bash
#running rupy-client ; called by rupy-server test cases (rupy-mc.sh) 

. `dirname "$0"`/env.sh

PACKAGE="gov.nasa.jpf.network.http"
declare -a REQ_FILES=("bin/www/abc.txt" "bin/www/alphabet-client.log" "bin/www/changes.txt" "bin/www/http-client.log" "bin/www/http-server.log")

Port=
NumThreads=1
if [ $# != 0 ]; then
	Port="_p $1 $2"
fi
if [ $# == 3 ]; then
	NumThreads=$3
fi

java -classpath ${CLASSPATH} ${PACKAGE}.HTTPClient $Port ${REQ_FILES[@]} $NumThreads 

#PID=$!

#trap "kill -9 ${PID}; exit" SIGTERM
#wait ${PID}

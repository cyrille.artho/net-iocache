#!/bin/bash

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"


if [ $# -eq 0 ]
then
	echo "USAGE : jget-mc.sh {number of connection}"
	exit
fi

PACKAGE="gov.nasa.jpf.network.jget"
PROG_ARG="${PACKAGE}.jget -conn $1 http://localhost:8090/www/abc.txt ${HOME}"
CLIENT_LOG="${HOME}/jget.log"
PORT="8090"

ulimit -t 3600

thttpd -p ${PORT} -l ${HOME}/tmp.log -d ${SCRIPT_DIR} -D &
THTTPD_PID=$!
echo "thttpd process ID: ${THTTPD_PID}"

echo "start JPF verifying jget with $1 connection(s) ..."
${RUN_EXAMPLE_CMD} ${JPF_ARG} ${PROG_ARG} > ${CLIENT_LOG}

# Kill the thttpd process.
echo "Kill the thttpd process."
kill ${THTTPD_PID}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <time.h>


#define MYPORT 	"1024"  // the port users will be connecting to
#define BACKLOG 	10     // how many pending connections queue will hold


typedef struct sockaddr_storage sockaddr_storage;
typedef struct addrinfo addrinfo;


int num_clients;
int num_msg;

// modified daytime server:
// (1) returns date string in standard format as by date(1)
// (2) simulates possible leap seconds with a certain probability
// (currently 0.5, can be adjusted by changing the divisor).

int main(int argc, char **argv) {
    sockaddr_storage their_addr;
    socklen_t addr_size;

    addrinfo hints;
    addrinfo *servinfo;
    addrinfo *p;

    int sockfd, new_fd;
    int yes = 1;
    int i;
    int rv = 0;

    const char *fixed_date = "Mon Aug  1 16:45:45 JST 2011\n";
    const char *fixed_date2 = "Mon Aug  1 16:45:60 JST 2011\n";

	if (argc < 2) {
	  perror("Usage: daytime-server (#clients)");
	  return 1;
	}

    srand(time(NULL));

    num_clients = (int) strtol(argv[1], NULL, 0);

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    getaddrinfo(NULL, MYPORT, &hints, &servinfo);

    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
	  if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
		perror("server: socket");
		continue;
	  }

	  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		perror("setsockopt");
		exit(1);
	  }

	  if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
		close(sockfd);
		perror("server: bind");
		continue;
	  }

	  break;
    }

    if (p == NULL) {
        fprintf(stderr, "server: failed to bind\n");
        return 2;
    }

    freeaddrinfo(servinfo); // all done with this structure

    listen(sockfd, BACKLOG);

    // now accept an incoming connection:
    addr_size = sizeof their_addr;

	for (i = 0;i < num_clients;i++) {
		printf("[daytime-server] waiting ...\n");
		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);

		// ready to communicate on socket descriptor new_fd!
		printf("accepted socket %d\n", new_fd);

		if (rand() > RAND_MAX / 4) {
			rv = send(new_fd, fixed_date, strlen(fixed_date), 0);
		} else {
			rv = send(new_fd, fixed_date2, strlen(fixed_date2), 0);
		}
		printf("[daytime-server] Send %d bytes\n", strlen(fixed_date));
		if (rv < 0)
			perror("[daytime-server] Sending fails");

		close(new_fd);
	}

	close(sockfd);
	
	printf("daytime-server ends\n");
	return 0;
}

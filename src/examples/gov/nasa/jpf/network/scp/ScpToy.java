package gov.nasa.jpf.network.scp;

import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.network.HelperMethods;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.StringTokenizer;

public class ScpToy {

	static class ScpWorker extends Thread {
		
		private static final int exponent = 5;
		
		Socket socket;
		
		InputStream in;
		
		PrintWriter out;
		
		int fileSize;
		
		int key;
		
		public ScpWorker(Socket sock, int size) throws IOException {
			socket = sock;
			in = sock.getInputStream();
			out = new PrintWriter(sock.getOutputStream());
			fileSize = size;
		}
		
		public void run() {
			byte[] buf = new byte[4];
			
			// Key exchange
			try {
				in.read(buf);
				int g = HelperMethods.byteArrayToInt(buf);
				in.read(buf);
				int n = HelperMethods.byteArrayToInt(buf);
				int a = (int) Math.pow(g, exponent) % n;
				
				out.print(a);
				out.flush();
				
				in.read(buf);
				int b = HelperMethods.byteArrayToInt(buf);
				System.out.printf("[ScpWorker] b: %d\n", b);
				
				key = (int) Math.pow(b, exponent) % n;
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			int count = 0;
			int msgLen = ScpToy.MSG.length();
			while (count < fileSize) {
				boolean paddingType = Verify.getBoolean();
				
				if (count + msgLen < fileSize) {
					out.print(padding(ScpToy.MSG, 32, paddingType));
					out.flush();
					count += msgLen;
				}
				else {
					out.print(padding(ScpToy.MSG.substring(0, fileSize - count), 32, paddingType));
					out.flush();
					count = fileSize;
				}
			}
			
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		static String padding(String base, int len, boolean type) {
			StringBuffer sb = new StringBuffer(base);
			
			while (sb.length() < len) {
				sb.append(type ? "?" : "0");
			}
			
			return sb.toString();
		}
	}
	
	private final static int SERVER_PORT = 19000;
	
	final static String MSG = "0123456789:;<=>?@ABCDEFFGHI";
	
	private static byte[] ip(String s) {
		StringTokenizer st = new StringTokenizer(s, ".");
		byte[] b = new byte[4];

		for (int i = 0; i < 4; i++) {
			b[i] = (byte) Integer.parseInt(st.nextToken());
		}

		return b;
	}
	
	// Usage: ScpToy (#threads) (message size) [IP address] [Port]
	public static void main(String[] args) throws IOException {
		InetAddress addr;
		Socket s[];
		Thread[] p;
		int num_threads, msgSize;
		int msg_start = 0;
		int port = SERVER_PORT;

		// If a port number is given.
		if (args.length > 3) {
			port = Integer.parseInt(args[3]);
		}
		
		if (args.length > 2)
			addr = InetAddress.getByAddress(ip(args[2]));
		else
			addr = InetAddress.getLocalHost();

		num_threads = Integer.parseInt(args[0]);
		msgSize = Integer.parseInt(args[1]);
		p = new Thread[num_threads];
		s = new Socket[num_threads];

		for (int i = 0; i < num_threads; i++) {
			s[i] = new Socket(addr, port);
			p[i] = new ScpWorker(s[i], msgSize);
		}

		for (int i = 0; i < num_threads; i++) {
			p[i].start();
		}
	}
}

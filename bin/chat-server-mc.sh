#!/bin/bash
# Verify the chat server.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

NUM_ACCEPT=2
NUM_WORKERS=1
if [ $# -gt 1 ]
then
	NUM_ACCEPT=$1
	NUM_WORKERS=$2
else
	echo "usage: chat-server-mc.sh {num_accept} {num_workers}"
	exit
fi

PACKAGE="gov.nasa.jpf.network.chat"
PROG_ARG="${PACKAGE}.ChatServer ${NUM_ACCEPT} ${NUM_WORKERS}"
JPF_ARG="+${OPTION_PREFIX}.main.termination=false +${OPTION_PREFIX}.boot.peer.command=${SCRIPT_DIR}/chat-client.sh"
SERVER_LOG="$HOME/chat-server.log"

echo "Start model-checking a chat server, with ${NUM_WORKERS} worker(s), waiting for ${NUM_ACCEPT} client(s) ..."
${RUN_EXAMPLE_CMD} ${JPF_ARG} ${PROG_ARG} > ${SERVER_LOG}


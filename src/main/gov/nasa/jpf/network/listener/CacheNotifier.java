//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.listener;

import java.nio.channels.SelectableChannel;

import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.network.SystemInfo;
import static gov.nasa.jpf.network.SystemInfo.jpfConfig;
import gov.nasa.jpf.network.cache.CacheLayer;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;

public class CacheNotifier extends ListenerAdapter {
	
	@Override
	public void stateAdvanced(Search search) {
		int id = search.getStateId();

		CacheLayer.getInstance().changeState(id);
	}

	@Override
	public void stateBacktracked(Search search) {
		int id = search.getStateId();

		CacheLayer.getInstance().changeState(id);
		CacheLayer.getInstance().backtrack(id);
	}

	@Override
	public void stateRestored(Search search) {
		stateBacktracked(search);
	}
	
	@Override
	public void searchFinished(Search search) {
		CacheLayer.getInstance().closePhysicalConnections();
	}
	
	@Override
	public void searchStarted(Search search) {
		if (jpfConfig == null)
			jpfConfig = search.getConfig();

		// retrieve net-iocache specific properties that are shared among multiple classes/peers
		SystemInfo.retrieveProperties(jpfConfig);
	}
	
	@Override
	public void threadTerminated(VM vm, ThreadInfo terminatedThread) {
		int idx = terminatedThread.getId();
		
		if (idx == 0) {
			CacheLayer.getInstance().notifyMainTerminated();
		}
	}
	
}

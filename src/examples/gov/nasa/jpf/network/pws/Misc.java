package gov.nasa.jpf.network.pws;
/*  Misc.java - functions that don't belong anywhere else :-)

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.io.*;
import java.util.Date;
import java.text.SimpleDateFormat;

public abstract class Misc
{
	public static void putSysMessage(int type, String message)
	{
		String typeStr;
		switch (type)
		{
			case 0: typeStr = "MESG"; break;
			case 1: typeStr = "WARNING"; break;
			case 2: typeStr = "<-->"; break;
			case 3: typeStr = "THREAD"; break;
			case 4: typeStr = "HTTP MESSAGE"; break;
			case 5: typeStr = "FILE"; break;
			case 6: typeStr = "CONF"; break;
			case 7: typeStr = "HELP"; break;
			case 8: typeStr = "ERROR"; break;
			default: typeStr = "-----"; break;
		}

		SimpleDateFormat temp = new SimpleDateFormat("HH:mm:ss");
		String text = "[" + temp.format(new Date()) + "] " + typeStr + " -- " + message;

		if (type == 8)
			System.err.println(text);

		if (pws.useLogFiles() == true)
		{
			try
			{
				switch (type)
				{
					case 1: case 8:
					{
						if (pws.errorLog != null)
						{   
						    pws.errorLog.write(text);
						    pws.errorLog.newLine();
						    pws.errorLog.flush();
						}
						break;
					}
					case 5:
					{
					    if (pws.accessLog != null)
					    {
					        pws.accessLog.write(text); 
    						pws.accessLog.newLine();
	    					pws.accessLog.flush();
	    				}
						break;
					}
				}
			}
			catch (IOException e)
			{}
		}
	}
	
	public static String getContentType(String filename)
	/*	extracts and returns the mimetype of files */
	{
		String extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase(); /* get position of last part of file (aka extension) */
		String mimeType = pws.getMimeType(extension);
		putSysMessage(0, extension + " --> " + mimeType);
		return mimeType;
	}

    public static String getActionHandler(String fileName)
	/* extracts and returns the possible preprocessor, if any */
	{
	    String mime = getContentType(fileName);
	    String preProcessor = pws.getActionHandler(mime);
	    return preProcessor;
	}
}


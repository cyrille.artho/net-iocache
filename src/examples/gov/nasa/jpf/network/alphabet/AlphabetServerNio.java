package gov.nasa.jpf.network.alphabet;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.Iterator;

public class AlphabetServerNio {
	public static final int SERVER_PORT = 18586;

	public static void main(String[] args) throws IOException {
		ServerSocketChannel serverChannel;
		Selector selector;

		int maxConnection;
		int currentConnectionCount = 0;

		try {
			maxConnection = Integer.parseInt(args[0]);
		} catch (Exception e) {
			maxConnection = 10;
		}

		serverChannel = ServerSocketChannel.open();
		serverChannel.configureBlocking(false);
		selector = SelectorProvider.provider().openSelector();
		serverChannel.socket().bind(new InetSocketAddress(SERVER_PORT));
		serverChannel.register(selector, SelectionKey.OP_ACCEPT);

		System.out.println("server ready");

		while (true) { //TODO condition darret
			selector.select(100);
			System.out.println("[SERVER] selection done: "+selector.selectedKeys().size()+" new events...");
			Iterator<SelectionKey> readyKeys = selector.selectedKeys().iterator();

			while (readyKeys.hasNext()) {
				SelectionKey key = readyKeys.next();
				readyKeys.remove();

				if (!key.isValid()) {
					key.channel().close();
					key.cancel();
					continue;
				}

				if (key.isAcceptable()) {
					SocketChannel newClient  = ((ServerSocketChannel) key.channel()).accept();
					newClient.configureBlocking(false);
					SelectionKey newKey = newClient.register(selector, SelectionKey.OP_READ);
					newKey.attach(new ConnectionBuffer());
					System.out.println(currentConnectionCount);
					currentConnectionCount ++;
					if (currentConnectionCount >= maxConnection) {
						System.out.println("stopped");
						key.interestOps(0);
					}
					System.out.println("[SERVER] accept a new connection");
				}

				if (key.isReadable()) {
					SocketChannel channel = (SocketChannel) key.channel();
					ConnectionBuffer buffer = (ConnectionBuffer) key.attachment();
					byte i;

					try {
						int bitRead = channel.read(buffer.in);
						if (bitRead==-1) {
							System.out.println("[SERVER] A client has left the server");
							channel.close();
							key.cancel();
						} else {
							buffer.in.flip(); // we prepare to read the buffer
							while (buffer.in.hasRemaining()) {
								i = buffer.in.get();
								System.out.println("[SERVER] read: "+i);
								buffer.out.put((byte) (i -  '0' + 'a'));
								if (!buffer.out.hasRemaining()) {
									System.out.println("[SERVER] The out buffer of a connection is full");
									break; // the out buffer is full, we can't read anymore from in buffer
								}
							}
							key.interestOps(SelectionKey.OP_WRITE);
							buffer.in.compact(); // we store the remaining unread data, if any...
						}
					} catch (IOException e) {
						System.out.println("[SERVER] Error while trying to read... Closing this channel");
						channel.close();
						key.cancel();
					}
				}
				else if (key.isWritable()) {
					ConnectionBuffer buffer = (ConnectionBuffer) key.attachment();
					buffer.out.flip();
					System.out.println("[SERVER] Trying to answer...");
					((SocketChannel) key.channel()).write(buffer.out);
					if (buffer.out.hasRemaining()) {
						// We still have some data to write so we register both event
						key.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
					} else {
						// We no longer have any data to write, so we deregister op_write
						key.interestOps(SelectionKey.OP_READ);
					}
					buffer.out.compact(); // we store any remaining data, if any...
				}
			}
		}
	}
}

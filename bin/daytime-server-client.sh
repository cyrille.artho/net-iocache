#!/bin/sh
SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

PACKAGE="gov.nasa.jpf.network.daytime"
SERVER_LOG="${HOME}/daytime-server.log"

NUM_THREAD=2

if [ $# -gt 0 ]
then
	NUM_THREAD=$1
fi

echo "start a daytime server in the background"
java -classpath $CLASSPATH ${PACKAGE}.DaytimeServer 1024 $NUM_THREAD > $SERVER_LOG &
sleep 1
echo "start a daytime client"
java -classpath $CLASSPATH ${PACKAGE}.DaytimeClient $NUM_THREAD


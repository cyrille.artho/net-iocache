//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import gov.nasa.jpf.vm.Verify;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Do a part of the OutputStream class for target applications.
 * 
 * @author watcharin
 * 
 */
public class CacheLayerOutputStream extends OutputStream {

	private Socket sock;
	private int id;

	public CacheLayerOutputStream(Socket sock) {
		this.sock = sock;
		this.id=sock.socketID;
	}

	native boolean isNextReadBlocked();

	native int native_write(byte[] b, int off, int len);

	private void checkMainAliveness() {
		if (Socket.isMainTerminationSet())
			Verify.ignoreIf(Socket.isMainAlive());
	}

	public void write(int b) throws IOException {
		write(new byte[] { (byte) b }, 0, 1);
	}

	public void write(byte[] b) throws IOException {
		write(b, 0, b.length);
	}

	public void write(byte[] b, int off, int len) throws IOException {
		checkMainAliveness();

		if (Socket.isExceptionSimulationSet()) {
			boolean exception = Verify.getBoolean();

			if (exception)
				throw new IOException("[Simulated] an I/O error occurs");
		}

		if (native_write(b, off, len) > 0) {
			throw new IOException("IOException in CacheLayerOutputStream");
		}
	}

	public void close() throws IOException {
		sock.close();
	}
	
}

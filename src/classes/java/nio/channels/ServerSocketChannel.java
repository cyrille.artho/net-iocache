package java.nio.channels;

import gov.nasa.jpf.vm.Verify;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.SocketAddress;

public class ServerSocketChannel extends SelectableChannel {
	private boolean blocking = true;
	private boolean open = true;
	private boolean readyToAccept=false;   //used to simulate non-blocking mode in which
	                                    //accept() may return false as long as readyForAccept is false
	                                    //readyForAccept is set to true dependent on choice generator 
	                                    //see isReadyForAccept();
	private ServerSocket serverSocket;
	
	SelectionKey key;


	private ServerSocketChannel() throws IOException {
		serverSocket = new ServerSocket(this);
	}

	/**
	 * bind the server socket associated with this server socket channel
	 * to a local socket address
	 * @param local 
	 * address to bind the server socket channel to
	 * @return this channel
	 * @throws IOException
	 */
	public final ServerSocketChannel bind(SocketAddress local) throws IOException {
		checkOpen();
		serverSocket.bind(local);
		return this;
	}
	
	/**
	 * bind the server socket associated with this server socket channel
	 * to a local socket address
	 * @param local 
	 * address to bind the server socket channel to
	 * @param backlog (ignored)
	 * 		  original meaning: the maximum number of pending connections 
	 * @return this channel
	 * @throws IOException
	 */
	public ServerSocketChannel bind(SocketAddress local, int backlog) throws IOException {
		bind(local); //we ignore backlog!
		return this;
	}
	
	/**
	 * SocketChannel accept()
	 * 	wait for new connection requests from client if in blocking mode
	 *  @return SocketChannel or null if in non-blocking mode and no pending connection requests
	 *  @see java.net.ServerSocket.accept(boolean blocking)
	 */
	public SocketChannel accept() throws IOException {
		checkOpen();
		checkBound();
		
		if (isBlocking() || isReadyToAccept()) {  
			readyToAccept=false;                                    //reset channel to not ready for accept;
			return new SocketChannel(serverSocket.accept(), true);  //call blocking physical accept() which boots client peer
		} else 
			return null;   //simulate fail of accept() in non-blocking mode
	}

	/**
	 * boolean isReadyToAccept()
	 *   Called in non-blocking mode: decide at random when calls to accept() should
	 *   actually return the SocketChannel which as been created in the first call
	 *   of accept().
	 *   @return false if accept() should return null in non-blocking mode 
	 *           true if accept() should return a previously accepted socketChannel;
	 *   @see accept()
	 */
	boolean isReadyToAccept() {
		//FIXME discuss with Cyrille how to deal with the infinite case (channel will never become ready for accept)
		//      see also SocketChannel.isReadable(), .isWritable()
		//      possible solutions:
		//         - use JPF property search.depth_limit
	   //         - introduce new property ''unsuccessful_nonblockingIO_limit''
		//         - avoid possible infinite polling loops in SUTs (ensure there is a timeout count whenever using nonblocking mode accept()/read()/write() 
		//boolean choice=((System.nanoTime() & 1) != 0);
		
		//return true if the channel has been set to acceptable in a previous call of isReadyForAccept()
		//Note: acceptable is reset to false in accept() 
		if (readyToAccept)
			return true;

		//if physically accepted but not yet logically acceptable, explore both cases: acceptable and not acceptable 
		//(by default start exploration with acceptable case unless configured differently)
		readyToAccept=Verify.getBoolean(FAILURE_FIRST); 
		return readyToAccept; 
	}

	public static ServerSocketChannel open() throws IOException {
		return new ServerSocketChannel();
	}

	public ServerSocket socket() {
		return this.serverSocket;
	}

	public boolean isOpen() {
		return open; 
	}

	public boolean isBlocking() {
		return blocking;
	}

	@Override
	public SelectableChannel configureBlocking(boolean b) throws IOException {
		checkOpen();

		blocking = b;
		return this;
	}

	public SelectionKey register(Selector selector, int opAccept) {
		SelectionKey key = selector.registerKey(this);
		key.interestOps(opAccept);
		this.key = key;
		return key;

	}

	@Override
	public void close() throws IOException {
		if (isOpen()) {
			open = false;
			serverSocket.close();
			if (key != null) {
				key.cancel();
			}
		}
	}

	@Override
	public final int validOps() {
		return SelectionKey.OP_ACCEPT;
	}

	private void checkOpen() throws ClosedChannelException {
		if (!isOpen()) {
			throw new ClosedChannelException();
		}
	}

	private void checkBound() throws NotYetBoundException {
		if (!socket().isBound()) {
			throw new NotYetBoundException();
		}
	}
}

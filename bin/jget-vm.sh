#!/bin/bash
# Verify Jget against thttpd with the checkpointing approach.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"


if [ $# -eq 0 ]
then
	echo "USAGE : jget-mc.sh {number of connection}"
	exit
fi

chk_vdso

NUM_CLIENTS=$1

# Set the location of checkpoints
export CHKPNT_DIR="${PROJ_TOP}/tmp-jget"

JPF_ARG=`echo "${JPF_ARG} +${OPT_VIRTUAL_MODE}=true +${OPT_CHECKPOINT_DIR}=${CHKPNT_DIR} +${OPT_LAZY}=false +${OPT_MAIN_TERMINATION}=false" | sed -e 's/CacheNotifier/VirtualizationCacheNotifier/' -e 's/CacheLogger/VirtualizationCacheLogger/'`
PACKAGE="gov.nasa.jpf.network.jget"
PORT="8090"
PROG_ARG="${PACKAGE}.jget -conn ${NUM_CLIENTS} http://localhost:8090/www/abc.txt ${HOME}"
CLIENT_LOG="${HOME}/jget.log"

# Recreate the checkpoint directory.
rm -rf ${CHKPNT_DIR}
mkdir ${CHKPNT_DIR}

env LD_PRELOAD="${LIB_DIR}/libmtcp.so:${CBUILD_DIR}/libmtcpsetup.so:${CBUILD_DIR}/libncp.so" LD_LIBRARY_PATH="${CBUILD_DIR}" MTCP_SIGCKPT="${MTCP_SIGNAL_NUMBER}" thttpd -p ${PORT} -l ${HOME}/tmp.log -d ${SCRIPT_DIR} -D 2> /dev/null &
# Save the peer PID.
echo $! > ${CHKPNT_DIR}/${PEER_PID_FILE}

sleep 1
${RUN_EXAMPLE_CMD} ${JPF_ARG} ${PROG_ARG} > ${CLIENT_LOG}

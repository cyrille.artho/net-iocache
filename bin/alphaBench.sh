ARGS="v=1"

if [[ "$5" == "Simple" ]]; then
   ARGS="$ARGS version=Simple"
fi

for ((c=$1; c<=$2; c++)); do
	for ((r=$3; r<=$4; r++)); do
		echo "Executing alphabet.sh for $c connection(s) and $r request(s)..."
		bin/alphabet.sh client-sut threads=$c messages=$r $ARGS &> "log/V1-alpha-client$5-c${c}r${r}.txt"
	done
done

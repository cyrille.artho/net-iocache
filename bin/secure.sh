#!/bin/bash

source "$(cd "$(dirname "$0")" && pwd)/utils-for-testing.source" || exit

PACKAGE="gov.nasa.jpf.network.alphabet"

set-parameters "$@"

# Only the IO_STRATEGY (-i) for secure-client-nd-dmtcp.sh used by ./bin/testall.sh is implemented below

checkParameter dmtcp || reportfail "only the dmtcp option is implemented for secure.sh"

if checkParameter server-sut
then ##### CASE 3
    nThreads=2  # default as in bin/testall.sh
    nMessages=2  # default as in bin/testall.sh
    set-server java ${PACKAGE}.NDAlphabetServer "$nThreads"
    set-client "$IOCACHE_HOME/c-build/secure-client" "$nThreads" "$nMessages"
elif checkParameter client-sut || checkParameter native
then ##### CASE 4
    nThreads=2  # default as in bin/testall.sh
    nMessages=2  # default as in bin/testall.sh
    set-client java ${PACKAGE}.NDAlphabetClient "$nThreads" "$nMessages"
    set-server "$IOCACHE_HOME/c-build/secure-server" "$nThreads" "$nMessages"
fi

TESTNAME="secure${ndsuffix}"

dotest

#!/bin/bash
# Measure saving/loading time of KVM. The saving and loading operations are done by "virsh", a common API for controlling VMs.

I=0
LOG="time.log"
MAX_ROUND=10
DELAY=3
# Temporary checkpoint file
CP="shot1.cp"

if [ "$#" -lt "2" ]
then
	echo "Usage: $0 (Source checkpoint) (Source disk) [#Round]"
	exit
fi

SOURCE_CP=$1
SOURCE_DISK=$2
# Temporary disk image
TMP_DISK="${HOME}/kvm-disk/guest.qcow2"

if [ "$#" -ge "3" ]
then
	MAX_ROUND=$3
fi

rm -f ${LOG}

while [ "$I" -lt "${MAX_ROUND}" ]
do
	echo "ROUND $I"
	# Always use the original disk.
	cp -a ${SOURCE_DISK} ${TMP_DISK}
	echo "Start loading"
	/usr/bin/time -f "Load: %e" virsh restore ${SOURCE_CP} 2>> ${LOG}
	sleep ${DELAY}

	rm -f ${CP}
	echo "Start saving"
	/usr/bin/time -f "Save: %e" virsh save ubuntu ${CP} 2>> ${LOG}
	
	I=`expr $I + 1`

	if [ "$I" -lt "${MAX_ROUND}" ]
	then
		sleep ${DELAY}
	fi
done

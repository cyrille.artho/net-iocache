#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>


typedef struct timespec timespec;
typedef struct timeval timeval;


// 2 seconds
const uint WAIT_TIME = 2;

pthread_mutex_t mutex;
pthread_cond_t cond_fin;
int fin = 0;


void *signal_run (void *id);


int main (int argc, char **argv) {
	pthread_t fifo_thread;
	timespec ts;
	timeval tp;
	
	// check #arguments
	if (argc < 2) {
		perror("Usage: ckpt-sig <FIFO file>");
		exit(1);	
	}

	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&cond_fin, NULL);
	memset(&ts, 0, sizeof(timespec));

	// Convert from timeval to timespec
	gettimeofday(&tp, NULL);
    ts.tv_sec  = tp.tv_sec;
    ts.tv_nsec = tp.tv_usec * 1000;
    ts.tv_sec += WAIT_TIME;

	pthread_create(&fifo_thread, NULL, signal_run, (void *) argv[1]);

	pthread_mutex_lock(&mutex);
	if (!fin) {
		pthread_cond_timedwait(&cond_fin, &mutex, &ts);
	}
	pthread_mutex_unlock(&mutex);

	// destroy mutex and condition
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond_fin);

	printf("[ckpt-sig] return %d\n", fin ? 0 : 1);
	return fin ? 0 : 1;
}

void *signal_run (void *id) {
	int num, fd;
	char *fifo = (char *) id;
	char s[300];
	
    mknod(fifo, S_IFIFO | 0666, 0);

    printf("waiting for readers...\n");
    fd = open(fifo, O_WRONLY);
    printf("got a reader\n");

	memset(s, 0, sizeof s);
	s[0] = 1;

	printf("write\n");
	if ((num = write(fd, s, strlen(s))) == -1)
	  perror("write");
	else
	  printf("speak: wrote %d bytes\n", num);

	close(fd);

	pthread_mutex_lock(&mutex);
	fin = 1;
	pthread_cond_signal(&cond_fin);
	pthread_mutex_unlock(&mutex);
	
	pthread_exit(NULL);
}

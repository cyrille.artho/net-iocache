package gov.nasa.jpf.network.pws;
/*  Server.java - thread class that is started when a new connection is initiated from a client

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.io.*;
import java.net.*;

public class Server extends Thread
{
    private Socket sock;
    private InetAddress remoteIP;
    
    private InputHandler input;
    private OutputHandler output;

    Server(Socket socketname, InetAddress socket_remoteIP)
    {
        this.sock = socketname;
        this.remoteIP = socket_remoteIP;
        
        this.input = new InputHandler(this.sock);
        this.output = new OutputHandler(this.sock);
    }

    public void run()
    {   
    	Connection conn = null;

	    try
	    {
		    conn = new Connection(input, output, remoteIP);
		    do
		    {
    		    conn.processRequest();
	    	    conn.doMagicStuff();
	    	} 
	    	while (conn.connectionType().startsWith("keep-alive"));

	    }
	    catch (UnknownProtocolException e)
	    {
	    }
    	catch (NoDataReceivedException e)
	    {
	    }
	    catch (ObsoleteHTTPBrowserException e)
    	{
    	}
	    catch (RFCViolationException e)
    	{
    	}
    }
}

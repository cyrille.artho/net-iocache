#!/bin/bash
# Run the scp implementation of JSch against Dropbear running on MTCP.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

if [ $# -lt 1 ]
then
	echo "Usage : $0 {number of threads}"
	exit
fi

NUM_CONN=$1

chk_vdso

# Set the location of checkpoints
export CHKPNT_DIR="${PROJ_TOP}/tmp-jsch"
# Set the SUT port number. This value must be exported so that DMTCP can read.
export SUT_PORT="8791"

JPF_ARG=`echo "${JPF_ARG} +${OPT_VIRTUAL_MODE}=true +${OPT_CHECKPOINT_DIR}=${CHKPNT_DIR} +${OPT_LAZY}=false +${OPT_DMTCP_ENABLED}=true +${OPT_SUT_PORT}=${SUT_PORT}" | sed -e 's/CacheNotifier/VirtualizationCacheNotifier/' -e 's/CacheLogger/VirtualizationCacheLogger/'`

PACKAGE="gov.nasa.jpf.network.scp"
PROG_ARG="${PACKAGE}.ScpTo ${HOME}/dummy watcharin@localhost:tmp.txt ${NUM_CONN}"
CLIENT_LOG="${HOME}/scp.log"

# Recreate the checkpoint directory.
rm -rf ${CHKPNT_DIR}
mkdir ${CHKPNT_DIR}
# Remove any checkpoint file left on this directory.
rm -f ${SCRIPT_DIR}/*.dmtcp

# Must run the coordinator with option "background", otherwise the peer cannot restart.
${DMTCP_COORDINATOR} --ckptdir ${CHKPNT_DIR} --background

# Start JPF. It will wait until Proxy sets up a Unix domain channel.
${RUN_JPF_JNILIB} ${JPF_ARG} ${PROG_ARG} > ${CLIENT_LOG} &
JPF_PID=$!
sleep 2

# Start a server running on DMTCP.
${DMTCP_CHECKPOINT} ${CBUILD_DIR}/scp-server ${NUM_CONN} &
# Save the peer PID.
echo $! > ${CHKPNT_DIR}/${PEER_PID_FILE}
# Start Proxy. Must run in background to avoid the problem about "stdout".
${DMTCP_CHECKPOINT} ${CBUILD_DIR}/proxy &

# Wait until JPF terminates.
wait ${JPF_PID}

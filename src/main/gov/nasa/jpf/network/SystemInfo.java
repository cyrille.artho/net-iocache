package gov.nasa.jpf.network;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.JPFConfigException;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

//FIXME complete re-factoring of jpf-net-iocache configuration property management 
public class SystemInfo {

	private static Logger log = JPF.getLogger("gov.nasa.jpf.network.SystemInfo");
	public static Config jpfConfig;
	
	public static final String PROJ_NAME = "jpf-net-iocache";

// FIXME: Options from jpfConfig should be retrieved only once;
// String constants for option names are not necessary then!
// (except for PROJ_NAME which should be kept)
	/////////////////////////////////////////////////////////////////////
	//configuring the priority of choices when exploring non-blocking 
	//accept/connect/read/write in nio
	/**
	 * configuring the ORDER of choices when exploring the state space of
	 * nonblocking accept/read/write in nio nioFailureFirst=false: explore the
	 * successful branch first, i.e., ServerSocketChannel.accept() returns a
	 * valid SocketChannel; SocketChannel.read() reads more than 0 bytes from a
	 * channel; SocketChannel.write() writes more than 0 bytes to a channel.
	 * nioFailureFirst=true: explore the unsuccessful branch first, i.e,
	 * ServerSocketChannel.accept() returns null (no pending connection
	 * request); SocketChannel.read() returns 0 bytes read (nothing is read);
	 * SocketChannel.write() returns 0 bytes written (nothing is written). Note:
	 * - Always BOTH the successful and unsuccessful branch are explored for
	 * each nonblocking accept/read/write, regardless of the value of
	 * nioFailureFirst. - In nonblocking mode, each call to accept(), read(), or
	 * write() generates an INDEPENDENT choice point, i.e., the state space may
	 * grow exponentially in the number of accept/read/write method calls, as it
	 * is the case in the following code:
	 * someServerSocketChannel.configureBlocking(false); SocketChannel ch=null;
	 * int i=0; while(ch==null) {ch = someServerSocketChannel.accept(); i++;} -
	 * In blocking mode, accept(), read(), and write() are assumed to be always
	 * successful, i.e., NO choice point is generated.
	 */
	public static boolean nioFailureFirst;

	private static final String BOOT_CMD = PROJ_NAME + ".boot.peer.command";
// FIXME: simplify code in VirtualizationCacheNotifier
	public static final String VIRTUAL_MODE = PROJ_NAME + ".virtual.mode";
// FIXME: simplify code in VirtualizationCacheNotifier
	public static final String DMTCP_ENABLED = PROJ_NAME + ".dmtcp.enabled";
// FIXME: simplify code in VirtualizationCacheNotifier
	public static final String SUT_PRIVATE_PORT = PROJ_NAME + ".sut_private_port";
// FIXME: simplify code in VirtualizationCacheNotifier
	public static final String PEER_PRIVATE_PORT = PROJ_NAME + ".peer_private_port";
// FIXME: simplify code in VirtualizationCacheNotifier
	public static final String CHECKPOINT_DIR = PROJ_NAME + ".checkpoint.dir";
// FIXME: simplify code in VirtualizationCacheNotifier
	public static final String STRATEGY = PROJ_NAME + ".strategy";

	/**
	 * parse and store net-iocache specific configuration properties
	 * @param jpfConfig: object to access jpf properties 
	 */
	public static void retrieveProperties(Config jpfConfig) {
		// FIXME: result should not be written back to property
		// but stored in a proper data structure (List<...>)
		// in SystemInfo.
		readBootCmd(BOOT_CMD);

		nioFailureFirst = jpfConfig.getBoolean(PROJ_NAME + "nio.failureFirst");
	}
	
	/* FIXME (PLEASE): No round-trip from list of string to joined
	   string and back to split string ;-( */
	////////////////////////////////////////////////////////////////////////////////
	// bootCmd: command for launching the remote client peer 
	private static String bootCmd = ""; 
	
	/**
	 * retrieve the command for booting the remote client peer
	 * @param property 
	 * 		  name of the property that configures the boot command
	 */
	private static void readBootCmd(String property) {
		if (jpfConfig.getString(property,"").trim().isEmpty())  //if no command configured...
			return;                                             //...we are done
		
		String[] cmdList=jpfConfig.getStringArray(property);    
		//normalize the command to a single string, using blank as a separator between 
		//the command and its arguments...
		StringBuffer cmd = new StringBuffer(cmdList[0]); //there should be at least one element in cmdList;
		for (int i = 1; i<cmdList.length; i++)           //append the arguments
			cmd.append(" ").append(cmdList[i]);
		
		if (!cmd.toString().equals(bootCmd)) {           //issue log message if not equal to the default value of bootCmd 
			bootCmd=cmd.toString();
		}
	}
	
	/**
	 * flatten list of strings to a single string. <br/>
	 * Used to convert boot command returned by {@link #getBootCmd(int, int)}
	 * into a single string for log output 
	 * @param list string list to convert to
	 * @return string concatenated from the elements of the lists using blank as a separator
	 * @see #getBootCmd(int, int)  
	 */
	public static String listToString(List<String> list) {
		if (list==null||list.isEmpty()) 
			return "";

		StringBuffer res=new StringBuffer();
		for(String s:list) 
			res.append(" ").append(s);
		return res.substring(1);
	}
	
	/**
	 * return the command including arguments for launching the remote client 
	 * peer as a list of strings. Abstract parameters %nativePort% and %logicalPort% 
	 * in the command string are substituted by the values in parameters <b>nativePort</b> and 
	 * <b>logicalPort</b>.  
	 * @param nativePort
	 * @param logicalPort
	 * @return command string list used to invoke the remote client peer
	 * @see #listToString(List)
	 */
	public static List<String> getBootCmd(int nativePort, int logicalPort) {
		assert bootCmd!=null : "Configuration parameter bootCmd must not be null.";
		List<String> res=new LinkedList<String>();
		
		if (bootCmd.isEmpty())   
			return res;          //there is nothing to do
		
		//instantiate abstract parameters in bootCmd with runtime values
		String cmd=bootCmd.replace("%nativePort%", ""+nativePort).replace("%logicalPort%", ""+logicalPort);
		String[] qtok=cmd.split("\"");  //Get " quoted tokens; strings at even indices (qtok[0], qtok[2], ...) are unquoted, at odd indices (qtok[1], qtok[3], ...) are quoted tokens 

		boolean quoted=false;
		for (String qt:qtok) {
			if (quoted) {             //if in quotes add token as it is
				if (qt.length()> 0)   //ignore empty tokens
					res.add(qt); 
			} else {                 //split tokens along whitespace
				for (String t:qt.split("\\s+")) {
					if (t.length()>0)  //ignore empty tokens 
						res.add(t);
				}
			}
			quoted=!quoted;
		}
		if (res.get(0).endsWith(".sh"))   //if command is a shell script then ...
			res.add(0,"bash");            //add explicit invocation of bash for compatibility with Windows/cygwin
		return res;
	}
}

#ifndef DMTCPSETUP_H_
#define DMTCPSETUP_H_

#define REPLAY_DATA_SIZE 2048
// The maximum number of owners of one connection.
#define MAX_OWNERS 4


typedef struct sockaddr_storage sockaddr_storage;
typedef struct timeval timeval;
typedef struct timespec timespec;
typedef struct sockaddr_un sockaddr_un;

typedef struct close_order {
	int fds[32];
	int size;
} close_order;

typedef struct Entry {
	int fd;
	int checked;
	uint16_t remote_port;
	// The ID of the process that will recover the connection of this entry.
	uint recovery_pid;
	// The IDs of the processes that will be waiting for the file descriptor sent by the recovering process.
	uint lazy_pids[MAX_OWNERS];
	uint lazy_pids_len;
	char replay_data[REPLAY_DATA_SIZE];
	ssize_t data_len;
} Entry;


// from sockettable.cpp
extern SocketInfo sock_table[32];
extern int sock_table_size;

const char *CHKPNT_DIR = "CHKPNT_DIR";
const char *PIPE_NAME = "ckpt_sig";
const char *SHARED_FILENAME = "recovery";
// The listening port used in "post_ckpt" and "post_restart".
const uint LISTENING_PORT = 8790;

const uint SHARED_FILE_HEADER_SIZE = 1;
const int FIFO_NAME_MAX_SIZE = 256;
const int UNIX_SOCKADDR_MAX_LEN = 256;
const int BACKLOG = 10;
const int RESERVED_LISTENING_SOCKFD = 30;


extern int _real_socket(int domain, int type, int protocol);
extern int _real_connect(int sockfd, const struct sockaddr *serv_addr, socklen_t addrlen);
extern int _real_bind(int sockfd, const struct sockaddr *my_addr, socklen_t addrlen);
extern int _real_listen(int sockfd, int backlog);
extern int _real_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
extern int _real_setsockopt(int s, int level, int optname, const void *optval, socklen_t optlen);
extern int _real_open2(const char *pathname, int flags);
extern int _real_close(int fd);
// File I/O functions
extern FILE *_real_fopen(const char *path, const char *mode);
extern int _real_fclose ( FILE *fp );

// Called before the main method.
void premain() __attribute__((constructor));
// Called before checkpointing.
void pre_ckpt();
// Called after checkpointing.
void post_ckpt();
// Called after successfully restarting the peer from a checkpoint.
void post_restart();

// Fill array "active" with non-zero FDs of sockets constituting connections.
void active_sock(int *active, size_t *size);

int socket_info_index(int fd);

// Setup a connection that has been closed by "pre_ckpt".
void setup_conn_by_accepting(int fd, const uint *lazy_pids, uint lazy_pids_len);
void setup_conn_by_connecting(int fd);
void setup_conn_by_waiting(int fd, uint recovery_pid);

// Notify the I/O cache that a certain operation has been completed.
void notify_cache();

//-- close_order operations --
close_order *close_order_new();
void close_order_free(close_order *order);
void close_order_add(close_order *order, int fd, int number);
int close_order_get(const close_order *order, int number);
int close_order_size(const close_order *order);
int close_order_clear(close_order *order);

// Read cookie from the given socket and return the attached number.
int close_number(int fd);

inline static int64_t timeval_to_ms(const timeval *tv) {
	return tv->tv_sec * 1000LL + tv->tv_usec / 1000;
}

inline static int64_t cur_time_ms() {
	timeval tv;

	gettimeofday(&tv, NULL);
	return timeval_to_ms(&tv);
}

#endif /* DMTCPSETUP_H_ */

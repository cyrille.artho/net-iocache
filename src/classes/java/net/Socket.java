//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package java.net;

import gov.nasa.jpf.vm.Verify;

import java.io.*;

import gov.nasa.jpf.network.cache.CacheLayerInputStream;
import gov.nasa.jpf.network.cache.CacheLayerOutputStream;

public class Socket {

	static int numSocket = 0;
	
	public int socketID;

	CacheLayerInputStream in=null;
	CacheLayerOutputStream out = null;
	
	private static boolean exception_simulation;

	private static boolean main_termination;

	static {
		Object lock = new Object();

		CacheLayerInputStream.setLock(lock);
	}

	public Socket() {
		socketID = numSocket++;
		native_Socket();
	}

	public Socket(String host, int port) {
		this();
		try {
			native_connect(InetAddress.getByName(host), port);
		} catch (UnknownHostException e) {
			System.err.println("[Socket : Socket(String, int)] " + e);
		}
	}

	public Socket(InetAddress addr, int port) throws IOException {
		this();
		native_connect(addr, port);
	}

	public Socket(InetAddress address, int port, InetAddress localAddr, int localPort) throws IOException {
		this(address, port);
	}

	public static boolean isExceptionSimulationSet() {
		return exception_simulation;
	}
	
	public static boolean isMainTerminationSet() {
		return main_termination;
	}
	
	private native void native_Socket();

	private native void native_connect(InetAddress addr, int port);

	public static native boolean isMainAlive();

	public native byte[] native_getInetAddress();

	public native int getPort();

	public native boolean isConnected();

	public native void close() throws IOException;

	public native boolean isClosed();

	public int getLocalPort() {
		return getPort(); // just return something for now
	}

	public InputStream getInputStream() throws IOException {
		if (in==null)
			in = new CacheLayerInputStream(this);
		return in;
	}

	public OutputStream getOutputStream() throws IOException {
		if (out==null)
			out = new CacheLayerOutputStream(this);
		return out;
	}

	/**
	 * FIXME proper simulation of blocking connect;
	 *       As for now, a thread calling connect() will never go into state BLOCKED.
	 *       Hence, connect() behaves like a non-blocking operation on the model level
	 *       (although the native peer might block for a certain time on the jpf level).  
		 * @param endpoint
	 * @throws IOException
	 */
	public void connect(SocketAddress endpoint) throws IOException {
		// -- properties dependent --
		if (exception_simulation) {
			boolean exception = Verify.getBoolean();

			if (exception)
				throw new IOException("[Simulated] an error occurs during the connection");
		}
		// -- properties dependent --

		if (endpoint instanceof InetSocketAddress) {
			InetSocketAddress inet = (InetSocketAddress) endpoint;

			native_connect(inet.getAddress(), inet.getPort());
		} else
			System.err.println("The specified socket address is not an instance of InetSocketAddress.");
	}

	
	public InetAddress getInetAddress() {
		try {
			return InetAddress.getByAddress(native_getInetAddress());
		} catch (UnknownHostException e) {
			System.err.println("[Socket : getInetAddress()] " + e);
			System.exit(1);
			return null;
		}
	}
	
	
	/**
	 * Returns the address of the endpoint this socket is connected to, 
	 * or null if it is unconnected. 
	 * @return a SocketAddress reprensenting the remote endpoint of this socket, 
	 *         or null if it is not connected yet.
	 */
	public SocketAddress getRemoteSocketAddress() {
		return new InetSocketAddress(getInetAddress(), getPort());
	}
	
	/**
	 * setSoTimeout(int timeout)
	 *   Dummy method to avoid compile error message in eclipse;
	 *   should never be executed!
	 */
	public void setSoTimeout(int timeout) throws SocketException {
		throw new UnsupportedOperationException("Net-iocache does not yet support method 'Socket.setSoTimeOut(int)'.");
	}

	/**
	 * getSoTimeout()
	 *   dummy method to avoid compile error message in eclipse;
	 *   should never be executed!
	 */
	public synchronized int getSoTimeout() throws SocketException {
		throw new UnsupportedOperationException("Net-iocache does not yet support method 'int Socket.getSoTimeOut()'.");
	 }
	
	public void setTcpNoDelay(boolean on) throws SocketException {
		// stub
	}

	public void setSoLinger(boolean on, int linger) throws SocketException {
		// stub
	}

	public boolean isChannelReadable() throws IOException {
		return in.isChannelReadable();
	}
	
	public String toString() {
		StringBuffer res=new StringBuffer("Socket[addr=");
		res.append(getInetAddress().getHostAddress());
		res.append(", port="+getPort());
		res.append(", localport="+getLocalPort());
		return res.append("]").toString();
	}
}

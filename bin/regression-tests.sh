#!/bin/bash

source "$(cd "$(dirname "$0")" && pwd)/utils-for-testing.source" || exit

cd "$IOCACHE_HOME" || reportfail

FMT="%-68s: "


ISTTY=0
# is file descriptor 1 a tty??
[ -t 1 ] && ISTTY=1

color-if-tty()
{
    case "$1" in
	green)  code=32
	    ;;
	red)    code=31
	    ;;
	yellow) code=33
	    ;;
	* | black) code=30
	    ;;
    esac
    if [ "$ISTTY" = "1" ]
    then
	echo -e "\033[${code}m${2}\033[0m"
    else
	echo -e "$2"
    fi
}

printok() {
	echo -e "[   $(color-if-tty green "OK")   ]"
}

printfailed() {
	echo -e "[ $(color-if-tty red "FAILED") ]"
} 

printdiff() {
	echo -e "[  $(color-if-tty yellow "DIFF")  ]"
} 

printna() {
	echo -e "[  $(color-if-tty yellow "N/A")   ]"
}

printtemplate() {
    echo -e "[$(color-if-tty yellow "No Template")]"
}

runtest()
{
    printf "${FMT}" "  $*"

    # Here is the line that runs the test, supressing output.  Copies
    # of stdout and stderr are in the ./tests/ directory, if needed.
    "$@" >"$IOCACHE_HOME/last-reg-test.stdout" 2>"$IOCACHE_HOME/last-reg-test.stderr"
    RC="$?"

    case "$RC" in
	0)
	    printok
	    ;;
	1)
	    printdiff
	    cat <<EOF
To see differences, do
diff $(readlink "$IOCACHE_HOME/tests/prev-testrun-0")/*filtered
EOF
            ;;
	2)
	    printtemplate
            ;;
	*)
	    printfailed
	    echo -n "  "  # indent a bit
	    head -n 15 "$IOCACHE_HOME/last-reg-test.stderr"
	    ;;
    esac

    stderrout="$(head -n 5 "$IOCACHE_HOME/tests/prev-testrun-0/"*.stderr 2>/dev/null)"
    if [ "$stderrout" != "" ]
    then
	echo "  Output was sent to stderr. The first five lines were:"
	echo "$stderrout"
    fi
}

echo
echo running test cases:
echo 
runtest bin/helloworld.sh client-sut v=3
runtest bin/helloworld.sh server-sut v=3

runtest bin/alphabet.sh client-sut threads=2 messages=2 v=2
runtest bin/alphabet.sh client-sut nd threads=1 messages=2 v=2
runtest bin/alphabet.sh server-sut threads=2 messages=2 v=2
runtest bin/alphabet.sh server-sut nd threads=2 messages=2 v=2
runtest bin/alphabet.sh server-sut maxAccept=0 nd threads=3 messages=2 v=4
runtest bin/alphabet.sh server-sut maxAccept=1 nd threads=3 messages=2 v=4

runtest bin/http.sh client-sut threads=2 v=2
runtest bin/http.sh client-sut nd threads=2 v=2
runtest bin/http.sh server-sut dynamicPort threads=2 v=2
runtest bin/http.sh server-sut dynamicPort nd threads=2 v=2

#todo include quick rupy test case with PreciseRaceDetector and search.multiple_errors=false
#todo include nio and udp versions of alphabet client/server

cat <<EOF

more tests coming soon....
EOF


#./bin/http.sh client-sut 1>/dev/null && echo OK
#./bin/http.sh server-sut 1>/dev/null && echo OK

#./bin/secure.sh client-sut dmtcp 1>/dev/null && echo OK
#./bin/secure.sh server-sut dmtcp 1>/dev/null && echo OK

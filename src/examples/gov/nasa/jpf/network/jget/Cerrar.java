//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.jget;



/* $Id: Cerrar.java 128 2006-07-25 02:48:17Z cartho $ */

import java.io.*;

class Cerrar extends Thread {

    FileOutputStream fos;
    RandomAccessFile raf,raf_log;
    public Cerrar(FileOutputStream fos, RandomAccessFile raf,RandomAccessFile raf_log) {
        this.fos=fos;
        this.raf=raf;
        this.raf_log=raf_log;

    }
    public void run() {
        try {
            if(fos!=null)
                fos.close();
            if(raf!=null)
                raf.close();
            if(raf_log!=null)
                raf_log.close();
        } catch(Exception e) {}
    }
}

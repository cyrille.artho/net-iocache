/****************************************************************************
 *   Copyright (C) 2010-2012 by Watcharin Leungwattanakit                   *
 *   le.watcharin@gmail.com                                                 *
 *                                                                          *
 *   This file is part of the dmtcp/src module of DMTCP (DMTCP:dmtcp/src).  *
 *                                                                          *
 *  DMTCP:dmtcp/src is free software: you can redistribute it and/or        *
 *  modify it under the terms of the GNU Lesser General Public License as   *
 *  published by the Free Software Foundation, either version 3 of the      *
 *  License, or (at your option) any later version.                         *
 *                                                                          *
 *  DMTCP:dmtcp/src is distributed in the hope that it will be useful,      *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *  GNU Lesser General Public License for more details.                     *
 *                                                                          *
 *  You should have received a copy of the GNU Lesser General Public        *
 *  License along with DMTCP:dmtcp/src.  If not, see                        *
 *  <http://www.gnu.org/licenses/>.                                         *
 ****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include "syscallwrappers.h"


const char *DEV_URANDOM = "/dev/urandom";
const char *DEV_RANDOM = "/dev/random";
const char *SUT_PORT = "SUT_PORT";


void send_flag() {
	struct addrinfo hints;
	struct addrinfo *servinfo;
	struct addrinfo *p;
	const int PACKET_SIZE = 1;
	int rv;
	int fd = _real_socket(AF_INET, SOCK_STREAM, 0);
	static char *str_port = getenv(SUT_PORT);
	char packet[PACKET_SIZE];

	if (fd < 0) {
		perror("[send_flag] Fail to create socket");
		_real_exit(1);
	}

	if (!str_port) {
		perror("[send_flag] Fail to obtain the SUT port");
		_real_exit(1);
	}

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	getaddrinfo(NULL, str_port, &hints, &servinfo);

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((rv = _real_connect(fd, p->ai_addr, p->ai_addrlen)) == -1) {
			continue;
		}

		break;
	}

	if (p == NULL) {
	  fprintf(stderr, "[send_flag] failed to bind to port %s\n", str_port);
		_real_exit(1);
	}

	freeaddrinfo(servinfo); // all done with this structure

	// Build a packet.
	packet[0] = (char) 1;

	// Send ND Flag.
	rv = send(fd, packet, sizeof(packet), 0);
	if (rv < 0) {
		perror("[srand] Fail to send ND Flag");
		_real_close(fd);
		_real_exit(1);
	}

	_real_close(fd);
}

extern "C" ssize_t read(int fd, void *buf, size_t count) {
	struct stat fd_stat;
	static int urandom_major = 0;
	static int urandom_minor;
	static int random_major;
	static int random_minor;
	int fd_major;
	int fd_minor;

	// Obtain the stat of "random" and "urandom" during the first call.
	if (urandom_major == 0) {
		struct stat rand_stat;

		// Obtain stat of "urandom".
		stat(DEV_URANDOM, &rand_stat);
		urandom_major = major(rand_stat.st_rdev);
		urandom_minor = minor(rand_stat.st_rdev);

		// Obtain stat of "random".
		stat(DEV_RANDOM, &rand_stat);
		random_major = major(rand_stat.st_rdev);
		random_minor = minor(rand_stat.st_rdev);
	}

	// Obtain the stat of the file to be read.
	fstat(fd, &fd_stat);
	fd_major = major(fd_stat.st_rdev);
	fd_minor = minor(fd_stat.st_rdev);

	// If the file to be read is a random number generator.
	if ((fd_major == urandom_major && fd_minor == urandom_minor) || (fd_major
			== random_major && fd_minor == random_minor)) {
		send_flag();
	}

	return _real_read(fd, buf, count);
}

extern "C" time_t time(time_t *tloc) {
	if (tloc == NULL) {
		send_flag();
	}

	return _real_time(tloc);
}

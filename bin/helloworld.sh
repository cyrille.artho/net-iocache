#!/bin/bash

source "$(cd "$(dirname "$0")" && pwd)/utils-for-testing.source" || exit

set-specific-help \
"JAVA HELLOWORLD CLIENT and SERVER

This script runs/test a minimal java client and server. There are no
specific options for this client/server pair.
"
set-parameters "$@"
DOTJPF="
$DOTJPF
jpf-net-iocache.lazy.connect = false
jpf-net-iocache.main.termination=false

jpf-net-iocache.peerResponse.maxDelayMillis = 30 
"
set-server java helloWorld.HelloServer
set-client java helloWorld.HelloClient

TESTNAME="helloworld"
dotest

//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import static gov.nasa.jpf.network.SystemInfo.PROJ_NAME;
import static gov.nasa.jpf.network.SystemInfo.jpfConfig;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.JPFConfigException;
import gov.nasa.jpf.network.SystemInfo;
import gov.nasa.jpf.network.cache.event.Event;
import gov.nasa.jpf.network.cache.event.SendReadyEvent;
import gov.nasa.jpf.network.cache.event.RecvByteEvent;
import gov.nasa.jpf.network.cache.event.RecvEofEvent;
import gov.nasa.jpf.network.cache.event.SendByteEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author franz
 *
 */
public class CacheLayer {

	/**
	 * <tt>CacheLayerServerSocket</tt> used for the communication of the 
	 * iocache with remote client peers.<br/> 
	 * It extends <tt>ServerSocket</tt> with field <b>logicalPort</b>
	 * - the port the corresponding model level ServerSocket is bound to.
	 * This may be different to the <i>native</i> port returned by 
	 * {@link  CacheLayerServerSocket#getLocalPort()} if
	 * 'jpf-net-iocache.dynamicPort=true'. 
	 * @see configuration file 'jpf.properties'
	 */
	protected class CacheLayerServerSocket extends ServerSocket {
		private int logicalPort;
		
		public CacheLayerServerSocket(int nativePort, int logicalPort) throws IOException {
			super(nativePort);
			//if parameter logicalPort==0 assign port chosen by the system as internal logical port
			this.logicalPort=logicalPort==0?getLocalPort():logicalPort;
		}
		
		public int getLogicalPort() {
			return logicalPort;
		}
	}
	
	/**
	 * ExecuteRemotePeer: thread that launches the remote client peer 
	 *   sending connection requests to the SUT. This thread is started  
	 *   each time a server SUT invokes accept on a server socket. 
	 */
	class ExecutePeerThread extends Thread {
		private Process p = null;
		private int ID;
		private int nativePort;
		private int logicalPort;

		public ExecutePeerThread(int id, int nativePort, int logicalPort) {
			this.ID=id;
			this.nativePort=nativePort;
			this.logicalPort=logicalPort;
		}
		
		public void interrupt() {
		    // the next line send kill -SIGTERM to the
		    // process.  If This is not done here, the
		    // following interrupt() will have no
		    // effect if the thread below stays
		    // blocked on is.readline().  Killing the
		    // process closes the pipe and lets
		    // is.readline() unblock.
		    p.destroy();
		    super.interrupt();
		}

		public void run() {
			ProcessBuilder builder;
			BufferedReader is;
			String  line,
					peerName="",
					peerShortName=""+ID;
			List<String> bootCmd=SystemInfo.getBootCmd(nativePort, logicalPort);

			try {
				builder = new ProcessBuilder(bootCmd);
				peerName=ID+" '"+SystemInfo.listToString(bootCmd)+"'";     

				// Merge error stream into output stream
				builder.redirectErrorStream(true);

				if (log.isLoggable(Level.INFO)) 
						log.info("starting PEER "+peerName+" ...");
				p = builder.start();
			} catch (Exception e) {
				assert false:"[CacheLayer.ExecutePeerThread.run] failed to start remote peer process "+peerName+":\n"+e;
			}
			try {
				is = new BufferedReader(new InputStreamReader(p.getInputStream()));

				// Log standard output and error of peer
				if (log.isLoggable(Level.INFO)) {
					while ((line = is.readLine()) != null) {
						log.info("[PEER "+peerShortName+"] " + line);
					}
				}
				p.waitFor();
			} catch (IOException e) {
				//A likely reason for an IOException at this point is the "Stream closed" condition 
				//  which occurs if the process p has already terminated at the time is.readLine() is invoked above.
				//  Since this is not a real problem, we can ignore such an IOException.  
				//Unfortunately, there seems to be no straight forward way to check for the "Stream closed" condition.
				//As a workaround, we ignore IOExceptions generally if process p has terminated normally and 
				//  add a warning into the log only if p is still running or has terminated with an error code. 
				
				//check if process p has terminated normally
				boolean terminatedNormally=false;
				try {
					terminatedNormally = p.exitValue()==0;
				} catch (IllegalThreadStateException ie) {
					// remote process still alive, so it did not terminate normally. 
					p.destroy();
				}
				if (!terminatedNormally) {
					log.warning("[CacheLayer.ExecutePeerThread.run] error when executing remote peer process "+peerName+":\n"+e);
				}
			} catch (InterruptedException e) {
				// If this thread is interrupted, kill the process.
				p.destroy();
			}
		}
	}

	// -- constant values --
	// There is only one cache layer in each virtual machine.
	private static final CacheLayer INSTANCE = new CacheLayer();

	//The maximum time limit in ms for a client peer to issue a connection 
	//request to the server SUT after been launched by bootPeer(id).
	private static final int ACCEPT_TIMEOUT = 30000;

	// The size of buffer used to store data received in polling the server. We
	// assume that this is the maximum size of the receivedd message.
	protected static final int BUFFER_SIZE = 16 * 1024; // 16KB

	// A byte buffer we use to store data received when the cache layer polls a peer.
	static final byte[] BUFFER = new byte[BUFFER_SIZE];

	// -- constant values --

	static SocketTreeTable stt;

	/**
	 * A list of milestones for each state.
	 */
	static List<Milestone> milestones;

	/**
	 * The list of all physical connections.
	 */
	static List<PhysicalConnection> conn_pool = new LinkedList<PhysicalConnection>();

	/**
	 * conn_list[i] is a physical connection corresponding to logical connection i
	 */
	public static List<PhysicalConnection> conn_list = new ArrayList<PhysicalConnection>();

	/**
	 * The number of accept calls in each state.
	 */
	static List<Integer> num_accept = new ArrayList<Integer>();

	/**
	 * The "close" status at present of each logical connection.
	 */
	static BitSet close = new BitSet();

	/**
	 * The list of trees in the cache layer.
	 */
	static List<SendRecvCache> commData;

	/**
	 * The server IP address that the cache layer sends a connection request to.
	 */
	static InetAddress server_addr;

	/**
	 * server socket used for the communication between the iocache and 
	 * remote client peers trying to connect to the SUT
	 */
	protected static CacheLayerServerSocket clSSocket;

	/**
	 * The number of connections accepted by the cache layer at present.
	 */
	public static int cur_accept;

	////////////////////////////////////////////////////////////////////////////////
	// delay in ms for waiting for a message from the peer (default: 150ms)
	// see also CacheLayer.nonBlockRead(...) and NoCacheLayer.read(...)
	/**
	 * sets the time span in ms within a peer is expected to send his complete
	 * initial message after being executed and a complete response to a
	 * previously received message, respectively, as configured by property
	 * "jpf-net-iocache.peerResponse.maxDelayMillis" (default: 150 ms). It is
	 * used to recognize and store send-recv patterns in the iocache.
	 * Increase this value, if iocache keeps dropping delayed messages or
	 * associating them with the wrong messages. Note: A value of 0 indicates
	 * infinite delay, i.e. after executing a peer and after each network output
	 * of the SUT, jpf will block possibly infinitely until a message from the
	 * remote peer is received.
	 * 
	 * @param property Name of the property that sets the peer poll delay
	 */

	private static int peerPollDelay;

	protected static long restart = 0;

	public static Logger log = JPF.getLogger("gov.nasa.jpf.network.cache.CacheLayer");

	static String cacheDir;

	/**
	 * The server port number that the cache layer sends a connection request to.
	 */
	static int server_port = 0;

	private static long totalBytesRead = 0;

	private static long cache_miss = 0;

	private static boolean virtual_mode = false;

	private static boolean dmtcpEnabled = false;

	/**
	 * If this variable is true, the cache layer will connect after the target code has sent the first message out. If
	 * this variable is false, the cache layer will connect immediately after the target code has called connect().
	 */
	static boolean lazyConnect;

	static boolean dynamicPort;

	static boolean isServerSUT = false;

	/**
	 * The list of threads that start peer processes. <i>peerThreads[i]</i> is associated with logical connection <i>i</i>.
	 */
	static List<Thread> peerThreads = new ArrayList<Thread>();

	private boolean isMainAlive = true;

	static {
		reset();
	}

	protected CacheLayer() {
		peerPollDelay = 
			jpfConfig.getInt(PROJ_NAME + ".peerResponse.maxDelayMillis", 150);
		if (peerPollDelay < 0)
			throw new JPFConfigException("Peer delay must be positive.");
		lazyConnect = jpfConfig.getBoolean(PROJ_NAME + ".lazy.connect", true);
		dynamicPort = jpfConfig.getBoolean(PROJ_NAME + ".dynamicPort", false);
	}

	/**
	 *
	 * @return A cache layer, depending on the configuration specified.
	 */
	public static CacheLayer getInstance() {
		if (virtual_mode) 
			return dmtcpEnabled ? DMTCPProxyCacheLayer.getInstance() : CheckpointCacheLayer
					.getInstance();
		
		return INSTANCE;
	}

	/**
	 *
	 * @param id
	 * @return True, if logical connection <i>id</i> is "logically" closed; false, otherwise.
	 */
	public boolean isSocketClosed(int id) {
		return close.get(id);
	}

	public static void setServerSUT(boolean value) {
		isServerSUT = value;
	}
	
	public static void setVirtualMode(boolean mode) {
		virtual_mode = mode;
	}

	public static void setDMTCPEnabled(boolean enabled) {
		dmtcpEnabled = enabled;
	}

	public static void setCacheDir(String dir) {
		assert dir != null;
		cacheDir = dir;
	}

	public static boolean isVirtualMode() {
		return virtual_mode;
	}

	public void notifyMainTerminated() {
		isMainAlive = false;
	}

	public void onSocketCreated(int id) {
		addNewConnection(id);
	}
	
	protected static void reset() {
		SendRecvCache rr_tree = new SendRecvCache(0);

		commData = new ArrayList<SendRecvCache>();
		stt = SocketTreeTable.getInstance();

		milestones = new ArrayList<Milestone>();

		commData.add(rr_tree);
	}

	public boolean isMainAlive() {
		return isMainAlive;
	}

	/**
	 * A pair of pointers is added to the tree associated with the new socket.
	 *
	 * @param id
	 *            The socket ID of the new connection.
	 */
	protected void addNewConnection(int id) {
		int treeForNewSocket;
		
		if (isServerSUT) {
			treeForNewSocket = id;
		}
		else {
			treeForNewSocket = 0;
		}

		assert treeForNewSocket <= commData.size() : String.format("Invalid tree id %d (commData.size() = %d)", treeForNewSocket, commData.size());
		if (treeForNewSocket == commData.size()) {
			// Add a new instance of a tree data structure for storing traces between the new peer.
			commData.add(new SendRecvCache(cur_accept));
		}
		
		// Add a pair of pointers to the tree associated with the new socket
		commData.get(treeForNewSocket).addPointers();
		conn_list.add(null);
	}
	
	/**
	 * Execute user's specified command to boot a peer. The peer connects to the socket of the cache layer.
	 *
	 * @param id
	 *            The id of a logical connection associated to the new peer.
	 *
	 * @return The accepted Socket instance representing the connection to the new peer.
	 */
	Socket bootPeer(int id) throws IOException {
		Thread execPeer;
		
		execPeer = new ExecutePeerThread(id,clSSocket.getLocalPort(),clSSocket.getLogicalPort());

		// The first time this logical connection boots a peer.
		if (id == peerThreads.size())
			peerThreads.add(execPeer);
		else {
			Thread t = peerThreads.get(id);

			// Kill the previous thread if alive.
			if (t.isAlive()) {
				t.interrupt();
			}

			// Assign the new thread to this logical connection.
			peerThreads.set(id, execPeer);		   
		}

		//start thread that launches the remote peer
		execPeer.start();

		Socket res=null;
		try {
			res=clSSocket.accept();
		} catch (SocketTimeoutException e) {
			log.severe("[CacheLayer.bootPeer(id="+id+")] accept timed out (did not receive a connection request from remote peer "+id+" within "+ACCEPT_TIMEOUT+" ms).");
			throw new IOException("Native accept timed out (i.e., CacheLayer did not receive a connection request from remote peer "+id+" within "+ACCEPT_TIMEOUT+" ms).");
		}
		assert res != null : "[CacheLayer.bootPeer(id="+id+")] failed to receive connection request from remote peer "+id;
		return res;
	}

	/**
	 * read data from PhysicalConnection id to buffer with delay as configured in 
	 * property 'jpf-net-iocache.peerResponse.maxDelayMillis' ms (default: 150 ms). 
	 * @param conn    the PhysicalConnection to read from;
	 * @param buffer  stores the read bytes.
	 * @param wait if true wait X ms for a message.
	 * @return >0 (the number of bytes that have been read); 
	 *         -1 (eof) if no data is available and the peer has closed the connection;  
	 *         0  (timeout) if no data is available within 
	 *            'jpf-net-iocache.peerResponse.maxDelayMillis' ms and 
	 *            the peer has not closed the connection. 
	 * @throws IOException
	 */
	protected int delayedRead(PhysicalConnection conn, byte[] buffer, boolean wait) throws IOException {
      int delay=wait ? peerPollDelay:1;  //if non-blocking read set delay to minimum of 1ms
      int res=conn.read(buffer, delay);  //res: number of bytes that have been read; -1 eof, 0 timeout

      return res;
	}

	/**
	 * Poll a peer that the logical connection <i>id</i> is connecting for receiving messages. If a peer is closed, do
	 * nothing; otherwise, wait a certain time for receiving messages and add recvEvent into the RR tree if there
	 * is an incoming message.
	 *
	 * @param id
	 * @throws IOException
	 */
	void pollPeer(int id) throws IOException {
		pollPeer(id, false, true);
	}
	
	void pollPeer(int id, boolean drop) throws IOException {
		pollPeer(id, drop, true);
	}

//	void pollPeer(int id, boolean drop) throws IOException {
//		pollPeer(id, DELAY, drop);
//	}
//
//	void pollPeer(int id, int delay) throws IOException {
//		pollPeer(id, delay, false);
//	}

	/**
	 *
	 * @param id    id of logical connection to read from
	 * @param drop  if set to true, this method does not add a polled message to the data structure.
	 * @param wait  if true, wait for peerResponse.maxDelayMillis ms before polling the peer
	 * @return The size of the polled message in bytes.
	 * @throws IOException
	 */
	void pollPeer(int id, boolean drop, boolean wait) throws IOException {
		PhysicalConnection conn = conn_list.get(id);
		if (conn.isClosed()) {
			log.info("[CacheLayer : pollServer(int), id="+id+"] This socket is closed. Nothing to poll.");
			return;
		}

		int bytes = delayedRead(conn, BUFFER, wait);       //read available data from conn to BUFFER
		if (drop) 
			return;
		
		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));
		rr_tree.setConnId(id);
		rr_tree.addRecvEvent(BUFFER, 0, bytes);
		conn.setInStreamPos(rr_tree.limit());
		if (bytes > 0) { 
			cache_miss += bytes;
			log.info("[CACHE : pollPeer(int), id="+id+"] "+bytes+" byte(s) received.");
			pollPeer(id,drop,false);         //poll peer again without delay to check for eof condition 
		} else if (bytes==-1) {              //EOF
			log.info("[CACHE : pollPeer(int), id="+id+ "] EOF (-1) received.");
		} else if (wait && bytes==0) { 
			log.warning("[CACHE : pollPeer("+id+")] No response from PEER '"+id+"' within "+peerPollDelay+" ms " +
		                "(a delayed response may be dropped or associated with a later request).");
		} else assert !wait && bytes==0 : "Illegal result '"+bytes+"' returned by CacheLayer.delayedRead("+id+")";
	}

	/**
	 *
	 * @param id
	 * @return true, if the logical connection <i>id</i> is not closed and there is 
	 *          no event (data byte or eof) available to be read from the iocache;
	 *          otherwise, false.
	 */
	public boolean isNextReadBlocked(int id) {
		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));

		rr_tree.setConnId(id);
		log.finer("[CacheLayer.isNextReadBlocked(id="+id+")] trying to read from logical channel "+id+" (status: "+(close.get(id)?"closed":"open")+"; " +
				rr_tree.recvEventAvailable()+" data bytes/eof ready to be retrieved from the iocache)...");

		if (close.get(id))   //if logical connection is not open...
			return false;    //...return false  (do not block reading in the model class).
		
		boolean noRespAvail = rr_tree.recvEventAvailable()==0;
		//if there is no data ready to be read from the iocache 
		//  and not yet another message has been sent out (i.e., rr_tree.canAdd(null)==true)
		//  poll the remote peer again to receive its response to the last sent message (second chance)
		if (noRespAvail && rr_tree.canAdd(null)) try {
			log.finer("[CacheLayer.isNextReadBlocked(id="+id+")] polling the peer again... ");
			pollPeer(id);
			noRespAvail = rr_tree.recvEventAvailable()==0;
		} catch (IOException e ) {
			assert false : "[CacherLayer.isNextReadBlocked] error while polling the remote peer: "+e;
		}
		
		return noRespAvail ;
	}

	/**
	 * Read a received string from the tree and store it in <i>buffer</i>.
	 *
	 * @param id
	 * @param buffer
	 * @return -1 if the next recvEvent is EOF; otherwise, the number of bytes read.
	 * @throws IOException
	 */
	public int read(int id, byte[] buffer) throws IOException {
		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));
		rr_tree.setConnId(id);
		int respAvail=rr_tree.recvEventAvailable();
		if (respAvail==0 || buffer.length==0)    //Currently no recvEvent ready to be read or no space in buffer 
			return 0;
		
		if (rr_tree.getRecvEvent() instanceof RecvEofEvent) {       //Current recvEvent is RecvEofEvent
			if (log.isLoggable(Level.FINER)) 
				log.finer("[CacheLayer.read] EOF (-1)");				
			return -1;
		}

		int bytesRead;
		int maxBytesRead=Math.min(buffer.length,respAvail);
		for(bytesRead=0; bytesRead<maxBytesRead; bytesRead++,rr_tree.moveRecvPtr()) {
			Event recvEvent=rr_tree.getRecvEvent();
			if (!(recvEvent instanceof RecvByteEvent)) 
				break;
			buffer[bytesRead] = ((RecvByteEvent) recvEvent).b;
		}
		
		//some bookkeeping and logging 
		totalBytesRead += bytesRead;
		if (log.isLoggable(Level.FINER))
			log.finer("[CacheLayer.read] " + new String(buffer,0,bytesRead));

		return bytesRead;
	}

	
	/**
	 * write a message to the iocache;<br/> 
	 * On cache miss, send the message physically to the remote peer;<br/>
	 * On cache conflict, add a branch to the cache, restart the remote peer, 
	 *   and replay the cached data.
	 * @param id connection id to write to
	 * @param msg byte array containing the message to send
	 * @param off offset of the message in <b>msg</b> 
	 * @param len length of the message in <b>msg</b>
	 * @throws IOException
	 */
	@SuppressWarnings("null")  //Suppress null pointer warnings on phyConn
	public void write(int id, byte[] msg, int off, int len) throws IOException {
		Event[] events = new Event[len];

		for (int i = 0; i < len; i++) {
			events[i] = new SendByteEvent(msg[off + i]);
		}

		SendRecvCache rr_tree;
		int treeId = stt.getTreeID(id);
		assert treeId >= 0 : "Negative tree ID";

		rr_tree = commData.get(treeId);
		rr_tree.setConnId(id);

		int lastCommon;
		// Move through common bytes as much as possible.
		for (lastCommon = 0; lastCommon < len; lastCommon++) {
			// replayed message
			if (rr_tree.isNextSendEvent(events[lastCommon])) {
				rr_tree.moveSendPtr(events[lastCommon]);
			} else
				break;
		}
		if (lastCommon==len)    //if message is already completely in the cache  
			return;             // --> nothing else to do.
		
		// Something left to physically write
		// If part of the message is common, shift the arrays first.
		if (lastCommon > 0) {
			int newLen = len - lastCommon;
			Event[] newEvents = new Event[newLen];
			byte[] newMsg = new byte[newLen];
			System.arraycopy(events, lastCommon, newEvents, 0, newLen);
			System.arraycopy(msg, lastCommon + off, newMsg, 0, newLen);

			events = newEvents;
			msg = newMsg;
			len = newLen;
			off = 0;
		}

		PhysicalConnection phyConn=conn_list.get(id);
		boolean phyConnOpen=!(phyConn==null || phyConn.isClosed());
		// Check if message msg can be added to the current branch.
		if (phyConnOpen && rr_tree.canAdd(events[0])) {
			log.fine("[CacheLayer.write] add request '"+new String(msg,off,len)+"' to current branch of RRTree...");
//				if (!rr_tree.isEmpty())
//					rebindConnection(id);            //todo Cyrille: what is this for?
			for (Event e : events)
				rr_tree.addSendEvent(e);
			
			phyConn.write(msg, off, len);
			phyConn.setOutStreamPos(rr_tree.getSendPtr());
		}
		else { // create a new branch: replay and add message msg to a new branch
			// Replace an old connection OR lazy connect.
			boolean createConnection = rr_tree.hasMessage() || !phyConnOpen; 
			log.fine("[CacheLayer.write] add request '"+new String(msg,off,len)+"' to   n e w   branch of RRTree (createConnection="+createConnection+")... ");
			for (Event e : events) {
				rr_tree.addSendEvent(e);
			}
		
			if (createConnection) 
				createPhysicalConnection(id);
			else // NOT lazy connect
				conn_list.get(id).setOutStreamPos(rr_tree.getSendPtr());
		}
		pollPeer(id);
	}

	/**
	 * Read and write messages in <i>rr_tree</i> from the beginning to the active sentEvent pointer via <i>newSocket</i>.
	 *
	 * @param newSocket
	 *            The socket to be read and written.
	 * @param rr_tree
	 *            The tree that stores read/write history.
	 * @throws IOException
	 */
	@SuppressWarnings("resource") 
	private void replay(Socket newSocket, SendRecvCache rr_tree) throws IOException {
		List<Event> event_path = rr_tree.getEventPath();
		if (event_path.size()<=1)     //if at most one event...
			return;                   //...nothing to replay (root node cannot be replayed!)
		
		if (log.isLoggable(Level.FINE))
			log.fine("[CacheLayer.replay] replaying cached IO of logical connection "+rr_tree.getConnId()+"...");
		try {
			InputStream newIn = newSocket.getInputStream();
			OutputStream newOut = newSocket.getOutputStream();
			byte[] buf=new byte[16384];   //16k write buffer
			int pos=0;
			for (Event e : event_path) {
				if (e instanceof SendByteEvent) {
					buf[pos++]=((SendByteEvent) e).b;  //add data byte to output buffer
					if (pos==buf.length) {
						flushReplayBuf(buf,pos,newOut);
						pos=0;
					}
				} else if (e instanceof RecvByteEvent) {
					flushReplayBuf(buf, pos, newOut);
					pos=0;
					int b=newIn.read();
					if (log.isLoggable(Level.FINER))
						log.finer("[CacheLayer.replay] read "+(char)b);
				}
			}
			flushReplayBuf(buf, pos, newOut);
		} catch (IOException e) {
			log.severe("[CacheLayer.replay] error while replaying cache content: IOException '"+e.getMessage()+"'");
			throw e;
		}
	}
	
	/**
	 * writes replay output buffer to the remote peer 
	 * 
	 * @param buf data bytes to write
	 * @param len number of bytes to write
	 * @param out OutputStream to write to 
	 * @throws IOException
	 */
	private void flushReplayBuf(byte[] buf, int len, OutputStream out) throws IOException {
		if (len==0)
			return;
		out.write(buf, 0, len);
		log.finer("[CacheLayer.replay] wrote '"+new String(buf,0,len)+"'");
	}
	
	
	/**
	 * Create a new PhysicalConnection instance from <i>newSocket</i>. Add it into the connection pool and bind to
	 * logical connection <i>id</i>.
	 *
	 * @param newSocket
	 *            A new socket.
	 * @param id
	 *            The identifier of the logical connection.
	 * @throws IOException
	 */
	PhysicalConnection setNewConnection(Socket newSocket, int id) throws IOException {
		PhysicalConnection new_conn;
		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));

		rr_tree.setConnId(id);

		new_conn = new PhysicalConnection(newSocket, rr_tree.getSendPtr(), rr_tree.limit());

		conn_pool.add(new_conn);
		// newly created logical connection
		if (id >= conn_list.size()) {
			conn_list.add(new_conn);
		}
		// existed logical connection
		else {
			conn_list.set(id, new_conn);
		}

		return new_conn;
	}

	/**
	 * If the SUT is a server, move the sentEvent pointer to the ready node. If there is no ready node, add it first.
	 *
	 * @param id
	 *            The identifier of the logical connection.
	 */
	void getReady(int id) {
		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));

		rr_tree.setConnId(id);

		// check Ready event existence for the server-side
		if (isServerSUT) {
			if (!rr_tree.isEmpty()) {
				if (rr_tree.sendPtrAtRoot())
					rr_tree.moveSendPtr(SendReadyEvent.getInstance());
			} else {
				rr_tree.addSendEvent(SendReadyEvent.getInstance());
			}
		}
	}

	/**
	 * Create a new physical connection for a logical connection <i>id</i>.
	 *
	 * @param id
	 * @throws IOException
	 */
	PhysicalConnection createPhysicalConnection(int id) throws IOException {
		log.fine("[CacheLayer.createPhysicalConnection(id="+id+
					")] create physical connection for logical connection "+id+"...");

		SendRecvCache rr_tree = commData.get(stt.getTreeID(id));
		Socket newSocket;

		rr_tree.setConnId(id);
		restart++;

		// initiate a new socket
		if (!isServerSUT) { // The cache layer can initiate a connection by itself.
			newSocket = createSocket(server_addr, server_port);
		}
		// The cache layer needs a user's command to initiate a connection.
		else {
			newSocket = bootPeer(id);
		}
		log.finer("[CacheLayer.createPhysicalConnection(id="+id+
				")] created connection "+newSocket+" for remote peer "+id+".");

		replay(newSocket, rr_tree);
		getReady(id);

		return setNewConnection(newSocket, id);
	}

	/**
	 * Save every send/recvEvent pointer and close flag in the milestone. Save the number of connection accepts in
	 * this state. Save the socket-tree table.
	 *
	 * @param id
	 *            The identifier of the state to be saved.
	 */
	void saveState(int id) {
		// There is only one tree in case of client checking, so we start with 0 by default.
		SendRecvCache rr_tree = commData.get(0);
		Milestone stone = milestones.get(id);

		for (int i = 0, num_sock = conn_list.size(); i < num_sock; i++) {
			if (isServerSUT) {
				assert i < commData.size() : String.format("[CACHE] Attempt to obtain a non-existing tree (%d); #Trees = %d", i, commData.size());
				rr_tree = commData.get(i);
			}

			rr_tree.setConnId(i);
			stone.addReqPtr(rr_tree.getSendPtr());
			stone.addResPtr(rr_tree.getRecvPtr());
			stone.setClose(i, close.get(i));
		}

		stone.setMainAlive(isMainAlive);
		// set the number of accept calls of a new state
		num_accept.add(cur_accept);

		stt.save();
	}

	/**
	 * Restore every send/recvEvent pointer saved in the milestone and set their values in the tree. Restore the
	 * number of connection accepts. Bind each logical connection with a physical connection in the pool.
	 *
	 * @param id
	 *            The identifier of the state that JPF backtracks to.
	 */
	void restoreState(int id) {
		// There is only one tree in case of client checking, so we start with 0 by default.
		SendRecvCache rr_tree = commData.get(0);
		Milestone stone;
		int req;
		int res;
		boolean close_flag;

		if (id >= 0) {
			stone = milestones.get(id);
			cur_accept = num_accept.get(id);
		}
		// id == -1, use an empty milestone
		else {
			stone = EmptyMilestone.getInstance();
			cur_accept = 0;
		}

		for (int i = 0, num_sock = conn_list.size(); i < num_sock; i++) {
			if (i < stone.numConnections()) {
				req = stone.getReqPtr(i);
				res = stone.getResPtr(i);
				close_flag = stone.isClosed(i);
			}
			// If current connections are more than ones in the milestone, set to default values
			else {
				req = 0;
				res = 0;
				close_flag = false;
			}

			if (isServerSUT) {
				assert i < commData.size() : "[CACHE] Attempt to obtain a non-existing tree";
				rr_tree = commData.get(i);
			}

			rr_tree.setConnId(i);
			rr_tree.setSendPtr(req);
			rr_tree.setRecvPtr(res);
			close.set(i, close_flag);
			stt.restore(id);
		}

		isMainAlive = stone.isMainAlive();
	}

	/**
	 * Change the current send/recvEvent pointers to new values according to the state identifier <i>id</i>. Assign a
	 * suitable physical connection to each logical connection. This method is invoked by the CacheNotifier listener
	 * every time state transition takes place.
	 *
	 * @param id
	 */
	public void changeState(int id) {
		int max_state_id = milestones.size() - 1;

		// not visited before
		if (max_state_id < id) {
			milestones.add(new Milestone());
			saveState(id);
		}
	}

	public void backtrack(int id) {
		restoreState(id);
	}

	protected Socket createSocket(InetAddress addr, int port) {
		if (log.isLoggable(Level.FINE))
			log.fine("[CacheLayer.createSocket] open physical connection at "+addr+":"+port+"...");

		try {
			return new Socket(addr, port);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Physically connect to a peer, if logical connection <i>id</i> has not been created yet. The peer class of Socket
	 * calls this method.
	 *
	 * @param id
	 * @param addr
	 * @param port
	 * @throws IOException
	 */
	public void connect(int id, InetAddress addr, int port) throws IOException {
		stt.setTreeID(id, 0);

		if (server_addr == null) {
			server_addr = addr;
			server_port = port;
		}
		
		// new socket
		if (conn_list.get(id) == null) {
			// Connect at once, if we are not lazy.
			if (!lazyConnect) {
				PhysicalConnection conn = createPhysicalConnection(id);

				onConnectionEstablished(conn);
				pollPeer(id);
			}
		}
	}

	
	/**
	 * creates a <i>native</i> ServerSocket instance on the specified <b>logicalPort</b>; 
	 * This ServerSocket instance is used when the {@link #accept()} method is called in the sequel;
	 * Note that the native port assigned by the CacheLayer may be different 
	 * from <b>logicalPort</b> if 'jpf-net-iocache.dynamicPort=true' 
	 * @param logicalPort to create ServerSocket at; if <b>logicalPort</b><tt>==0</tt> a ServerSocket will be
	 *        created a free port chosen by the system (see <b>return</b> value);
	 * @return logical port at which the ServerSocket has been created; 
	 *         This is equal to parameter <b>logicalPort</b> in case <b>logicalPort</b><tt>>0</tt>.
	 * @throws IOException
	 * @see #accept()
	 * @see 'jpf-net-iocache.dynamicPort' in file jpf.properties
	 */
	public int bind(int logicalPort) throws IOException {
		if (clSSocket!=null) {   //if clSSocket is already bound (possible after backtracking)...
			                     //... ensure that we bind to the same logical port used previously 
			                     //    (re-binding a server port to a different port or binding multiple server sockets is not supported yet)
			                     // FIXME add support for more than one server socket to CacheLayer
			if (logicalPort!=0 && logicalPort!=clSSocket.getLogicalPort()) {
				log.severe("[CacheLayer.bind(logicalPort="+logicalPort+")] cannot bind server socket to another port " +
						"(current cache layer server socket is bound to logicalPort="+clSSocket.getLogicalPort()+", nativePort="+clSSocket.getLocalPort()+")");
				assert false: "Net-iocache does not support multiple server sockets bound to different ports, yet! Please use only one server socket.";
			}
		} else {                          //create and bind a new cache layer server socket
			int  nativePort=logicalPort;  //default: use the same port as in the ServerSocket model
			if (dynamicPort)
				nativePort = 0;           //...use dynamic port allocation --> System will choose a free port
			try {
			   clSSocket = new CacheLayerServerSocket(nativePort,logicalPort);
			   clSSocket.setSoTimeout(ACCEPT_TIMEOUT);  //ensure that jpf will not be blocked forever when a client peer fails to send a connection request
			} catch (Exception e) {
			   log.severe("[CacheLayer.bind(logicalPort="+logicalPort+")] failed creating native server socket on port "+nativePort+": "+e);
			   throw(new IOException(e));
			}
			log.fine("[CacheLayer.bind(logicalPort="+logicalPort+")] created new native server socket at port "+clSSocket.getLocalPort()+".");
		}
		
		return clSSocket.getLogicalPort();
	}

	
	/**
	 * This method is called by the peer class of ServerSocket instances in order to establish 
	 * a channel between the application and its peers. If the physical connections are not
	 * enough, create one more physical connection by accepting a connection request. 
	 * If there is no Ready event in the tree, add one.
	 *
	 * @return socketID of the created <i>native</i> socket of the accepted connection to the remote peer.
	 * @throws IOException
	 * @see #bind(int)
	 */
	public int accept() throws IOException {
		int num_connection = conn_list.size(); 
		int idle;
		
		assert clSSocket!=null : "Native ServerSocket has not been bound to a port prior to calling accept().";
		assert cur_accept < num_connection : "there are accept calls more than the number of connections";

		// new connection
		if (conn_list.get(cur_accept) == null) {
			PhysicalConnection conn;

			stt.setTreeID(cur_accept, commData.size() - 1);
			
			conn = createPhysicalConnection(cur_accept);
			onConnectionEstablished(conn);
			pollPeer(cur_accept);
		} else {
			idle = stt.unusedTree();
			stt.setTreeID(cur_accept, idle);
			getReady(idle);
		}
		return cur_accept++;
	}

	/**
	 * This method is called by the peer class of Socket.
	 *
	 * @param id
	 * @throws IOException
	 */
	public void close(int id) throws IOException {
		if (close.get(id))        //nothing to do if already closed  
			return;
		close.set(id);
		if (log.isLoggable(Level.FINE))
			log.fine("[CacheLayer.close(id="+id+")] closed open logical connection.");
		
		// Closing physical connection corresponding to logical connection id
		PhysicalConnection phyConn=conn_list.get(id);
		if (phyConn==null || phyConn.isClosed())  //if none or already closed...
			return;                               //nothing to do!

		if (log.isLoggable(Level.FINE))
			log.fine("[CacheLayer.close(id="+id+")] closing open physical connection...");
		phyConn.close();              //close physical connection
		conn_pool.remove(phyConn);    //remove from pool of open physical connections.
	}

	public int numTrees() {
		return commData.size();
	}

	public static long getCacheHit() {
		long hit = totalBytesRead - cache_miss;

		return hit >= 0 ? hit : 0;
	}

	public static long getCacheMiss() {
		return cache_miss;
	}

	public static long getNumRestart() {
		return restart;
	}

	public void closePhysicalConnections() {
		for (PhysicalConnection pc : conn_pool) {
			try {
				pc.close();
			} catch (IOException e) {
				assert false : e;
			}
		}
	}


	@SuppressWarnings("unused")
	void onConnectionEstablished(PhysicalConnection conn) {
		//not used yet
	}

}

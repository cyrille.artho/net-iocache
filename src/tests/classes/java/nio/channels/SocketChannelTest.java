package classes.java.nio.channels;

import gov.nasa.jpf.util.test.TestJPF;
import gov.nasa.jpf.network.echo.Server;

import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import static org.junit.Assert.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ConnectionPendingException;
import java.nio.channels.NoConnectionPendingException;
import java.nio.channels.Selector;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.NotYetConnectedException;

public final class SocketChannelTest extends TestJPF {
	private static Process serverProcess;

	@BeforeClass
	public static void startServer() throws IOException {
		String[] cmd = { "java", "-cp", "build/examples/", "gov.nasa.jpf.network.echo.Server" };
		serverProcess = new ProcessBuilder(cmd).start();
	}

	@AfterClass
	public static void stopServer() {
		serverProcess.destroy();
	}

	@Test
	public void shouldThrowNotYetConnectedExceptionOnRead() throws IOException {
		if (verifyUnhandledException(NotYetConnectedException.class.getName(), "+jpf-net-iocache.lazy.connect=false")) {
			final SocketChannel socketChannel = connect(false);
			final ByteBuffer buffer = ByteBuffer.allocate(8192);
			socketChannel.read(buffer);
		}
	}

	@Test
	public void shouldThrowNotYetConnectedExceptionOnWrite() throws IOException {
		if (verifyUnhandledException(NotYetConnectedException.class.getName(), "+jpf-net-iocache.lazy.connect=true")) {
			final SocketChannel socketChannel = connect(false);
			final String message = "Hello from client";
			socketChannel.write(ByteBuffer.wrap(message.getBytes()));
		}
	}

	@Test
	public void shouldReachOpenState() throws IOException {
		if (verifyNoPropertyViolation()) {
			final SocketChannel socketChannel = SocketChannel.open();
			assertTrue(socketChannel.isOpen());
		}
	}

	@Test
	public void shouldClose() throws IOException {
		if (verifyNoPropertyViolation()) {
			final SocketChannel socketChannel = SocketChannel.open();
			socketChannel.close();
			assertFalse(socketChannel.isOpen());
		}
	}

	/*
	 * SocketChannel's non-blocking connect connects server nondeterministically i.e.,
	 *	1. Connects immediately
	 *	2. Doesn not connect immediately and finishes connect by invoking finishConnect().
	 * Therefore, isConnected() returns true or false nondeterministically and we test both cases.
	 */
	@Test(expected=AssertionError.class)
	public void shouldNonDeterministicallyTransitToConnectedState1() throws IOException {
		if (verifyNoPropertyViolation()) {
			final SocketChannel socketChannel = connect(false);
			assertTrue(socketChannel.isConnected());
		}
	}

	@Test(expected=AssertionError.class)
	public void shouldNonDeterministicallyTransitToConnectedState2() throws IOException {
		if (verifyNoPropertyViolation()) {
			final SocketChannel socketChannel = connect(false);
			assertFalse(socketChannel.isConnected());
		}
	}

	@Test(expected=AssertionError.class)
	public void shouldNonDeterministicallyTransitToPendingState1() throws IOException {
		if (verifyNoPropertyViolation()) {
			final SocketChannel socketChannel = connect(false);
			assertTrue(socketChannel.isConnected());
		}
	}

	@Test(expected=AssertionError.class)
	public void shouldNonDeterministicallyTransitToPendingState2() throws IOException {
		if (verifyNoPropertyViolation()) {
			final SocketChannel socketChannel = connect(false);
			assertFalse(socketChannel.isConnected());
		}
	}

	@Test
	public void shouldTransitFromPendingStateToConnectedState() throws IOException {
		if (verifyNoPropertyViolation()) {
			final SocketChannel socketChannel = connect(false);
			if (!socketChannel.isConnected()) {
				while (!socketChannel.finishConnect()) {}
				if (!socketChannel.isConnected()) {
					fail();
				}
			}
		}
	}

	@Test
	public void trivial() throws IOException {
		if (verifyNoPropertyViolation()) {
			SocketChannel sc = SocketChannel.open();
			Selector sel = Selector.open();
			assertEquals(sc.keyFor(sel), null);
			sc.configureBlocking(false);

			Integer i = new Integer(0);
			SelectionKey sk = sc.register(sel, SelectionKey.OP_READ, i);
			assertEquals(sc.keyFor(sel), sk);
			assertEquals(sk.attachment(), i);

			Object t = new Object();
			sk.attach(t);
			assertEquals(sk.attachment(), t);

			sk.isReadable();
			sk.isWritable();
			sk.isConnectable();
			sk.isAcceptable();
		}
	}

	@Test
	public void testChannelClose() throws IOException {
		if (verifyNoPropertyViolation()) {
			SocketChannel sc = SocketChannel.open();
			Selector sel = Selector.open();
			sc.configureBlocking(false);
			SelectionKey sk = sc.register(sel, SelectionKey.OP_READ);
			sk.channel().close();
			assertFalse(sk.isValid());
			assertFalse(sk.channel().isOpen());
		}
	}

	@Test
	public void finishConnect() throws IOException {
		if (verifyNoPropertyViolation("+jpf-net-iocache.main.termination=false","+jpf-net-iocache.lazy.connect=true")) {
			boolean ok = false;
			SocketChannel.open();
			try {
				SocketChannel.open().finishConnect();
			} catch (NoConnectionPendingException e) { ok = true; }
			assert (ok);
			ok = false;
			SocketChannel sc = SocketChannel.open();
			sc.configureBlocking(false);
			try {
				sc.finishConnect();
			} catch (NoConnectionPendingException e) { ok = true; }
			assert (ok);
			sc = SocketChannel.open();
			sc.configureBlocking(false);
			sc.connect(new InetSocketAddress("google.com", 80));
		}
	}


	@Test
	public void closedChannelExc() throws IOException {
		if (verifyNoPropertyViolation()) {
			boolean ok = false;
			final SocketChannel sc = connect(false);
			sc.close();
			try {
				sc.finishConnect();
			} catch (ClosedChannelException e) { ok = true; }
			assert (ok);
			ok = false;
			sc.close();
			try {
				sc.connect(new InetSocketAddress("localhost", Server.PORT));
			} catch (ClosedChannelException e) { ok = true; }
			assert (ok);
		}
	}

	@Test
	public void connectionPending() throws IOException {
		if (verifyNoPropertyViolation("+jpf-net-iocache.lazy.connect=true")) {
			boolean ok = false;
			final InetSocketAddress addr = new InetSocketAddress("localhost", Server.PORT);
			final SocketChannel socketChannel = SocketChannel.open();
			socketChannel.configureBlocking(false);
			ok = socketChannel.connect(addr);
			try {
				if (!ok) {
					socketChannel.connect(addr);
				}
			} catch (ConnectionPendingException e) {
				ok = true;
			}
			assert (ok);
		}
	}

	private SocketChannel connect(final boolean blocking) throws IOException {
		final SocketChannel socketChannel = SocketChannel.open();
		socketChannel.configureBlocking(blocking);
		socketChannel.connect(new InetSocketAddress("localhost", Server.PORT));
		return socketChannel;
	}
}

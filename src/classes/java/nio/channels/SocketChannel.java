package java.nio.channels;

import gov.nasa.jpf.vm.Verify;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * List of lock objets.
 *
 * <ul>
 *	<li>readLock</li>
 *	<li>writeLock</li>
 * </ul>
 *
 * read()/write() are synchronized on readLock/writeLock.
 */
public class SocketChannel extends SelectableChannel {
	
	private enum ConnectionState {NOT_CONNECTED, PENDING, CONNECTED, CLOSED}
	
	private Socket socket;
	private boolean blocking = true;
	private List<SelectionKey> keys;
	private InputStream in;
	private OutputStream out;

	private ConnectionState connState=ConnectionState.NOT_CONNECTED; 
	private SocketAddress pendingConnAddr;
	private boolean readyToConnect = false;
	private boolean readyToRead = false;
	private boolean readyToWrite = false;

	private Object readLock  = new Object();
	private Object writeLock = new Object();

	protected SocketChannel(Socket socket) throws IOException {
		this.socket = socket;
		in = socket.getInputStream();
		out = socket.getOutputStream();
	}

	protected SocketChannel(Socket socket, boolean connected) throws IOException {
		this.socket = socket;
		in = socket.getInputStream();
		out = socket.getOutputStream();
		connState = connected?ConnectionState.CONNECTED:ConnectionState.NOT_CONNECTED;
	}

	public static SocketChannel open() throws IOException {
		return new SocketChannel(new Socket());
	}

	public static boolean open(SocketAddress remote) throws IOException {
		SocketChannel socketChannel = SocketChannel.open();
		return socketChannel.connect(remote);
	}

	@Override
	public SelectableChannel configureBlocking(boolean b) throws IOException {
		checkOpen();

		blocking = b;
		return this;
	}

	public boolean connect(SocketAddress remote) throws IOException, ClosedChannelException {
		checkConnectionState(ConnectionState.NOT_CONNECTED);
		
		pendingConnAddr=remote;
		connState=ConnectionState.PENDING;
		return tryConnect();
	}
	
	public boolean finishConnect() throws IOException {
        //if already connected return true (according to Java API doc)
		if (isConnected())
			return true;

		checkConnectionState(ConnectionState.PENDING);  //throws NoConnectionPendingException if not pending
		
		return tryConnect();
	}

	private boolean tryConnect() throws IOException {
		if (blocking || isReadyToConnect()) {
			blockingConnect(pendingConnAddr);
			return true;
		} else
			return false;		
	}
	
	private synchronized void blockingConnect(SocketAddress remote) throws IOException {
		socket.connect(remote);
		readyToConnect=false;
		connState = ConnectionState.CONNECTED;
	}

	public SelectionKey register(Selector sel, int ops, Object att) throws ClosedChannelException {
		checkOpen();

		SelectionKey key = sel.registerKey(this);
		key.interestOps(ops);
		key.attach(att);
		addKey(key);

		return key;
	}

	public SelectionKey register(Selector sel, int ops) throws ClosedChannelException {
		return register(sel, ops, null);
	}

	public final SelectionKey keyFor(Selector sel) {
		if (keys != null) {
			for (SelectionKey key : keys) {
				if (key != null && key.selector() == sel) {
					return key;
				}
			}
		}
		return null;
	}

	/**
	 * read received bytes from <tt>this</tt> channel into <tt>ByteBuffer buf</tt>;</br>
	 * If <tt>buf.remaining()==0</tt> then nothing is read and 0 is returned, 
	 *      </br>otherwise</li>
	 *   <ul>
	 *   <li> if <tt>this</tt> channel is in blocking mode then either -1 (EOF) 
	 *        or the number of read bytes > 0 is returned;</li>
	 *   <li> if <tt>this</tt> channel is in non-blocking mode then a choice point for the
	 *        following two cases is created and both branches are explored by jpf:</li>
	 *     <ol>
	 *     <li> successful case  :  number of read bytes > 0 or -1 (EOF) is returned;
	 *     <li> unsuccessful case:  0 is returned.
	 *     </ol>  
	 *   </ul>
	 * 	 FIXME proper simulation of blocking read;
	 *       Because of pro-active polling of the peer after output operations, 
	 *       a thread calling read() will not go into state BLOCKED.
	 *       Hence, read() behaves almost like a non-blocking operation on the 
	 *       model level.  
	 * @param buf ByteBuffer to store the read data
	 * @return number of read bytes or -1 in case of EOF
	 * @throws IOException (NotYetConnectedException) if channel is not yet connected.
	 * @throws ClosedChannelException if channel has been closed prior to read.
	 */
	public int read(ByteBuffer buf) throws IOException, ClosedChannelException {
		checkConnectionState(ConnectionState.CONNECTED);
		int count = buf.remaining();
		if (count==0) return 0;  //no space in the buffer
		//nonblocking mode: explore both readable and non-readable execution branch. 
	   //Note: isReadable() must not be called in blocking mode 
		//      because it introduces a choice in the search space.
		if (!(blocking || isReadyToRead())) return 0;

		synchronized (readLock) {
			byte[] bytes=new byte[count];
			count = in.read(bytes);
			if (count>0) {
				buf.put(bytes, 0, count);
				readyToRead = false;
				// only reset readyToRead when data is consumed;
			        // remain readyToRead in case of EOF
			}
		}
		return count;
	}

	/**
	 * Write either none or all bytes.
	 * FIXME proper simulation of blocking write;
	 *       As for now, a thread calling write() will never go into state BLOCKED;
	 *       Hence, write() behaves like a non-blocking operation on the model level
	 *       (although the native peer might block on the jpf level).  
	 */
	public int write(ByteBuffer buf) throws IOException {
		checkConnectionState(ConnectionState.CONNECTED);
		int count = buf.remaining();
		if (count==0) return 0;  //nothing to write
		//nonblocking mode: explore both writable and non-writable execution branch. 
	   //Note: isWritable() must not be called in blocking mode 
		//      because it introduces a choice in the search space.
		if (!(blocking || isReadyToWrite())) return 0;

		synchronized (writeLock) {
			byte[] bytes=new byte[count];
			buf.get(bytes);
			out.write(bytes);
			readyToWrite = false;
		}
		return count;
	}

	public Socket socket(){
		return socket;
	}

	@Override
	public void close() throws IOException {
		if (isOpen()) {
			socket.close();
			if (keys != null) {
				for (SelectionKey key : keys) {
					key.cancel();
				}
			}
			connState=ConnectionState.CLOSED;
		}
	}

	@Override
	public final int validOps() {
		return (SelectionKey.OP_READ | SelectionKey.OP_WRITE | SelectionKey.OP_CONNECT);
	}

	public boolean isBlocking() {
		return blocking;
	}

	@Override
	public boolean isOpen() {
		return connState!=ConnectionState.CLOSED;
	}

	public boolean isConnectionPending() {
		return connState==ConnectionState.PENDING;
	}

	public boolean isConnected() {
		return connState==ConnectionState.CONNECTED;
	}
	
	boolean isReadyToConnect() {
		//FIXME discuss with Cyrille how to deal with the infinite case (channel will never become writable) 
		//      see isReadable() above 

		//return false if channel is not open or no connection attempt has been made (call of connect()) 
		if (!(isOpen() && isConnectionPending()))
			return false;

		//return true if the channel has already been set to readyForConnect in a previous call of isReadyForConnect()
		if (readyToConnect)
			return true;

		//if open and pending but not yet connected, explore both cases: remain in pending state or move to connected 
		//(by default start exploration with connected case unless configured differently)
		readyToConnect=Verify.getBoolean(FAILURE_FIRST);
		return readyToConnect;
	}

	boolean isReadyToRead() {
		//FIXME discuss with Cyrille how to deal with the infinite case (channel will never become readable) 
		//      possible solutions:
		//         - use JPF property search.depth_limit
        //         - introduce new property ''unsuccessful_nonblockingIO_limit''
		//         - avoid possible infinite polling loops in SUTs (ensure there is a timeout count whenever using nonblocking mode accept()/read()/write() 

		//return false if channel is not logically connected 
		if (!isConnected())
			return false;

		//return true if the channel has been set to readable in a previous call of isReadyForRead()
		//Note: readable is reset to false in read() 
		if (readyToRead)
			return true;

		//if not yet readable, explore both cases: readable and not readable 
		//(by default start exploration with readable case unless configured differently)
		readyToRead=Verify.getBoolean(FAILURE_FIRST); 
		return readyToRead;
	}

	boolean isReadyToWrite() {
		//FIXME discuss with Cyrille how to deal with the infinite case (channel will never become writable) 
		//      see isReadyForRead() above 

		//return false if channel is not logically connected 
		if (!isConnected())
			return false;

		//return true if the channel has been set to writable in a previous call of isReadyForWrite()
		//Note: writable is reset to false in write() 
		if (readyToWrite)
			return true;

		//if not yet writable, explore both cases: writable and not writable 
		//(by default start exploration with writable case unless configured differently)
		readyToWrite=Verify.getBoolean(FAILURE_FIRST); 
		return readyToWrite;
	}
	
	private void checkOpen() throws ClosedChannelException {
		if (!isOpen()) {
			throw new ClosedChannelException();
		}
	}

	private void checkConnectionState(ConnectionState expectedState) throws ClosedChannelException,ConnectionPendingException,AlreadyConnectedException,NoConnectionPendingException,NotYetConnectedException {
		if (expectedState==ConnectionState.CLOSED)  //if the current connection is expected to be closed..
			return;                                 //... nothing else to be checked;
		
		checkOpen();                                //otherwise make sure that the connection has not been closed already
		
		if (expectedState==ConnectionState.NOT_CONNECTED && isConnectionPending()) 
			throw new ConnectionPendingException();
		if (expectedState==ConnectionState.NOT_CONNECTED && isConnected()) 
			throw new AlreadyConnectedException();
		if (expectedState==ConnectionState.PENDING && !isConnectionPending()) 
			throw new NoConnectionPendingException();
		if (expectedState==ConnectionState.CONNECTED && !isConnected()) 
			throw new NotYetConnectedException();
	}

	private void addKey(SelectionKey key) {
		if (keys == null) {
			keys = new ArrayList<SelectionKey>();
		}
		keys.add(key);
	}
}

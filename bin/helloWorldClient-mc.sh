#!/bin/bash
. `dirname "$0"`/env.sh                   #change to project top directory and load library functions
                                            #such as 'exitOnNoFreePort' and 'delayedKill'
echo Model checking `pwd`/build/examples/helloWorld/HelloClient.class
echo 

exitOnNoFreePort 10001 10099             #Retrieve a free port between 10001 and 10099 in $_Port
                                         #  or exit if there is none
java -cp build/examples helloWorld.HelloServer $_Port &
ServerPID=$!                             #remember process ID of HelloServer
sleep 1                                  #Wait a bit to ensure that HelloServer is ready

 #execute jpf on HelloClient
../jpf-core/bin/jpf src/examples/helloWorld/HelloClient.jpf +target.args=$_Port

delayedKill "HelloServer" $ServerPID 1  #Kill HelloServer if it is still running after 1 second
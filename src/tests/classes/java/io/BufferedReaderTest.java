package classes.java.io;

import gov.nasa.jpf.util.test.TestJPF;

import org.junit.Test;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.CharArrayReader;

public class BufferedReaderTest extends TestJPF {

	@Test
	public void readLineTest() throws Exception {
		if (verifyNoPropertyViolation()) {

			BufferedReader br = new BufferedReader(new CharArrayReader(new char[0]));
			String sCurrentLine = br.readLine();
		}
	}
}

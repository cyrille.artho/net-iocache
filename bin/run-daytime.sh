#!/bin/sh
ulimit -t 7200

J=3
while [ $J -lt 4 ]
do
        J=`expr $J + 1`
	I=0
	while [ $I -lt 1 ]
	do
	        I=`expr $I + 1`
		echo "$J daytime clients, run $I..."
	        /usr/bin/time ./daytime-client-mc.sh $J >& daytime-client-4-353-$J-$I.out
		mv ~/client.log daytime-client-4-353-$J-$I.log
		sleep 5
	done
done

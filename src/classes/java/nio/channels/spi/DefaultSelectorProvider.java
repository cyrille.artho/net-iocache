package java.nio.channels.spi;

import java.io.IOException;
import java.nio.channels.SelectorImpl;

public class DefaultSelectorProvider extends SelectorProvider {
	public AbstractSelector openSelector() throws IOException {
		return new SelectorImpl();
	}
}

//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.daytime;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class DaytimeServer {

	public final static int DEFAULT_PORT = 1024; // 13;

	public static void main(String[] args) {
		int num_accept = Integer.parseInt(args[1]);
		int port = DEFAULT_PORT;

		if ((args != null) && (args.length > 0)) {
			try {
				port = Integer.parseInt(args[0]);
				if (port < 0 || port >= 65536) {
					System.out.println("Port must between 0 and 65535");
					return;
				}
			} catch (NumberFormatException e) {
				// use default port
			}
		}

		try {
			ServerSocket server = new ServerSocket(port);
			Socket connection = null;

			for (int i = 0; i < num_accept || num_accept == 0; i++) {
				try {
					connection = server.accept();
					OutputStreamWriter out = new OutputStreamWriter(connection
							.getOutputStream());
					Date now = new Date();
					out.write(now.toString() + "\r\n");
					out.flush();
					connection.close();
				} catch (IOException e) {
				} finally {
					try {
						if (connection != null)
							connection.close();
					} catch (IOException e) {
					}
					System.out.println("Connection closed.");
				}
			}
		} // end try
		catch (IOException e) {
			System.err.println(e);
		}

	} // end main

} // end DaytimeServer

package gov.nasa.jpf.network.cache;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.network.cache.event.Event;
import gov.nasa.jpf.network.cache.event.SendReadyEvent;
import gov.nasa.jpf.network.cache.event.RecvByteEvent;
import gov.nasa.jpf.network.cache.event.RecvEofEvent;
import gov.nasa.jpf.network.cache.event.RecvEvent;
import gov.nasa.jpf.network.cache.event.SendEvent;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class SendRecvCache {

	class Node {

		List<Node> children = new ArrayList<Node>();

		Event event;

		Node parent;

		int nid;
		int nconnId;      //ID of the logical connection under which the node has been created

		private int depth = 0;

		/**
		 * The ID of the last available recvEvent (only for a sendEvent node).
		 */
		private int limit;

		Node() {
			this.nid = numNodes++;  //nid is automatically increased
			this.nconnId=connId;    //store the current logical connection in the node for later reference 
		}

		int getDepth() {
			return depth;
		}

		/**
		 * Add a node as a child of this node, update the depth value of the added node, and initialize field
		 * <i>limit</i> of the child.
		 * 
		 * @param next
		 */
		void addChild(Node next) {
			children.add(next);
			next.parent = this;
			next.depth = depth + 1;

			// Initialize field "limit" of the child node
			if (event instanceof SendEvent)
				next.limit = limit;
			else
				next.limit = nid;

			log.finer("[TREE " + tree_id + "] Add " + next + " as a child of " + this);
		}

		/**
		 * 
		 * @return The first child of the node.
		 */
		Node getChild() {
			return children.get(0);
		}

		/**
		 * 
		 * @param sendEvent
		 * @return The child node that holds event <i>sendEvent</i>
		 */
		Node getChild(Event request) {
			for (Node n : children) {
				if (n.event.equals(request))
					return n;
			}

			assert false : "[RRTree.Node : getChild(Event)] No child is corresponding to the event " + request;
			return null;
		}

		boolean hasChild() {
			return !children.isEmpty();
		}

		boolean hasChild(Event request) {
			for (Node n : children) {
				if (n.event.equals(request))
					return true;
			}
			return false;
		}

		int numChildren() {
			return children.size();
		}

		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append("[Node: nid=" + nid + ", nconnId="+nconnId+"; event=");
			if (event instanceof SendEvent)
				sb.append("request ");
			else if (event instanceof RecvEvent)
				sb.append("response ");
			sb.append(event);
			sb.append("]");

			return sb.toString();
		}

		int getLimit() {
			return limit;
		}

		void setLimit(int newLimit) {
			assert newLimit >= 0 : "[Node : setLimit()] New limit is negative.";
			limit = newLimit;
		}
	}

	private static final int SEND_POINTER = 1;
	private static final int RECV_POINTER = -1;
	private static final int NONE = 0;

	/**
	 * <i>sendPtrs[i]</i> is the current sendEvent pointer position of logical connection <i>i</i>
	 */
	static List<Integer> sendPtrs = new ArrayList<Integer>();

	/**
	 * <i>recvPtrs[i]</i> is the current recvEvent pointer position of logical connection <i>i</i>
	 */
	static List<Integer> recvPtrs = new ArrayList<Integer>();

	int tree_id;

	int numNodes = 0;

	/**
	 * The list of all nodes in this tree.
	 */
	List<Node> nodes = new ArrayList<Node>();

	/**
	 * The active logical connection id.
	 */
	int connId;

	private static Logger log;

	public SendRecvCache(int id) {
		tree_id = id;
		createNode(); // initialize a tree with an empty node
	}

	// Create a new instance of node and add it into the list.
	private Node createNode() {
		Node n = new Node();
		nodes.add(n);
		return n;
	}

	/**
	 * Add a pair of pointers (a sendEvent pointer and a recvEvent pointer).
	 */
	public void addPointers() {
		sendPtrs.add(0);
		recvPtrs.add(0);
	}

	static void reset() {
		sendPtrs = new ArrayList<Integer>();
		recvPtrs = new ArrayList<Integer>();
	}

	static void setLogger(Logger logger) {
		log = logger;
	}

	/**
	 * Add new sendEvent event <i>e</i> to the tree. Assume that it is possible to add <i>e</i> into the tree.
	 * 
	 * @param e
	 */
	void addSendEvent(Event e) {
		addSendEvent(nodes.get(getSendPtr()), e);
	}

	/**
	 * Considering <i>start</i> as the root, search until a leaf node or a node whose child is a sendEvent node is found.
	 * Add the new sendEvent node containing event <i>e</i> to the node that the search was stopped.
	 * 
	 * @param start
	 * @param e
	 */
	private void addSendEvent(Node start, Event e) {
		Node next;
		boolean ok = false;

		while (!ok) {
			if (start.numChildren() == 0 || start.getChild().event instanceof SendEvent) {
				next = createNode();
				next.event = e;
				start.addChild(next);
				sendPtrs.set(connId, next.nid); // move the sendEvent pointer
				ok = true;
			} else
				start = start.getChild();
		}
	}

	/**
	 * adds recvEvent to the iocache.
	 * @param buffer  contains the data bytes to add to the iocache;
	 * @param off     start offset; 
	 * @param len     eof (-1) or number of bytes to add to the iocache;<br/> 
	 *                if len>0 then buffer[off], buffer[off+1], ..., buffer[off+len-1] 
	 *                are each added as RecvByteEvent to the iocache;<br/>
	 *                if len==-1 then the instance of RecvEofEvent object is added to the iocache. 
	 */
	void addRecvEvent(byte[] res, int off, int len) {
		// add the received message to the leaf
		if (len==0)         //nothing to do
			return;
		
		if (len==-1) {         //recvEvent is eof 
			log.fine("[TREE " + tree_id + "] add response " + RecvEofEvent.getInstance()+" to current branch of RRTree...");
			addRecvEvent(RecvEofEvent.getInstance());
		} else {
			log.fine("[TREE " + tree_id + "] add response '" + new String(res, off, len)+"' to current branch of RRTree...");
			for (int i = 0; i < len; i++) 
				addRecvEvent(new RecvByteEvent(res[off + i]));
		}
	}

	/**
	 * Add the recvEvent node containing event <i>e</i> to the leaf below the active sendEvent node, assuming that there
	 * are no branches between the active sendEvent node and the leaf.
	 * 
	 * @param e
	 */
	void addRecvEvent(Event e) {
		Node req = getSendNode();
		Node cur = req;
		Node next;

		while (cur.hasChild()) {
			assert cur.numChildren() == 1 : "[TREE, addResponse] Multiple children";
			cur = cur.getChild();
		}

		next = createNode();
		next.event = e;

		cur.addChild(next);
		req.setLimit(next.nid); // Update field "limit" of the sendEvent pointer to the latest recvEvent ID.
	}

	/**
	 * 
	 * @param v
	 * @return A path from the root node to node "v" (including the root node and "v").
	 */
	List<Integer> getPath(int v) {
		return getPath(0, v);
	}

	/**
	 * 
	 * @param u
	 * @param v
	 * @return If there is a path from "u" to "v" and node "v" is in a deeper
	 *         level than node "u", return the path from node "u" to node "v"
	 *         (including "u" and "v"). Otherwise, return an empty list.
	 */
	List<Integer> getPath(int u, int v) {
		List<Integer> path = new LinkedList<Integer>();
		Node cur = nodes.get(v);
		int id;

		path.add(v);
		while (cur.nid != 0 && cur.nid != u) {
			cur = cur.parent;
			id = cur.nid;
			path.add(0, id);
		}
		
		// Cannot find "u".
		if (cur.nid == 0 && u != 0)
			path.clear();

		return path;
	}

	/**
	 * 
	 * @return A path from the root node to the current sendEvent node inclusive, excluding any non-sendEvent nodes.
	 */
	List<Integer> getSendPath() {
		return getSendPath(0, getSendPtr());
	}

	/**
	 * 
	 * @param u
	 * @param v
	 * @return A path from node "u" to node "v" (including "u" and "v") excluding any non-sendEvent nodes. Assume that
	 *         there is a path from "u" to "v" and node "v" is in a deeper level than node "u".
	 */
	List<Integer> getSendPath(int u, int v) {
		List<Integer> path = new LinkedList<Integer>();
		Node cur = nodes.get(v);
		int id;

		path.add(v);
		while (cur.nid != u) {
			cur = cur.parent;
			id = cur.nid;

			if (cur.event instanceof SendEvent)
				path.add(0, id);
		}

		return path;
	}
	
	List<List<Integer>> getSendPaths() {
		List<List<Integer>> li = new LinkedList<List<Integer>>();
		int i = 0;
		
		for (int req : sendPtrs) {
			assert req < nodes.size() : "Connection " + i + ": Request ptr is larger than size";
			li.add(getSendPath(0, req));
			i++;
		}
		
		return li;
	}

	/**
	 * 
	 * @return A list of events in the path from the root node to the sendEvent pointer.
	 */
	public List<Event> getEventPath() {
		List<Integer> i_path = getPath(getSendPtr());
		List<Event> e_path = new LinkedList<Event>();

		for (int i : i_path) {
			e_path.add(nodes.get(i).event);
		}

		return e_path;
	}
	
	/**
	 * 
	 * @param u
	 * @param v
	 * @return A list of events in the path from node <i>u</i> to node <i>v</i>.
	 */
	public List<Event> getEventPath(int u, int v) {
		List<Integer> i_path = getPath(u, v);
		List<Event> e_path = new LinkedList<Event>();

		for (int i : i_path) {
			e_path.add(nodes.get(i).event);
		}

		return e_path;
	}

	Node getSendNode() {
		return nodes.get(getSendPtr());
	}

	Node getRecvNode() {
		return nodes.get(getRecvPtr());
	}

	/**
	 * The method decides that which pointer is higher, sendEvent pointer or recvEvent pointer.
	 * 
	 * @return <i>SEND_POINTER</i>, if the sendEvent pointer is higher; RECV_POINTER, if the recvEvent pointer is
	 *         higher; otherwise, false.
	 */
	int abovePointer() {
		int req = getSendNode().getDepth();
		int res = getRecvNode().getDepth();

		if (res > req)
			return SEND_POINTER;
		else if (res < req)
			return RECV_POINTER;
		else
			return NONE;
	}

	public int getSendPtr() {
		return sendPtrs.get(connId);
	}

	public void setSendPtr(int req_ptr) {
		sendPtrs.set(connId, req_ptr);
	}

	public int getRecvPtr() {
		return recvPtrs.get(connId);
	}

	public void setRecvPtr(int res_ptr) {
		recvPtrs.set(connId, res_ptr);
	}

	/**
	 * Move the active sendEvent pointer to a next sendEvent node. If there are more than one sendEvent node next to the
	 * current node, choose the node that contains event <i>e</i>.
	 * 
	 * @param e
	 *            A desired next event
	 */
	void moveSendPtr(Event e) {
		Node q_node;
		Node cur;
		int req_ptr = getSendPtr();

		q_node = nodes.get(req_ptr);

		if (q_node.getChild().event instanceof SendEvent) {
			cur = q_node.getChild(e);
		} else {
			cur = q_node.getChild();
			while (cur.event instanceof RecvEvent) {
				if (cur.hasChild(e))
					cur = cur.getChild(e);
				else
					cur = cur.getChild();
			}
		}

		sendPtrs.set(connId, cur.nid);
	}

	public void moveRecvPtr() {
		List<Integer> path;
		int above = abovePointer();
		int req_ptr = getSendPtr();
		int res_ptr = getRecvPtr();
		Node res = nodes.get(res_ptr);
		Node cur;

		if (above == SEND_POINTER || above == NONE) {
			cur = res.getChild();
			assert cur.event instanceof RecvEvent : "[RRTree : moveResPtr()] cannot move the response pointer further.";
			assert cur.nid != res_ptr : "[RRTree : moveResPtr()] Response pointer and its child have the same ID.";
			recvPtrs.set(connId, cur.nid);
		} else {
			assert above == RECV_POINTER : "[RRTree : moveResPtr()]";
			path = getPath(res_ptr, req_ptr);
			for (int id : path) {
				if (id != res_ptr && nodes.get(id).event instanceof RecvEvent) {
					recvPtrs.set(connId, id);
					return;
				}
			}

			cur = nodes.get(req_ptr);
			assert cur.hasChild() : "[RRTree : moveResPtr()] cannot move the response pointer further.";
			cur = cur.getChild();
			assert cur.event instanceof RecvEvent : "[RRTree : moveResPtr()] cannot move the response pointer further.";
			recvPtrs.set(connId, cur.nid);
		}
	}

	/**
	 * returns the number of <tt>RecvEvent</tt> objects available to be read from the 
	 * iocache, associated with the most recently sent sendEvent; </br>
	 * Available {@link RecvEvent} objects may be 0 ore more 
	 * {@link RecvByteEvent} objects, representing a string of data bytes, 
	 * possibly followed by a <b>single</b> {@link RecvEofEvent} object, 
	 * representing that EOF has been reached. For instance, if recvEvent 
	 * objects (65, 66, <EOF>) are available to be read
	 * from the cache, recvEventAvailable() returns 3.</br> 
	 * It is not possible that {@link RecvByteEvent} objects in the sequel of a
	 * a {@link RecvEofEvent} are available in the cache as a recvEvent 
	 * to a <b>single</b> preceding sendEvent. <br/>
	 * If <tt>recvEventAvailable()</tt> returns a value >0 then 
	 *      <tt>nextRecvEvent()</tt> may be called to receive the next recvEvent 
	 *      (<tt>RecvByteEvent</tt> or <tt>RecvEofEvent</tt>) from the iocache. 
	 * @return number of <tt>RecvEvent</tt> objects ready to be read. 
	 * @see #nextRecvEvent
	 * @see #moveRecvPtr
	 */
	public int recvEventAvailable() {
		List<Integer> path;
		int req_ptr = getSendPtr();
		int res_ptr = getRecvPtr();
		int count = 0;
		int limit = nodes.get(req_ptr).limit;

		path = getPath(res_ptr, limit);

		for (int id : path) {
			if (id != res_ptr && nodes.get(id).event instanceof RecvEvent)
				count++;
		}

		return count;
	}

	/**
	 *  returns the <tt>RecvEvent</tt> at the current position of the iocache;</br>
	 *  <b>Pre-condition</b>: At least one RecvEvent object must be ready to be
	 *  read at the current position.  
	 * @return RecvEvent at the current position of the iocache.
	 */
	protected RecvEvent getRecvEvent() {
		assert recvEventAvailable()>0 : "There is no RecvEvent object ready to be read in iocache.\n" +
				"Check if responseAvailable()>0 before calling getResponseEvent()";
		Node req = getSendNode();
		Node res = getRecvNode();
		Node cur;
		List<Integer> path;
		int req_ptr = getSendPtr();
		int res_ptr = getRecvPtr();

		if (res.getDepth() < req.getDepth()) {
			// search from res_ptr to req_ptr
			path = getPath(res_ptr, req_ptr);
			for (int id : path) {
				if (id != res_ptr && nodes.get(id).event instanceof RecvEvent) {
					return (RecvEvent) nodes.get(id).event;
				}
			}

			// search from req_ptr downward
			cur = req;
		} else {
			cur = res;
		}

		res = cur.getChild();

		assert res.event instanceof RecvEvent : "[RRTree : nextResponseEvent()] the found node is not a response node";
		return (RecvEvent) res.event;
	}

	
	/**
	 * 
	 * @return Limit of the sendEvent node. Limit is the ID of the last available recvEvent node for a sendEvent node.
	 */
	int limit() {
		return getSendNode().getLimit();
	}

	/**
	 * Check if <i>e</i> is a sendEvent event next to the active sendEvent pointer.
	 * 
	 * @param e
	 *            A sendEvent.
	 * @return True if <i>e</i> is a sendEvent next to the active sendEvent pointer; false, otherwise.
	 */
	boolean isNextSendEvent(Event e) {
		Node q_node;
		Node cur;
		int req_ptr = getSendPtr();

		q_node = nodes.get(req_ptr);

		if (!q_node.hasChild())
			return false;
		else if (q_node.getChild().event instanceof SendEvent) {
			return q_node.hasChild(e);
		} else {
			cur = q_node.getChild();
			while (cur.event instanceof RecvEvent) {
				if (cur.hasChild(e))
					cur = cur.getChild(e);
				else if (cur.hasChild())
					cur = cur.getChild();
				else
					return false;
			}

			return cur.event.equals(e);
		}
	}

	private boolean canAdd(Node start) {
		if (start.event instanceof SendEvent)
			return false;
		else if (start.numChildren() == 0)
			return true; // leaf is a recvEvent
		else {
			for (Node n : start.children) {
				if (canAdd(n))
					return true;
			}
			return false;
		}
	}

	/**
	 * Can <i>e</i> be added into some branch of the sendEvent pointer node?
	 * We apply the 'independent connection' assumption, i.e. a separate io-store is maintained for each connection 
	 * Caching strategy: create separate branch for each connection on root node.
     *
	 * @param e inherited from abstract super class but not used here
	 * @return
	 */
	boolean canAdd(Event e) {
		Node q_node = nodes.get(getSendPtr());

		//  a new node can be added as long as the current node does not have 
		//  already sendEvent ancestors of the current connection 
		boolean noChildOfCurrentConnection=true; //assume that there is no child node of the current connection (in this case, the recvEvent event can be added)
		for (Node n : q_node.children) {
			if (n.nconnId!=connId)             //ignore branches of connections different from the current connection which may be the case if the current node is root  
				continue;                     
			noChildOfCurrentConnection=false;  //child of current connection found!
			if (canAdd(n))                  //check if a sendEvent node can be added to this child
				return true;
		}
		return noChildOfCurrentConnection;     //false if there has been child nodes of the current connection and none of them allows adding a new sendEvent node
	}

	boolean hasMessage() {
		// If the tree has a message, it must not be empty.
		// If Node 1 is not a ready event, it must be a message.
		// If the number of nodes is greater than two, at least one message is 
		// certainly in the tree. 
		return !isEmpty() && (!nodes.get(1).event.equals(SendReadyEvent.getInstance()) || nodes.size() > 2);
	}
	
	/**
	 * A tree is empty, if it has no node other than the root node.
	 * 
	 * @return
	 */
	boolean isEmpty() {
		return !nodes.get(0).hasChild();
	}

	public int getConnId() {
		return connId;
	}

	public void setConnId(int connId) {
		assert connId >= 0 && connId < sendPtrs.size() : "[RRTree : setConnId()] Invalid connection ID, " + connId;
		this.connId = connId;
	}

	int numLogicalConnection() {
		return sendPtrs.size();
	}
	
	/**
	 * Check if the active sendEvent pointer is at the beginning position.
	 * @return True if the active sendEvent pointer is at the beginning.
	 */
	boolean sendPtrAtRoot() {
		return getSendPtr() == 0;
	}

	static {
		log = JPF.getLogger("gov.nasa.jpf.network.cache.CacheLayer");
	}
	
}

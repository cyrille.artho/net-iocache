package gov.nasa.jpf.network.listener;

import java.io.File;
import java.io.IOException;

import gov.nasa.jpf.network.SystemInfo;
import static gov.nasa.jpf.network.SystemInfo.jpfConfig;
import gov.nasa.jpf.network.cache.*;
import gov.nasa.jpf.network.cache.strategy.*;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.util.JPFSiteUtils;

public class VirtualizationCacheNotifier extends CacheNotifier {

	public void searchFinished(Search search) {
		ProcessBuilder builder;
		Process p;
		CheckpointCacheLayer cache = (CheckpointCacheLayer) CacheLayer
				.getInstance();
		String kill = cache.killPeerScript();

		builder = new ProcessBuilder(kill);

		super.searchFinished(search);

		try {
			p = builder.start();
			p.waitFor();
		} catch (IOException e) {
			assert false : "[VIRT_CacheNotifier] Fail to kill MTCP process";
		} catch (InterruptedException e) {
			assert false : "[VIRT_CacheNotifier] Fail to kill MTCP process";
		}

		cache.onFinish();
	}

	public void searchStarted(Search search) {
		super.searchStarted(search);

		String sValue;
		boolean dmtcpEnabled = jpfConfig.getBoolean(SystemInfo.DMTCP_ENABLED,
				false);
		boolean value = false;

		if (jpfConfig.containsKey(SystemInfo.VIRTUAL_MODE)) {
			value = jpfConfig.getBoolean(SystemInfo.VIRTUAL_MODE);
			CacheLayer.setVirtualMode(value);
			CacheLayer.setDMTCPEnabled(dmtcpEnabled);
			CacheLayer.setCacheDir(getCacheDir());

			// Exclusive options for the DMTCP cache.
			if (dmtcpEnabled) {
				int sutPort = jpfConfig.getInt(SystemInfo.SUT_PRIVATE_PORT,
						8791);

				// In case Proxy Cache is used.
				DMTCPProxyCacheLayer.setSutCommPort(sutPort);
			}
		}

		// If the virtual mode is activated.
		if (value) {
			CheckpointCacheLayer c;

			assert jpfConfig.containsKey(SystemInfo.CHECKPOINT_DIR) : "Invalid key";
			sValue = jpfConfig.getString(SystemInfo.CHECKPOINT_DIR);
			CheckpointCacheLayer.setCheckpointDir(sValue);
			assert CacheLayer.getInstance() instanceof CheckpointCacheLayer;

			c = (CheckpointCacheLayer) CacheLayer.getInstance();
			c.onStart();
			
			// Set strategy if given by user.
			if (jpfConfig.containsKey(SystemInfo.STRATEGY)) {
				sValue = jpfConfig.getProperty(SystemInfo.STRATEGY);
				
				if (sValue.equals("always"))
					c.setStrategy(new AlwaysStrategy(c));
				else if (sValue.equals("io"))
					c.setStrategy(new IOOnlyStrategy(c));
				else if (sValue.equals("nd"))
					c.setStrategy(new NDOnlyStrategy());
				else
					assert false : "[VirtualizationCacheNotifier : searchStarted] Incorrect strategy";
			}
		}
	}

	/**
	 * get location of jpf-net-iocache from site.properties
	 * 
	 * @return null if it doesn't exist
	 */
	private static String getCacheDir() {
		String userHome = System.getProperty("user.home");
		File f = new File(userHome, "jpf/site.properties");
		if (!f.isFile()) {
			f = new File(userHome, ".jpf/site.properties");
			if (!f.isFile()) {
				return null;
			}
		}

		String path = JPFSiteUtils.getMatchFromFile(f.getAbsolutePath(),
				"jpf-net-iocache");
		if (path != null) {
			File coreDir = new File(path);
			if (coreDir.isDirectory()) {
				return coreDir.getAbsolutePath();
			}
		}

		return null;
	}

}

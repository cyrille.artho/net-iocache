#define BACKLOG 10
#define MC_SOCK_NAME "slave-mc"

// List of operators
#define OP_CONNECT 1
#define OP_ACCEPT 2
#define OP_CLOSE 3
#define OP_CREATE_SOCK 4
#define OP_BIND 5

#define MAX_FDS 8

typedef struct sockaddr_un sockaddr_un;
typedef struct sockaddr sockaddr;
typedef struct timespec timespec;
typedef struct timeval timeval;
typedef struct addrinfo addrinfo;
typedef struct sockaddr_storage Sockaddr_storage;

extern int _real_close(int fd);
extern int _real_socket(int domain, int type, int protocol);
extern int _real_connect(int sockfd, const sockaddr *addr, socklen_t addrlen);
extern pid_t _real_getpid();

int sendfd(int s, int fd, char *buf, int len);
int recvfd(int s);


const long SELECT_TIMEOUT_USEC = 5000L;
const int RECV_PACKET_MAX_SIZE = 32;
const int FIFO_NAME_MAX_SIZE = 256;

const char *CHKPNT_DIR = "CHKPNT_DIR";
const char *PIPE_NAME = "ckpt_sig";


inline static int64_t timeval_to_ms(const timeval *tv) {
	return tv->tv_sec * 1000LL + tv->tv_usec / 1000;
}

inline static int64_t cur_time_ms() {
	timeval tv;

	gettimeofday(&tv, NULL);
	return timeval_to_ms(&tv);
}

// Return the port number of a given address
uint get_port(const Sockaddr_storage *addr) {
	struct sockaddr_in6 *addr_v6;
	struct sockaddr_in *addr_v4;

	if (addr->ss_family == AF_INET) {
		addr_v4 = (struct sockaddr_in *) addr;
		return ntohs(addr_v4->sin_port);
	} else if (addr->ss_family == AF_INET6) {
		addr_v6 = (struct sockaddr_in6 *) addr;
		return ntohs(addr_v6->sin6_port);
	}

	return 0;
}

uint local_port(int sockfd) {
	struct sockaddr_storage addr;
	socklen_t addrlen = sizeof addr;

	memset(&addr, 0, sizeof addr);
	getsockname(sockfd, (struct sockaddr *) &addr, &addrlen);

	return get_port(&addr);
}

uint remote_port(int sockfd) {
	struct sockaddr_storage addr;
	socklen_t addrlen = sizeof addr;

	memset(&addr, 0, sizeof addr);
	getpeername(sockfd, (struct sockaddr *) &addr, &addrlen);

	return get_port(&addr);
}

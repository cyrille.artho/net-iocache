package java.nio.channels.spi;

import java.util.concurrent.atomic.AtomicBoolean;

import java.io.IOException;
import java.nio.channels.Selector;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public abstract class AbstractSelector extends Selector {
	private AtomicBoolean selectorOpen = new AtomicBoolean(true);

	public final void close() throws IOException {
		boolean open = selectorOpen.getAndSet(false);
		if (open == false) {
			return;
		}

		closeImpl();
	}

	protected abstract void closeImpl() throws IOException;

	protected AbstractSelector(){
		super();
	}

	public boolean isOpen() {
		return selectorOpen.get();
	}
}

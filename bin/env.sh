#!/bin/bash

#this sets variables used in scripts executing test cases and sources 
#  functions.source - a library of commonly used functions

# Check if running on cygwin and adjust environment-specific settings
case "`uname`" in 
	CYGWIN*)
		echo OS `uname` - using settings for cygwin:
		MaxHeapMem=1024
		PathConverter="cygpath -wp "
		echo "   MaxHeapMem=${MaxHeapMem}m; using '${PathConverter}' for Unix-to-Windows path conversion."
		;;
	*)
		echo OS `uname` - using settings for Linux:
		MaxHeapMem=2048
		PathConverter="echo "
		echo "   MaxHeapMem=${MaxHeapMem}m; no path conversion."		
esac

# Remember the directory of this script and change to the parent directory (project root directory)
cd `dirname "$0"`
SCRIPT_DIR=`pwd`
cd ..

# Check if all necessary variables are set. If not, set them with default values.

if [ -z "${NETWORK_PACKAGE}" ]
then
	NETWORK_PACKAGE="gov.nasa.jpf.network"
fi

if [ -z "${PROJ_TOP}" ]
then
	#cd `pwd | sed -e 's/\/bin$//'`	# The parent directory (..)
	PROJ_TOP=.   #The current directory
fi

if [ -z "${NETWORK_DIR}" ]
then
	NETWORK_DIR=`echo ${NETWORK_PACKAGE} | sed 's|\.|/|g'` # replace . with /
fi

if [ -z "${SRC_DIR}" ]
then
	SRC_DIR=${PROJ_TOP}/src
fi

if [ -z "${MODEL_DIR}" ]
then
	MODEL_DIR=${SRC_DIR}/classes
fi

if [ -z "${EXAMPLE_DIR}" ]
then
	EXAMPLE_DIR=${SRC_DIR}/examples
fi

if [ -z "${BUILD_DIR}" ]
then
	BUILD_DIR=${PROJ_TOP}/build
fi

if [ -z "${BUILD_MAIN_DIR}" ]
then
	BUILD_MAIN_DIR=${BUILD_DIR}/main
fi

# Model classes
if [ -z "${BUILD_CLASSES_DIR}" ]
then
	BUILD_CLASSES_DIR=${BUILD_DIR}/classes
fi

# Peer classes
if [ -z "${BUILD_PEERS_DIR}" ]
then
	BUILD_PEERS_DIR=${BUILD_DIR}/peers
fi

if [ -z "${BUILD_EXAMPLES_DIR}" ]
then
	BUILD_EXAMPLES_DIR=${BUILD_DIR}/examples
fi

if [ -z "${LIB_DIR}" ]
then
	LIB_DIR=${PROJ_TOP}/lib
fi

if [ -z "${CBUILD_DIR}" ]
then
	CBUILD_DIR=${PROJ_TOP}/c-build
fi

# The directory that contains the library files (*.so.*) required in native method calls.
if [ -z "${JNI_LIB_DIR}" ]
then
	JNI_LIB_DIR=${CBUILD_DIR}
fi

if [ -z "${DMTCP_DIR}" ]
then
	DMTCP_DIR=${PROJ_TOP}/tools/dmtcp
fi

if [ -z "${DMTCP_SRC_DIR}" ]
then
	DMTCP_SRC_DIR=${PROJ_TOP}/tools/dmtcp/dmtcp/src
fi

if [ -z "${SERVER_LOG}" ]
then
	SERVER_LOG=~/server.log
fi

if [ -z "${CLIENT_LOG}" ]
then
	CLIENT_LOG=~/client.log
fi

if [ -z "${JPF_HOME}" ]
then
	JPF_HOME="${PROJ_TOP}/../jpf-core"
fi

if [ -z "${JPF_SRC}" ]
then
	JPF_SRC="${JPF_HOME}/src"
fi

if [ -z "${JPF_SRC_MAIN}" ]
then
	JPF_SRC_MAIN="${JPF_SRC}/main"
fi

if [ -z "${JPF_SRC_MODELS}" ]
then
	JPF_SRC_MODELS="${JPF_SRC}/classes"
fi

if [ -z "${JPF_SRC_PEERS}" ]
then
	JPF_SRC_PEERS="${JPF_SRC}/peers"
fi

if [ -z "${JPF_BUILD}" ]
then
	JPF_BUILD="${JPF_HOME}/build"
fi

if [ -z "${VM_ARG}" ]
then
	VM_ARG="-Xmx${MaxHeapMem}m -ea"
fi

# If a node uses the default signal number, use this number instead.
if [ -z "${MTCP_SIGNAL_NUMBER}" ]
then
	MTCP_SIGNAL_NUMBER="18"
fi

if [ -z "${MTCP_PID_FILE}" ]
then
	MTCP_PID_FILE="_mtcp_pid.txt"
fi

if [ -z "${PEER_PID_FILE}" ]
then
	PEER_PID_FILE="_peer_pid.txt"
fi

if [ -z "${CLASSPATH}" ]
then
	CLASSPATH=`$PathConverter "${BUILD_MAIN_DIR}:${BUILD_CLASSES_DIR}:${BUILD_PEERS_DIR}:${BUILD_EXAMPLES_DIR}:${JPF_BUILD}/jpf.jar:${JPF_BUILD}/jpf-classes.jar:${JPF_BUILD}/examples:${JPF_HOME}/lib/bcel.jar:${LIB_DIR}/webdavlib-2.0.jar:${LIB_DIR}/xercesImpl.jar"`
fi

if [ -z "${RUN_EXAMPLE_CMD}" ]
then
	RUN_EXAMPLE_CMD="java ${VM_ARG} -jar ${JPF_BUILD}/RunJPF.jar"
fi

if [ -z "${RUN_JPF_JNILIB}" ]
then
	RUN_JPF_JNILIB="java ${VM_ARG} -Djava.library.path=${JNI_LIB_DIR} -jar ${JPF_BUILD}/RunJPF.jar"
fi

if [ -z "${OPTION_PREFIX}" ]
then
	OPTION_PREFIX="jpf-net-iocache"
fi

if [ -z "${OPT_LAZY}" ]
then
	OPT_LAZY="${OPTION_PREFIX}.lazy.connect"
fi

if [ -z "${OPT_VIRTUAL_MODE}" ]
then
	OPT_VIRTUAL_MODE="${OPTION_PREFIX}.virtual.mode"
fi

if [ -z "${OPT_DMTCP_ENABLED}" ]
then
	OPT_DMTCP_ENABLED="${OPTION_PREFIX}.dmtcp.enabled"
fi

if [ -z "${OPT_MAIN_TERMINATION}" ]
then
	OPT_MAIN_TERMINATION="${OPTION_PREFIX}.main.termination"
fi

if [ -z "${OPT_CHECKPOINT_DIR}" ]
then
	OPT_CHECKPOINT_DIR="${OPTION_PREFIX}.checkpoint.dir"
fi

if [ -z "${OPT_BOOT_CMD}" ]
then
	OPT_BOOT_CMD="${OPTION_PREFIX}.boot.peer.command"
fi

if [ -z "${OPT_SOURCEPATH}" ]
then
	OPT_SOURCEPATH="${OPTION_PREFIX}.sourcepath"
fi

if [ -z "${OPT_SUT_PORT}" ]
then
	OPT_SUT_PORT="${OPTION_PREFIX}.sut_private_port"
fi

if [ -z "${JPF_ARG}" ]
then
	JPF_ARG="+classpath=${BUILD_EXAMPLES_DIR} +report.console.property_violation=error,trace +listener=gov.nasa.jpf.network.listener.CacheNotifier,gov.nasa.jpf.network.listener.CacheLogger +search.min_free=16K"
fi

if [ -z "${DMTCP_CHECKPOINT}" ]
then
	DMTCP_CHECKPOINT="${DMTCP_DIR}/bin/dmtcp_checkpoint -q"
fi

if [ -z "${DMTCP_COORDINATOR}" ]
then
	DMTCP_COORDINATOR="${DMTCP_DIR}/bin/dmtcp_coordinator"
fi

if [ -z "${DMTCP_RESTART}" ]
then
	DMTCP_RESTART="${DMTCP_DIR}/bin/dmtcp_restart"
fi

if [ -z "${DMTCP_COMMAND}" ]
then
	DMTCP_COMMAND="${DMTCP_DIR}/bin/dmtcp_command"
fi

if [ -z "${DMTCP_QUIT}" ]
then
	DMTCP_QUIT="${DMTCP_COMMAND} --quiet -q"
fi

if [ -z "${DMTCP_SAVE}" ]
then
	DMTCP_SAVE="${DMTCP_COMMAND} --quiet -c"
fi

source $SCRIPT_DIR/functions.source
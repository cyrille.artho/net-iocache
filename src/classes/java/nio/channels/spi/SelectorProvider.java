package java.nio.channels.spi;

import java.io.IOException;

public abstract class SelectorProvider {

	private static SelectorProvider provider;

	public static SelectorProvider provider(){
		if (provider == null) {
			provider = new DefaultSelectorProvider();
		}
		return provider;
	}

	public abstract AbstractSelector openSelector() throws IOException;
}

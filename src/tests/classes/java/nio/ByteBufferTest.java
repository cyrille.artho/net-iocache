package classes.java.nio;

import gov.nasa.jpf.util.test.TestJPF;

import org.junit.Test;
import static org.junit.Assert.*;

import java.nio.BufferUnderflowException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;

public final class ByteBufferTest extends TestJPF {
	@Test
	public void allocateShouldThrowExceptionIfSizeIsNegative() {
		if (verifyNoPropertyViolation()) {
			try {
				ByteBuffer.allocate(-1);
				fail();
			}
			catch (IllegalArgumentException e) {
			}
		}
	}

	@Test
	public void allocateDirectShouldThrowExceptionIfSizeIsNegative() {
		if (verifyNoPropertyViolation()) {
			try {
				ByteBuffer.allocateDirect(-1);
				fail(); 
			}
			catch (IllegalArgumentException e) {
			}
		}
	}

	@Test
	public void duplicateShouldShareContent() {
		if (verifyNoPropertyViolation()) {
			final ByteBuffer original  = ByteBuffer.allocate(16);
			original.put("foobar".getBytes());

			final ByteBuffer duplicate = original.duplicate();

			assertEquals(original.array(), duplicate.array());

			original.put("baz".getBytes());

			assertEquals(original.array(), duplicate.array());
		}
	}

	@Test
	public void getCharShouldThrowExecptionOnBufferUnderflow() {
		if (verifyNoPropertyViolation()) {
			char c;
			final ByteBuffer buf = ByteBuffer.allocate(3);
			try {
				c = buf.getChar();
			} catch (BufferUnderflowException e) {
				fail(); 
			}
			try {
				c = buf.getChar();
				fail(); 
			}
			catch (BufferUnderflowException e) {}
		}
	}

	@Test
	public void putCharShouldThrowExecptionOnBufferOverflow() {
		if (verifyNoPropertyViolation()) {
			char c = '\n';
			final ByteBuffer buf = ByteBuffer.allocate(3);
			try {
				buf.putChar(c);
			} catch (BufferOverflowException e) {
				fail(); 
			}
			try {
				buf.putChar(c);
				fail(); 
			}
			catch (BufferOverflowException e) {}
		}
	}

	@Test
	public void getCharShouldReturnCharsInsertedByputChar() {
		if (verifyNoPropertyViolation()) {
			char c1,c2;
			final ByteBuffer buf = ByteBuffer.allocate(4);
			try {
				buf.putChar('a'); 
				buf.putChar('\n'); 
			} catch (BufferOverflowException e) {
				fail(); 
			}
			try {
				buf.rewind();
				c1=buf.getChar();
				c2=buf.getChar();
				assertEquals(c1, 'a');
				assertEquals(c2, '\n');
				c1=buf.getChar(0);
				c2=buf.getChar(2);
				assertEquals(c1, 'a');
				assertEquals(c2, '\n'); 
			}
			catch (BufferUnderflowException e) {
				fail();
			}
		}
	}

	// For debug
	private String getString(final ByteBuffer bb) {
		byte[] bytes = new byte[bb.remaining()];
		bb.get(bytes);
		return new String(bytes);
	}
}

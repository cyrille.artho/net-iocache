//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

import gov.nasa.jpf.vm.DefaultAttributor;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.classfile.Field;

/** Fixed isMethodAtomic implementation. */
public class FixedAttributor extends DefaultAttributor {
  
  public boolean isMethodAtomic (JavaClass jc, Method mth, String uniqueName) {
    if (jc.getPackageName().startsWith( "java.")) {
      String clsName = jc.getClassName();
      
      if (clsName.equals("java.net.ServerSocket")) {
        return false;
      }
      if (clsName.equals("java.net.Socket")) {
        return false;
      }
      if (clsName.equals("java.io.BufferedReader")) {
	return false;
      }
      if (clsName.equals("java.io.PipedInputStream")) {
        if ((uniqueName.startsWith("receive(")) ||
	    (uniqueName.startsWith("read("))) {
          return false;
        }
      }
      if (clsName.equals("java.lang.WaitForShutdown")) {
        return false;
      }
    }
    return super.isMethodAtomic(jc, mth, uniqueName);
  }
}

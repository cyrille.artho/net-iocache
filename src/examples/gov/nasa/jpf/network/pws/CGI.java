package gov.nasa.jpf.network.pws;
/*  CGI.java -- classes and functions for CGI processing

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.net.*;
import java.io.*;
import java.util.*;

public abstract class CGI
{
	/* CGI version information */
	private static int cgiMajor = 1;
	private static int cgiMinor = 1;
	

	/* the runCGI function is now only usable with GET requests. No post, sorry. Not yet atleast :-) */
	public static Process runCGI(String cgiString, String Path, String queryString, String remoteAddr,
	                             String remoteHost, String requestMethod, String serverName)
	{
		int environmentCount = 10;
		String [] environment = new String[environmentCount];
		
		/* Environment variables
		    AUTH_TYPE = "Basic" | "Digest" | token | "" (not reqd unless set)
		    CONTENT_LENGTH = "" | 1*digit
		    CONTENT_TYPE = "" | media-type (eg application/x-www-form-urlencoded)
		    GATEWAY_INTERFACE = "CGI" "/" major "." minor
		    PATH_INFO = "" | ( "/" path )
		    QUERY_STRING = query-string --> the variables
		    ... see the draft-standard. 
		    
		    Preliminary support for PHP will be included here. It can be extended lateron.
		*/
		Path = Path.replace('\\','/').trim();
        environment[0] = "SERVER_SOFTWARE=" + pws.serverName() + "/" + pws.serverVersion();
        environment[1] = "SERVER_NAME=" + serverName;
        environment[2] = "GATEWAY_INTERFACE=CGI/" + cgiMajor + "." + cgiMinor;
        
        environment[3] = "SERVER_PROTOCOL=" + "HTTP/1.1";
        environment[4] = "SERVER_PORT=" + pws.getSetting("port");
        environment[5] = "REQUEST_METHOD=" + requestMethod;
        /* PATH_INFO
        The extra path information, as given by the client. In other    words, scripts can be accessed by their virtual pathname, followed    by extra information at the end of this path. The extra    information is sent as PATH_INFO. This information should be    decoded by the server if it comes from a URL before it is passed    to the CGI script.
        
        PATH_TRANSLATED
        The server provides a translated version of PATH_INFO, which takes    the path and does any virtual-to-physical mapping to it.*/
        environment[6] = "SCRIPT_NAME=" + cgiString;
        environment[7] = "QUERY_STRING=" + queryString;
        environment[8] = "REMOTE_HOST=" + remoteHost;
        environment[9] = "REMOTE_ADDR=" + remoteAddr;
        
        /*AUTH_TYPE
        If the server supports user authentication, and the script is    protects, this is the protocol-specific authentication method used    to validate the user.
        
        REMOTE_USER
        If the server supports user authentication, and the script is    protected, this is the username they have authenticated as.
        
        REMOTE_IDENT
        If the HTTP server supports RFC 931 identification, then this    variable will be set to the remote user name retrieved from the server. Usage of this variable should be limited to logging only.   
        
        CONTENT_TYPE
        For queries which have attached information, such as HTTP POST and    PUT, this is the content type of the data.
        
        CONTENT_LENGTH
        The length of the said content as given by the client.*/        
        


        /* call the binary, get the info, and return it.*/
        try
        {
            Misc.putSysMessage(0, "Executing: " + cgiString);
            for (int i=0; i < environmentCount; i++)
                Misc.putSysMessage(0, "Environment: " + environment[i]);
            Process Handler = Runtime.getRuntime().exec(cgiString, environment);
            return Handler;
        }
        catch (IOException e)
        {
            Misc.putSysMessage(0, "Error " + e + " while execing " + cgiString);
            return null;
        }
        
   	}
}

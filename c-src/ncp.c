#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <dlfcn.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <assert.h>

#include "ncp.h"


static const int RECV_TIMEOUT = 20000;
static const int CONNECT_TIMEOUT = 15000;
// Number of attempts to send a packet
static const int RETRY = 5;
// Timeout to wait for an acknowledgement.
static const int SHORT_TIMEOUT = 400;
// Proxy waits "PROXY_POLL_TIMEOUT" milliseconds for an incoming packet in one loop.
static const int PROXY_POLL_TIMEOUT = 20;
// Temporary reserved file descriptors for the pipe.
static const int PIPE_READ = 30;
static const int PIPE_WRITE = 31;

static pthread_mutex_t proxy_mutex = PTHREAD_MUTEX_INITIALIZER;


SocketTable* sock_table[NUM_SOCKETS];
pthread_t 	proxy 				= 0;
int			nd_flag				= 0;

pthread_mutex_t ncp_save_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t ncp_save_cond = PTHREAD_COND_INITIALIZER;
int ncp_busy = 0;
int ncp_save_req = 0;

static int write_one_byte_to_pipe() {
	int rtnval;

	do {
		rtnval = write(PIPE_WRITE, "", 1);
	}
	while (rtnval == 0 || (rtnval < 0 && errno == EINTR));

	return rtnval;
}

static int read_all_from_pipe() {
	int rv;
	char buf[64];

	do {
		rv = read(PIPE_READ, buf, sizeof(buf));
	}
	while (rv > 0 || (rv < 0 && errno == EINTR));

	return rv;
}

inline static int64_t timeval_to_ms(const Timeval *tv) {
	return tv->tv_sec * 1000LL + tv->tv_usec / 1000;
}

static int64_t cur_time_ms() {
	Timeval tv;

	gettimeofday(&tv, NULL);
	return timeval_to_ms(&tv);
}

// [override]
//int pthread_create(pthread_t *thread,
//		const pthread_attr_t *attr, void *(*start_routine)(void*),
//		void *arg) {
//	static int (*real_pthread_create)(pthread_t *, const pthread_attr_t *, void *(*)(void*), void *) = NULL;
//	int createAttr = (attr == NULL);
//	int rv;
//
//	real_pthread_create = real_pthread_create ? real_pthread_create : dlsym(RTLD_NEXT, "pthread_create");
//
//	if (createAttr) {
//		pthread_attr_t low_size;
//		pthread_attr_init(&low_size);
//		int ss = pthread_attr_setstacksize(&low_size, 16384);
//		printf("Low stack size: %d\n", ss);
//		rv = real_pthread_create(thread, &low_size, start_routine, arg);
//		pthread_attr_destroy(&low_size);
//	}
//	else {
//		printf("Normal stack size\n");
//		rv = real_pthread_create(thread, attr, start_routine, arg);
//	}
//
//	return rv;
//}

// [override]
int socket(int domain, int type, int protocol) {
	int p;

	pthread_mutex_lock(&proxy_mutex);
	if (!proxy) {
		ncp_init();
	}
	pthread_mutex_unlock(&proxy_mutex);

	// new UDP socket
	p = gnu_socket(domain, SOCK_DGRAM, 0);
	// allocate a row in Socket Table
	socket_fds_alloc(p);
	return p;
}

// [override]
int listen(int sockfd, int backlog) {
	uint 	local_port 	= usocket_socket_port(sockfd);

	assert (sock_table[sockfd] != NULL);

	// ignore calling accept on port zero
	if (!local_port) {
		return 0;
	}

	printf("[listen] wait on fd %d for CONNECT @ port %d\n",
		sockfd, local_port);

	sock_table[sockfd]->ctrl = 1;

	return 0;
}

// [override]
int accept(int sockfd, Sockaddr *addr, socklen_t *addrlen) {
	static int (*real_socket)(int, int, int) = NULL;

	char 	buf[MAXBUFLEN];
	char 	accept[ACCEPT_PACKET_SIZE];
	int 	byte_recv;
	int 	child_sock;

	Sockaddr_in 	*addr_v4;
	Sockaddr_in6 	*addr_v6;

	assert (sock_table[sockfd] != NULL);

	if (sock_table[sockfd]->non_block) {
		if (is_ctrl_sink_empty(sockfd)) {
			errno = EWOULDBLOCK;
			return -1;
		}
	}

	byte_recv = read_ctrl_sink(sockfd, buf);

	// Fill 'addr' according to its family
	if (*addrlen >= sizeof(Sockaddr_in6)) {
		addr_v6 = (Sockaddr_in6 *) addr;
		*addr_v6 = *((Sockaddr_in6 *) &sock_table[sockfd]->addr);
	}
	else if (*addrlen >= sizeof(Sockaddr_in)) {
		addr_v4 = (Sockaddr_in *) addr;
		*addr_v4 = *((Sockaddr_in *) &sock_table[sockfd]->addr);
	}
	else
		perror("[accept] Unknown family address");

	// Fill 'addrlen'
	*addrlen = sock_table[sockfd]->addrlen;

	// Check if it is a correct "connect" packet.
	if (is_connect_packet(buf, byte_recv))
		printf("[accept] Connect packet received\n");
	else {
		perror("[accept] not a CONNECT packet");
		return -1;
	}

	real_socket = real_socket ? real_socket : dlsym(RTLD_NEXT, "socket");

	// create a new socket to handle an incoming connection
	if (addr->sa_family == AF_INET)
		child_sock = real_socket(PF_INET, SOCK_DGRAM, 0);
	else if (addr->sa_family == AF_INET6) {
		child_sock = real_socket(PF_INET6, SOCK_DGRAM, 0);
	} else {
		perror("[accept] Unknown family");
		return -1;
	}

	socket_fds_alloc(child_sock);
	sock_table[child_sock]->addr = *(Sockaddr_storage *) addr;
	sock_table[child_sock]->addrlen = *addrlen;

	// Create and send an accept packet
	accept_packet(child_sock, accept);
	sendto(child_sock, accept, ACCEPT_PACKET_SIZE, 0, addr, *addrlen);
	// save local address of new socket
	ncp_save_local_addr(child_sock);

	return child_sock;
}

// [override]
int connect(int sockfd, const struct sockaddr *serv_addr, socklen_t addrlen) {
	char buf[MAXBUFLEN];
	int i;
	int done = 0;

	assert (sock_table[sockfd] != NULL);

	// ignore connection to port zero
	if (serv_addr->sa_family == AF_INET6) {
		struct sockaddr_in6 *v6 = (struct sockaddr_in6 *) serv_addr;

		if (!ntohs(v6->sin6_port))
			return 0;
	} else if (serv_addr->sa_family == AF_INET) {
		struct sockaddr_in *tmp = (struct sockaddr_in *) serv_addr;

		if (!ntohs(tmp->sin_port))
			return 0;
	}

	connect_packet(sockfd, buf);

	for (i = 0; i < RETRY && !done; i++) {
		// send a CONNECT packet
		sendto(sockfd, buf, CONNECT_PACKET_SIZE, 0, serv_addr, addrlen);
		printf("[connect] send a CONNECT packet\n");

		ncp_save_local_addr(sockfd);

		// receive an Accept packet
		if (queue_peek(sock_table[sockfd]->ctrl_sink, NULL, CTRL_PACKET_SIZE, CONNECT_TIMEOUT)) {
			int byte_recv = read_ctrl_sink(sockfd, buf);

			// check if it is an Accept packet
			if (!is_accept_packet(buf, byte_recv)) {
				// Give up.
				i = RETRY;
			}

			done = 1;
		}
	}

	if (i == RETRY) {
		perror("[connect] Accept packet not received");
		return -1;
	}

	return 0;
}

// [override]
ssize_t send(int s, const void *buf, size_t len, int flags) {
	char recv_buf[CTRL_PACKET_SIZE];
	char packet[MAXBUFLEN];
	int rv;
	int i;
	int done 		= 0;
	assert (sock_table[s] != NULL);
	int packet_size	= data_packet(s, buf, len, packet);

//	int64_t elapsed = cur_time_ms();
	for (i = 0; i < RETRY; i++) {
		// send a data packet
		rv = sendto(s, packet, packet_size, flags, (struct sockaddr *) &sock_table[s]->addr,
				sock_table[s]->addrlen);

		// Fail in 'sendto'.
		if (rv < 0) {
			done = 1;
			break;
		}

		// If something is in the control queue.
		if (queue_peek(sock_table[s]->ctrl_sink, NULL, CTRL_PACKET_SIZE, SHORT_TIMEOUT)) {
			read_ctrl_sink(s, recv_buf);
//			printf("[send] ACK %d arrives (%dth retry)\n", packet_seq_number(recv_buf), i);
			done = 1;
			break;
		}
	}

//	elapsed = cur_time_ms() - elapsed;
//	printf("[send(%d)] Elapsed time: %lld\n", s, elapsed);

	if (!done) {
		errno = ECONNRESET;
		return -1;
	}

	return rv;
}

// [override]
ssize_t recv(int s, void *buf, size_t len, int flags) {
	char 	packet[MAXBUFLEN];
//	int64_t begin = cur_time_ms();
	int		rv;

	int retry		= 0;
	int max_retries	= 360;

	assert (sock_table[s] != NULL);

	while (retry < max_retries) {
		if (queue_peek(sock_table[s]->data_sink, NULL, 1, RECV_TIMEOUT)) {
			read_data_sink(s, packet);
			rv = packet_data(packet, buf);
//			char *str_buf = (char *) buf;
//			str_buf[rv] = '\0';
//			printf("[%s] Returns %d bytes: %s\n", __func__, rv, str_buf);
//			printf("[%s] Socket: %d, Time elapsed: %lld\n", __func__, s, cur_time_ms() - begin);
			return rv;
		}
		else {
			retry++;
		}
	}

	// count as the socket is closed
	printf("[recv] Socket %d: no data\n", s);
	return 0;
}

// [override]
int bind (int sockfd, const Sockaddr *myaddr, socklen_t addrlen) {
	int res;

	assert (sock_table[sockfd] != NULL);

	memcpy(&sock_table[sockfd]->local_addr, myaddr, addrlen);
	sock_table[sockfd]->local_addrlen = addrlen;

	if (ncp_is_port_bound(get_port((const Sockaddr_storage *) myaddr)))
		res = -1;
	else
		res = gnu_bind(sockfd, myaddr, addrlen);

	return res;
}

// [override]
ssize_t write (int filedes, const void *buffer, size_t size) {
	static int (*real_write)(int, const void *, size_t) = NULL;

	// Non-network file descriptor
	if (sock_table[filedes] == NULL) {
		real_write = real_write ? real_write : dlsym(RTLD_NEXT, "write");
		return real_write(filedes, buffer, size);
	}
	// Network file descriptor
	else
		return send(filedes, buffer, size, 0);
}

// [override]
ssize_t writev(int fd, const struct iovec *iov, int iovcnt) {
	static ssize_t (*real_writev)(int, const struct iovec *, int) = NULL;

	// Non-network file descriptor
	if (sock_table[fd] == NULL) {
		real_writev = real_writev ? real_writev : dlsym(RTLD_NEXT, "writev");
		return real_writev(fd, iov, iovcnt);
	}
	// Network file descriptor
	else {
		char packet[MAXBUFLEN];
		int totalBytes = 0;
		int i;
		memset(packet, 0, MAXBUFLEN);

		for (i = 0;i < iovcnt;i++) {
			memcpy(packet + totalBytes, iov[i].iov_base, iov[i].iov_len);
			totalBytes += iov[i].iov_len;
		}
		assert(totalBytes <= MAXBUFLEN);

		return send(fd, packet, totalBytes, 0);
	}
}

// [override]
ssize_t read (int filedes, void *buffer, size_t size) {
	static int (*real_read)(int, void *, size_t) = NULL;

	// Non-network file descriptor
	if (sock_table[filedes] == NULL) {
		real_read = real_read ? real_read : dlsym(RTLD_NEXT, "read");
		return real_read(filedes, buffer, size);
	}
	// Network file descriptor
	else
		return recv(filedes, buffer, size, 0);
}

// [override]
int close (int filedes) {
	static int (*real_close)(int) = NULL;
	int rv;

	real_close = real_close ? real_close : dlsym(RTLD_NEXT, __func__);

	// Network file descriptor
	if (sock_table[filedes] != NULL) {
		queue_free (sock_table[filedes]->ctrl_sink);
		queue_free (sock_table[filedes]->data_sink);
		free (sock_table[filedes]);
		sock_table[filedes] = NULL;
	}

	rv = real_close(filedes);

	return rv;
}

void ncp_lock_before_poll() {
	pthread_mutex_lock(&ncp_save_mutex);
	while (ncp_busy || ncp_save_req)
		pthread_cond_wait(&ncp_save_cond, &ncp_save_mutex);
	ncp_busy = 1;
}

void ncp_unlock_after_poll() {
	ncp_busy = 0;
	pthread_cond_signal(&ncp_save_cond);
	pthread_mutex_unlock(&ncp_save_mutex);
}

static int pollfd(struct pollfd *fd) {
	queue *data;
	queue *ctrl;

	/* Accepting socket */
	if (sock_table[fd->fd]->ctrl) {
		/* "control socket" to accept connections */
		ctrl = sock_table[fd->fd]->ctrl_sink;
		int available = queue_peek (ctrl, NULL, 1, 0);

		fd->revents = available ? POLLIN : 0;

		return available > 0;
	}

	/* data socket */
	switch (fd->events) {
		case POLLIN:
		data = sock_table[fd->fd]->data_sink;
		int available = queue_peek (data, NULL, 1, 0);

		fd->revents = available ? POLLIN : 0;
		return available;
		case POLLOUT:
			fd->revents = POLLOUT;
		return 1;
		default: /* not supported yet */
		fprintf(stderr, "*** poll used on unsupported data, may not work.\n");
		return 0;
	}
}

// [override]
int poll (struct pollfd fds[], nfds_t nfds, int timeout) {
	// printf("[%s] Start\n", __func__);

	// Poll information for file FDs and the pipe.
	Pollfd *file_fds = malloc(sizeof(Pollfd) * (nfds + 1));
	int64_t limit;
	int64_t current;
	int i;
	int num_file_fds = 0;
	int round = 0;

	memset(file_fds, 0, sizeof(Pollfd) * (nfds + 1));

	// Calculate time limit
	current = cur_time_ms();
	limit = current + timeout;

	// Fill poll information for the file FDs.
	for (i = 0; i < nfds; i++) {
		if (sock_table[fds[i].fd] == NULL) {
			file_fds[num_file_fds++] = fds[i];
		}
	}

	// Fill poll information for the pipe into the last position.
	file_fds[num_file_fds].fd = PIPE_READ;
	file_fds[num_file_fds].events = POLLIN;
	num_file_fds++;

	while (1) {
		int i;
		int file_ready = 0;
		int n = 0;

		if (round == 0) {
			// Poll all file FDs with timeout 0.
			if (num_file_fds > 1) {
				do {
					file_ready = gnu_poll(file_fds, num_file_fds - 1, 0);
				} while (file_ready < 0 && errno == EINTR);
			}

			round = 1;
		} else {
			int rv;

			// Poll file FDs and the pipe with the remaining timeout.
			do {
				current = cur_time_ms();
				int64_t timeout = limit - current;
				rv = gnu_poll(file_fds, num_file_fds, timeout > 0 ? timeout : 0);
			} while (rv < 0 && errno == EINTR);
//			printf("[%s] Return from gnu_poll: %d\n", __func__, rv);

			// Check if any file FDs are ready. Do not check the pipe.
			for (i = 0; i < num_file_fds - 1; i++) {
				if (file_fds[i].revents) {
					file_ready++;
				}
			}

			// Adjust timeout
			current = cur_time_ms();
			round = 2;
		}

		// Remove all tokens from the pipe.
		read_all_from_pipe();

		// Check emptiness of the queues
		for (i = 0; i < nfds; i++) {
			// If it is a socket.
			if (sock_table[fds[i].fd]) {
				int rv = pollfd(&fds[i]);
				n += rv;
			}
		}

		// Prepare output and return.
		if (file_ready > 0 || n > 0 || (round > 1 && limit <= current)) {
			int idx = 0;
			for (i = 0; i < nfds; i++) {
				// If it is a file FD, copy the return events from "file_fds"
				if (sock_table[fds[i].fd] == NULL) {
					fds[i].revents = file_fds[idx++].revents;
				}
			}

			free(file_fds);
//			printf("[%s] Return %d\n", __func__, file_ready + n);
			return file_ready + n;
		}
	}
}

// [override]
int fcntl(int fd, int cmd, ...) {
	static int (*real_fcntl)(int, int, ...) = NULL;
	va_list args;
	long largs;

	// FIXME There may be more than one optional argument following.
	va_start(args, cmd);
	largs = va_arg(args, long);
	va_end(args);

	if (sock_table[fd] != NULL) {
		// Support only NONBLOCK
		if (cmd == F_SETFL && largs & O_NONBLOCK) {
			sock_table[fd]->non_block = 1;
		}

		return 0;
	}

	real_fcntl = real_fcntl ? real_fcntl : dlsym(RTLD_NEXT, "fcntl");

	return real_fcntl(fd, cmd, largs);
}

// [GNU C lib]
int gnu_socket(int domain, int type, int protocol) {
	static int (*real_socket)(int, int, int) = NULL;

	real_socket = real_socket ? real_socket : dlsym(RTLD_NEXT, "socket");

	return real_socket(domain, type, protocol);
}

// [GNU C lib]
int gnu_bind(int sockfd, const Sockaddr *myaddr, socklen_t addrlen) {
	static int (*real_bind)(int, const Sockaddr *, socklen_t) = NULL;

	real_bind = real_bind ? real_bind : dlsym(RTLD_NEXT, "bind");

	return real_bind(sockfd, myaddr, addrlen);
}

// [GNU C lib]
int gnu_poll(struct pollfd *fds, nfds_t nfds, int timeout) {
	static int (*real_poll)(struct pollfd fds [], nfds_t nfds, int timeout) = NULL;

	real_poll = real_poll ? real_poll : dlsym(RTLD_NEXT, "poll");

	return real_poll(fds, nfds, timeout);
}

// [GNU C lib]
int gnu_close(int fd) {
	static int (*real_close)(int) = NULL;

	real_close = real_close ? real_close : dlsym(RTLD_NEXT, "close");

	return real_close(fd);
}

int connect_packet(int sock_fd, char *buffer) {
	memset(buffer, 0, CONNECT_PACKET_SIZE);

	buffer[0] = sock_fd;
	buffer[5] = 0x01;

	return CONNECT_PACKET_SIZE;
}

int is_connect_packet(const char *packet, int size) {
	return size == CONNECT_PACKET_SIZE && packet[5] == 0x01;
}

int accept_packet(int sock_fd, char *buffer) {
	memset(buffer, 0, ACCEPT_PACKET_SIZE);

	buffer[0] = sock_fd;
	buffer[5] = 0x02;

	return ACCEPT_PACKET_SIZE;
}

int is_accept_packet(const char *packet, int size) {
	return size == ACCEPT_PACKET_SIZE && packet[5] == 0x02;
}

int data_packet(int sockfd, const void *data, int data_len, char *packet) {
	memset(packet, 0, 10 + data_len);

	packet[0] = sockfd;
	*((int*) (packet + 1)) = get_packet_number(sockfd);
	*((int*) (packet + 6)) = data_len;

	// Notify the cache that this is a packet containing a ND message.
	if (nd_flag) {
		nd_flag = 0;
		packet[5] = 0x10;
	}

	inc_packet_number(sockfd);
	memcpy(packet + 10, data, data_len);

	return data_len + 10;
}

int ack_packet(int sockfd, int seq, char *packet) {
	memset(packet, 0, CTRL_PACKET_SIZE);

	packet[0] 					= sockfd;
	*((int*) (packet + 1)) 	= seq;
	packet[5]					= 0x04;

	return CTRL_PACKET_SIZE;
}

int is_ack_packet(const char *packet, int size) {
	return size == CTRL_PACKET_SIZE && packet[5] == 0x04;
}

int is_data_packet(const char *packet) {
	int len = packet_data_len(packet);

	return len > 0;
}

int packet_seq_number(const char *packet) {
	return *((int*) (packet + 1));
}

int packet_data_len(const char *packet) {
	return *((int*) (packet + 6));
}

// Fill the data part of a given packet to storage and return the data length
int packet_data(const char *packet, char *storage) {
	int len = packet_data_len(packet);

	memcpy(storage, packet + 10, len);

	return len;
}

// Return the port number of a given address
uint get_port(const Sockaddr_storage *addr) {
	struct sockaddr_in6 *addr_v6;
	struct sockaddr_in *addr_v4;

	if (addr->ss_family == AF_INET) {
		addr_v4 = (struct sockaddr_in *) addr;
		return ntohs(addr_v4->sin_port);
	} else if (addr->ss_family == AF_INET6) {
		addr_v6 = (struct sockaddr_in6 *) addr;
		return ntohs(addr_v6->sin6_port);
	}

	return 0;
}

uint usocket_socket_port(int sockfd) {
	struct sockaddr_storage addr;
	socklen_t addrlen = sizeof addr;

	memset(&addr, 0, sizeof addr);
	getsockname(sockfd, (struct sockaddr *) &addr, &addrlen);

	return get_port(&addr);
}

void ncp_clear_socket_entry(int idx) {
	sock_table[idx]->send_seq = 0;
	sock_table[idx]->expected_seq = 0;
	sock_table[idx]->addrlen = 0;
	sock_table[idx]->local_addrlen = 0;
	sock_table[idx]->ctrl = 0;
	sock_table[idx]->non_block = 0;

	memset(&sock_table[idx]->addr, 0, sizeof(Sockaddr_storage));
	memset(&sock_table[idx]->local_addr, 0, sizeof(Sockaddr_storage));
}

// Allocate a block each in the socket descriptor array and the packet number array
void socket_fds_alloc(int socket_fd) {
	assert (socket_fd < NUM_SOCKETS);
	// New row in the table
	assert (sock_table[socket_fd] == NULL);
	sock_table[socket_fd] = malloc (sizeof (SocketTable));
	sock_table[socket_fd]->data_sink = queue_new(DATA_SINK_SIZE);
	sock_table[socket_fd]->ctrl_sink = queue_new(DATA_SINK_SIZE);

	ncp_clear_socket_entry(socket_fd);
}

int get_packet_number(int socket_fd) {
	return sock_table[socket_fd]->send_seq;
}

// Increase the packet number for sending by one.
void inc_packet_number(int socket_fd) {
	sock_table[socket_fd]->send_seq++;
}

// Return the expected sequence number of a given socket
int get_expected_seq(int socket_fd) {
	return sock_table[socket_fd]->expected_seq;
}

// Increase the expected sequence number by one.
void inc_expected_seq(int socket_fd) {
	sock_table[socket_fd]->expected_seq++;
}

// Copy a given packet to the data sink of a given socket.
void write_data_sink(int socket_fd, char *packet, int size) {
	queue* sink = sock_table[socket_fd]->data_sink;

	queue_put(sink, packet, size);
}

// Read a packet from the data sink of a given socket
// Return the size of the returned packet in byte
int read_data_sink(int socket_fd, char *packet) {
	queue *sink = sock_table[socket_fd]->data_sink;
	int data_len;
	int *p_data_len;
	char buff[MAXBUFLEN];

	queue_peek(sink, buff, CTRL_PACKET_SIZE, -1);

	p_data_len = (int *) (buff + 6);
	data_len = *p_data_len;

	queue_remove(sink, packet, CTRL_PACKET_SIZE + data_len);

	return CTRL_PACKET_SIZE + data_len;
}

int is_data_sink_empty(int socket_fd) {
	queue *sink = sock_table[socket_fd]->data_sink;

	return queue_peek (sink, NULL, 1, 0) == 0;
}

// Copy a given packet to the control sink of a given socket.
void write_ctrl_sink(int socket_fd, char *packet) {
	queue *sink = sock_table[socket_fd]->ctrl_sink;

	queue_put(sink, packet, CTRL_PACKET_SIZE);
}

// Read a packet from the control sink of a given socket
// Return the size of the returned packet in byte
int read_ctrl_sink(int socket_fd, char *packet) {
	queue *sink	= sock_table[socket_fd]->ctrl_sink;

	queue_remove(sink, packet, CTRL_PACKET_SIZE);

	return CTRL_PACKET_SIZE;
}

int is_ctrl_sink_empty(int socket_fd) {
	queue *sink = sock_table[socket_fd]->ctrl_sink;

	return queue_peek (sink, NULL, 1, 0) == 0;
}

void ncp_set_pipe_nonblock() {
	int flags;
	int rv;

	// Set both ends of the pipe to the non-block mode.
	flags = fcntl(PIPE_READ, F_GETFL, 0);
	rv = fcntl(PIPE_READ, F_SETFL, flags | O_NONBLOCK);
	assert(!rv);
	flags = fcntl(PIPE_WRITE, F_GETFL, 0);
	rv = fcntl(PIPE_WRITE, F_SETFL, flags | O_NONBLOCK);
	assert(!rv);
}

void ncp_change_fd(int from, int to) {
	int rv = dup2(from, to);
	assert(rv > 0);
	rv = gnu_close(from);
	assert(rv == 0);
}

void ncp_init() {
	int fds[2];
	// Create a socket pair between the proxy and a program thread.
	int rv = socketpair(AF_UNIX, SOCK_STREAM, 0, fds);

	if (rv) {
		perror("ERROR; socketpair()");
		exit(-1);
	}

	// Only duplicate. Do not close.
	dup2(fds[0], PIPE_READ);
	dup2(fds[1], PIPE_WRITE);
	
	ncp_set_pipe_nonblock();
	memset (sock_table, 0, sizeof(sock_table[0]) * NUM_SOCKETS);
	rv = pthread_create(&proxy, NULL, proxy_run, (void *) 1);

	if (rv) {
		perror("ERROR; pthread_create()");
		exit(-1);
	}
}

// Main method for the proxy
void *proxy_run(void *id) {
	Pollfd 	poll_subjects[NUM_SOCKETS];
	char 	packet[MAXBUFLEN];
	char 	ack[CTRL_PACKET_SIZE];

	ssize_t 	recv_packet_size;
	int 		rv;
	int			i;
	int			seq_num;
	int 		expected;
	int	 		num_subjects;
	int	idx;

	while (1) {
		num_subjects = 0;

		// Read the list of registered sockets
		for (i = 0;i < NUM_SOCKETS;i++) {
			if (sock_table[i] != NULL && usocket_socket_port(i) != 0) {
				poll_subjects[num_subjects].fd = i;
				poll_subjects[num_subjects].events = POLLIN;
				num_subjects++;
			}
		}

		do {
			rv = gnu_poll(poll_subjects, num_subjects, PROXY_POLL_TIMEOUT);
		} while (rv < 0 && errno == EINTR);

		if (rv > 0) {
			for (i = 0;i < num_subjects;i++) {
				if (poll_subjects[i].revents & POLLIN) {
					idx = poll_subjects[i].fd;
					expected = get_expected_seq(idx);
					sock_table[idx]->addrlen = sizeof(Sockaddr_storage);
					recv_packet_size = recvfrom(idx, packet, MAXBUFLEN, 0,
							(Sockaddr *) &sock_table[idx]->addr, &sock_table[idx]->addrlen);
					seq_num = packet_seq_number(packet);

					// data packet
					if (is_data_packet(packet)) {
						if (seq_num == expected) {
							ack_packet(idx, expected, ack);
							sendto(idx, ack, CTRL_PACKET_SIZE, 0, (struct sockaddr *) &sock_table[idx]->addr,
									sock_table[idx]->addrlen);

							inc_expected_seq(idx);
							write_data_sink(idx, packet, recv_packet_size);
							write_one_byte_to_pipe();
							// printf("[%s] Data packet on %d, %d bytes\n", __func__, poll_subjects[i].fd, recv_packet_size);
						}
						// Old packet comes. The opponent might not have got an ACK. Send the ACK again.
						else if (seq_num < expected) {
							ack_packet(idx, seq_num, ack);
							sendto(idx, ack, CTRL_PACKET_SIZE, 0, (struct sockaddr *) &sock_table[idx]->addr,
									sock_table[idx]->addrlen);
							printf("[%s] Old packet %d\n", __func__, seq_num);
						}
					}
					// control packet
					else {
						write_ctrl_sink(idx, packet);
						write_one_byte_to_pipe();

						// printf("[%s] forward packet %d to the control sink of socket %d\n", __func__, seq_num, poll_subjects[i].fd);
					}
				}
			}
		}
	}
}

// If the given socket has a local address, rebind it; otherwise, do nothing.
void ncp_rebindsock(int sockfd) {
	int newsock;
	int rv;
	// if the socket was bound before, recover it.
	assert (sock_table[sockfd] != NULL);
	if (sock_table[sockfd]->local_addrlen > 0) {
		printf("[rebind] id %d\n", sockfd);
		newsock = gnu_socket(PF_INET, SOCK_DGRAM, 0);
		if (newsock != sockfd) {
			rv = dup2 (newsock, sockfd);
			if (rv == -1) {
				perror ("Failed to rebind.");
			}
			close (newsock);
		}
		rv = gnu_bind(sockfd,
			      (Sockaddr *) &sock_table[sockfd]->local_addr,
			      sock_table[sockfd]->local_addrlen);

		if (rv == -1) {
			perror("Failed to rebind");
		}
	}
}

// Save the local address associated to a given virtual socket.
void ncp_save_local_addr(int sock) {
	Sockaddr_storage *p_local_addr;
	socklen_t *p_addr_len;
	uint addr_sz = sizeof(Sockaddr_storage);

	p_local_addr = &sock_table[sock]->local_addr;
	p_addr_len = &sock_table[sock]->local_addrlen;
	*p_addr_len = (socklen_t) addr_sz;

	memset(p_local_addr, 0, addr_sz);
	getsockname(sock, (Sockaddr *) p_local_addr, p_addr_len);
}

// Called after MTCP has restarted the process. Rebind every socket if possible.
void ncp_rebind_all_sockets() {
	int i;
	int new_pipes[2];

	for (i = 0; i < NUM_SOCKETS; i++) {
		if (sock_table[i] != NULL)
			ncp_rebindsock(i);
	}

	socketpair(AF_UNIX, SOCK_STREAM, 0, new_pipes);

	// Change to the reserved numbers.
	ncp_change_fd(new_pipes[0], PIPE_READ);
	ncp_change_fd(new_pipes[1], PIPE_WRITE);

	ncp_set_pipe_nonblock();
	printf("[rebindsock] rebind sockets OK\n");
}

void ncp_print_port(const Sockaddr_storage *addr) {
	if (addr->ss_family == AF_INET6) {
		struct sockaddr_in6 *v6 = (struct sockaddr_in6 *) addr;
		printf("[print_port] V6 port %u\n", ntohs(v6->sin6_port));
	} else if (addr->ss_family == AF_INET) {
		struct sockaddr_in *tmp = (struct sockaddr_in *) addr;
		printf("[print_port] V4 port %u\n", ntohs(tmp->sin_port));
	} else {
		printf("[print_port] unknown family\n");
	}
}

int ncp_is_port_bound(uint port) {
	int i;

	for (i = 0; i < NUM_SOCKETS; i++) {
		if (sock_table[i] != NULL && usocket_socket_port(i) == port) {
			return 1;
		}
	}
	return 0;
}

// @override
void srand (uint seed) {
	static void (*real_srand)(uint) = NULL;

	real_srand = real_srand ? real_srand : dlsym(RTLD_NEXT, "srand");
	nd_flag = 1;

	real_srand(seed);
}

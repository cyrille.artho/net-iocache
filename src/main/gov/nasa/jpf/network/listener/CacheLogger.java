package gov.nasa.jpf.network.listener;

import gov.nasa.jpf.JPF;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.VM;
import gov.nasa.jpf.vm.LocalVarInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.network.cache.CacheLayer;
import gov.nasa.jpf.search.Search;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.logging.Logger;

public class CacheLogger extends ListenerAdapter {

	static Logger log = JPF.getLogger("gov.nasa.jpf.network.listener");

	// public void executeInstruction(JVM vm) {
	// Instruction ist = vm.getNextInstruction();
	//
	// if (ist != null) {
	// System.out.printf("%s [%s]\n", ist.getMnemonic(), ist.getFileLocation());
	// }
	// }

	@Override
	public void stateRestored(Search search) {
		int id = search.getStateId();
		log.fine("----------------------------------- [" + search.getDepth() + "] restored " + id);
	}
	
	// --- the ones we are interested in
	@Override
	public void searchStarted(Search search) {
		log.info("----------------------------------- search started");
	}
	/*
	 * getCurrentCallStack(VM vm)
	 *   returns the call stack of the current state in the search context 
	 * @param vm  current vm of JPF
	 * @return    method signatures of the call stack of the currently explored state
	 */
	private String getCurrentCallStack(VM vm) {
		if(vm.getCurrentThread()==null ||vm.getCurrentThread().iterator()==null) 
			return "";
		
		StringBuffer res=new StringBuffer();
		Iterator<StackFrame> i=vm.getCurrentThread().iterator();
		boolean indent=false;
		while(i.hasNext()) {
			StackFrame sf=i.next();
			String mi,args;
			//don't output the main method and direct calls from the JPF framework
			if (!(sf.getMethodInfo()==null || sf.isDirectCallFrame() || (mi=sf.getMethodInfo().getFullName()).endsWith(".main([Ljava/lang/String;)V"))) {
				//indent output lines except first line 
				if (indent) res.append("        "); else indent=true;
				args=getArgValues(sf);
				//Replace ArgsSignature with Runtime Values, if available
				if (args.length()==0) res.append(mi);
				else {
					res.append(mi.substring(0, mi.indexOf("(")+1));
					res.append(args);
					res.append(mi.substring(mi.indexOf(")")));
				}
				res.append("\n");				
			}
		}
		//get rid of the last new line
		if (res.length()>1) res.deleteCharAt(res.length()-1);
		return res.toString();
	}
	
	/*
	 * getArgValues(StackFrame sf)
	 *   returns the values of the arguments passed to the current method of sf 
	 * @param sf  current StackFrame 
	 * @return    list of argument assignments 'type1 name1=val1, type2 name2=val2,...' 
	 */
	private String getArgValues(StackFrame sf) {
		LocalVarInfo args[]=sf.getMethodInfo().getArgumentLocalVars();
		if (args == null) return "";   //no arguments passed

		StringBuffer res=new StringBuffer();
		for (int i = 0; i < args.length; i++) {
			//don't include implicit 'this' argument of non-static methods
			if (args[i]!=null && !args[i].getName().equals("this")) { 
				res.append(", "+args[i].getType()+" ");
				res.append(args[i].getName()+"=");
				res.append(sf.getLocalValueObject(args[i]));
			}
		}
		//delete leading ", "
		if (res.length()>1) res.replace(0, 2, "");
		return res.toString();
	}

	
	@Override
	public void stateAdvanced(Search search) {
		StringBuffer s = new StringBuffer();
		String callStack="";
		int id = search.getStateId();
		int depth=search.getDepth();
		
		s.append("----------------------------------- [" + depth + "] forwarded to " + id);
		if (search.isNewState()) {
			s.append(" (new)");
			callStack=getCurrentCallStack(search.getVM());
		} else {
			s.append(" (visited)");
		}

		if (search.isEndState()) {
			s.append(" END state reached at search depth "+depth+".\n");
		}

		if (search.isNewState() && !search.isEndState() && callStack.length()>0) 
			log.finer(callStack);
		if (search.isEndState())
			log.info(s.toString());
		else
			log.fine(s.toString());
	}
	
//	@Override
//	public void stateProcessed(Search search) {
//		int id = search.getStateId();
//		log.fine("----------------------------------- [" + search.getDepth() + "] done: " + id);
//	}
	
	@Override
	public void stateBacktracked(Search search) {
		int id = search.getStateId();
		String msg="--------------------------------- [" + search.getDepth() + "] bktracked to " + id;
		if (search.isProcessedState()) log.finest(msg+" (done)");
		else log.fine("--"+msg);
	}
	
	@Override
	public void searchFinished(Search search) {
		log.info("----------------------------------- search finished");
		showStats();
	}
	
	void showStats() {
		log.info("Cache HIT: " + CacheLayer.getCacheHit());
		log.info("Cache MISS: " + CacheLayer.getCacheMiss());
		log.info("Opened CONNECTIONS: " + CacheLayer.getNumRestart());
	}
	
}

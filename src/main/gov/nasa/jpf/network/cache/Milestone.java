//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

/**
 * The Milestone instance stores information of every logical connection in a model-checking state.
 * 
 * @author watcharin
 * 
 */
public class Milestone {

	/**
	 * reqPointers[i] is a request pointer of logical connection i
	 */
	List<Integer> reqPointers = new ArrayList<Integer>();

	/**
	 * resPointers[i] is a response pointer of logical connection i
	 */
	List<Integer> resPointers = new ArrayList<Integer>();

	/**
	 * closedBit[i] is, 1 if connection i is closed;
	 * 					0 if connection i is active.
	 */
	BitSet closedBits = new BitSet();
	
	private boolean isMainAlive = true;

	/**
	 * Add a request pointer position of a new connection.
	 * 
	 * @param ptr
	 */
	void addReqPtr(Integer ptr) {
		reqPointers.add(ptr);
	}

	/**
	 * Add a response pointer position of a new connection.
	 * 
	 * @param ptr
	 */
	void addResPtr(Integer ptr) {
		resPointers.add(ptr);
	}

	int getReqPtr(Integer connId) {
		return reqPointers.get(connId);
	}

	int getResPtr(Integer connId) {
		return resPointers.get(connId);
	}

	void setClose(int conn, boolean value) {
		closedBits.set(conn, value);
	}

	boolean isClosed(int conn) {
		return closedBits.get(conn);
	}

	/**
	 * 
	 * @return The number of logical connections whose pointers are kept in this milestone.
	 */
	int numConnections() {
		return reqPointers.size();
	}
	
	void setMainAlive(boolean flag) {
		isMainAlive = flag;
	}
	
	boolean isMainAlive() {
		return isMainAlive;
	}
	
}

class EmptyMilestone extends Milestone {
	
	private static final Milestone INSTANCE = new EmptyMilestone();
	
	private EmptyMilestone() {		
	}
	
	public static Milestone getInstance() {
		return INSTANCE;
	}
	
	int numConnections() {
		return 0;
	}
}

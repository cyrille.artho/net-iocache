#!/bin/sh
# rename all ".log" files in the current directory to ".out" files.

LOG=`find ./log -name '*.log'`

for FILE in ${LOG}
do
	OUT=`echo ${FILE} | sed 's/.log$/.out/'`
	mv ${FILE} ${OUT}
done


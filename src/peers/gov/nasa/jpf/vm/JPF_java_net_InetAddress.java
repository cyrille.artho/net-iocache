//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.vm;

import gov.nasa.jpf.annotation.MJI;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;


public class JPF_java_net_InetAddress extends NativePeer{

	/**
	 * Store for mapping model level InetAddress objects, 
	 * identified by their objref integer value, to their native level InetAddress object;
	 * This is necessary, because InetAddress model objects itself do not have any fields.
	 * All properties such as getHostName() are retrieved from the native peer to
	 * ensure 100% consistency between model and native level behavior of InetAddress.
	 * We assume that InetAddress objects do not change their state after being 
	 * created, i.e., while model checking, a host is not assigned a new name or 
	 * IP addresses. This allows for InetAddress objects as static persistent 
	 * objects and store them on the native peer (no backtracking required).
	 * See also {@link #nativeToModelInetAddress(MJIEnv, InetAddress)},
	 *          {@link #modelToNativeInetAddress(int)}
	 */
	static HashMap<Integer,InetAddress> modelToNativeMap=new HashMap<Integer,InetAddress>();
	/**
	 * reverse mapping for fast lookup of the model object reference id for a native InetAddress object;
	 * See also {@link #nativeToModelInetAddress(MJIEnv, InetAddress)}
	 */
	static HashMap<InetAddress,Integer> nativeToModelMap=new HashMap<InetAddress,Integer>();
	
	/**
	 * generates a new model level InetAddress and
	 * associates it with the corresponding native
	 * level InetAddress for later reference
	 * @param env access to the MJI environment
	 * @param nativeAddr native InetAddress to associate the new model InetAddress object with
	 * @return the object reference to the new model InetAddress object
	 */
	private static int nativeToModelInetAddress(MJIEnv env, InetAddress nativeAddr) {
		assert nativeAddr!=null: "[JPF_java_net_InetAddress.nativeToModelInetAddress] native InetAddress associated with a new model InetAddress object must not be null.";
		//Check first, if there is already a model InetAddress object for the given nativeAddr;
		//This ensures that object equality, checked via equals(), is preserved on the the model level.
		//I.e., if and only if iaddr1.equals(iaddr2) is true on the native level, it is also true for the
		//corresponding objects on the model level.
		Integer modelAddr=nativeToModelMap.get(nativeAddr);
		if (modelAddr!=null) {
			int res=modelAddr.intValue();
			assert res!=MJIEnv.NULL:"[JPF_java_net_InetAddress.nativeToModelInetAddress] failed to retrieve a model InetAddress for native InetAddress object '"+nativeAddr+"'";
			return res;
		}
		//if no corresponding model InetAddress object found
		//create a new one and associate it with the native address
		int res=env.newObject("java.net.InetAddress");
		nativeToModelMap.put(nativeAddr,res);
		modelToNativeMap.put(res, nativeAddr);
		return res;		
	}
	
	/**
	 * retrieves the native InetAddress object associated with
	 * a model InetAdress object reference
	 * @param modelAddr object reference of the model InetAddress object to retrieve 
	 * @return corresponding native level InetAddress object
	 */
	private static InetAddress modelToNativeInetAddress(int modelAddr) {
		InetAddress res=modelToNativeMap.get(modelAddr);
		assert res!=null: "[JPF_java_net_InetAddress.modelToNativeInetAddress] failed to retrieve native InetAddress for a model InetAddress object.";
		return res;
	}
	
	@MJI
	public static int getAllByName(MJIEnv env, @SuppressWarnings("unused") int objRef, int strHost) {
		int res=MJIEnv.NULL;
		String host=env.getStringObject(strHost);
		try {
			InetAddress[] addresses=InetAddress.getAllByName(host);
			res=env.newObjectArray("java.net.InetAddress", addresses.length);
			for (int i=0; i<addresses.length;i++) { 
				env.setReferenceArrayElement(res, i, nativeToModelInetAddress(env, addresses[i]));
			}
		} catch (UnknownHostException e){
			env.throwException("java.net.UnknownHostException", e.getMessage());
			res=MJIEnv.NULL;
		}
		return res;
	}

	@MJI
	public static int getByName(MJIEnv env, @SuppressWarnings("unused") int objRef, int strHost) {
		int res=MJIEnv.NULL;
		String host=env.getStringObject(strHost);
		try {
			res=nativeToModelInetAddress(env, InetAddress.getByName(host));
		} catch (UnknownHostException e){
			env.throwException("java.net.UnknownHostException", e.getMessage());
		}
		return res;
	}

	@MJI
	public static int getByAddress(MJIEnv env, @SuppressWarnings("unused") int objRef, int byteArrAddr) {
		int res=MJIEnv.NULL;
		byte[] addr=env.getByteArrayObject(byteArrAddr);
		try {
			res=nativeToModelInetAddress(env, InetAddress.getByAddress(addr));
		} catch (UnknownHostException e){
			env.throwException("java.net.UnknownHostException",e.getMessage());
		}
		return res;
	}

	@MJI
	public static int getLocalHost(MJIEnv env, @SuppressWarnings("unused") int objRef) {
		int res=MJIEnv.NULL;
		try {
			res=nativeToModelInetAddress(env, InetAddress.getLocalHost());
		} catch (UnknownHostException e){
			env.throwException("java.net.UnknownHostException",e.getMessage());
		}
		return res;
	}

	@MJI
	public int getCanonicalHostName(MJIEnv env, int objref) {
		InetAddress iaddr=modelToNativeInetAddress(objref);
		return env.newString(iaddr.getCanonicalHostName());
	}

	@MJI
	public int getHostName(MJIEnv env, int objref) {
		InetAddress iaddr=modelToNativeInetAddress(objref);
		return env.newString(iaddr.getHostName());
	}

	@MJI
	public int getHostAddress(MJIEnv env, int objref) {
		InetAddress iaddr=modelToNativeInetAddress(objref);
		return env.newString(iaddr.getHostAddress());
	}

	@MJI
	public int getAddress(MJIEnv env, int objref) {
		InetAddress iaddr=modelToNativeInetAddress(objref);
		return env.newByteArray(iaddr.getAddress());
	}
	
	@MJI
	public boolean isLoopbackAddress(@SuppressWarnings("unused") MJIEnv env, int objref) {
		InetAddress iaddr=modelToNativeInetAddress(objref);
		return iaddr.isLoopbackAddress();
	}
	
	/**
	 * native peer of toString method: use longer name encoding the
	 * type of the return value to avoid confusion with the toString
	 * method inherited from Object which returns String not int.
	 * @param env
	 * @param objref
	 * @return
	 */
	@MJI
	public int toString____Ljava_lang_String_2(MJIEnv env, int objref) {
		InetAddress iaddr=modelToNativeInetAddress(objref);
		return env.newString(iaddr.toString());
	}
}

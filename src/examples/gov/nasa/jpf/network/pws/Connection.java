package gov.nasa.jpf.network.pws;
/*  Connection.java -- classes and functions to handle client connections

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.net.*;
import java.io.*;
import java.util.*;

public class Connection
{
	private InputHandler input;
	private OutputHandler output;

	private String httpMethod;
	private String httpURI;
	private String httpURIPath;
	private String httpURIVars;

	private String httpVersion;
	private int httpMajor;
	private int httpMinor;

	private String protocol;

	private Map httpHeaders = new HashMap();
	
	private InetAddress remoteIP;
	
	Connection (InputHandler c_input, OutputHandler c_output, InetAddress sock_remoteIP)
	/* class constructor */
	{
        this.remoteIP = sock_remoteIP;
        this.input = c_input;
        this.output = c_output;
	}

    public String connectionType()
    {
        String conType = (String) httpHeaders.get("connection");
        conType = conType.toLowerCase().trim();
        if (conType == null) conType = "close";
        
        return conType;
    }

/****************************************************************************************************
 ****************************************************************************************************
 ****************************************************************************************************/

	public void processRequest () throws UnknownProtocolException, NoDataReceivedException,
	                                     ObsoleteHTTPBrowserException, RFCViolationException
	/*	processes incoming requests from clients.
		This function is called first on connection of a new thread.
		Extracts the various parts from the messages that are gotten from the client.
	*/
	{
		
		String aRequest = "";

		/* get the line, but get rid of the (possibly) empty line */
        try
		{
			do
			{
				aRequest = input.readLine();
			} while ((aRequest.length() == 0));
		}
        catch (NullPointerException e)
        {
             aRequest = "";
        }

        /* now we must 'process' what we've gotten... If it is a normal request, it would normally 
           start with <method> <file> HTTP/X.Y
           X -> HttpMajor, Y -> HttpMinor (version levels)
		   
           There are some things that you normally have to keep in mind concerning http protocol 
           versions: I've read that version 0.9 doesn't support several things, amongst which the 
           Status codes (500 - server made a booboo e.g.)
        */

		if (aRequest.length() == 0)
		    throw new NoDataReceivedException();

	    StringTokenizer requestInTokens = new StringTokenizer(aRequest);
	    httpMethod = requestInTokens.nextToken();

		if (requestInTokens.hasMoreTokens() == true)
		{
			try
			{
				httpURI = URLDecoder.decode(requestInTokens.nextToken(), pws.getEncoding());
			}
			catch (UnsupportedEncodingException e)
			{
			}
		}
		else
			httpURI = "/";

    	/*remove last '/' (as many as there are) from the URI
       	  not strictly necessary, and i guess it can be done it a way that looks 'better'
	      but it's friggin' late and i don't feel like searching too long on it :-) */
   		while (httpURI.length() > 1 && httpURI.charAt(httpURI.length() - 1) == '/')
	        httpURI = httpURI.substring(0,httpURI.length() - 1);

		/* check for < symbol. This is not allowed, and leads to scripting attacks. see http://rfc.net/rfc2396.html page 10 */
		if (httpURI.indexOf('<') != -1)
		{
			httpURI = httpURI.substring(0, httpURI.indexOf('<'));
		} 
		
		/* check if there are any variables passed, we need to split them off */
		if (httpURI.indexOf('?') != -1)
		{
			httpURIPath = httpURI.substring(0, httpURI.indexOf('?'));
			httpURIVars = httpURI.substring(httpURI.indexOf('?') + 1);
		}
		else
			httpURIPath = httpURI;
			
		

		if (requestInTokens.hasMoreTokens() == true)
       		httpVersion = requestInTokens.nextToken();
		else
			httpVersion = "HTTP/0.9";
				
    	StringTokenizer versionInTokens = new StringTokenizer(httpVersion,"/.");
  	    protocol = versionInTokens.nextToken();

		if (protocol.equals("Secure-HTTP") == true)
		{
			// do secure http processing here
		}
		else
		{
	       	if (protocol.equals("HTTP") != true)
    	    {
   	            /* throw an error in the ring
		    	   we can only handle http requests, we're not going to turn this thing into a 
		    	    multispacial multiphasic multidimensional server toolkit :-) */
			    output.outputError(601,protocol);
				throw new UnknownProtocolException(protocol);
	       	}
			else
    		{
				/* get HTTP version */
    	    	httpMajor = Integer.parseInt(versionInTokens.nextToken());
       			httpMinor = Integer.parseInt(versionInTokens.nextToken());

       			output.sendHeaders(true);

	    		String header = input.readLine();
		        String name = null;
				String value = "";

		        while (header != null && header.length() > 0)
				{
   				    /* process the rest of the headers
           			HTTP/1.1 requires a 'host' header to be sent, check on this one */
    
    				int positionOfColon = header.indexOf(":");
		        	/*convert header-key to lowercase to prevent problems*/
					if (positionOfColon != -1)
					    httpHeaders.put(header.substring(0,positionOfColon).trim().toLowerCase(), header.substring(positionOfColon + 1).trim());
					
	    	    	header = input.readLine();
				}
					
				output.setHttpHeaders(httpHeaders);
				
	   	    	/* if all headers are passed us, we check if the 'host' has been sent, as required in the HTTP/1.1 specs */
			    if ((httpMajor > 0) && (httpMinor > 0) && (httpHeaders.containsKey("host") == false))
				{
				    output.outputError(400,"");
		        	throw new RFCViolationException("RFC2068");
	   	    	}
	   	    	
	   	    	/* put Connection: close header in if it isn't there */
	   	    	if (httpHeaders.containsKey("connection") == false)
	   	    	    httpHeaders.put("connection","close");
	       	}
		}
    }

/*********************************************************************************************************
 *********************************************************************************************************
 *********************************************************************************************************/



/* Servicer part - actually services the connection */

	public void doMagicStuff()
	/* This procedure gets and outputs the thing(s) requested by the client. 
		If someone wants to implement other methods, it is in this function that you're needed :-)
	*/
	{
		HashMap methodList = new HashMap();
		methodList.put("GET","0");
		methodList.put("HEAD","1");
		methodList.put("POST","2");
		methodList.put("TRACE","3");

		if (methodList.get(httpMethod) != null)
        {
            /* look if we can find the file requested */
            FileFinder findFile = new FileFinder(httpURIPath);
            int methodID = Integer.parseInt(methodList.get(httpMethod).toString());
            switch(methodID)
            {
                case 0: /* START OF GET METHOD */
                case 1: /* START OF HEAD METHOD */
           		{
           		    if (methodID == 1)
           		        output.sendBody(false);    /* for HEAD method, we don't send body's out) */
                        
           		    switch (findFile.status())
           		    {
           		        case -1:    /* nothing found */
           		        {
           		            output.outputError(404,httpURI);
           		            break;
           		        }
           		        case 0:     /* must send dirlisting */
           		        {
           		            output.outputDirectoryListing(httpURIPath);
           		            break;
           		        }
           		        case 1:     /* must transmit file */
                        {           		        
            		        output.ProcessAndOutputFile(findFile.theFile(), httpMethod, httpURIVars, remoteIP);
            		        break;
            		    }
            		}
        		    break;
        		} /* END OF HEAD METHOD */
        		  /* END OF GET METHOD */
    
                case 2: /* START OF POST METHOD */
                {
                    break;
                } /* END OF POST METHOD */
                
                case 3: /* START OF TRACE METHOD */
                {
                    /* trace: respond with 200 OK, and send all the headers back that we've received
                              in the entity body */
                    output.outputTrace(httpURI);
                    break;
                } /* END OF TRACE METHOD */
            }
        }
		else
		{
    		/* it is a method that is not implemented. Error on it. */
			output.outputError(501, httpMethod);
		}
	}
}
//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.alphabet;

import java.net.InetAddress;
import java.net.Socket;
import java.util.StringTokenizer;

public class AlphabetClient {

	public final static String MSG = "0123456789:;<=>?@ABCDEFFGHI";

	private static byte[] getByte(String a) {
		int n = a.length();
		byte[] m = new byte[n];

		for (int i = 0; i < n; i++) {
			m[i] = (byte) a.charAt(i);
		}

		return m;
	}

	private static byte[] ip(String s) {
		StringTokenizer st = new StringTokenizer(s, ".");
		byte[] b = new byte[4];

		for (int i = 0; i < 4; i++) {
			b[i] = (byte) Integer.parseInt(st.nextToken());
		}

		return b;
	}

	// Usage: AlphabetClient (#threads) (#messages) [options]
	//
	// Options:
	// --ip <IP address>
	// --port <port number>
	// 
	// --samemsg
	// Every thread produces the same set of messages.
	public static void main(String[] args) throws Exception {
		InetAddress addr = InetAddress.getLocalHost();
		Socket s[];
		Thread[] p, c;
		int num_threads, num_messages;
		int msg_start = 0;
		int port = AlphabetServer.SERVER_PORT;
		boolean sameMsg = false;

		// Parse optional arguments
		for (int i = 2; i < args.length; i++) {
			if (args[i].equals("--ip")) {
				addr = InetAddress.getByAddress(ip(args[++i]));
			}
			else if (args[i].equals("--port")) {
				port = Integer.parseInt(args[++i]);
			}
			else if (args[i].equals("--samemsg")) {
				sameMsg = true;
			}
		}

		num_threads = Integer.parseInt(args[0]);
		num_messages = Integer.parseInt(args[1]);
		p = new Thread[num_threads];
		c = new Thread[num_threads];
		s = new Socket[num_threads];


		for (int i = 0; i < num_threads; i++) {
			s[i] = new Socket(addr, port);
			System.out.println("[AlphabetClient: main] created "+(i+1)+
					". connection to "+addr+":"+port);

			byte[] messages=getByte(MSG.substring(msg_start, msg_start + num_messages));
			p[i] = new Producer(s[i].getOutputStream(), messages);
			c[i] = new Consumer(s[i].getInputStream(), messages);
			
			// Do not shift if option "samemsg" is enabled.
			if (!sameMsg)
				msg_start += num_messages;
		}

        
//		System.out.println("[AlphabetClient: main] starting "+num_threads+
//							" producer thread(s), each sending "+num_messages+ 
//							" message(s), as well as "+num_threads+" consumer thread(s), " +
//									"receiving the response from the alphabet server...");

		for (int i = 0; i < num_threads; i++) {
			p[i].start();
			c[i].start();
		}
//		do not use join() in main thread if jpf-net-iocache.main.termination=true		
//		for (int i=0; i<num_threads; i++) {    
//			p[i].join();
//			c[i].join();
//		}

//		System.out.println("[AlphabetClient: main] says good bye!");
	}
}

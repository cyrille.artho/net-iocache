//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.chat;


import java.io.*;
import java.net.*;
import junit.framework.TestCase;

public class TestChatClientOne extends TestCase {
    String serverAddr = "localhost";
    int serverPort = 4444;

    public TestChatClientOne(String name) {
        super(name);
    }

    public void testOne() {
        String toprint = "TeStStRiNg";
        String toread;
        try {
            Socket socket = new Socket();
            InetSocketAddress addr = new InetSocketAddress("localhost", 4444);
            socket.connect(addr);

            InputStreamReader istr =
                new InputStreamReader(socket.getInputStream());
            BufferedReader in = new BufferedReader(istr);
            PrintWriter out =
                new PrintWriter(socket.getOutputStream());

            out.println(toprint);
            out.flush();

            toread = in.readLine();

            out.close();
            socket.close();

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) { }

            assertTrue(toread.endsWith(toprint));
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void testTwo() {
        String toprint = "TeStStRiNg";
        String toread1, toread2 = null;
        try {
            Socket socket = new Socket();
            InetSocketAddress addr = new InetSocketAddress("localhost", 4444);
            socket.connect(addr);

            InputStreamReader istr =
                new InputStreamReader(socket.getInputStream());
            BufferedReader in = new BufferedReader(istr);
            PrintWriter out =
                new PrintWriter(socket.getOutputStream());

            out.println(toprint);
            out.flush();
            toread1 = in.readLine();

            try {
                Thread.sleep(10);
            } catch (Exception e) { }

            out.println(toprint);
            out.flush();
            toread2 = in.readLine();

            out.close();
            socket.close();

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) { }

            assertTrue(toread1.endsWith(toprint));
            assertTrue((toread2 == null) || (toread2.endsWith(toprint)));
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}

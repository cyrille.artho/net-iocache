/* -*-mode:java; c-basic-offset:2; indent-tabs-mode:nil -*- */

package gov.nasa.jpf.network.scp;

import com.jcraft.jsch.*;

import gov.nasa.jpf.vm.Verify;

import javax.swing.*;
import java.io.*;

public class ScpTo {

	static class Worker extends Thread {

		private JSch jsch;

		private String user;

		private String host;

		private String lfile;

		private String rfile;

		Worker(JSch engine, String user, String host, String lfile, String rfile) {
			jsch = engine;
			this.user = user;
			this.host = host;
			this.lfile = lfile;
			this.rfile = rfile;
		}

		public void run() {
			try {
				Session session = jsch.getSession(user, host, PORT);

				// username and password will be given via UserInfo interface.
				Verify.beginAtomic();
				UserInfo ui = new MyUserInfo();
				session.setUserInfo(ui);
				session.connect();

				boolean ptimestamp = true;

				// exec 'scp -t rfile' remotely
				String command = "scp " + (ptimestamp ? "-p" : "") + " -t "
						+ rfile;
				Channel channel = session.openChannel("exec");
				((ChannelExec) channel).setCommand(command);

				// get I/O streams for remote scp
				OutputStream out = session.getOutputStream();
				Verify.endAtomic();

				channel.connect();

				if (ptimestamp) {
					long lastMod = 1276004506000L;

					command = "T " + (lastMod / 1000) + " 0";
					// The access time should be sent here,
					// but it is not accessible with JavaAPI ;-<
					command += (" " + (lastMod / 1000) + " 0\n");
				}

				// send "C0644 filesize filename", where filename should not
				// include
				// '/'
				// long filesize=_lfile.length();
				long filesize = MESSAGE.length();
				command = "C0644 " + filesize + " ";
				if (lfile.lastIndexOf('/') > 0) {
					command += lfile.substring(lfile.lastIndexOf('/') + 1);
				} else {
					command += lfile;
				}
				command += "\n";

				// send a content of lfile
				// fis=new FileInputStream(lfile);
				// byte[] buf=new byte[1024];
				int msgLen = MESSAGE.length();
				byte[] buf = new byte[msgLen + 1];
				byte paddingByte = (byte) Verify.getInt(0, 1);
				
				buf[msgLen] = paddingByte;
				
				System.arraycopy(MESSAGE.getBytes(), 0, buf, 0, msgLen);
				
				// while(true){
				// int len=fis.read(buf, 0, buf.length);
				// if(len<=0) break;
				// out.write(buf, 0, len); //out.flush();
				// }
				System.out.printf("[Worker : run] send a content of lfile\n");
				out.write(buf, 0, buf.length);
				out.flush();

				// fis.close();
				// fis=null;
				// send '\0'
				buf[0] = 0;
				out.write(buf, 0, 1);
				out.flush();
				out.close();

//				channel.disconnect();
//				session.disconnect();
				System.out.printf("[Worker : run] End\n");
			} catch (JSchException e) {
				assert (false);
			} catch (IOException e) {
				assert (false);
			}
		}

	}

	static final String MESSAGE = "Text sent by scp\n";

	static final int PORT = 19000;

	public static void main(String[] arg) throws JSchException {
		if (arg.length != 3) {
			System.err.println("usage: java ScpTo file1 user@remotehost:file2 (#connections)");
			System.exit(-1);
		}

		String lfile = arg[0];
		String user = arg[1].substring(0, arg[1].indexOf('@'));
		arg[1] = arg[1].substring(arg[1].indexOf('@') + 1);
		String host = arg[1].substring(0, arg[1].indexOf(':'));
		String rfile = arg[1].substring(arg[1].indexOf(':') + 1);
		int numConn = Integer.parseInt(arg[2]);

		JSch jsch = new JSch();
		
		for (int i = 0; i < numConn; i++) {
			new Worker(jsch, user, host, lfile, rfile).start();
		}
	}
	
	static int checkAck(InputStream in) throws IOException {
		int b = in.read();
		// b may be 0 for success,
		// 1 for error,
		// 2 for fatal error,
		// -1
		if (b == 0)
			return b;
		if (b == -1)
			return b;

		if (b == 1 || b == 2) {
			StringBuffer sb = new StringBuffer();
			int c;
			do {
				c = in.read();
				sb.append((char) c);
			} while (c != '\n');
			if (b == 1) { // error
				System.out.print(sb.toString());
			}
			if (b == 2) { // fatal error
				System.out.print(sb.toString());
			}
		}
		return b;
	}

	public static class MyUserInfo implements UserInfo, UIKeyboardInteractive {
		public String getPassword() {
			return passwd;
		}

		public boolean promptYesNo(String str) {
			Object[] options = { "yes", "no" };
			int foo = JOptionPane.showOptionDialog(null, str, "Warning",
					JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
					null, options, options[0]);
			return foo == 0;
		}

		String passwd;

		public String getPassphrase() {
			return null;
		}

		public boolean promptPassphrase(String message) {
			return true;
		}

		public boolean promptPassword(String message) {
			return false;
		}

		public void showMessage(String message) {
			JOptionPane.showMessageDialog(null, message);
		}

		public String[] promptKeyboardInteractive(String destination,
				String name, String instruction, String[] prompt, boolean[] echo) {
			return null;
		}
	}
}

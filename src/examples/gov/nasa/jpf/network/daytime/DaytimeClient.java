//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.daytime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;

public class DaytimeClient {

	static class ClientThread extends Thread {
		int id;
		int port;

		ClientThread(int id, int port) {
			this.id = id;
			this.port = port;
		}

		public void run() {
			try {
				Socket socket = new Socket();
				InetSocketAddress addr = new InetSocketAddress("localhost", port);
				socket.connect(addr);
				InputStreamReader istr = new InputStreamReader(socket.getInputStream());
				BufferedReader in = new BufferedReader(istr);
				String line;
				while ((line = in.readLine()) != null) {
					System.out.println("[DaytimeClient.ClientThread : run(), id=" + id + "] Received " + line);
				}
			} catch (IOException e) {
			}
		}
	}

	public final static void main(String args[]) {
		ClientThread[] t;
		int num_thread = Integer.parseInt(args[0]);
		int port = 1024;

		if (args.length > 1)
			port = Integer.parseInt(args[1]);

		t = new ClientThread[num_thread];

		for (int i = 0; i < num_thread; i++)
			t[i] = new ClientThread(i, port);

		for (int i = 0; i < num_thread; i++)
			t[i].start();
	}

}

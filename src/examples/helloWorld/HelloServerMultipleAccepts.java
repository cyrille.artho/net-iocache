package helloWorld;

import java.io.*;
import java.net.*;

public class HelloServerMultipleAccepts {

	public static final int SERVER_PORT = 18586;
	private static int accessCount=0;

	private static void wait(int millis) {
		try{Thread.sleep(millis);}catch(InterruptedException e){};
	}
	
	private static void processRequest(ServerSocket ssock) throws IOException {
		Socket sock = ssock.accept();
		System.out.println("[HelloServer.processRequest] accepted "+(++accessCount)+". connection.");
		
		InputStream  in =sock.getInputStream();
		OutputStream out=sock.getOutputStream();

		byte[] buf=new byte[10];
		int len;
		do {
			len=in.read(buf);
			if (len>0) {
				String request=new String(buf,0,len);
				System.out.println("[HelloServer.processRequest] received message '"+request+"'.");
				
				final String response="world!";
				out.write(response.getBytes()); 
				System.out.println("[HelloServer.processRequest] sent message '"+response+"'.");			
			}
		} while (len!=-1);
	    wait(50);    //wait a bit to synchronize output with the client in the log file
		System.out.println("[HelloServer.processRequest] received eof.");						
		
		sock.close();
	}
	
	/**
	 * Probably the simplest 'non-trivial' client-server application.
	 */
	public static void main(String[] args) throws IOException {
		int maxAccepts=args.length>0?Integer.parseInt(args[0]):0;
		
		ServerSocket ssock = new ServerSocket(SERVER_PORT);
		System.out.println("[HelloServer.main] waiting for"+
				(maxAccepts<0?"":" a maximum of "+maxAccepts)+
				" connection requests at port "+SERVER_PORT);
		
		for (int i=0; i!=maxAccepts; i++)  
			processRequest(ssock);
		
	    ssock.close();
		System.out.println("[HelloServer.main] closed server port and says good bye!");
	}
}

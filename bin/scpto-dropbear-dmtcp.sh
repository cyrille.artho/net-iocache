#!/bin/bash
# Run the ScpTo implementation of JSch against Dropbear running on DMTCP.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

chk_vdso

# Set the location of checkpoints
export CHKPNT_DIR="${PROJ_TOP}/tmp-jsch"
# Set the SUT port number. This value must be exported so that DMTCP can read.
export SUT_PORT="8791"

# Use an external Jsch, not the one in directory "examples".
JPF_ARG="+classpath=${HOME}/jsch/tmp +report.console.property_violation=error,trace +listener=gov.nasa.jpf.network.listener.CacheNotifier,gov.nasa.jpf.network.listener.CacheLogger +search.min_free=16K"

# Option "main.termination" must be false.
JPF_ARG=`echo "${JPF_ARG} +${OPT_MAIN_TERMINATION}=false +${OPT_VIRTUAL_MODE}=true +${OPT_CHECKPOINT_DIR}=${CHKPNT_DIR} +${OPT_LAZY}=false +${OPT_DMTCP_ENABLED}=true +${OPT_SUT_PORT}=${SUT_PORT}" | sed -e 's/CacheNotifier/VirtualizationCacheNotifier/' -e 's/CacheLogger/VirtualizationCacheLogger/'`

PROG_ARG="ScpTo ${HOME}/dummy watcharin@localhost:tmp.txt"
CLIENT_LOG="${HOME}/scp.log"

# Recreate the checkpoint directory.
rm -rf ${CHKPNT_DIR}
mkdir ${CHKPNT_DIR}
# Remove any checkpoint file left on this directory.
rm -f ${SCRIPT_DIR}/*.dmtcp

# Must run the coordinator with option "background", otherwise the peer cannot restart.
${DMTCP_COORDINATOR} --ckptdir ${CHKPNT_DIR} --background

# Start JPF. It will wait until Proxy sets up a Unix domain channel.
${RUN_JPF_JNILIB} ${JPF_ARG} ${PROG_ARG} > ${CLIENT_LOG} &
JPF_PID=$!
sleep 2

# Start a server running on DMTCP.
${DMTCP_CHECKPOINT} ${HOME}/dropbear/dropbear -F -E -s -d ${HOME}/dropbear_dss_host_key -r ${HOME}/dropbear_rsa_host_key -p 8000 &
# Save the peer PID.
echo $! > ${CHKPNT_DIR}/${PEER_PID_FILE}
# Start Proxy. Must run in background to avoid the problem about "stdout".
${DMTCP_CHECKPOINT} ${CBUILD_DIR}/proxy &

# Wait until JPF terminates.
wait ${JPF_PID}

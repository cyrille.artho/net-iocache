//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.vm;

import static gov.nasa.jpf.network.SystemInfo.PROJ_NAME;
import gov.nasa.jpf.network.cache.CacheLayer;

import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.NativePeer;
import java.io.IOException;

public class JPF_java_net_ServerSocket extends NativePeer{

	/**
	 * checks whether the limit for number of accept, as configured in 
	 * property jpf-net-iocache.exitSUT.onAccept.count, has been reached. 
	 * @param env reference to the Model Java Interface (MJI) environment
	 * @param objRef MJI reference to 'this' object 
	 * @param acceptCount current number of calls to accept() 
	 * @return exit status code >=0 configured in 
	 * jpf-net-iocache.exitSUT.onAccept.exitStatus if accept limit has been reached <br>
	 *         or -1 if accept limit is not reached or does not apply.
	 */
	
	/**
	 * native peer of SocketServer.accept():
	 * advise CacheLayer to launch the remote client peer and 
	 * accept its 'physical' connection request.
	 * If the limit for the number of accept, as configured in 
	 * property jpf-net-iocache.maxAccept, has been reached,
	 * state space exploration is suspended 
	 * @param env reference to the Model Java Interface (MJI) environment
	 * @param objRef MJI reference to 'this' object 
	 * @param acceptCount total of accepted connection requests on the current execution path
	 * @return socket id >=0 of the accepted socket for communicating with the remote peer <br>
	 *         or -1 if no physical socket has been accepted because of accept limit reached or IOException
	 */
	@MJI
	public int nativeAccept__I__I(MJIEnv env, int objRef, int acceptCount) throws IOException {
		//check limit for number of accepts allowed on a single execution trace
		if (acceptCount==env.getConfig().getInt(PROJ_NAME + ".maxAccept", -1)) {
			CacheLayer.log.info("[ServerSocket.accept] reached limit "+acceptCount+
					" for the number of accepted connections;" +
					"backtracking to the last incomplete choice point...");
			env.getVM().getSystemState().setIgnored(true); //this breaks the current transition and stops further state exploration
			return -1;
		}
		
		CacheLayer.setServerSUT(true);  //FIXME switch to server mode --> distinction between server mode and client mode should be eliminated in future
		CacheLayer cache = CacheLayer.getInstance();
		return cache.accept();
	}

	/**
	 * bind server socket to a logical port
	 * @param env reference to the Model Java Interface (MJI) environment
	 * @param objRef MJI reference to 'this' object 
	 * @param logicalPort port the server socket to bind to or 0 if a free port should be chosen
	 * @return port the server socket has been bound to (==<b>logicalPort</b> if <b>logicalPort</b>>0) 
	 * @throws IOException
	 */
	@MJI
	public int nativeBind__I__I(MJIEnv env, int objRef, int logicalPort) throws IOException {
		return CacheLayer.getInstance().bind(logicalPort);
	}
	
}

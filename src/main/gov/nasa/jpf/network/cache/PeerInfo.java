package gov.nasa.jpf.network.cache;

import gov.nasa.jpf.network.HelperMethods;

import java.util.ArrayList;
import java.util.List;

/**
 * An instance of this type contains the information of the peer processes
 * corresponding to each physical connection.
 * 
 * @author watcharin
 * 
 */
public class PeerInfo implements Cloneable {

	/**
	 * pids[i] is the PID of the peer of physical connection <i>i</i>. This list
	 * corresponds to member <i>conn_list</i> of <i>CacheLayer</i>.
	 */
	private List<List<Integer>> pids = new ArrayList<List<Integer>>();

	public PeerInfo() {
		this(new ArrayList<List<Integer>>());
	}

	private PeerInfo(List<List<Integer>> pids) {
		this.pids = pids;
	}

	protected Object clone() {
		List<List<Integer>> copy = new ArrayList<List<Integer>>();

		for (List<Integer> lst : pids) {
			List<Integer> inner = new ArrayList<Integer>();

			for (int i : lst) {
				inner.add(i);
			}

			copy.add(inner);
		}

		return new PeerInfo(copy);
	}

	void addConnection() {
		pids.add(new ArrayList<Integer>());
	}

	/**
	 * Register the given PID to the last connection.
	 * 
	 * @param pid
	 */
	void register(int pid) {
		int idx = pids.size() - 1;

		register(pid, idx);
	}

	void register(int pid, int conn) {
		pids.get(conn).add(pid);
	}

	void unregister(int pid, int conn) {
		List<Integer> owners = pids.get(conn);
		boolean success = owners.remove(new Integer(pid));
		assert(success);
	}

	int getFirstPeerPid(int conn) {
		return pids.get(conn).get(0);
	}
	
	int[] getPeerPids(int conn) {
		List<Integer> lst = pids.get(conn);
		
		return HelperMethods.collectionToIntArray(lst);
	}
	
	boolean isOwnedBy(int conn, int pid) {
		return pids.get(conn).contains(pid);
	}

}

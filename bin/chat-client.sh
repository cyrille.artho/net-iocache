#!/bin/bash
# Start the chat client.

. `dirname "$0"`/env.sh

PACKAGE="gov.nasa.jpf.network.chat"

java -classpath ${CLASSPATH} ${PACKAGE}.ChatClientRunner 1

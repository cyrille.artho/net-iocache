/*
  This program simulates behavior of a SSH server.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <fcntl.h>
#include <pthread.h>
#include <math.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>


#define MYPORT 	"19000"  // the port users will be connecting to
#define BACKLOG 	10     // how many pending connections queue will hold


typedef struct sockaddr_storage sockaddr_storage;
typedef struct addrinfo addrinfo;


void *worker_run(void *);


const int num_branches = 2;
int num_clients;


int main(int argc, char **argv) {
    sockaddr_storage their_addr;
    socklen_t addr_size;

    addrinfo hints;
    addrinfo *servinfo;
    addrinfo *p;
	pthread_t threads[8];

    int sockfd, new_fd;
    int yes = 1;
	int rv;
	int i;

	if (argc < 2) {
	  perror("Usage: secure-server (#clients)");
	  return 1;
	}

	num_clients = (int) strtol(argv[1], NULL, 0);

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    getaddrinfo(NULL, MYPORT, &hints, &servinfo);

    // loop through all the results and bind to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
	  if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
		perror("server: socket");
		continue;
	  }

	  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		perror("setsockopt");
		exit(1);
	  }

	  if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
		close(sockfd);
		perror("server: bind");
		continue;
	  }

	  break;
    }

    if (p == NULL) {
        fprintf(stderr, "server: failed to bind\n");
        return 2;
    }

    freeaddrinfo(servinfo); // all done with this structure

    listen(sockfd, BACKLOG);

    // now accept an incoming connection:
    addr_size = sizeof their_addr;

	for (i = 0;i < num_clients;i++) {
		printf("waiting ...\n");
		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);

		// ready to communicate on socket descriptor new_fd!
		printf("accepted socket %d\n", new_fd);

		rv = pthread_create(&threads[i], NULL, worker_run, (void *) new_fd);
		if (rv) {
			printf("ERROR; return code from pthread_create() is %d\n", rv);
			exit(-1);
		}
	}

	close(sockfd);

	for (i = 0;i < num_clients;i++) {
		pthread_join(threads[i], NULL);
	}
	
	printf("scp-server ends\n");
	return 0;
}

void *worker_run(void *id) {
	char c[32];
	int new_fd = (int) id;
	int rv;
	int g = htonl(5);
	int n = htonl(23);
	int b;
	int y;
	int a;
	int key;

	// Receive a greeting message.
	recv(new_fd, c, sizeof(c), 0);
	
	// Send G and n.
	send(new_fd, &g, sizeof(g), 0);
	send(new_fd, &n, sizeof(n), 0);

	// Key exchange.
	srand(time(0));
	y = rand();
	b = (int) pow(g, y) % n;
	recv(new_fd, &a, sizeof(a), 0);
	send(new_fd, &b, sizeof(b), 0);
	key = (int) pow(a, y) % n;

	memset(c, 0, sizeof c);

	rv = 1;
	while (rv > 0) {
		rv = recv(new_fd, c, sizeof(c), 0);
		printf("[%s][%d] Packet received (%d)\n", __func__,(int) id, rv);
	}
	
	close(new_fd);
	pthread_exit(NULL);
}

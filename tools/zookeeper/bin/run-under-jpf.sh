#!/bin/bash

JPF=~/jpf/jpf-core/bin/jpf

ZOOKEEPER_DIR=~/zookeeper

CLASSES=$ZOOKEEPER_DIR/build/classes
TESTS=$ZOOKEEPER_DIR/build/test/classes

LIBDIR=$ZOOKEEPER_DIR/build/lib
DEPS=$(echo $(find "$LIBDIR" -name "*.jar") | sed "s|\s|:|g")

TEST_LIBDIR=$ZOOKEEPER_DIR/build/test/lib
TEST_DEPS=$(echo $(find "$TEST_LIBDIR" -name "*.jar") | sed "s|\s|:|g")

CLASSPATH=$CLASSES:$TESTS:$DEPS:$TEST_DEPS

if [[ -n "$1" ]]
then
  MAIN_CLASS=$1
else
  MAIN_CLASS=org.apache.zookeeper.test.ZooKeeperTestClient
fi

echo $JPF +classpath=$CLASSPATH $MAIN_CLASS
$JPF +classpath=$CLASSPATH $MAIN_CLASS

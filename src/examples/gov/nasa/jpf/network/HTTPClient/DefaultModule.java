//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

/*
 * @(#)DefaultModule.java				0.3-3 06/05/2001
 *
 *  This file is part of the HTTPClient package
 *  Copyright (C) 1996-2001 Ronald Tschalr
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA 02111-1307, USA
 *
 *  For questions, suggestions, bug-reports, enhancement-requests etc.
 *  I may be contacted at:
 *
 *  ronald@innovation.ch
 *
 *  The HTTPClient's home page is located at:
 *
 *  http://www.innovation.ch/java/HTTPClient/ 
 *
 */

package gov.nasa.jpf.network.HTTPClient;

import java.io.IOException;
import java.net.ProtocolException;


/**
 * This is the default module which gets called after all other modules
 * have done their stuff.
 *
 * @version	0.3-3  06/05/2001
 * @author	Ronald Tschalr
 */
class DefaultModule implements HTTPClientModule
{
    /** number of times the request will be retried */
    private int req_timeout_retries;


    // Constructors

    /**
     * Three retries upon receipt of a 408.
     */
    DefaultModule()
    {
	req_timeout_retries = 3;
    }


    // Methods

    /**
     * Invoked by the HTTPClient.
     */
    public int requestHandler(Request req, Response[] resp)
    {
	return REQ_CONTINUE;
    }


    /**
     * Invoked by the HTTPClient.
     */
    public void responsePhase1Handler(Response resp, RoRequest req)
    {
    }


    /**
     * Invoked by the HTTPClient.
     */
    public int responsePhase2Handler(Response resp, Request req)
	    throws IOException
    {
	/* handle various response status codes until satisfied */

	int sts  = resp.getStatusCode();
	switch(sts)
	{
	    case 408: // Request Timeout

		if (req_timeout_retries-- == 0  ||  req.getStream() != null)
		{
		    Log.write(Log.MODS, "DefM:  Status " + sts + " " +
				    resp.getReasonLine() + " not handled - " +
				    "maximum number of retries exceeded");

		    return RSP_CONTINUE;
		}
		else
		{
		    Log.write(Log.MODS, "DefM:  Handling " + sts + " " +
					resp.getReasonLine() + " - " +
					"resending request");

		    return RSP_REQUEST;
		}

	    case 411: // Length Required
		if (req.getStream() != null  &&
		    req.getStream().getLength() == -1)
		    return RSP_CONTINUE;

		try { resp.getInputStream().close(); }
		catch (IOException ioe) { }
		if (req.getData() != null)
		    throw new ProtocolException("Received status code 411 even"+
					    " though Content-Length was sent");

		Log.write(Log.MODS, "DefM:  Handling " + sts + " " +
				    resp.getReasonLine() + " - resending " +
				    "request with 'Content-length: 0'");

		req.setData(new byte[0]);	// will send Content-Length: 0
		return RSP_REQUEST;

	    case 505: // HTTP Version not supported
		return RSP_CONTINUE;

	    default:
		return RSP_CONTINUE;
	}
    }


    /**
     * Invoked by the HTTPClient.
     */
    public void responsePhase3Handler(Response resp, RoRequest req)
    {
    }


    /**
     * Invoked by the HTTPClient.
     */
    public void trailerHandler(Response resp, RoRequest req)
    {
    }
}

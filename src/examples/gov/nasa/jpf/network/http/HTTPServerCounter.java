//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.http;

import java.net.*;
import java.io.*;

// Small, simple HTTP server
public class HTTPServerCounter {

    static final int PORT = 8080;

	String NAME;

	String VERSION;

	int serverPort;

	// No command line parameters are required
	public static void main(String args[]) {
		HTTPServerCounter server = new HTTPServerCounter("[HTTPServerCounter]", "1.0", PORT);
		server.run(args);
	}

	// Create an HTTPServerCounter for a particular TCP port
	public HTTPServerCounter(String name, String version, int port) {
		this.NAME = name;
		this.VERSION = version;
		this.serverPort = port;
	}

	// Display name and version number
	public void displayVersionInfo() {
		System.out.println(NAME + " version " + VERSION+" started...");
	}

	// Run until interrupted
	public void run(String[] args) {
		//displayVersionInfo();
		try {
			// Get a server socket
			ServerSocket server = getServerSocket();
			int localPort = server.getLocalPort();
			int numAccept = Integer.parseInt(args[0]);
			// Let us know that you're listening
			System.out.println(NAME + " is listening on port " + localPort + ".");
//			if (numAccept!=0) 
//				System.out.println(NAME + " will terminate after having accepted and served "+numAccept+" connection(s).");
			for (int i = 0; i < numAccept || numAccept == 0; i++) {
				// Accept a connection
				Socket client = server.accept();
				// Handle the connection with a separate thread
				System.out.println(NAME+ " accepted "+(i+1)+". connection. Start HTTPServerThread...");
				(new HTTPServerCounterThread(client)).start();
			}
		} catch (Exception ex) {
			System.out.println(NAME + " unable to listen on " + serverPort + ".");
			ex.printStackTrace();
			System.exit(1);
		}
//		System.out.println(NAME+" says good bye!");
	}

	// Get a server socket on the hard-wired server port
	ServerSocket getServerSocket() throws Exception {
		return new ServerSocket(serverPort);
	}
}

// Handle a single server connection
class HTTPServerCounterThread extends HTTPServerThread {
	
	static int counter = 0;

	// Keep track of the client socket
	public HTTPServerCounterThread(Socket client) {
		super(client);
	}

	// A simple HTTP 1.0 response
	void sendFile(BufferedOutputStream out, File file) {
		try {
			DataInputStream in = new DataInputStream(new FileInputStream(file));
			int len = (int) file.length();
			byte buffer[] = new byte[len];
			in.readFully(buffer);
			in.close();
			out.write(getBytes("HTTP/1.0 200 OK\r\n"));
			out.write(getBytes("Content-Length: " + buffer.length + "\r\n"));
			out.write(getBytes("Content-Type: text/html\r\n"));
			out.write(getBytes("Counter: " + ++counter + "\r\n\r\n"));
			out.write(buffer);
			out.flush();
			out.close();
			System.out.println("\n[HTTPServerCounterThread.sendFile, id="+id+"] file sent: " + file.getCanonicalPath()+
					           "\n[HTTPServerCounterThread.sendFile, id="+id+"]   "+len+" bytes; counter="+counter+".\n");
		} catch (IOException ex) {
			System.err.println("[sendFile()] " + ex);
			try {
				System.err.println("[HTTPServerCounterThread.sendFile, id="+id+"]  error when sending file "+ file.getCanonicalPath() +":\n"+ex);
				out.write(getBytes("HTTP/1.0 400 " + "No can do" + "\r\n"));
				out.write(getBytes("Content-Type: text/html\r\n\r\n"));
			} catch (IOException ioe) {}
		}
	}
}
//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import gov.nasa.jpf.JPF;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.logging.Logger;

/**
 * PhysicalConnection represents an actual connection maintained by the cache layer. It keeps both in-stream and out-stream
 * positions as node identifiers in the tree.
 * 
 * @author watcharin
 * 
 */
public class PhysicalConnection {

	static int num_instances = 0;

	Socket socket;

	InputStream in_stream;

	OutputStream out_stream;
	
	Logger log = JPF.getLogger("gov.nasa.jpf.network.cache.PhysicalConnection");

	int id;

	/**
	 * How much the connection sends messages out. It is stored as a node id.
	 */
	int out_stream_pos;

	/**
	 * How much the connection reads messages. It is stored as a node id.
	 */
	int in_stream_pos;

	PhysicalConnection() {
		
	}
	
	PhysicalConnection(Socket s, int out, int in) throws IOException {
		assert s != null : "Parameter socket is null";
		socket = s;
		in_stream = s.getInputStream();
		out_stream = s.getOutputStream();
		id = num_instances++;
		out_stream_pos = out;
		in_stream_pos = in;
	}

	InputStream getInputStream() {
		return in_stream;
	}

	OutputStream getOutputStream() {
		return out_stream;
	}

	/**
	 * 
	 * @param out
	 * @param in
	 * @return True, if the in-stream and out-stream positions equal to <i>in</i> and <i>out</i>, respectively; otherwise, false.
	 */
	boolean match(int out, int in) {
		return out_stream_pos == out && in_stream_pos == in;
	}

	void setOutStreamPos(int out) {
		out_stream_pos = out;
	}

	void setInStreamPos(int in) {
		in_stream_pos = in;
	}

	int available() throws IOException {
		return in_stream.available();
	}
	
	/**
	 * blocking read of a single byte from this physical connection;
	 * Returns -1 (eof) if no more data is available and the peer has closed the connection.  
	 * @return 0..255 (received data byte) or -1 (eof)
	 * @throws IOException
	 */
	int read() throws IOException {
		return in_stream.read();
	}
	
	/**
	 * Tries to read at most <b>buffer.length</b> bytes from this physical connection; 
	 * Sleeps for <b>delay</b> ms before reading data from the input stream; This is 
	 * to ensure that the message received is complete, i.e., not just a partial response to a 
	 * previously sent request. 
	 * @param buffer   buffer larger than 0 bytes to store the read data.
	 * @param delay    time in milliseconds to wait for a message; if 0 then perform an undelayed blocking read 
	 * @return   
	 * -1 (<i>eof</i>) if no data is available and the peer has closed the connection; <br/>
	 * 0 (<i>timeout</i>) if no message has received within <b>timeout</b> ms and the peer has not closed the connection;<br/> 
	 * > 0 (<i>number of read bytes</i>) otherwise. 

	 * @throws IOException
	 */
	int read(byte[] buffer, int delay) throws IOException {
		assert buffer!=null && buffer.length>0 : "Buffer must not be null or 0 bytes large."; 

		if (delay>1) try {
			Thread.sleep(delay-1);
		} catch (InterruptedException e) {
			assert false : "Severe error '"+e.getMessage()+
			               "' in PhysicalConnection.read() with delay "+delay+"\n"+e.getStackTrace();
		}
		
		int bytes=0; 
		synchronized (socket) {                       //ensure atomic access to this socket
			int savedTimeout = socket.getSoTimeout();  //save current time out setting
			socket.setSoTimeout(delay==0?0:1);         //block for maximum 1 ms if delay>0 (simulates non-blocking read)
			try {
				bytes = in_stream.read(buffer);
			} catch (InterruptedIOException e) {}
			socket.setSoTimeout(savedTimeout);         //restore previous setting
		}
		return bytes;
	}

	void write(byte c) throws IOException {
		out_stream.write(c);
	}

	void write(byte[] msg, int off, int len) throws IOException {
		out_stream.write(msg, off, len);
	}
	
	Socket getSocket() {
		return socket;
	}

	public boolean isConnected() {
		return socket.isConnected();
	}

	void close() throws IOException {
		if (isClosed())     //if already closed...
			return;          //... nothing to do!

		in_stream.close();
		out_stream.close();
		socket.close();
	}

	boolean isClosed() {
		return socket.isClosed();
	}
	
	/**
	 * 
	 * @return True if the remote socket is closed; false, otherwise.
	 */
	boolean isRemoteSocketClosed() {
		final int TIMEOUT = 10;
		
		if (isClosed())
			return true;
		
		try {
			int c;
			
			// If there is something to read, it is not closed.
			if (in_stream.available() > 0)
				return false;
			
			// Set small timeout.
			socket.setSoTimeout(TIMEOUT);			
			// If method "read" returns -1, the remote socket is closed.
			c = in_stream.read();
			assert c < 0;
			
			return true;
		}
		catch (SocketTimeoutException e) {
			return false;
		}
		catch (IOException e) {
			return true;
		} finally {
			try {
				// Disable timeout.
				socket.setSoTimeout(0);
			} catch (SocketException e) {
				assert false;
			}
		}
	}

	public InetAddress getInetAddress() {
		return socket.getInetAddress();
	}

	public int getPort() {
		return socket.getPort();
	}
	
	public int getLocalPort() {
		return socket.getLocalPort();
	}

	public String toString() {
		return "[" + socket.toString() + " out=" + out_stream_pos + ",in=" + in_stream_pos + "]";
	}
	
	void changeSocket(Socket s) {
		socket = s;
		
		// Must update input/output streams.
		try {
			in_stream = s.getInputStream();
			out_stream = s.getOutputStream();
		} catch (IOException e) {
			assert false : "Cannot get I/O streams";
		}
	}
	
}

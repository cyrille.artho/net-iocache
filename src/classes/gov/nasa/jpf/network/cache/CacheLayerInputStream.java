//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import gov.nasa.jpf.vm.Verify;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * Do a part of the InputStream class. The target application reads streams from instances of this class instead of those from the
 * InputStream class.
 * 
 * @author watcharin
 * 
 */
public class CacheLayerInputStream extends InputStream {

	static Object lock;
	//static final int TimeOut=10000;    //set time out for read() to 10 seconds
	//static final int TimeOut=0;      //set time out for read() to infinite

	private Socket sock;
	private int id;

	public CacheLayerInputStream(Socket sock) {
		this.sock = sock;
		this.id = sock.socketID;
	}
	
	native int native_available();

	native int native_read() throws IOException;

	native int native_read(byte[] b, int off, int len) throws IOException;

	native boolean isNextReadBlocked();
	
	/*
	 * native boolean waitForReadReady(int timeOutMillis)
	 *   Waiting until at least one byte is received from the network peer or timeOutMillis is reached
	 *   called by read(...) methods
	 *   @param timeOutMillis: timeOut limit in milliseconds: if zero then wait until message is received 
	 *                         from the peer or an IOException has occurred 
	 *   @return true  if a message has received (and can be read from the cache subsequently) 
	 *           false if no message has been received within timeOutMillis or an IOException has occurred
	 *   @see read(...) 
	 *           
	 */
	//native boolean waitForReadReady(int timeOutMillis);
	
	private String waitForReadReady() {
		String res=null;
		try {
			while (isNextReadBlocked()) 
				lock.wait(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
			res=e.getMessage();
		}
		return res;
	}
	

	public static void setLock(Object lock) {
		CacheLayerInputStream.lock = lock;
	}

	private void checkMainAliveness() {
		if (Socket.isMainTerminationSet())
			Verify.ignoreIf(Socket.isMainAlive());
	}

	public int read() throws IOException {
		int res = 0;

		checkMainAliveness();

		if (Socket.isExceptionSimulationSet()) {
			boolean exception = Verify.getBoolean();

			if (exception)
				throw new IOException("[Simulated] an I/O error occurs");
		}

		synchronized (lock) {
			String err=waitForReadReady();
			if (err!=null) 
				throw new IOException("Native read from peer failed ("+err+").");
			else 
				res = native_read();
		}
		return res;
	}

	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}
	
	public int read(byte[] b, int off, int len) throws IOException {
		int res = 0;

		checkMainAliveness();

		if (Socket.isExceptionSimulationSet()) {
			boolean exception = Verify.getBoolean();

			if (exception)
				throw new IOException("[Simulated] an I/O error occurs");
		}

		synchronized (lock) {
			String err=waitForReadReady();
			if (err!=null) 
				throw new IOException("Native read from peer failed ("+err+").");
			else 
				res = native_read(b, off, len);
		}
		return res;
	}

	public boolean isChannelReadable() {
		return !isNextReadBlocked();
	}

	public int available() {
		return native_available();
	}

	public void close() throws IOException {
		sock.close();
	}
	
}

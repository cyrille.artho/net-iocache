#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/poll.h>
#include <netinet/in.h>
#include <mtcp.h>

#include "mtcpsetup.h"
#include "ncp.h"


// From mtcp.c
extern int dmtcp_info_pid_virtualization_enabled;

const int HOUR = 3600;
const int FIFO_NAME_MAX_SIZE = 512;
const char *CHKPNT_DIR = "CHKPNT_DIR";
const char *TMP_CHKPNT = "testmtcp.mtcp";
const char *PIPE_NAME = "ckpt_sig";


// This function blocks until it gets a signal to write a checkpoint from the cache.
void ckpt_trigger(int sec) {
	char s[32];
	char fifo[FIFO_NAME_MAX_SIZE];
	char *dir = getenv(CHKPNT_DIR);
	fd_set set;
	int num, fd;

	if (!dir)
		perror("CHKPNT_DIR is not defined");

	// Build the pipe name.
	memset(fifo, 0, FIFO_NAME_MAX_SIZE);
	sprintf(fifo, "%s/%s", dir, PIPE_NAME);

	mknod(fifo, S_IFIFO | 0666, 0);

	fd = open(fifo, O_RDONLY);

    /* Initialize the file descriptor set. */
    FD_ZERO (&set);
    FD_SET (fd, &set);

    select(FD_SETSIZE, &set, NULL, NULL, NULL);

    if (FD_ISSET(fd, &set)) {
        if ((num = read(fd, s, 32)) == -1)
    		perror("read");
    }

    gnu_close(fd);
//	printf("[%s] Save state\n", __func__);
}

void mtcpsetup_premain() {
	char *chkpnt_dir = getenv(CHKPNT_DIR);
	char tmp_chkpnt_full[FIFO_NAME_MAX_SIZE];

	if (!chkpnt_dir)
		perror("Environmental variable CHKPNT_DIR is not defined");

	// Must initialize this external variable, otherwise an error occurs when restarting
    dmtcp_info_pid_virtualization_enabled = 0;

    // Build the full temporary checkpoint name.
    memset(tmp_chkpnt_full, 0, FIFO_NAME_MAX_SIZE);
    sprintf(tmp_chkpnt_full, "%s/%s", chkpnt_dir, TMP_CHKPNT);

//    pid_t tid = (pid_t) syscall (SYS_gettid);
//
//    printf("[init] tid %d\n", tid);
//    printf("[init] pid %d\n", getpid());

	// Replace function sleep with our custom function.
    mtcp_set_callbacks(ckpt_trigger, NULL, mtcpsetup_post_ckpt, NULL, NULL, NULL);
    mtcp_init(tmp_chkpnt_full, HOUR, 1);
    mtcp_ok();
}

void mtcpsetup_post_ckpt(int is_restarting, char *dummy) {
//	printf("[%s]\n", __func__);
	char *dir = getenv(CHKPNT_DIR);
	char fifo[FIFO_NAME_MAX_SIZE];
	char s[2];
	int fd;
	int num;
	
	if (!dir)
		perror("CHKPNT_DIR is not defined");

	// in case of restoring, rebind sockets
	if (is_restarting)
		ncp_rebind_all_sockets();

	// Build the pipe name.
	memset(fifo, 0, FIFO_NAME_MAX_SIZE);
	sprintf(fifo, "%s/%s", dir, PIPE_NAME);

	// notify the cache in both cases
	mknod(fifo, S_IFIFO | 0666, 0);
	memset(s, 0, sizeof s);
	
	fd = open(fifo, O_WRONLY);
	s[0] = 1;
		
	if ((num = write(fd, s, strlen(s))) == -1)
		perror("write");

	gnu_close(fd);
}

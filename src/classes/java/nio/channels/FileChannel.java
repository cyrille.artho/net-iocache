package java.nio.channels;

import java.io.IOException;
import java.nio.ByteBuffer;

public abstract class FileChannel {
	public abstract int read(ByteBuffer dst) throws IOException;
}

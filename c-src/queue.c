/** Thread-safe queue allowing concurrent addition or removal of
    multiple elements. */
/* Each put/remove operation is atomic, although the size of a block
   removed may not correspond to the size of the corresponding block
   that was added in the past. */
/* C port of Queue.java */
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/errno.h>
#include "queue.h"

queue *queue_new (int size) {
    assert (size <= INT_MAX / 2);
    queue *q = malloc (sizeof (queue));
    q->data = malloc (sizeof (q->data[0]) * size);
    q->size = size;
    q->p = q->c = 0;
    pthread_mutex_init (&q->lock, NULL);
    pthread_cond_init (&q->empty, NULL);
    pthread_cond_init (&q->full, NULL);
    return q;
}

/** Atomic put for multiple elements. Blocks until space available. */
void queue_put (queue *q, char *items, int len) {
    int i;
    assert (len <= q->size);
    pthread_mutex_lock (&q->lock);
    while (q->c < q->p - q->size + len) {
	pthread_cond_wait (&q->full, &q->lock);
    }
    assert (q->p >= q->c);
    for (i = 0; i < len; i++) {
	q->data[q->p++ % q->size] = items[i];
    }
    assert (q->p - q->size <= q->c);
    pthread_cond_broadcast (&q->empty);
    pthread_mutex_unlock (&q->lock);
}

/** Atomic put for one element. Blocks if queue full. */
void queue_putItem (queue *q, char item) {
    queue_put (q, &item, 1);
}

/* Must hold lock on q->lock when calling this method. */
/* @return the number of elements available (0 or len). */
static int wait_for_data (queue *q, int len, int timeout) {
    struct timespec ts;
    struct timeval tv;
    assert (len <= q->size);
    if (timeout == 0) {
	if (q->c + len > q->p) {
	    return 0;
	} else {
	    return len;
	}
    }
    /* prepare timeval in case timeout is needed */
    if (timeout > 0) {
		gettimeofday(&tv, NULL);
		ts.tv_sec = tv.tv_sec + (timeout / 1000);
		ts.tv_nsec = tv.tv_usec * 1000 + (timeout % 1000) * 1000000;

		if (ts.tv_nsec > 1000000000) {
			ts.tv_nsec -= 1000000000;
			ts.tv_sec += 1;
		}
	}

    while (q->c + len > q->p) {
	if (timeout == -1) {
	    pthread_cond_wait (&q->empty, &q->lock);
	} else {
	    if (pthread_cond_timedwait (&q->empty, &q->lock, &ts) ==
		ETIMEDOUT) {
		return 0;
	    }
	}
    }
    assert (q->p >= q->c);
    assert (q->p - q->size <= q->c);
    return len;
}

/** Atomic remove for multiple elements. Blocks until data available. */
void queue_remove (queue *q, char *storage, int len) {
    int i;
    int available;
    pthread_mutex_lock (&q->lock);
    available = wait_for_data (q, len, -1);
    assert (available == len); /* timeout currently not needed/supported */
    for (i = 0; i < len; i++) {
	storage[i] = q->data[q->c++ % q->size];
    }
    pthread_cond_broadcast (&q->full);
    pthread_mutex_unlock (&q->lock);
}

/** Atomic remove for one element. Blocks until data available. */
char queue_removeItem (queue *q) {
    char result;
    queue_remove (q, &result, 1);
    return result;
}

/**
 * Atomic non-blocking remove multiple elements as many as possible.
 *
 * @return The number of bytes removed.
 */
int queue_remove_nonblock (queue *q, char *storage, int len) {
    int i = 0;

    pthread_mutex_lock (&q->lock);

    // Check availability byte-by-byte.
    while (i < len && wait_for_data(q, 1, 0)) {
    	storage[i++] = q->data[q->c++ % q->size];
    }

    pthread_cond_broadcast (&q->full);
    pthread_mutex_unlock (&q->lock);

    return i;
}

/** Atomic access for multiple elements. Blocks until data available,
    or until timeout occurs; a timeout of -1 waits indefinitely, and
    a timeout of 0 makes this operation non-blocking.
    @return the number of elements (0 or len). */
/*  Storage may be NULL in which case number of items available is
    returned but items are not stored. */
int queue_peek (queue *q, char *storage, int len, int timeout) {
    int i;
    int available;
    pthread_mutex_lock (&q->lock);
    available = wait_for_data (q, len, timeout);
    assert ((available == len) || ((timeout >= 0) && (available == 0)));
    if (storage != NULL) {
	for (i = 0; i < available; i++) {
	    storage[i] = q->data[(q->c + i) % q->size];
	}
    }
    pthread_mutex_unlock (&q->lock);
    return available;
}

void queue_free (queue *q) {
    pthread_cond_destroy (&q->empty);
    pthread_cond_destroy (&q->full);
    pthread_mutex_destroy (&q->lock);
    free (q->data);
    free (q);
}

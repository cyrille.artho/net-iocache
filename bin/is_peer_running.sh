#!/bin/bash
# Check if a real peer process is running in DMTCP.
# If one peer is running, return 0.
# If more than one peer is running, return 1.
# Otherwise, return 2 (error).

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

ERR_LOG="${CHKPNT_DIR}/err.log"

COUNT=`${DMTCP_COMMAND} -s | grep 'NUM_PEERS' | sed 's/NUM_PEERS=\([[:digit:]]\)/\1/'`

# Note: COUNT should not be zero since Proxy is supposed to be running.
if [ "${COUNT}" -eq "0" ]
then
	exit 2
fi

# If there is only one node, assume it is Proxy, so no peer is running.
if [ "${COUNT}" -eq "1" ]
then
	exit 1
fi

exit 0

//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.webdav;
/*
 *  WebDAVConnection.java  Version 0.5  2000-06-24
 *
 *  Copyright (C) 2000 by B.C. Holmes
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA 02111-1307, USA
 *
 *  Most of the real work in this package is done by Ronald Tschalr's
 *  HTTPClient package.
 */

import java.io.IOException;
import java.io.StringWriter;


import org.apache.util.DOMWriter;

import org.w3c.dom.Document;

import gov.nasa.jpf.network.HTTPClient.*;


/**
 * This class implements the client methods for a WebDAV Connection. It was written to test WebDAV servers (and, in particular,
 * the Slide code).
 * 
 */

public class WebDAVConnection {
	public static final int DEPTH_ZERO = 0;
	public static final int DEPTH_ONE = 1;
	public static final int DEPTH_INFINITY = 2;
	public static final int DEPTH_UNSPECIFIED = 3;

	public WebDAVConnection(String hostName) {
		this(hostName, 8080);
	}

	public WebDAVConnection(String hostName, int portNumber) {
		m_Connection = new HTTPConnection(hostName, portNumber);
	}

	public HTTPResponse doPropfind(String resource, int depth) throws IOException, ModuleException {
		return doPropfind(resource, depth, false);
	}

	public HTTPResponse doPropfind(String resource, int depth, boolean propertyNamesOnly) throws IOException, ModuleException {
		StringBuffer buffer = new StringBuffer();

		buffer.append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
		buffer.append("<D:propfind xmlns:D=\"DAV:\">");
		if (propertyNamesOnly)
			buffer.append("<D:propname/>");
		else
			buffer.append("<D:allprop></D:allprop>");
		buffer.append("</D:propfind>");

		byte[] bufferAsBytes = buffer.toString().getBytes();
		return doPropfind(resource, depth, bufferAsBytes);
	}

	public HTTPResponse doPropfind(String resource, int depth, Document document) throws IOException, ModuleException {
		StringWriter stringWriter = new StringWriter();
		DOMWriter domWriter = new DOMWriter(stringWriter, false);
		domWriter.print(document);

		System.out.println("[CONN : doPropfind()]\n--- Begin Request ---\n" + stringWriter.toString() + "\n--- End Request ---");
		return doPropfind(resource, depth, stringWriter.toString().getBytes());
	}

	protected HTTPResponse doPropfind(String resource, int depth, byte[] buffer) throws IOException, ModuleException {
		NVPair[] headers = new NVPair[4];
		headers[0] = CLIENT;
		if (depth == DEPTH_ZERO)
			headers[1] = new NVPair("depth", "0");
		else if (depth == DEPTH_ONE)
			headers[1] = new NVPair("depth", "1");
		else if (depth == DEPTH_INFINITY)
			headers[1] = new NVPair("depth", "infinity");
		else
			throw new IllegalArgumentException("Invalid depth value");
		headers[2] = new NVPair("Content-type", "text/xml");
		headers[3] = new NVPair("Content-length", Integer.toString(buffer.length));

		return m_Connection.ExtensionMethod("PROPFIND", resource, buffer, headers);
	}

	public HTTPResponse doProppatch(String resource, Document document) throws IOException, ModuleException {
		StringWriter stringWriter = new StringWriter();
		DOMWriter domWriter = new DOMWriter(stringWriter, false);
		domWriter.print(document);
		byte[] buffer = stringWriter.toString().getBytes();

		NVPair[] headers = new NVPair[3];
		headers[0] = CLIENT;
		headers[1] = new NVPair("Content-type", "text/xml");
		headers[2] = new NVPair("Content-length", Integer.toString(buffer.length));

		return m_Connection.ExtensionMethod("PROPPATCH", resource, buffer, headers);
	}

	private HTTPConnection m_Connection = null;

	private static final NVPair CLIENT = new NVPair("client", "org.bcholmes.webdav.client.WebDAVConnection 0.5");
}
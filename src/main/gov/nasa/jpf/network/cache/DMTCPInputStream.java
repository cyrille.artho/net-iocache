package gov.nasa.jpf.network.cache;

import gov.nasa.jpf.network.HelperMethods;

import java.io.*;
import java.util.*;

public class DMTCPInputStream extends FilterInputStream {

	private static final int EXTRA_INFO_SIZE = 1;

	private static final int HEADER_SIZE = EXTRA_INFO_SIZE + 4;
	
	private Queue<Byte> buffer = new LinkedList<Byte>();
	
	protected DMTCPInputStream(InputStream in) {
		super(in);
	}
	
	public int available() throws IOException {
		while (in.available() > 0) {
			readPacket();
		}
		
		return buffer.size();
	}
	
	public int read() throws IOException {
		byte[] b = new byte[1];
		int rv = read(b, 0, 1);
		
		if (rv < 0)
			return rv;
		
		return b[0];
	}
	
	public int read(byte[] b) throws IOException {
		return read(b, 0, b.length);
	}
	
	public int read(byte[] b, int off, int len) throws IOException {
		Iterator<Byte> iterator;
		int toReturn = Math.min(available(), len);
		
		iterator = buffer.iterator();
		
		for (int i = 0; i < toReturn; i++) {
			b[off + i] = iterator.next();
			// Must remove from the queue.
			iterator.remove();
		}
		
		return toReturn;
	}

	/**
	 * This method is called after a packet header has been read. It is supposed
	 * to be overriden by a subclass.
	 * 
	 * @param header
	 *            A packet header.
	 */
	protected void onHeaderRead(byte[] header) {		
	}
	
	protected int getExtraInfoSize() {
		return EXTRA_INFO_SIZE;
	}
	
	protected int getHeaderSize() {
		return HEADER_SIZE;
	}
	
	/**
	 * Read one packet.
	 * @return The size of the received payload in byte.
	 * @throws IOException
	 */
	private int readPacket() throws IOException {
		byte[] buff = new byte[HEADER_SIZE];
		int payloadLen = 0;

		in.read(buff, 0, HEADER_SIZE);
		onHeaderRead(buff);

		// Find the size of payload.
		payloadLen = HelperMethods.byteArrayToInt(buff, EXTRA_INFO_SIZE);
		buff = new byte[payloadLen];

		// Read payload and add it to the input queue.
		in.read(buff, 0, payloadLen);
		for (int i = 0; i < payloadLen; i++) {
			buffer.add(buff[i]);
		}
		
		return payloadLen;
	}
	
}

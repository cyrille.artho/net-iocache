/*
 * ncp.h
 *
 *  Created on: 2009/12/11
 *      Author: watcharin
 */

#ifndef NCP_H
#define NCP_H

// Maximum size in bytes of a packet to be received.
#define MAXBUFLEN 		4096
// Maximum number of sockets
#define NUM_SOCKETS	64

// Size of a control packet in bytes
#define CTRL_PACKET_SIZE		10
#define CONNECT_PACKET_SIZE 	CTRL_PACKET_SIZE
#define ACCEPT_PACKET_SIZE 	CTRL_PACKET_SIZE

// Size of the data sink in bytes
#define DATA_SINK_SIZE MAXBUFLEN * 16

#endif /* NCP_H_ */

#include "queue.h"


typedef struct sockaddr			Sockaddr;
typedef struct sockaddr_storage 	Sockaddr_storage;
typedef struct sockaddr_in			Sockaddr_in;
typedef struct sockaddr_in6		Sockaddr_in6;
typedef struct pollfd				Pollfd;
typedef struct timeval Timeval;

typedef struct SocketTable {
	int 				send_seq;
	int 				expected_seq;
	Sockaddr_storage 	addr;
	socklen_t			addrlen;
	Sockaddr_storage	local_addr;
	socklen_t			local_addrlen;
	queue				*data_sink;
	queue				*ctrl_sink;
	unsigned int				ctrl:1;
	int					non_block;
} SocketTable;


void	socket_fds_alloc(int socket_fd);
uint 	usocket_socket_port(int sockfd);
void 	*proxy_run(void *id);
void 	ncp_init();

int 	connect_packet(int sock_fd, char *buffer);
int 	accept_packet(int sock_fd, char *buffer);
int 	data_packet(int sockfd, const void *data, int data_len, char *packet);

int 	read_data_sink(int socket_fd, char *packet);
int 	read_ctrl_sink(int socket_fd, char *packet);

int 	is_connect_packet(const char *packet, int size);
int 	is_accept_packet(const char *packet, int size);

int 	is_data_sink_empty(int socket_fd);
int 	is_ctrl_sink_empty(int socket_fd);

int 	packet_data(const char *packet, char *storage);
int 	packet_seq_number(const char *packet);

int 	get_packet_number(int socket_fd);
void 	inc_packet_number(int socket_fd);

int 	packet_data_len(const char *packet);
uint 	get_port(const Sockaddr_storage *addr);

void	ncp_rebindsock(int sockfd);
void 	ncp_rebind_all_sockets();
void	ncp_save_local_addr(int sock);
int		ncp_real_fd(int virt_fd);
void 	ncp_clear_socket_table(int idx);
int 	ncp_is_port_bound(uint port);

int gnu_socket(int domain, int type, int protocol);
int gnu_bind(int sockfd, const Sockaddr *myaddr, socklen_t addrlen);
int gnu_poll (struct pollfd *fds, nfds_t nfds, int timeout);
int gnu_close(int fd);

#ifndef _HELPER_H
#define _HELPER_H

#include <sys/time.h>


typedef struct timeval timeval;


inline static int64_t timeval_to_ms(const timeval *tv) {
	return tv->tv_sec * 1000LL + tv->tv_usec / 1000;
}

inline static int64_t cur_time_ms() {
	timeval tv;

	gettimeofday(&tv, NULL);
	return timeval_to_ms(&tv);
}

#endif

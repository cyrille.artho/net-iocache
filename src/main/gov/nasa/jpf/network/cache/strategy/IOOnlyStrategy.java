package gov.nasa.jpf.network.cache.strategy;

import gov.nasa.jpf.network.cache.CheckpointCacheLayer;

public class IOOnlyStrategy implements CheckpointingStrategy {

	CheckpointCacheLayer parent;
	
	public IOOnlyStrategy(CheckpointCacheLayer cl) {
		parent = cl;
	}
	
	@Override
	public boolean shouldSave(int stateId) {
		return stateId == 0 || CheckpointCacheLayer.isSaveFlagSet();
	}

	@Override
	public void setIOFlag() {
		parent.setSaveFlag();
	}

}

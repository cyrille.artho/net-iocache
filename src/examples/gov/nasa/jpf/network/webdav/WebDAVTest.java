//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.webdav;
/*
 *  WebDAVTest.java  Version 0.5  2000-06-24
 *
 *  Copyright (C) 2000 by B.C. Holmes
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA 02111-1307, USA
 *
 *  Most of the real work in this package is done by Ronald Tschalr's
 *  HTTPClient package.
 */

import java.io.IOException;
import java.io.FileInputStream;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

import gov.nasa.jpf.network.HTTPClient.*;


public class WebDAVTest {
	public static void main(String[] args) throws IOException, ModuleException,
			SAXException, ParseException {

		if (args == null || args.length < 1)
			throw new IllegalArgumentException(
					"specify either PROPPATCH or PROPFIND");

		HTTPResponse rsp = null;

		if (args[0].equalsIgnoreCase("PROPPATCH"))
			rsp = example5(args);
		else if (args[0].equalsIgnoreCase("PROPFIND") && args.length == 2)
			rsp = example3(args);
		else if (args[0].equalsIgnoreCase("PROPFIND") && args.length > 2)
			rsp = example4(args);
		else
			throw new IllegalArgumentException(
					"specify either PROPPATCH or PROPFIND");

		if (rsp.getStatusCode() >= 300) {
			System.err.println("Received Error: " + rsp.getReasonLine());
			System.err.println(new String(rsp.getData()));
		} else {
			byte[] data = rsp.getData();
			System.out.println(new String(data));
		}
	}

	public static HTTPResponse example3(String[] args) throws ModuleException,
			IOException, ParseException {
		if (args == null || args.length < 2)
			throw new IllegalArgumentException("no URI provided");

		URI uri = new URI(args[1]);

		WebDAVConnection con = new WebDAVConnection(uri.getHost());
		return con.doPropfind(uri.getPath(), WebDAVConnection.DEPTH_INFINITY,
				false);
	}

	public static HTTPResponse example4(String[] args) throws ModuleException,
			IOException, SAXException, ParseException {
		if (args == null || args.length < 2)
			throw new IllegalArgumentException("no URI provided");
		else if (args.length < 3)
			throw new IllegalArgumentException("no input XML provided");

		String xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<D:propfind xmlns:D=\"DAV:\">\n<D:allprop></D:allprop>\n</D:propfind>\n";

		URI uri = new URI(args[1]);

		WebDAVConnection con = new WebDAVConnection(uri.getHost());
		return con.doPropfind(uri.getPath(), WebDAVConnection.DEPTH_ONE, xml
				.getBytes());
	}

	public static HTTPResponse example5(String[] args) throws ModuleException,
			IOException, SAXException, ParseException {
		if (args == null || args.length < 2)
			throw new IllegalArgumentException("no URI provided");
		else if (args.length < 3)
			throw new IllegalArgumentException("no input XML provided");

		InputSource input = new InputSource(new FileInputStream(args[2]));
		DOMParser parser = new DOMParser();
		parser.parse(input);
		Document document = parser.getDocument();

		URI uri = new URI(args[1]);

		WebDAVConnection con = new WebDAVConnection(uri.getHost());
		return con.doProppatch(uri.getPath(), document);
	}
}

/*
 * mtcpsetup.h
 *
 *  Created on: 2010/01/20
 *      Author: watcharin
 */

#ifndef MTCPSETUP_H_
#define MTCPSETUP_H_


#endif /* MTCPSETUP_H_ */

void mtcpsetup_premain() __attribute__((constructor));
void mtcpsetup_post_ckpt(int is_restarting, char *dummy);

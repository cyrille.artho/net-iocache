#!/bin/bash

source "$(cd "$(dirname "$0")" && pwd)/utils-for-testing.source" || exit

PACKAGE="gov.nasa.jpf.network.http"
REQ_FILES=("www/abc.txt" "www/lorem-ipsum-1.txt" "www/lorem-ipsum-2.txt" "lorem-ipsum-3.txt" "lorem-ipsum-4.txt")
set-specific-help \
"HTTP client and server

This script runs/tests various setups of a http client/server scenario.

Specific options for choosing the client and server programs are:

    nd  -- run non-deterministic client and server

Specific options for choosing program parameters are:

    threads=x   -- number of threads in SUT (or the server for the run native 
                   option)
"

set-parameters "$@"

SUT="native"
if checkParameter server-sut
then
	SUT="server-sut"
    DOTJPF="$DOTJPF
jpf-net-iocache.lazy.connect = false
jpf-net-iocache.main.termination=true
jpf-net-iocache.peerResponse.maxDelayMillis = 100
"
elif checkParameter client-sut
then
	SUT="client-sut"
    DOTJPF="$DOTJPF
jpf-net-iocache.lazy.connect = true
jpf-net-iocache.main.termination=true
jpf-net-iocache.peerResponse.maxDelayMillis = 20
"
fi

checkParameter threads nThreads 2

# separate out all 4 combinations of {client,server}x{nd,non-nd}
if checkParameter nd
then
    ndsuffix="-nd"
    if [ "$SUT" == "server-sut" ]
    then ##### CASE 1
	set-client java ${PACKAGE}.HTTPClient "-port \$1 \$2" "${REQ_FILES[@]}"
	# for -nd, note using HTTPServerCounter, (not HTTPServer)
	set-server java ${PACKAGE}.HTTPServerCounter "$nThreads"
    elif [ "$SUT" == "client-sut" ]
    then ##### CASE 2
	# for nd, note the extra "-nondet" parameter
	set-client java ${PACKAGE}.HTTPClient -nondet "-threads $nThreads" "${REQ_FILES[@]}" 
	set-server java ${PACKAGE}.HTTPServer 0 # 0 means keep accepting until server is killed
    else
	reportfail "not implemented"
    fi
else
    ndsuffix=""
    if [ "$SUT" == "server-sut" ]
    then ##### CASE 3
	set-client java ${PACKAGE}.HTTPClient "-port \$1 \$2" "${REQ_FILES[@]}"
	set-server java ${PACKAGE}.HTTPServer "$nThreads"
	else  ##### CASE 4
	set-client java ${PACKAGE}.HTTPClient "-threads $nThreads" "${REQ_FILES[@]}"
	set-server java ${PACKAGE}.HTTPServer "$nThreads"
    fi
fi

TESTNAME="http${ndsuffix}-$nThreads"

# the servers run relative to the test dir, so must copy the files inside there
make-test-output-directories "$TESTNAME-$SUT"
cp -r "$SCRIPT_DIR/www" "$TESTRUN_DIR" || reportfail "could not copy www server files to test dir"

dotest

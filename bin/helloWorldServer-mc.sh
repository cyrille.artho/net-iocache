#!/bin/bash
. `dirname "$0"`/env.sh               #change to project top directory and load library functions
                                        #such as 'exitOnNoFreePort' 
echo Model checking `pwd`/build/examples/helloWorld/HelloServer.class
echo 

exitOnNoFreePort 10001 10099             #Retrieve a free port between 10001 and 10099 in $_Port
                                         #  or exit if there is none

#execute jpf on HelloServer using Port $_Port on HelloServer (+target.args=$_Port) 
../jpf-core/bin/jpf src/examples/helloWorld/HelloServer.jpf +target.args=$_Port

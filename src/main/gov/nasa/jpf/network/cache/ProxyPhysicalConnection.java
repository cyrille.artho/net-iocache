package gov.nasa.jpf.network.cache;

import java.io.IOException;
import java.net.Socket;

public class ProxyPhysicalConnection extends PhysicalConnection {

	private int proxyFd;

	public ProxyPhysicalConnection(int proxyFd, Socket s, int in, int out)
			throws IOException {
		super(s, in, out);

		this.proxyFd = proxyFd;
	}
	
	public int getProxyFd() {
		return proxyFd;
	}
	
	void close() throws IOException {
		socket.close();
	}
	
}

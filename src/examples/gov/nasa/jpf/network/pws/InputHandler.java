package gov.nasa.jpf.network.pws;
/*  InputHandler.java -- all functions related to input

    Copyright (C) 2000 - 2004 Jan De Luyck & Kris Van Hulle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

import java.net.*;
import java.io.*;
import java.util.*;

public class InputHandler
{
	private Socket sock;
   	private BufferedReader in;
   	
    public InputHandler(Socket i_sock)
    {
        try
        {
            this.sock = i_sock;
            this.in = new BufferedReader(new InputStreamReader(this.sock.getInputStream()));
        }
        catch (IOException e)
		{
			Misc.putSysMessage(1, "IO Exception caught: " + e);
		}
    }
    
    public String charReadLine()
    {
        /* reads 1 line */

        /* we need to put a timeout on input from the users
           because otherwise we could be waiting here a VERY long time :-( */
        Timer timeOutTimer = new Timer(true);    /* true to make the timer daemonstyle */
        TimeOutHandler timeOut = new TimeOutHandler(Thread.currentThread());
        timeOutTimer.schedule(timeOut, Integer.parseInt(pws.getSetting("timeout")) * 1000);
        
        Misc.putSysMessage(0, "Starting Timer... on " + Thread.currentThread().getName() + "...");

        String temp = null;
        int character = 0;
        try
        {
            /* read until we get a '\n' */
            do
            {
                if (this.in.ready())
                {
                    character = this.in.read();
                    temp += (char) character;
                }
            }
            while ((character != -1) && (character != '\n') && (character != '\n') && (Thread.currentThread().isInterrupted() == false));
            
           if (Thread.currentThread().isInterrupted() == true)
            {
            /* check if this thread has been interrupted, if so we go back with a
            ThreadInterruptedException() */
                Misc.putSysMessage(1,"Thread interrupted...");
                temp = "";
            }
                
            /* manipulate the string a bit */
            if (temp.equals("") == false)
            {
                temp = temp.substring(0, temp.length() - 1); /* remove the final '\n' or '-1' */
                if (temp.charAt(temp.length() - 1) == '\r') temp = temp.substring(0, temp.length() - 1);
                temp = temp.trim();
            }
            
            if (temp.equals("")) temp = null;
        }
        catch (IOException e)
        {
            Misc.putSysMessage(1,"InputHandler: " + e);
        }
        finally
        {
            /* stop the timer, we got our stuff */
            Misc.putSysMessage(0, "Stopping Timer... on " + Thread.currentThread().getName() + "...");
            timeOutTimer.cancel();
        }


        Misc.putSysMessage(0, "Temp String: " + temp);
        return temp;
    }
    
    public String readLine()
    {
        String temp;
        
        try
        {
            temp = this.in.readLine();
            if (temp != null) temp = temp.trim();
        }
        catch (IOException e)
        {
            //Misc.putSysMessage(1, "InputHandler: " + e);
            temp = null;
        }
        
        //Misc.putSysMessage(0, "Temp String: " + temp);
        return temp;
        
    }
    
    public void close()
    {
        /* closes the input stream */
        try
        {
            this.in.close();
        }
        catch (IOException e)
        {
            Misc.putSysMessage(1,"InputHandler: " + e);
        }
    }
}
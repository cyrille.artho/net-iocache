#!/bin/bash

if [ $# == 0 ]; then
	echo "Simple 'find free port' tool"
	echo "Usage:    ffport.sh <StartPort> [<EndPort>]"
	echo "          output an unused port in the range from <StartPort> to <EndPort>"
	echo 
	echo "Examples: ffport.sh 8000 8888      (check ports from 8000 up to including 8888)"
	echo "          ffport.sh 8888 8000      (check ports from 8888 down to incl.   8000)"
	echo "          ffport.sh 8000           (check ports from 8000 up to incl.    65535)"
	echo "          ffport.sh 8000 8000      (check port 8000, only)"
	echo 
	echo "Output:   Number of first free port found"
	echo "          or 'NULL' if all ports in the given range are in use"
	echo
	exit
fi

source `dirname "$0"`/functions.source
getFreePort $1 $2
#!/bin/bash

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

PACKAGE="gov.nasa.jpf.network.jget"

NUM_CONNECTION=1
if [ $# -gt 0 ]
then
	NUM_CONNECTION=$1
fi

echo "start jget with ${NUM_CONNECTION} connection(s) ..."
java -classpath ${CLASSPATH} ${PACKAGE}.jget -c ${NUM_CONNECTION} http://localhost:8080/abc ${HOME}

//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.alphabet;

import gov.nasa.jpf.vm.Verify;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class NDAlphabetClient {

	/**
	 * 
	 * @param args [The number of connections] [The number of messages]
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		InetAddress addr = InetAddress.getLocalHost();
		int numConns = (args.length < 1) ? 1 : Integer.parseInt(args[0]);
		int numMsgs = (args.length < 2) ? 2 : Integer.parseInt(args[1]);
		byte msg = (byte) '0';
		int port = AlphabetServer.SERVER_PORT;

		Socket[] sock = new Socket[numConns];
		Thread[] producer = new Thread[numConns];
		Thread[] consumer = new Thread[numConns];

		for (int i = 0;i < numConns;i++) {
			sock[i] = new Socket(addr, port);
			System.out.println("[NDAlphabetClient: main] created "+(i+1)+
					". connection to "+addr+":"+port);

			byte[] buf = new byte[numMsgs];   //buffer storing all messages to be send to connection i
			for (int j = 0; j < numMsgs; j++) {
				int offset = Verify.getInt(0, 1);  //introduce a choice --> "non-deterministic SUT"
				buf[j] = (byte) (msg + offset);
				msg+=2;
			}

			producer[i] = new Producer(sock[i].getOutputStream(), buf);
			consumer[i] = new Consumer(sock[i].getInputStream(), buf);
		}

//		System.out.println("[NDAlphabetClient: main] starting "+numConns+
//				" producer thread(s), each sending "+numMsgs+ 
//				" message(s), as well as "+numConns+" consumer thread(s), " +
//						"receiving the response from the alphabet server...");

		for (int i = 0;i < numConns;i++) {
			producer[i].start();
			consumer[i].start();
		}
		
//		do not use join() in main thread if jpf-net-iocache.main.termination=true		
//		for (int i=0; i<num_threads; i++) {    
//			p[i].join();
//			c[i].join();
//		}		

//		System.out.println("[NDAlphabetClient: main] says good bye!");
	}
}

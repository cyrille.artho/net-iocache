package java.nio.channels;

import gov.nasa.jpf.vm.Verify;
import java.io.IOException;

public abstract class SelectableChannel {
	public abstract void close() throws IOException;
	public abstract boolean isOpen(); 
	public abstract SelectableChannel configureBlocking(boolean block) throws IOException;
	public abstract int validOps();

	//get configuration data from JPF 
	private native boolean getFailureFirst();
	protected final boolean FAILURE_FIRST=getFailureFirst();
}

//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.alphabet;

import gov.nasa.jpf.vm.Verify;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class NDTimeClient extends Thread {

	private static final int PORT = 47000;

	private OutputStream os;
	private InputStream is;
	private String message;
	private int id;

	NDTimeClient(int id, OutputStream out, InputStream in, String msg) {
		this.id = id;
		os = out;
		is = in;
		message = msg;
	}

	/**
	 * 
	 * @param args
	 *            [The number of connections]
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		InetAddress addr = InetAddress.getLocalHost();
		Socket[] s;
		Thread[] p;
		int numConns = (args.length < 1) ? 1 : Integer.parseInt(args[0]);
		String msg;
		boolean choice;

		s = new Socket[numConns];
		p = new Thread[numConns];

		for (int i = 0; i < s.length; i++) {
			String prefix = String.format("%c000 ", '0' + i);
			s[i] = new Socket(addr, PORT);
			choice = Verify.getBoolean();
			msg = choice ? "ddmmyy" : "yymmdd";

			p[i] = new NDTimeClient(i, s[i].getOutputStream(), s[i]
					.getInputStream(), prefix + msg);
		}

		for (int i = 0; i < s.length; i++) {
			p[i].start();
		}
	}

	public void run() {
		byte[] buffer = new byte[64];
		
		try {
			os.write(message.getBytes());
			is.read(buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

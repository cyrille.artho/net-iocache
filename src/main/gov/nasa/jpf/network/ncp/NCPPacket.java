package gov.nasa.jpf.network.ncp;

public class NCPPacket {
	
	private static final int CTRL_PACKET_SIZE = 10;
	
	private int socket_fd;
	
	private int number;
	
	private boolean isConnectPacket;
	
	private boolean isAcceptPacket;
	
	private boolean isAckPacket;
	
	private boolean isClosePacket;
	
	private boolean isNDPacket;
	
	private int data_len;
	
	private byte[] content;
	
	public NCPPacket(byte[] rawData) {
		assert rawData.length >= 10 : "[PACKET] Invalid raw data";
		
		socket_fd = (int) rawData[0];
		number = byteArrayToInt(rawData, 1);
		isConnectPacket = (rawData[5] & 0x01) != 0;
		isAcceptPacket = (rawData[5] & 0x02) != 0;
		isAckPacket = (rawData[5] & 0x04) != 0;
		isClosePacket = (rawData[5] & 0x08) != 0;
		isNDPacket = (rawData[5] & 0x10) != 0;
		data_len = byteArrayToInt(rawData, 6);
		
		if (data_len > 0) {
			content = new byte[data_len];
			System.arraycopy(rawData, 10, content, 0, data_len);
		}
		else
			content = new byte[0];
	}
	
	public static byte[] createAcceptPacket(int fd) {
		byte[] raw = new byte[CTRL_PACKET_SIZE];
		
		raw[0] = (byte) fd;
		raw[5] = 0x02;
		
		return raw;
	}
	
	public static byte[] createConnectPacket(int fd) {
		byte[] raw = new byte[CTRL_PACKET_SIZE];
		
		raw[0] = (byte) fd;
		raw[5] = 0x01;
		
		return raw;
	}
	
	public static byte[] createAckPacket(int fd, int seqNum) {
		byte[] raw = new byte[CTRL_PACKET_SIZE];
		byte[] byteSeqNum = intToByteArray(seqNum);
		
		// Fill socket id and the control byte
		raw[0] = (byte) fd;
		raw[5] = 0x04;
		
		System.arraycopy(byteSeqNum, 0, raw, 1, 4);
		
		return raw;
	}
	
	public static byte[] createDataPacket(int fd, int seqNum, byte[] data, int off, int len) {
		byte[] raw = new byte[CTRL_PACKET_SIZE + len];
		byte[] byteSeqNum = intToByteArray(seqNum);
		byte[] byteLen = intToByteArray(len);
		
		// Fill socket id and control byte
		raw[0] = (byte) fd;
		raw[5] = 0;
		
		// Fill data length and the sequence number
		System.arraycopy(byteSeqNum, 0, raw, 1, 4);
		System.arraycopy(byteLen, 0, raw, 6, 4);
		
		System.arraycopy(data, off, raw, 10, len);
		
		return raw;
	}
	
    public int getSocket_fd() {
		return socket_fd;
	}

	public int getNumber() {
		return number;
	}

	public boolean isConnectPacket() {
		return isConnectPacket;
	}

	public boolean isAcceptPacket() {
		return isAcceptPacket;
	}

	public boolean isAckPacket() {
		return isAckPacket;
	}

	public boolean isClosePacket() {
		return isClosePacket;
	}

	public boolean isNDFlagSet() {
		return isNDPacket;
	}
	
	public int getData_len() {
		return data_len;
	}

	public byte[] getContent() {
		return content;
	}

	/**
	 * Assume little-endian order of <i>b</i>.
	 * @param b
	 * @param offset
	 * @return
	 */
	private static int byteArrayToInt(byte[] b, int offset) {
        int value = 0;
        for (int i = 0; i < 4; i++) {
            int shift = (4 - 1 - i) * 8;
            value += (b[3 - i + offset] & 0x000000FF) << shift;
        }
        return value;
    }
    
	/**
	 * 
	 * @param value
	 * @return 4-element little-endian array of <i>value</i>.
	 */
	private static byte[] intToByteArray(int value) {
		byte[] b = new byte[4];
		for (int i = 0; i < 4; i++) {
			int offset = (3 - i) * 8;
			b[3 - i] = (byte) ((value >>> offset) & 0xFF);
		}
		return b;
	}

}

package gov.nasa.jpf.network.cache.strategy;

import gov.nasa.jpf.network.cache.CheckpointCacheLayer;

public class AlwaysStrategy implements CheckpointingStrategy {

	CheckpointCacheLayer parent;
	
	public AlwaysStrategy(CheckpointCacheLayer cl) {
		parent = cl;
	}
	
	@Override
	public boolean shouldSave(int stateId) {
		return true;
	}
	
	@Override
	public void setIOFlag() {
		parent.setSaveFlag();
	}
	
}

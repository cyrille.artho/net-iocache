//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.network.cache;

import java.util.*;

/**
 * A table mapping from socket id to tree id. Usually all sockets are mapped to the same tree in case of client
 * checking, and they are mapped one-to-one in case of server checking.
 * 
 * @author watcharin
 * 
 */
public class SocketTreeTable implements Cloneable {

	private static SocketTreeTable instance = new SocketTreeTable();

	private static List<List<Integer>> tableList = new ArrayList<List<Integer>>();

	private static int maxSocket = 0;

	private List<Integer> table;

	private SocketTreeTable() {
		table = new ArrayList<Integer>();
	}

	public static SocketTreeTable getInstance() {
		return instance;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		int counter = 0;

		sb.append("Socket ID\tTree ID\n");
		for (Integer i : table) {
			sb.append(counter++ + "\t" + i + "\n");
		}

		return sb.toString();
	}

	public void addSocket() {
		maxSocket++;
		table.add(-1);
	}

	public void setTreeID(int socketID, int treeID) {
		table.set(socketID, treeID);
	}

	public int getTreeID(int socketID) {
		return table.get(socketID);
	}

	public int maxSocketID() {
		return maxSocket - 1;
	}

	public void save() {
		tableList.add(new ArrayList<Integer>(table));
	}

	public void restore(int id) {
		if (id >= 0)
			table = new ArrayList<Integer>(tableList.get(id));
		else
			table = new ArrayList<Integer>();

		while (table.size() < maxSocket)
			table.add(-1);
	}

	public int unusedTree() {
		int numTrees = CacheLayer.getInstance().numTrees();

		for (int i = 0; i < numTrees; i++) {
			if (!table.contains(i))
				return i;
		}

		return -1;
	}
}

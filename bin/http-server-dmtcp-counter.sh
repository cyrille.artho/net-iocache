#!/bin/bash
# Run the HTTP server SUT with "curl" running on DMTCP.

SCRIPT_DIR=`dirname "$0"`
SCRIPT_DIR=`cd ${SCRIPT_DIR}; pwd`
. "${SCRIPT_DIR}/env.sh"

chk_vdso_exit

# Set the location of checkpoints
export CHKPNT_DIR="${PROJ_TOP}/tmp-http-server"
# Set the SUT port number. This value must be exported so that DMTCP can read.
export SUT_PORT="8791"

JPF_ARG=`echo "${JPF_ARG} +${OPT_VIRTUAL_MODE}=true +${OPT_CHECKPOINT_DIR}=${CHKPNT_DIR} +${OPT_LAZY}=false +${OPT_DMTCP_ENABLED}=true +${OPT_SUT_PORT}=${SUT_PORT}" | sed -e 's/CacheNotifier/VirtualizationCacheNotifier/' -e 's/CacheLogger/VirtualizationCacheLogger/'`
PACKAGE="gov.nasa.jpf.network.http"
PROG_ARG="${PACKAGE}.HTTPServerCounter 2"
SERVER_LOG="${HOME}/`output_file $0`"
SERVER_PORT=8080

# Recreate the checkpoint directory.
rm -rf ${CHKPNT_DIR}
mkdir ${CHKPNT_DIR}
# Remove any checkpoint file left on this directory.
rm -f ${SCRIPT_DIR}/*.dmtcp

# Must run the coordinator with option "background", otherwise the peer cannot restart.
${DMTCP_COORDINATOR} --ckptdir ${CHKPNT_DIR} --background

# Start JPF running the server side. It will wait until Proxy sets up a Unix domain channel.
${RUN_JPF_JNILIB} ${JPF_ARG} ${PROG_ARG} > ${SERVER_LOG} &
JPF_PID=$!
sleep 2

# Start Proxy.
echo "Start Proxy"
${DMTCP_CHECKPOINT} ${CBUILD_DIR}/proxy &
echo "Proxy PID: $!"
sleep 1

# Start the client side.
echo "Start curl (HTTP client)"
${DMTCP_CHECKPOINT} curl http://localhost:${SERVER_PORT}${SCRIPT_DIR}/$0 &
${DMTCP_CHECKPOINT} curl http://localhost:${SERVER_PORT}${SCRIPT_DIR}/$0 &
# Save the peer PID.
PEER_PID=$!
echo "Client PID: ${PEER_PID}"
echo "${PEER_PID}" > ${CHKPNT_DIR}/${PEER_PID_FILE}

# Wait the JPF process.
wait ${JPF_PID}

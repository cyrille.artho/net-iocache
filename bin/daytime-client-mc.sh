#!/bin/bash
# Verify the daytime client.

SCRIPT_DIR=`dirname "$0"`
. "${SCRIPT_DIR}/env.sh"

if [ $# -lt 1 ]
then
	echo "Usage : $0 <#threads>+"
	echo "-m Move the log file from home to the log directory."
	exit
fi

PACKAGE="gov.nasa.jpf.network.daytime"
OUT_FILENAME=`output_file $0`
LOG="${HOME}/${OUT_FILENAME}"
JPF_FILE="${EXAMPLE_DIR}/gov/nasa/jpf/network/daytime/DaytimeClient.jpf"
LOG_DIR="log"

move_log() {
	# Add parameters to the log file name.
	PARAM=`echo ${OUT_FILENAME} | sed -e "s/\.log/-$1\.log/"`
	mv ${LOG} ${LOG_DIR}/${PARAM}
}

run() {
	echo "start a daytime server"
	java -classpath $CLASSPATH ${PACKAGE}.DaytimeServer 1024 $1 > /dev/null &
	sleep 2
	echo "start JPF verifying $1 daytime client(s)"
	${RUN_EXAMPLE_CMD} ${JPF_FILE} $1 > ${LOG}

	if [ "${MOVE_LOG}" -eq "1" ]
	then
		move_log $1 $2
	fi
}

# Check if option "-m" is set.
MOVE_LOG=0
if [ $1 = "-m" ]
then
	MOVE_LOG=1
	shift
fi

# Run until no case remains
while [ $# -gt 0 ]
do
	run $1
	shift
done

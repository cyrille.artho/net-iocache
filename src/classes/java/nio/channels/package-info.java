/**
 * This package contains socket related classes and exceptions.
 *
 * All classes and method needed to model check Apache Zookeeper is implemented.
 * A precautions is that class hierarchy is much simpler than that of the original java.
 */
package java.nio.channels;
